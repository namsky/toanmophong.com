<?=$this->section('content');?>
	<!-- CONTENT -->
	<div class="container">
		<div class="row">
			<?php
			if(isset($widgets) && is_array($widgets)) {
				foreach($widgets as $key=>$widget) {
					$args = [];
					$args['title'] = $widget->title;
					$args['type'] = $widget->type;
					if(!empty($widget->title_heading)) $args['title_heading'] = $widget->title_heading;
					if(!empty($widget->category_heading)) $args['category_heading'] = $widget->category_heading;
					if(!empty($widget->post_heading)) $args['post_heading'] = $widget->post_heading;
					if($widget->category) $args['category'] = $widget->category;
					if($widget->tags) {
						if(strpos($widget->tags, ',')) $tags = explode(',', $widget->tags);
						else $tags = [intval($widget->tags)];
						$args['tags'] = $tags;
					}
					if($widget->excludes) {
						if(strpos($widget->excludes, ',')) $excludes = explode(',', $widget->excludes);
						else $excludes = [intval($widget->excludes)];
						$args['excludes'] = $excludes;
					}
					if($widget->post_type) $args['post_type'] = $widget->post_type;
					if($widget->limit) $args['limit'] = $widget->limit;
					if($widget->hot) $args['hot'] = $widget->hot;
					if($widget->column) $column = $widget->column;
					else $column = 12;
					echo '<div class="col-sm-12 col-lg-'.$column.'">';
					echo widget('NghienCode/Box_news', $args);
					echo '</div>';
					if($key==0) echo widget('Cms/Ads', ['zone_id'=>2]);
				}
			}
			?>
		</div>
	</div>
<?=$this->endSection();?>