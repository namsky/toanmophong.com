<?php
$url = URL;
$list = '';
if($total_page > 1) {
	for($i=1; $i<=$total_page; $i++) {
$list .= '
	<url>
		<loc>'.$url.'/news/sitemap-'.$i.'.xml</loc>
		<changefreq>Daily</changefreq>
		<priority>1</priority>
		<lastmod>'.date('c', time()).'</lastmod>
	</url>
';
	}
} else {
$list .= '
	<url>
		<loc>'.$url.'/news/sitemap.xml</loc>
		<changefreq>Daily</changefreq>
		<priority>1</priority>
		<lastmod>'.date('c', time()).'</lastmod>
	</url>
';
}
$list .= '
	<url>
		<loc>'.$url.'/category/sitemap.xml</loc>
		<changefreq>Daily</changefreq>
		<priority>1</priority>
		<lastmod>'.date('c', time()).'</lastmod>
	</url>
';
$list .= '
	<url>
		<loc>'.$url.'/tag/sitemap.xml</loc>
		<changefreq>Daily</changefreq>
		<priority>1</priority>
		<lastmod>'.date('c', time()).'</lastmod>
	</url>
';
$sitemap = <<<Sitemap
<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="{$url}/sitemap.xsl"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
{$list}
</urlset>
Sitemap;
echo $sitemap;