<?php
$url = URL;
$list = '';
if(is_array($posts))
foreach($posts as $post) {
	$post_url = post_url($post);
	$percent = '0.1';
	$last_activity = $post->updated?$post->updated:$post->published;
	$list .= '
		<url>
			<loc>'.$post_url.'</loc>
			<changefreq>Daily</changefreq>
			<priority>'.$post->sitemap_priority.'</priority>
			<lastmod>'.date('c', $last_activity).'</lastmod>
		</url>
	';
}
$sitemap = <<<Sitemap
<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="{$url}/sitemap.xsl"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
{$list}
</urlset>
Sitemap;
echo $sitemap;
?>