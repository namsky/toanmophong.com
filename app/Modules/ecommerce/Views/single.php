<?=$this->section('content');?>
	<!-- CONTENT -->
	<!--div class="ads-box my-4">
		<a href="#" target="_blank">
			<img alt="ADS" src="" />
		</a>
	</div-->
	<div class="single-header">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?=URL?>"><i class="fa fa-home"></i></a></li>
							<?php
							if(!empty($parents)) {
								foreach($parents as $parent) {
									echo '<li class="breadcrumb-item"><a href="'.category_url($parent).'">'.$parent->name.'</a></li>';
								}
							}
							if(isset($item->categories)) {
								$categories = $item->categories;
								$category = reset($categories);
								if(isset($category->id)) {
									echo '<li class="breadcrumb-item"><a href="'.category_url($category).'">'.$category->name.'</a></li>';
								}
							}
							?>
							<?php
							if(isset($item->title)) {
							?>
							<li class="breadcrumb-item active d-none d-md-block"><?=$item->title?></li>
							<?
							}
							?>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="single-news">
		<div class="container">
			<div class="row">
				<div class="header col-12">
					<div class="category">
					<?php if($item->categories) foreach($item->categories as $category) { ?>
						<a href="<?=category_url($category);?>"><?=$category->name;?></a>
					<?php } ?>
					</div>
					<?=$cms->schemas('item_page', ['url'=>post_url($item),'title'=>$item->title]);?>
					<h1 class="title"><?=isset($item->title)?$item->title:''?></h1>
                    <?php if(!empty($item->summary)) { ?>
					<div class="summary"><?=$item->summary?></div>
                    <?php } ?>
				</div>
				<div class="single-info col-12 row">
					<div class="author-avatar float-left">
						<img alt="" src="<?=!empty($item->user->avatar)?show_thumb($item->user->avatar,40,40):CDN.'/images/no-avatar.jpg'?>" />
					</div>
					<div class="ml-2 float-left">
						<div class="author-name">
							<p>By</p>
							<span>
								<a href="javascript:;" title="" rel="author"><?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?></a>
							</span>
						</div>
						<div class="single-date">
							<span>
								<i class="far fa-clock"></i>&nbsp;
								<time datetime="<?=date("Y-m-d", $item->published)?>"><?=show_date($item->published)?></time>
							</span>
						</div>
					</div>
					<div class="single-social">
						<ul class="single-social-share">
							<li class="single-social-facebook">
								<a href="#" onclick="window.open('http://www.facebook.com/sharer.php?t=<?=isset($item->title)?$item->title:''?>&amp;u=<?=post_url($item);?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<li class="single-social-twitter d-none d-sm-inline-block">
								<a href="#" onclick="window.open('http://twitter.com/share?text=<?=isset($item->title)?$item->title:''?>&amp;url=<?=post_url($item);?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post">
									<i class="fab fa-twitter"></i>
								</a>
							</li>
							<li class="single-social-com d-none d-sm-inline-block">
								<a href="#comments">
									<i class="far fa-comment"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-12 col-lg-8">
					<?=($item->is_adsense)?widget('Cms/Ads', ['zone_id'=>8]):'';?>
					<div class="single-content">
						<?php
							$content = $item->content;
							$cdn = cdn_url();
							if($cdn) {
								$content = preg_replace('/<img(.*?)src=\"http:\/\/(.*?)\"/', '<img$1src="$2"', $content);
								$content = preg_replace('/<img(.*?)src=\"https:\/\/(.*?)\"/', '<img$1src="$2"', $content);
								$content = preg_replace('/<img(.*?)src=\"(.*?)\"/', '<img$1src="'.$cdn.'$2"', $content);
								$content = preg_replace('/&nbsp;/', ' ', $content);
							}
							if(cms_config('lazy_load')) {
								$content = preg_replace('/<img(.*?)class=\"(.*?)\"/', '<img$1', $content);
								$content = preg_replace('/<img(.*?)src=/', '<img src="'.CDN.'/images/blank.gif" class="lazy"$1data-src=', $content);
							}
							if($item->is_adsense) {
								$ads = widget('Cms/Ads', ['zone_id'=>7]);
								$count = substr_count($content,"</p>");
								$pos = round($count/2);
								$ads_pos = 0;
								if($pos) {
									$i = 0;
									while($i<$pos) {
										$ads_pos = strpos($content, "</p>", $ads_pos);
										$ads_pos = $ads_pos + 4;
										$i++;
									}
								}
								if($ads_pos < strlen($content)) {
									$content_first = substr($content, 0, $ads_pos);
									$content_last = substr($content, $ads_pos+1);
									echo $content_first;
									echo $ads;
									echo $content_last;
								} else echo $content;
							} else echo $content;
						?>
					</div>
					<?php
					if(isset($item->private_download) && $item->private_download) {
						$is_privated = private_download();
					?>
					<div class="private_download <?=$is_privated?'active':''?>">
						<?=$is_privated?$item->private_download:'********'?>
					</div>
					<?
					}
					?>
					<?php if($item->tags) { ?>
					<div class="single-tags mb-3">
						<span class="single-tags-header">TAGS</span>
						<span class="single-tags-content">
						<?php
						$tags = [];
						foreach($item->tags as $tag) {
							$tags[] = $tag->id;
						?>
							<a href="<?=tag_url($tag);?>"><h4><?=$tag->name;?></h4></a>
						<?php } ?>
						</span>
					</div>
					<?php } ?>
					<?php if($item->faqs) { ?>
					<div class="single-faqs mb-3">
						<div class="faq-title">Câu hỏi thường gặp</div>
						<?php
						foreach($item->faqs as $faq) {
						?>
						<div class="faq-items">
							<a class="faq-question collapsed" data-toggle="collapse" href="#faq-<?=$faq->id?>" aria-expanded="false" aria-controls="faq-<?=$faq->id?>">
								<h3><?=$faq->question?></h3>
								<i class="fas fa-angle-down rotate-icon"></i>
							</a>
						</div>
						<div class="collapse faq-content" id="faq-<?=$faq->id?>">
							<p><?=$faq->answer?></p>
						</div>
						<?php
						}
						echo $cms->schemas('faq', ['faqs' => $item->faqs]);
						?>
					</div>
					<?php } ?>
					<?=($item->is_adsense)?widget('Cms/Ads', ['zone_id'=>9]):'';?>
					<?php if($item->tags) { ?>
					<?=widget('NghienCode/Box_news', ['type'=>'related', 'title'=>'Bài viết liên quan', 'post_heading' => 'h4', 'tags'=>$tags, 'excludes' => $item->id]) ?>
					<?php } ?>
				</div>
				<div class="col-sm-12 col-lg-4">
                    <?php
                    echo widget('Cms/Ads', ['zone_id'=>3]);
                    echo widget('Cms/Ads', ['zone_id'=>4]);
                    if(isset($widgets) && is_array($widgets)) {
                        foreach($widgets as $key=>$widget) {
                            $args = [];
                            $args['title'] = $widget->title;
                            $args['type'] = $widget->type;
                            if(!empty($widget->title_heading)) $args['title_heading'] = $widget->title_heading;
                            if(!empty($widget->category_heading)) $args['category_heading'] = $widget->category_heading;
                            if(!empty($widget->post_heading)) $args['post_heading'] = $widget->post_heading;
                            if($widget->category) {
                                if($widget->category==-1) {
                                    if(!empty($categories)) {
                                        $category = reset($categories);
                                        if(!empty($category->id)) $args['category'] = $category->id;
                                    }
                                } else {
                                    $args['category'] = $widget->category;
                                }
                            }
                            if($widget->tags) {
                                if(strpos($widget->tags, ',')) $tags = explode(',', $widget->tags);
                                else $tags = [intval($widget->tags)];
                                $args['tags'] = $tags;
                            }
                            if($widget->excludes) {
                                if(strpos($widget->excludes, ',')) $excludes = explode(',', $widget->excludes);
                                else $excludes = [intval($widget->excludes)];
                                $args['excludes'] = $excludes;
                            }
                            if($widget->post_type) $args['post_type'] = $widget->post_type;
                            if($widget->limit) $args['limit'] = $widget->limit;
                            if($widget->hot) $args['hot'] = $widget->hot;
                            echo '<div class="widget">';
                            echo widget('NghienCode/Box_news', $args);
                            echo '</div>';
                        }
                    }
                    echo widget('Cms/Ads', ['zone_id'=>5]);
                    ?>
				</div>
				<div class="col-12">
					<?=widget('NghienCode/Box_news', ['type'=>'line', 'title' => 'Tin liên quan', 'category' => 9, 'excludes' => $item->id]) ?>
				</div>
			</div>
		</div>
	</div>
<?=$this->endSection();?>