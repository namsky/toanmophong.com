<?=$this->section('content');?>
	<!-- CONTENT -->
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?=URL?>"><i class="fa fa-home"></i></a></li>
							<li class="breadcrumb-item active">RSS Channels</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="box box-white">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2>RSS Channels</h2>
					<p>RSS ( viết tắt từ Really Simple Syndication ) là một tiêu chuẩn định dạng tài liệu dựa trên XML nhằm giúp người sử dụng dễ dàng cập nhật và tra cứu thông tin một cách nhanh chóng và thuận tiện nhất bằng cách tóm lược thông tin vào trong một đoạn dữ liệu ngắn gọn, hợp chuẩn. Dữ liệu này được các chương trình đọc tin chuyên biệt ( gọi là News reader) phân tích và hiển thị trên máy tính của người sử dụng. Trên trình đọc tin này, người sử dụng có thể thấy những tin chính mới nhất, tiêu đề, tóm tắt và cả đường link để xem toàn bộ tin.</p>
					<h2>Danh mục</h2>
					<div class="rss-channels">
						<a href="<?=URL?>/rss/newest.rss" target="_blank">
							<i class="fa fa-rss-square"></i> Trang chủ (<?=URL?>/rss/newest.rss)
						</a>
						<?php if(isset($categories) && count($categories)) { ?>
						<?php foreach($categories as $category) { ?>
						<a href="<?=URL?>/rss-category/<?=$category->slug?>.rss" target="_blank">
							<i class="fa fa-rss-square"></i> <?=$category->name?> (<?=URL?>/rss-category/<?=$category->slug?>.rss)
						</a>
						<?php } ?>
						<?php } ?>
					</div>
					<h2>Tags</h2>
					<div class="rss-channels">
						<?php if(isset($tags) && count($tags)) { ?>
						<?php foreach($tags as $tag) { ?>
						<a href="<?=URL?>/rss-tag/<?=$tag->slug?>.rss" target="_blank">
							<i class="fa fa-rss-square"></i> <?=$tag->name?> (<?=URL?>/rss-tag/<?=$tag->slug?>.rss)
						</a>
						<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?=$this->endSection();?>