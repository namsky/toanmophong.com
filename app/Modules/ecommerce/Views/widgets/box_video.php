	<?php
		if(isset($items) && is_array($items) && count($items) > 1) {
	?>
	<div class="box box-video">
		<div class="container">
			<div class="row">
				<div class="col-12 box-header">
					<?php
						if(empty($title)) $title = '-';
						if(!empty($title_heading)) {
							$title_html = '<'.$title_heading.' class="box-header-title">'.$title.'</'.$title_heading.'>';
						} else $title_html = '<span class="box-header-title">'.$title.'</span>';
						echo $title_html;
					?>
				</div>
				<div class="col-sm-12 col-lg-8">
					<div class="item feature">
						<div class="thumb">
							<a href="<?=post_url($items[0])?>" rel="bookmark">
								<?=show_img($items[0]->thumb, 864, 480, $items[0]->title);?>
								<div class="video-play">
									<span class="fas fa-play"></span>
								</div>
							</a>
						</div>
						<div class="art-text">
							<?php
								$categories = $items[0]->categories;
								if(is_array($categories) && count($categories)) {
									$category = reset($categories);
									if(!empty($category_heading)) {
										$category_title = '<'.$category_heading.' class="category">'.$category->name.'</'.$category_heading.'>';
									} else $category_title = '<div class="category">'.$category->name.'</div>';
							?>
							<a href="<?=category_url($category)?>"><?=$category_title?></a>
							<?php } ?>
							<div class="title">
								<a href="<?=post_url($items[0])?>" rel="bookmark">
									<?php
										if(!empty($post_heading)) {
											$post_title = '<'.$post_heading.' class="title-text">'.$items[0]->title.'</'.$post_heading.'>';
										} else $post_title = '<div class="title-text">'.$items[0]->title.'</div>';
										echo $post_title;
									?>
								</a>
							</div>
							<div class="detail">
								<div class="author float-left">
									<a href="javascript:;" title="" rel="author"><?=isset($items[0]->user)?$items[0]->user->name:cms_config('site_name')?></a>
								</div>
								<div class="date float-right"><i class="far fa-clock"></i> <?=show_date($items[0]->published)?></div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-lg-4">
				<?php for($i=1; $i<count($items); $i++) { ?>
					<div class="art-grid">
						<div class="thumb">
							<a href="<?=post_url($items[$i])?>" rel="bookmark">
								<?=show_img($items[$i]->thumb, 88, 88, $items[$i]->title);?>
							</a>
							<?php
								if($items[$i]->type==1) {
							?>
							<div class="video-play">
								<span class="fas fa-play"></span>
							</div>
							<?php } ?>
						</div>
						<div class="art-text">
							<?php
								$categories = $items[$i]->categories;
								if(is_array($categories) && count($categories)) {
									$category = reset($categories);
									if(!empty($category_heading)) {
										$category_title = '<'.$category_heading.' class="category">'.$category->name.'</'.$category_heading.'>';
									} else $category_title = '<div class="category">'.$category->name.'</div>';
							?>
							<a href="<?=category_url($category)?>"><?=$category_title?></a>
							<?php } ?>
							<div class="title">
								<a href="<?=post_url($items[$i])?>" rel="bookmark">
									<?php
										if(!empty($post_heading)) {
											$post_title = '<'.$post_heading.' class="title-text">'.$items[$i]->title.'</'.$post_heading.'>';
										} else $post_title = '<div class="title-text">'.$items[$i]->title.'</div>';
										echo $post_title;
									?>
								</a>
							</div>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>