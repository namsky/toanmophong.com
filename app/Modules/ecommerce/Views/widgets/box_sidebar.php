					<div class="sidebar sticky_sidebar">
						<?=widget('Cms/Ads', ['zone_id'=>3]);?>
						<?=widget('Cms/Ads', ['zone_id'=>4]);?>
						<div class="sidebar_widget">
							<div class="widget-header">
							<?php
								if(!empty($title)) {
									if(!empty($title_heading)) {
										$title_html = '<'.$title_heading.' class="widget-title">'.$title.'</'.$title_heading.'>';
									} else $title_html = '<span class="widget-title">'.$title.'</span>';
									echo $title_html;
								}
							?>
							</div>
							<div class="widget-content">
							<?php
							foreach($items as $item) {
								if(!empty($post_heading)) {
									$post_title = '<'.$post_heading.' class="title-text">'.$item->title.'</'.$post_heading.'>';
								} else $post_title = '<div class="title-text">'.$item->title.'</div>';
							?>
								<div class="widget-item">
									<div class="title">
										<a href="<?=post_url($item)?>" rel="bookmark">
											<?=$post_title?>
										</a>
									</div>
								</div>
							<?php } ?>
							</div>
						</div>
						<?=widget('Cms/Ads', ['zone_id'=>5]);?>
					</div>