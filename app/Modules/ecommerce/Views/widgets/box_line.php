	<?php
		if(isset($items) && is_array($items) && count($items) > 1) {
	?>
	<div class="box box-line">
		<div class="row">
			<div class="col-12 box-header">
				<?php
					if(empty($title)) $title = '-';
					if(!empty($title_heading)) {
						$title_html = '<'.$title_heading.' class="box-header-title">'.$title.'</'.$title_heading.'>';
					} else $title_html = '<div class="box-header-title">'.$title.'</div>';
					echo $title_html;
				?>
			</div>
			<?php
			foreach($items as $item) {
			?>
			<div class="col-sm-12 col-md-6 col-lg-3">
				<div class="item">
					<div class="thumb">
						<a href="<?=post_url($item)?>" rel="bookmark">
							<?=show_img($item->thumb, 305, 220, $item->title);?>
						</a>
					</div>
					<div class="art-text">
						<div class="title">
							<a href="<?=post_url($item)?>" rel="bookmark">
								<?php
									if(!empty($post_heading)) {
										$post_title = '<'.$post_heading.' class="title-text">'.$item->title.'</'.$post_heading.'>';
									} else $post_title = '<div class="title-text">'.$item->title.'</div>';
									echo $post_title;
								?>
							</a>
						</div>
						<div class="detail">
							<div class="author float-left">
								<a href="javascript:;" title="" rel="author"><?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?></a>
							</div>
							<div class="date float-right"><i class="far fa-clock"></i> <?=show_date($item->published)?></div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php } ?>