<?=$this->section('content');?>
	<!-- CONTENT -->
	<div class="archives-header">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?=URL?>"><i class="fa fa-home"></i></a></li>
							<?php
							if(isset($page)) {
								switch($page) {
									case 'search':
										echo '<li class="breadcrumb-item active">Tìm kiếm: '.htmlentities($key).'</li>';
										break;
									case 'category':
										if(!empty($parents)) {
											foreach($parents as $item) {
												echo '<li class="breadcrumb-item"><a href="'.category_url($item).'">'.$item->name.'</a></li>';
											}
										}
										echo '<li class="breadcrumb-item active">'.$category->name.'</li>';
										break;
									case 'tag':
										echo '<li class="breadcrumb-item active">'.$tag->name.'</li>';
										break;
								}
							}
							?>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<?php
	if(!empty($category->name) && !empty($category->summary)) {
	?>
	<div class="box box-category">
		<div class="container">
			<div class="col">
				<h2 class="category_name"><?=$category->name?></h2>
				<div class="category_summary viewmore-content"><?=$category->summary?></div>
			</div>
		</div>
		<div class="viewmore-area">
			<button class="btn btn-sm viewmore" data-status="false">XEM THÊM</button>
		</div>
	</div>
	<?php
	}
	?>
	<?php
	$last_post = 0;
	if(count($items) > 6) {
	?>
	<div class="box box-grid">
		<div class="container">
			<div class="row">
				<?php
				$item = array_shift($items);
				?>
				<div class="col-sm-12 col-md-6 col-lg-8">
					<div class="item feature">
						<div class="thumb">
							<a href="<?=post_url($item)?>" rel="bookmark">
								<?=show_img($item->thumb, 864, 480, $item->title);?>
							</a>
						</div>
						<div class="art-text">
							<?php
								$categories = $item->categories;
								if(is_array($categories) && count($categories)) {
									$category = reset($categories);
							?>
							<a href="<?=category_url($category)?>">
								<h2 class="category"><?=$category->name?></h2>
							</a>
							<?php } ?>
							<div class="title">
								<a href="<?=post_url($item)?>" rel="bookmark">
									<h1 class="title-text"><?=$item->title?></h1>
								</a>
							</div>
						</div>
					</div>
				</div>
				<?php
				$item = array_shift($items);
				?>
				<div class="col-sm-12 col-md-6 col-lg-4">
					<div class="item feature">
						<div class="thumb">
							<a href="<?=post_url($item)?>" rel="bookmark">
								<?=show_img($item->thumb, 864, 480, $item->title);?>
							</a>
						</div>
						<div class="art-text">
							<?php
								$categories = $item->categories;
								if(is_array($categories) && count($categories)) {
									$category = reset($categories);
							?>
							<a href="<?=category_url($category)?>">
								<h3 class="category"><?=$category->name?></h3>
							</a>
							<?php } ?>
							<div class="title">
								<a href="<?=post_url($item)?>" rel="bookmark">
									<h2 class="title-text"><?=$item->title?></h2>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php
				for($i=2; $i<6; $i++) {
					$item = array_shift($items);
				?>
				<div class="col-sm-12 col-md-6 col-lg-3">
					<div class="item">
						<div class="thumb">
							<a href="<?=post_url($item)?>" rel="bookmark">
								<?=show_img($item->thumb, 305, 172, $item->title);?>
							</a>
						</div>
						<div class="art-text">
							<div class="title">
								<a href="<?=post_url($item)?>" rel="bookmark">
									<h3 class="title-text"><?=$item->title?></h3>
								</a>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="box box-white box-blog">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-8">
					<?php
					if(empty($items)) {
					?>
					<div class="no-data">
						Không có dữ liệu
					</div>
					<?php
					}
					?>
					<?php foreach($items as $item) { ?>
					<div class="art-grid">
						<div class="thumb">
							<a href="<?=post_url($item)?>" rel="bookmark">
								<?=show_img($item->thumb, 305, 220, $item->title);?>
							</a>
						</div>
						<div class="art-text">
							<div class="title">
								<a href="<?=post_url($item)?>" rel="bookmark">
									<h4 class="title-text"><?=$item->title?></h4>
								</a>
							</div>
							<div class="detail">
								<div class="author float-left">
									<a href="javascript:;" title="" rel="author"><?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?></a>
								</div>
								<div class="date float-left ml-4"><i class="far fa-clock"></i> <?=show_date($item->published)?></div>
								<div class="clearfix"></div>
								<div class="description">
									<?=$item->summary?>
								</div>
								<div class="tags">
									<?php
									if(!empty($item->tags)) {
										foreach($item->tags as $tag) {
											echo '<a href="'.tag_url($tag).'">'.$tag->name.'</a>';
										}
									}
									?>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
					<div class="art-pager">
						<?=$paging?>
					</div>
				</div>
				<div class="col-sm-12 col-lg-4">
                    <?php
                        echo widget('Cms/Ads', ['zone_id'=>3]);
                        echo widget('Cms/Ads', ['zone_id'=>4]);
                        if(isset($widgets) && is_array($widgets)) {
                            foreach($widgets as $key=>$widget) {
                                $args = [];
                                $args['title'] = $widget->title;
                                $args['type'] = $widget->type;
                                if(!empty($widget->title_heading)) $args['title_heading'] = $widget->title_heading;
                                if(!empty($widget->category_heading)) $args['category_heading'] = $widget->category_heading;
                                if(!empty($widget->post_heading)) $args['post_heading'] = $widget->post_heading;
                                if($widget->category) {
                                    if($widget->category==-1) {
                                        if(!empty($category->id)) $args['category'] = $category->id;
                                    } else {
                                        $args['category'] = $widget->category;
                                    }
                                }
                                if($widget->tags) {
                                    if(strpos($widget->tags, ',')) $tags = explode(',', $widget->tags);
                                    else $tags = [intval($widget->tags)];
                                    $args['tags'] = $tags;
                                }
                                if($widget->excludes) {
                                    if(strpos($widget->excludes, ',')) $excludes = explode(',', $widget->excludes);
                                    else $excludes = [intval($widget->excludes)];
                                    $args['excludes'] = $excludes;
                                }
                                if($widget->post_type) $args['post_type'] = $widget->post_type;
                                if($widget->limit) $args['limit'] = $widget->limit;
                                if($widget->hot) $args['hot'] = $widget->hot;
                                echo '<div class="widget">';
                                echo widget('NghienCode/Box_news', $args);
                                echo '</div>';
                            }
                        }
                        echo widget('Cms/Ads', ['zone_id'=>5]);
                    ?>
				</div>
			</div>
		</div>
	</div>
<?=$this->endSection();?>