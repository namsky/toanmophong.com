<?=$this->section('content');?>
	<!-- CONTENT -->
	<div class="archives-header">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?=URL?>"><i class="fa fa-home"></i></a></li>
							<?php
							if(isset($page)) {
								switch($page) {
									case 'search':
										echo '<li class="breadcrumb-item active">Tìm kiếm: '.htmlentities($key).'</li>';
										break;
									case 'category':
										echo '<li class="breadcrumb-item active">'.$category->name.'</li>';
										break;
									case 'tag':
										echo '<li class="breadcrumb-item active">'.$tag->name.'</li>';
										break;
								}
							}
							?>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<?php
	$last_post = 0;
	if(count($items) > 6) {
	?>
	<div class="box box-grid">
		<div class="container">
			<div class="row">
				<?php
				$item = array_shift($items);
				?>
				<div class="col-sm-12 col-md-6 col-lg-8">
					<div class="item feature">
						<div class="thumb">
							<a href="<?=post_url($item)?>" rel="bookmark">
								<img width="100%" height="auto" src="<?=show_thumb($item->thumb, 864, 480)?>">
							</a>
						</div>
						<div class="art-text">
							<?php
								$categories = $item->categories;
								if(is_array($categories) && count($categories)) {
									$category = reset($categories);
							?>
							<h3 class="category">
								<a href="<?=category_url($category)?>"><?=$category->name?></a>
							</h3>
							<?php } ?>
							<div class="title">
								<a href="<?=post_url($item)?>" rel="bookmark">
									<h2 class="title-text"><?=$item->title?></h2>
								</a>
							</div>
						</div>
					</div>
				</div>
				<?php
				$item = array_shift($items);
				?>
				<div class="col-sm-12 col-md-6 col-lg-4">
					<div class="item feature">
						<div class="thumb">
							<a href="<?=post_url($item)?>" rel="bookmark">
								<img width="100%" height="auto" src="<?=show_thumb($item->thumb, 864, 480)?>">
							</a>
						</div>
						<div class="art-text">
							<?php
								$categories = $item->categories;
								if(is_array($categories) && count($categories)) {
									$category = reset($categories);
							?>
							<h3 class="category">
								<a href="<?=category_url($category)?>"><?=$category->name?></a>
							</h3>
							<?php } ?>
							<div class="title">
								<a href="<?=post_url($item)?>" rel="bookmark">
									<h2 class="title-text"><?=$item->title?></h2>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php
				for($i=2; $i<6; $i++) {
					$item = array_shift($items);
				?>
				<div class="col-sm-12 col-md-6 col-lg-3">
					<div class="item">
						<div class="thumb">
							<a href="<?=post_url($item)?>" rel="bookmark">
								<img width="100%" height="auto" src="<?=show_thumb($item->thumb, 305, 172)?>">
							</a>
						</div>
						<div class="art-text">
							<div class="title">
								<a href="<?=post_url($item)?>" rel="bookmark">
									<h2 class="title-text"><?=$item->title?></h2>
								</a>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="box box-white box-blog">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-8">
					<?php
					if(empty($items)) {
					?>
					<div class="no-data">
						Không có dữ liệu
					</div>
					<?php
					}
					?>
					<?php foreach($items as $item) { ?>
					<div class="art-grid">
						<div class="thumb">
							<a href="<?=post_url($item)?>" rel="bookmark">
								<img width="240px" height="135px" src="<?=show_thumb($item->thumb, 240, 135)?>">
							</a>
						</div>
						<div class="art-text">
							<h3 class="category">
								<a href="javascript:;"><?=isset($item->admin)?$item->admin->name:cms_config('site_name')?></a>
							</h3>
							<div class="title">
								<a href="<?=post_url($item)?>" rel="bookmark">
									<h2 class="title-text"><?=$item->title?></h2>
								</a>
							</div>
							<div class="detail">
								<div class="date date float-left"><i class="far fa-clock"></i> <?=date("d/m/Y", $item->published)?></div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				<?php } ?>
					<div class="art-pager">
						<?=$paging?>
					</div>
				</div>
				<div class="col-sm-12 col-md-4">
					<?=widget('Frontend/Box_news', ['type'=>'sidebar', 'category'=>1]) ?>
				</div>
			</div>
		</div>
	</div>
<?=$this->endSection();?>