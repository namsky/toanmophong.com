<!DOCTYPE html>
<html ⚡ lang="vi">
<head>
    <meta charset="utf-8" />
    <link rel="canonical" href="<?=post_url($item)?>">
    <meta name="viewport" content="width=device-width,minimum-scale=1">
    <title><?=$title?></title>
    <link rel="shortcut icon" href="https://cdn.glitch.com/d7f46a57-0ca4-4cca-ab0f-69068dec6631%2Fcheese-favicon.png?1540228964214" />
	<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
    <noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
</head>
<body>
    <header class="headerbar"><h2><?=isset($item->title)?$item->title:''?></h2></header>
    <main>
		<div class="main-content">
			<div class="below-hero">
				<h2 class="main-heading"><?=isset($item->summary)?$item->summary:''?></h2>
				<p class="main-text">
					<?php
						$content = $item->content;
						$content = preg_replace('/width="(.*)"/', '', $content);
						$content = preg_replace('/height="(.*)"/', '', $content);
						$content = str_replace('<img', '<amp-img width="1" height="1"', $content);
						$content = str_replace('<span class="toc_toggle">[ Ẩn ]</span>', '', $content);
						$content = str_replace('<iframe', '<amp-iframe width="600" height="400" layout="responsive"', $content);
						$content = str_replace('</iframe>', '</amp-iframe>', $content);
						echo $content;
					?>
				</p>
			</div>
		</div>
    </main>
</body>
</html>