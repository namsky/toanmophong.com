<!doctype html>
<html lang="vi-VN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?=isset($title)?$title:cms_config('site_name');?></title>
	<meta name="description" content="<?=isset($description)?$description:cms_config('site_desc');?>">
	<meta name="keywords" content="<?=isset($keywords)?$keywords:'';?>">
	<meta name="robots" content="index, follow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="alternate" href="<?=current_url();?>" hreflang="vi-vn" />
	<meta name="author" content="<?=URL?>" />
	<meta name="author" content="<?=URL?>/humans.txt" />
	<meta name="publisher" content="<?=URL?>/humans.txt" />
	<?php
	$site_favicon = cms_config('site_favicon');
	if($site_favicon) {
	?>
    <link rel="icon" type="image/png" href="<?=$site_favicon?>" />
	<?php
	}
	?>
	<?php
	$geo_region = cms_config('geo_region');
	if($geo_region) {
	?>
	<meta name="geo.region" content="<?=$geo_region?>" />
	<meta name="geo.placename" content="<?=cms_config('geo_placename')?>" />
	<meta name="geo.position" content="<?=cms_config('geo_latitude')?>;<?=cms_config('geo_longitude')?>" />
	<meta name="ICBM" content="<?=cms_config('geo_latitude')?>, <?=cms_config('geo_longitude')?>" />
	<?php
	}
	?>
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta property="og:site_name" content="<?=cms_config('site_name');?>">
	<meta property="og:type" content="article">
	<meta property="og:locale" content="vi_VN">
	<meta property="fb:app_id" content="">
	<meta property="fb:pages" content="">
	<meta property="og:title" content="<?=isset($title)?$title:cms_config('site_name');?>">
	<meta property="og:url" content="<?=current_url();?>">
	<meta property="og:description" content="<?=isset($description)?$description:cms_config('site_desc');?>">
	<?php
	if(isset($item) && isset($item->thumb)) {
	?>
	<meta property="og:image" content="<?=$item->thumb?>">
	<?php
	} else {
		$thumb = isset($thumb)?$thumb:CDN.'/themes/nghiencode/images/default-thumbnail.jpg';
	?>
	<meta property="og:image" content="<?=$thumb?>">
	<?php } ?>
	
	<?php if(isset($page) && $page == 'single') { ?>
	<link rel="amphtml" href="<?=post_url($item)?>/amp">
	<?php } ?>
	<link rel="canonical" href="<?=current_url();?>">
	<?php
		$text_color = cms_config('text_color');
		$text_hover_color = cms_config('text_hover_color');
		$primary_text_color = cms_config('primary_text_color');
		$primary_color = cms_config('primary_color');
		$primary_hover_color = cms_config('primary_hover_color');
		$secondary_text_color = cms_config('secondary_text_color');
		$secondary_color = cms_config('secondary_color');
		$secondary_hover_color = cms_config('secondary_hover_color');
		$background_color = cms_config('background_color');
		$border_color = cms_config('border_color');
	?>
	<style>
	:root {
		--text-color: <?=$text_color?$text_color:'#000'?>;
		--text-hover-color: <?=$text_hover_color?$text_hover_color:'#333'?>;
		--primary-text-color: <?=$primary_text_color?$primary_text_color:'#fff'?>;
		--primary-color: <?=$primary_color?$primary_color:'#e74c3c'?>;
		--primary-hover-color: <?=$primary_hover_color?$primary_hover_color:'#c0392b'?>;
		--secondary-text-color: <?=$secondary_text_color?$secondary_text_color:'#444'?>;
		--secondary-color: <?=$secondary_color?$secondary_color:'#3949AB'?>;
		--secondary-hover-color: <?=$secondary_hover_color?$secondary_hover_color:'#303F9F'?>;
		--background-color: <?=$background_color?$background_color:'#fff'?>;
		--border-color: <?=$border_color?$border_color:'#d1d1d1'?>;
	}
	</style>
	<?php $cms->show_asset('css'); ?>
	<?php $cms->show_asset('js'); ?>
	<!-- THEME STYLES -->
	<?=load_css('styles.css', 'themes/nghiencode'); ?>
	<?=load_js('core.js', 'themes/nghiencode'); ?>
	<?php if(isset($external_scripts)) {
		foreach($external_scripts as $script) echo '<script src="'.$script.'"></script>';
	} ?>
	<script>
		var base_url = "<?=URL?>";
	</script>
</head>
<body>
	<header>
		<div class="container">
			<div class="row header_main">
				<div class="col-4">
					<a class="navbar-brand" href="<?=URL?>">
						<?php
							$logo = cms_config('site_logo');
							if($logo) {
								if(!strstr($logo, 'http')) $logo = CDN.$logo;
								echo '<img src="'.$logo.'" alt="'.cms_config('site_name').'" />';
							} else echo cms_config('site_name');
						?>
					</a>
				</div>
				<div class="col-lg-4 d-lg-block d-none">
					<form class="form-inline my-2 my-lg-0 search-form" action="<?=URL?>/search/">
						<div class="searchbar">
							<input class="search_input" type="text" name="q" placeholder="Search..." autocomplete="off">
							<button class="search_icon"><i class="fas fa-search"></i></button>
						</div>
						<?=$cms->schemas('search');?>
					</form>
				</div>
                <?php
                if(cms_config('user_system')) {
                ?>
				<div class="col-lg-4 col-8 text-right">
					<ul class="navbar-nav right-menu d-none" id="unauthenticated">
						<li class="nav-item"><a class="nav-link btn-login" data-toggle="modal" href="#login">Đăng nhập</a></li>
						<li class="nav-item d-none d-sm-inline-block"><a class="nav-link btn-register" data-toggle="modal" href="#login">Đăng ký</a></li>
					</ul>
					<div class="d-none navbar" id="authenticated">
						<a class="btn btn-sm btn-primary d-none d-sm-inline-block" href="javascript:;" id="new_post">
							Viết bài
						</a>
						<div class="dropdown notification">
							<a class="btn dropdown-toggle" href="javascript:;" role="button" id="notification" data-toggle="dropdown">
								<i class="fa fa-bell"></i>
							</a>
							<div class="dropdown-menu" aria-labelledby="notification">
								<p class="title">Thông báo</p>
								<div class="content">
									<a class="dropdown-item" href="javascript:;"><i class="fas fa-circle"></i> Bài viết của bạn đã được phê duyệt</a>
									<a class="dropdown-item" href="javascript:;"><i class="fas fa-check-circle"></i> Bài viết của bạn đã được phê duyệt</a>
									<a class="dropdown-item" href="javascript:;"><i class="fas fa-check-circle"></i> Bài viết của bạn đã được phê duyệt</a>
									<a class="dropdown-item" href="javascript:;"><i class="fas fa-check-circle"></i> Bài viết của bạn đã được phê duyệt</a>
									<a class="dropdown-item" href="javascript:;"><i class="fas fa-check-circle"></i> Bài viết của bạn đã được phê duyệt</a>
									<a class="dropdown-item" href="javascript:;"><i class="fas fa-check-circle"></i> Bài viết của bạn đã được phê duyệt</a>
									<a class="dropdown-item" href="javascript:;"><i class="fas fa-check-circle"></i> Bài viết của bạn đã được phê duyệt</a>
									<a class="dropdown-item" href="javascript:;"><i class="fas fa-check-circle"></i> Bài viết của bạn đã được phê duyệt</a>
									<a class="dropdown-item" href="javascript:;"><i class="fas fa-check-circle"></i> Bài viết của bạn đã được phê duyệt</a>
									<a class="dropdown-item" href="javascript:;"><i class="fas fa-check-circle"></i> Bài viết của bạn đã được phê duyệt</a>
								</div>
								<p class="footer">
									<a href="#">XEM TẤT CẢ</a>
								</p>
							</div>
						</div>
						<div class="dropdown">
							<a class="btn dropdown-toggle" href="javascript:;" role="button" id="account_name" data-toggle="dropdown">
								<?=cms_config('site_name')?>
							</a>
							<div class="dropdown-menu" aria-labelledby="account_name">
								<a class="dropdown-item" href="<?=URL?>/profile/me">Trang cá nhân</a>
								<a class="dropdown-item" href="<?=URL?>/account/messages">Tin nhắn</a>
								<a class="dropdown-item" href="<?=URL?>/account/posts">Bài viết của bạn</a>
								<a class="dropdown-item" href="<?=URL?>/account/">Cài đặt tài khoản</a>
                                <hr class="m-1" />
								<a class="dropdown-item user-logout" href="javascript:;">Đăng xuất</a>
							</div>
						</div>
					</div>
				</div>
                <?php } ?>
			</div>
			<div class="navbar navbar-expand-lg">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false">
					<span class="fas fa-bars"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbar-collapse">	
					<?=widget('Cms/Main_menu') ?>
				</div>
			</div>
		</div>
	</header>
    <div class="content_body">
        <?=widget('Cms/Ads', ['zone_id'=>1]);?>
        <?=$this->renderSection('content')?>
    </div>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<a class="brand_name" href="<?=URL?>"><?=cms_config('site_name')?></a>
				</div>
				<div class="col-12 col-md-6">
					<p>
						<?=cms_config('footer_content_1')?><br />
						<?=cms_config('footer_content_2')?><br />
						<?=cms_config('footer_content_3')?>
					</p>
				</div>
				<div class="col-12 col-md-3">
                    <?php
                    $hotline = cms_config('hotline');
                    if($hotline) {
                    if(strpos($hotline, '@')) {
                    ?>
					<div class="float-left hotline">
                        <i class="fas fa-envelope"></i>
					</div>
					<div class="float-left">
						LIÊN HỆ:<br />
						<?=$hotline;?>
					</div>
                    <?php } else {?>
					<div class="float-left hotline">
						<i class="fa fa-phone"></i>
					</div>
					<div class="float-left">
						HOTLINE:<br />
						<?=$hotline;?>
					</div>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <?php } ?>
				</div>
				<div class="col-12 col-md-3">
					<div class="social">
						<?php
						$link = cms_config('social_facebook');
						if($link != false && $link != 'false') {
						?>
						<a class="facebook" href="<?=$link?>" target="_blank">
							<i class="fab fa-facebook-f"></i>
						</a>
						<?php } ?>
						<?php
						$link = cms_config('social_twitter');
						if($link != false && $link != 'false') {
						?>
						<a class="twitter" href="<?=$link?>" target="_blank">
							<i class="fab fa-twitter"></i>
						</a>
						<?php } ?>
						<?php
						$link = cms_config('social_skype');
						if($link != false && $link != 'false') {
						?>
						<a class="skype" href="<?=$link?>" target="_blank">
							<i class="fab fa-skype"></i>
						</a>
						<?php } ?>
						<?php
						$link = cms_config('social_youtube');
						if($link != false && $link != 'false') {
						?>
						<a class="youtube" href="<?=$link?>" target="_blank">
							<i class="fab fa-youtube"></i>
						</a>
						<?php } ?>
						<a class="rss" href="<?=URL?>/rss-channels/">
							<i class="fas fa-rss"></i>
						</a>
					</div>
				</div>
				<div class="back-to-top">
					<span class="fas fa-angle-up"></span>
				</div>
			</div>
		</div>
	</footer>
    <?php
    if(cms_config('user_system')) {
    ?>
	<!-- Modal -->
	<div class="modal fade" id="login" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content login-modal">
				<div class="row">
					<div class="col-6 d-lg-block d-none">
						<img class="background" src="<?=CDN?>/themes/nghiencode/images/bg-login.png" alt="" />
					</div>
					<div class="col-lg-6 col-md-12">
						<button type="button" class="close pt-1 pr-2" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<p class="h3 my-5 font-weight-normal" style="text-align: center">ĐĂNG NHẬP</p>
						<div class="social-login">
							<button class="btn facebook-btn social-btn" type="button"><span><i class="fab fa-facebook-f"></i> Facebook</span> </button>
							<button class="btn google-btn social-btn" type="button"><span><i class="fab fa-google"></i> Google</span> </button>
						</div>
						<p style="text-align:center"> OR  </p>
						<form class="form-signin">
							<input type="text" id="account" name="account" class="form-control mb-2" placeholder="Tài khoản" required>
							<input type="password" id="password" name="password" class="form-control mb-2" placeholder="Mật khẩu" required>
							
							<button class="btn btn-primary btn-submit" type="submit">ĐĂNG NHẬP <i class="fas fa-sign-in-alt"></i></button>
							<a href="#" id="forgot_pswd">Quên mật khẩu?</a>
							
							<button class="btn btn-block mt-5" type="button" id="btn-signup">ĐĂNG KÝ <i class="fas fa-long-arrow-alt-right"></i></button>
						</form>
						<form class="form-reset">
							<input type="email" id="reset_email" name="reset_email" class="form-control mb-2 mt-4" placeholder="Email" required>
                            <?php
                            if(cms_config('user_catcha')) {
                            ?>
							<div class="row mb-2">
								<div class="col-6 pr-1">
									<input type="text" name="secure_code" id="secure_code" class="form-control" placeholder="Mã bảo mật" required>
								</div>
								<div class="col-6 pl-1">
									<img class="secure_code" src="<?=URL?>/captcha.png?rand=<?=time()?>" alt="SecureCode" />
								</div>
							</div>
                            <?php } ?>
							<button class="btn btn-primary btn-submit mb-2" type="submit">RESET</button>
							<a href="#" class="mt-5" id="cancel_reset"><i class="fas fa-angle-left"></i> Quay lại</a>
						</form>
						<form class="form-signup">
							<input type="text" name="account" id="signup_account" class="form-control mb-2" placeholder="Tài khoản" required>
							<input type="text" name="email" id="signup_email" class="form-control mb-2" placeholder="Email" required>
							<div class="row mb-2">
								<div class="col-6 pr-1">
									<input type="password" name="password" id="signup_password" class="form-control" placeholder="Mật khẩu" required>
								</div>
								<div class="col-6 pl-1">
									<input type="password" name="confirm" id="signup_confirm" class="form-control" placeholder="Xác nhận" required>
								</div>
							</div>
                            <?php
                            if(cms_config('user_catcha')) {
                            ?>
							<div class="row mb-2">
								<div class="col-6 pr-1">
									<input type="text" name="secure_code" id="reset_secure_code" class="form-control" placeholder="Mã bảo mật" required>
								</div>
								<div class="col-6 pl-1">
									<img class="secure_code" src="<?=URL?>/captcha.png?rand=<?=time()?>" alt="SecureCode" />
								</div>
							</div>
                            <?php } ?>
							<button class="btn btn-primary btn-submit" type="submit">ĐĂNG KÝ</button>
							<a href="#" id="cancel_signup"><i class="fas fa-angle-left"></i> Quay lại</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script>
        $(".secure_code").attr("src", "<?=URL?>/captcha.png?rand=" + Math.random());
        $(".secure_code").click(function() {
            $(".secure_code").attr("src", "<?=URL?>/captcha.png?rand=" + Math.random());
        });
    </script>
    <?php } ?>
<?php
$gtm_id = cms_config('gtm_id');
if($gtm_id) {
?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','<?=$gtm_id?>');</script>
<!-- End Google Tag Manager -->
<?php } ?>
<?php
$facebook_app_id = cms_config('facebook_app_id');
if($facebook_app_id) {
?>
<script>
    window.fbAsyncInit = function() {
        FB.init({appId: '<?=$facebook_app_id?>', cookie: true, xfbml: true, version: 'v8.0'});
        FB.AppEvents.logPageView();   
    };
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<?php } ?>
</body>
</html>