<?php
namespace App\Modules\NghienCode\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Core\Cms\CmsController;

class Home extends CmsController
{
	public function __construct()
	{
	}
    public function index()
    {
		$this->view->setVar('title', cms_config('site_title'));
		$this->view->setVar('description', cms_config('site_description'));
		$this->view->setVar('keywords', cms_config('site_keywords'));
		
		$cached = cms_config('cache');
		$widgets = $cached?$this->cache->get('widget_page_1'):false;
		if(!$widgets) {
			$widgetModel = model('App\Modules\Cms\Models\WidgetModel');
			$widgets = $widgetModel->where('page_id', 1)->orderBy('order', 'ASC')->findAll();
			$this->cache->save('widget_page_1', $widgets, 86400);
        }
		$this->view->setVar('widgets', $widgets);
        $this->cms->load_vendors([
            'fontawesome',
            'jquery',
            'bootstrap',
            'sweetalert2',
            'jquery.lazyload',
        ]);
		return $this->view->render('home');
	}
	public function search()
	{
		$uri = $this->request->uri;
		$key = strip_tags($this->request->getGet('q'));
		$postModel = model('App\Modules\Cms\Models\PostModel');
		
		$cached = cms_config('cache');
		$widgets = $cached?$this->cache->get('widget_page_2'):false;
		if(!$widgets) {
			$widgetModel = model('App\Modules\Cms\Models\WidgetModel');
			$widgets = $widgetModel->where('page_id', 2)->orderBy('order', 'ASC')->findAll();
			$this->cache->save('widget_page_2', $widgets, 86400);
		}
        $this->view->setVar('widgets', $widgets);
        
		$this->view->setVar('title', 'tìm kiếm '.$key.' | '.cms_config('site_name'));
		$page = intval($this->request->getGet('page'));
		if($page == 0) $page = 1;
		$items = $cached?$this->cache->get('search_'.$key.'_items_p'.$page):false;
		$paging = $cached?$this->cache->get('search_'.$key.'_paging_p'.$page):false;
		if(!$items) {
			$items = $postModel->where('status', 1)->where('published<', time())->with('user', ['fields'=>'id,username,name'])->with('relations')->like('title', $key)->orLike('summary', $key)->orderBy('published', 'DESC')->paginate(12);
			$paging = $postModel->pager->links();
			$this->cache->save('search_'.$key.'_items_p'.$page, $items, 86400);
			$this->cache->save('search_'.$key.'_paging_p'.$page, $paging, 86400);
		}
		$this->view->setVar('key', $key);
		$this->view->setVar('items', $items);
		$this->view->setVar('paging', $paging);
		$this->view->setVar('page', 'search');
		$this->cms->load_vendors([
			'fontawesome',
			'jquery',
			'bootstrap',
            'sweetalert2',
            'jquery.lazyload',
		]);
		return $this->view->render('archives');
	}
	public function tag()
	{
		$request_uri = $this->request->getServer('REQUEST_URI');
		$last_of_uri = substr($request_uri, -1);
		if($last_of_uri == '/') {
			return cms_redirect($uri);
		}
		$uri = $this->request->uri;
		$slug = $uri->getSegment(2);
		$tagModel = model('App\Modules\Cms\Models\TagModel');
		$postModel = model('App\Modules\Cms\Models\PostModel');
		
		$cached = cms_config('cache');
		$widgets = $cached?$this->cache->get('widget_page_2'):false;
		if(!$widgets) {
			$widgetModel = model('App\Modules\Cms\Models\WidgetModel');
			$widgets = $widgetModel->where('page_id', 2)->orderBy('order', 'ASC')->findAll();
			$this->cache->save('widget_page_2', $widgets, 86400);
        }
		$this->view->setVar('widgets', $widgets);
        
		$tag = $cached?$this->cache->get('tag_'.$slug):false;
		if(!$tag) {
			$tag = $tagModel->where('slug', $slug)->first();
			$this->cache->save('tag_'.$slug, $tag, 86400);
		}
		if(isset($tag->id)) {
			$this->view->setVar('title', ($tag->title)?$tag->title:'Tag '.$tag->name.' | '.cms_config('site_name'));
			$this->view->setVar('description', ($tag->description)?$tag->name:cms_config('site_description'));
			$this->view->setVar('keywords', ($tag->keywords)?$tag->keywords:cms_config('site_keywords'));
			$page = intval($this->request->getGet('page'));
			if($page == 0) $page = 1;
			$items = $cached?$this->cache->get('tag_'.$slug.'_items_p'.$page):false;
			$paging = $cached?$this->cache->get('tag_'.$slug.'_paging_p'.$page):false;
			if(!$items) {
				$items = $postModel->where('status', 1)->where('published<', time())->with('user', ['fields'=>'id,username,name'])->with('relations')->orderBy('published', 'DESC')->tags([$tag->id])->paginate(12);
				$paging = $postModel->pager->links();
				$this->cache->save('tag_'.$slug.'_items_p'.$page, $items, 86400);
				$this->cache->save('tag_'.$slug.'_paging_p'.$page, $paging, 86400);
			}
			$this->view->setVar('tag', $tag);
			$this->view->setVar('items', $items);
			$this->view->setVar('paging', $paging);
			$this->view->setVar('page', 'tag');
			$this->cms->load_vendors([
				'fontawesome',
				'jquery',
				'bootstrap',
				'sweetalert2',
				'jquery.lazyload',
			]);
			return $this->view->render('archives');
		} else throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
	}
	public function router()
	{
		/* Controller of Category & Single Post */
		$request_uri = $this->request->getServer('REQUEST_URI');
		$last_of_uri = substr($request_uri, -1);
		if($last_of_uri == '/') {
			return cms_redirect($uri);
		}
		$uri = $this->request->uri;
		$slug = $uri->getSegment(1);
		$categoryModel = model('App\Modules\Cms\Models\CategoryModel');
		$postModel = model('App\Modules\Cms\Models\PostModel');
		
		$cached = cms_config('cache');
		$category = $cached?$this->cache->get('category_'.$slug):NULL;
		if(is_null($category)) {
			$category = $categoryModel->where('slug', $slug)->first();
			if(is_null($category)) $category = false;
			$this->cache->save('category_'.$slug, $category, 86400);
		}
		if(!isset($category->id)) {
			$post = $cached?$this->cache->get('post_'.$slug):NULL;
			$parents = $cached?$this->cache->get('post_'.$slug.'_parents'):false;
			if(is_null($post)) {
				$post = $postModel->with('faqs')->with('user', ['fields'=>'id,username,name,avatar'])->with('relations')->where('slug', $slug)->first();
				if(is_null($post)) {
					$post = false;
					$parents = false;
				}
				else {
					if(!empty($post->categories)) {
						$categories = $post->categories;
						if(!empty($categories)) $_category = array_pop($categories);
						if(!empty($_category->parent)) $parents = $categoryModel->get_parents($_category->parent);
					}
				}
				$this->cache->save('post_'.$slug.'_parents', $parents, 86400);
				$this->cache->save('post_'.$slug, $post, 86400);
			}
		}
		if(isset($category->id)) {
			$this->view->setVar('title', ($category->title)?$category->title:$category->name.' | '.cms_config('site_name'));
			$this->view->setVar('description', ($category->description)?$category->description:cms_config('site_description'));
			$this->view->setVar('keywords', ($category->keywords)?$category->keywords:cms_config('site_keywords'));
			$page = intval($this->request->getGet('page'));
			if($page == 0) $page = 1;
			$items = $cached?$this->cache->get('category_'.$slug.'_items_p'.$page):false;
			$paging = $cached?$this->cache->get('category_'.$slug.'_paging_p'.$page):false;
			$parents = $cached?$this->cache->get('category_'.$slug.'_parents'.$page):false;
			if(!$items) {
				$items = $postModel->where('status', 1)->where('published<', time())->with('user', ['fields'=>'id,username,name'])->with('relations')->orderBy('published', 'DESC')->categories([$category->id])->paginate(12);
				$paging = $postModel->pager->links();
				$parents = $categoryModel->get_parents($category->parent);
				$this->cache->save('category_'.$slug.'_items_p'.$page, $items, 86400);
				$this->cache->save('category_'.$slug.'_paging_p'.$page, $paging, 86400);
				$this->cache->save('category_'.$slug.'_parents'.$page, $parents, 86400);
			}
            $widgets = $cached?$this->cache->get('widget_page_2'):false;
            if(!$widgets) {
                $widgetModel = model('App\Modules\Cms\Models\WidgetModel');
                $widgets = $widgetModel->where('page_id', 2)->orderBy('order', 'ASC')->findAll();
                $this->cache->save('widget_page_2', $widgets, 86400);
            }
            $this->view->setVar('widgets', $widgets);

			$this->view->setVar('parents', $parents);
			$this->view->setVar('category', $category);
			$this->view->setVar('items', $items);
			$this->view->setVar('paging', $paging);
			$this->view->setVar('page', 'category');
			$this->cms->load_vendors([
				'fontawesome',
				'jquery',
				'bootstrap',
				'sweetalert2',
				'jquery.lazyload'
			]);
			return $this->view->render('archives');
		} elseif(isset($post->id)) {
            $widgets = $cached?$this->cache->get('widget_page_3'):false;
            if(!$widgets) {
                $widgetModel = model('App\Modules\Cms\Models\WidgetModel');
                $widgets = $widgetModel->where('page_id', 3)->orderBy('order', 'ASC')->findAll();
                $this->cache->save('widget_page_3', $widgets, 86400);
            }
            $this->view->setVar('widgets', $widgets);
        
			$amp = $uri->getSegment(2);
			$this->view->setVar('title', ($post->seo_title)?$post->seo_title:$post->title.' | '.cms_config('site_name'));
			$this->view->setVar('description', ($post->seo_description)?$post->seo_description:$post->summary);
			$this->view->setVar('keywords', ($post->seo_keywords)?$post->seo_keywords:cms_config('site_keywords'));
			$this->view->setVar('page', 'single');
			$this->view->setVar('item', $post);
			if(!empty($parents)) $this->view->setVar('parents', $parents);
            $postModel->increase_views($post->id);
			if($amp == 'amp') {
				$data = $this->view->getData();
				return view('\App\Modules\Frontend\Views\amp\single', $data);
			} else  {
				$this->cms->load_vendors([
					'fontawesome',
					'jquery',
					'bootstrap',
					'jquery.lazyload',
					'prism',
				]);
				return $this->view->render('single');
            }
		} else throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
	}
}