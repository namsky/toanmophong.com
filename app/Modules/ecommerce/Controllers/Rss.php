<?php
namespace App\Modules\NghienCode\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Core\Cms\CmsController;

class Rss extends CmsController
{
	public function __construct()
	{
	}
	public function index()
	{
		$this->view->setVar('title', 'RSS Channels');
		$this->view->setVar('description', cms_config('site_description'));
		$this->view->setVar('keywords', cms_config('site_keywords'));
		
		$categoryModel = model('App\Modules\Cms\Models\CategoryModel');
		$tagModel = model('App\Modules\Cms\Models\TagModel');
		$categories = $categoryModel->findAll();
		$tags = $tagModel->orderBy('count', 'DESC')->findAll(10);
		$this->view->setVar('categories', $categories);
		$this->view->setVar('tags', $tags);
		
        $this->cms->load_vendors([
            'fontawesome',
            'jquery',
            'bootstrap',
            'sweetalert2',
            'jquery.lazyload',
        ]);
		return $this->view->render('rss/channels');
	}
	public function item()
	{
		$categoryModel = model('App\Modules\Cms\Models\CategoryModel');
		$tagModel = model('App\Modules\Cms\Models\TagModel');
		$postModel = model('App\Modules\Cms\Models\PostModel');
		
		$this->view->setVar('title', 'RSS Channels');
		$this->view->setVar('description', cms_config('site_description'));
		$this->view->setVar('keywords', cms_config('site_keywords'));
		
		$uri = $this->request->uri;
		$type = $uri->getSegment(1);
		$slug = $uri->getSegment(2);
		if($slug) {
			$slug = str_replace('.rss', '', $slug);
			switch($type) {
				case "rss-category":
					$this->view->setVar('type', 'category');
					$category = $categoryModel->where('slug', $slug)->first();
					if(isset($category->id)) {
						$this->view->setVar('title', 'RSS thể loại '.$category->name);
						$items = $postModel->categories([$category->id])->findAll(40);
					}
					break;
				case "rss-tag":
					$this->view->setVar('type', 'tag');
					$tag = $tagModel->where('slug', $slug)->first();
					if(isset($tag->id)) {
						$this->view->setVar('title', 'RSS tag '.$tag->name);
						$items = $postModel->tags([$tag->id])->findAll(40);
					}
					break;
				default:
					$this->view->setVar('type', 'page');
					if($slug == 'newest')  {
						$this->view->setVar('title', 'RSS Mới nhất');
						$items = $postModel->findAll(40);
					}
					break;
			}
			if(isset($items)) $this->view->setVar('items', $items);
			else throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
			
			$this->response->setHeader("Content-Type", "Application/xml; charset=utf-8");
			$data = $this->view->getData();
			return view('\App\Modules\Frontend\Views\rss\archives', $data, ['debug'=>false]);
		} else throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
	}
}