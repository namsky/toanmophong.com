<?php
	if(!empty($items)) {
?>
	<?php if(!empty($items)) { ?>
            <?php foreach ($items as $item) { ?>
            <div class="news-item clearfix">
                <a href="<?=post_url($item)?>" class="news-item__img hover-glass"><img onerror="noImg(this, 180, 130)" src="<?=$item->thumb?>" style="width: 180px; height: 130px" alt="<?=$item->title?>"></a>
                <p class="news-item__date"><i class="far fa-clock"></i> <?=show_date($item->created)?></p>
                <a href="<?=post_url($item)?>" class="news-item__name"><?=$item->title?></a>
                <p><?=$item->summary?></p>
            </div>
	<?php }
    } ?>
<?php } ?>