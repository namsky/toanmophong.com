<?php
	if(isset($items) && is_array($items) && count($items)) {
?>
    <?php foreach($items as $item) { ?>

        <div class="col_news col-md-3 col-sm-6 col-xs-6">
            <div class="box_news clearfix">
                <a class="box_news_img" href="<?=post_url($item)?>" title="$item->title" class="my_glass"><img alt="$item->title" style="width: 300px;height: 260px" src="<?=$item->thumb?>" onError="noImg(this, 300, 260)" class="w100 trans03" /></a>
                <div class="right_news">
                    <h3 class="box_news_name">
                        <a href="<?=post_url($item)?>" title="<?=$item->title?>"><?=$item->title?></a>
                    </h3>
                    <div class="box_news_mota"><?=$item->summary?></div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>
