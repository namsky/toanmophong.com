<?php
	if(!empty($items) && count($items) > 1) {
?>
<div class="col-md-6">
	<div class="news-area">
		<div class="news-item big-news">
			<div class="news-images">
				<a href="<?=post_url($items[0])?>" title="<?=clear_utf8($items[0]->title)?>">
					<img src="<?=$items[0]->thumb?>" alt="<?=clear_utf8($items[0]->title)?>" width="100%">
				</a>
			</div>
			<div class="news-content">
				<a href="<?=post_url($items[0])?>" title="<?=clear_utf8($items[0]->title)?>"><?=$items[0]->title?></a>
				<p><?=!empty($items[0]->summary)?cutOf($items[0]->summary, 150):cutOf($items[0]->content, 150)?></p>
			</div>
		</div>
		<div class="news-list">
			<?php
				foreach($items as $key => $item) {
					if($key > 0) {
			?>
			<div class="news-item">
				<div class="news-images">
					<a href="<?=post_url($item)?>" title="<?=clear_utf8($item->title)?>">
						<img src="<?=$item->thumb?>" alt="<?=clear_utf8($item->title)?>" width="100%">
					</a>
				</div>
				<div class="news-content">
					<a href="<?=post_url($item)?>" title="<?=clear_utf8($item->title)?>"><?=$item->title?></a>
					<p><?=!empty($item->summary)?cutOf($item->summary, 100):cutOf($item->content, 100)?></p>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php } ?>