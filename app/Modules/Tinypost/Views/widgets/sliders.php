<?php
	if(!empty($sliders)) {
?>
        <div id="slider" class="owl-carousel owl-theme">
        <?php foreach($sliders as $item) { ?>
            <a class="slider-img" href="" title="">
                <picture>
                    <source class="owl-lazy" media="(max-width: 420px)" data-srcset="<?=$item->image?>">
                    <img class="owl-lazy" data-src="<?=$item->image?>" alt="<?=$item->name?>">
                </picture>
            </a>
        <?php } ?>
        </div>
<?php } ?>