<?php
	if(!empty($items)) {
?>
	<section class="intro-section scroll-sec" id="intro">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-sm-12">
					<img src="<?=CDN?>/themes/tinypost/images/logo/logo.png" alt="logo" class="about-logo">
					<img src="<?=CDN?>/themes/tinypost/images/seperate.png" alt="seperate" class="white-seperate">
					<?php foreach($items as $key=>$item) { 
						if($key == 0) {
					?>	
						<div class="content">
							<?=$item->content?>
							<img src="<?=CDN?>/themes/tinypost/images/seperate.png" alt="seperate" class="white-seperate">
						</div>
					<?php } else { ?>
						<div class="about-item">
							<p class="h5 text-warning"><?=$item->title?></p>
							<div class="content">
								<?=$item->content?>
							</div>
						</div>
					<?php } } ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>