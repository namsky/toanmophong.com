<?php
if(!empty($items) && count($items) >= 1) {
    ?>
    <div class="measure-grid">
    <?php
    if (!empty($items['hot'])) { ?>
        <a href="<?=post_url($items['hot'][0])?>" class="measure-item hover-glass">
            <img src="<?=$items['hot'][0]->thumb?>" alt="<?=$items['hot'][0]->title?>">
            <span class="measure-item__name"><?=$items['hot'][0]->title?></span>
        </a>
    <?php }?>
    <?php if(!empty($items['main']))  {?>
         <?php foreach($items['main'] as $key => $item) {
            ?>
            <a href="<?=post_url($item)?>" class="measure-item hover-glass">
                <img src="<?=$item->thumb?>" alt="<?=$item->title?>">
                <span class="measure-item__name"><?=$item->title?></span>
            </a>
        <?php }
    }?>
    </div>
<?php } ?>
