	<?php
		if(isset($items) && is_array($items) && count($items) > 1) {
	?>
	<div class="post__list" id="<?=$tab_id?>">
		<div class="first_post"> 
			<a href="<?=category_url($category)?>" class="view_more">Xem thêm <i class="more_icon"></i></a>
			<div class="first_post__thumbail">
				<a href="<?=post_url($items[0])?>" title="<?=$items[0]->title?>"> 
					<img src="<?=$items[0]->thumb?>" width="153" height="82" alt="<?=$items[0]->title?>"> 
				</a>
			</div>
			<div class="first_post__detail">
				<h4>
					<a href="<?=post_url($items[0])?>" title="<?=$items[0]->title?>"><?=$items[0]->title?></a>
				</h4> 
				<span class="time_update"><?=show_date($items[0]->published)?></span>
			</div>
		</div>
		<ul class="new_post">
			<?php
			foreach($items as $key => $item) {
				if($key > 0) {
			?>
			<li>
				<a href="<?=post_url($item)?>" title="<?=$item->title?>"> <i class="post_icon"></i> <span><?=$item->title?></span> <span class="time"><?=show_date($item->published)?></span> </a>
			</li>
			<?php } } ?>
		</ul>
	</div>
	<?php } ?>