<?php
if(!empty($items) && count($items) >= 1) {
    ?>
    <div class="service-owl owl-carousel">
        <?php if(!empty($items))  {?>
            <?php foreach($items as $key => $item) {
                    ?>
                    <div class="service-item">
                        <a href="<?=post_url($item)?>" class="service-item__name"><?=$item->title?></a>
                        <a href="do-dac-xac-dinh-vi-tri-cam-moc-ranh-gioi-thua-dat" class="service-item__img hover-glass">
                            <img onerror="noImg(this, 280, 230)" src="<?=$item->thumb?>" alt="<?=$item->title?>">
                        </a>
                    </div>
            <?php }
        }?>
    </div>
<?php } ?>
