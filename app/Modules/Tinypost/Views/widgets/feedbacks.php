<?php
	if(!empty($items)) {
?>
<section class="feedback-section position-relative">
		<div class="container">
			<div class="section-title text-center mb-5">
				<h2>Khách hàng nói gì về Tiny post</h2>
			</div>
			<div class="feedback-slider">
				<?php foreach($items as $item) { ?>
				<div class="feedback slider-item">
					<img src="<?=CDN?>/themes/tinypost/images/dark_frame.png" alt="dark-frame" width="100%">
					<div class="feedback-image">
						<img src="<?=$item->image?>" alt="customer">
					</div>
					<div class="feedback-content text-uppercase">
						<p><?=$item->customer?></p>
						<p><?=$item->content?></p>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</section>
<?php } ?>