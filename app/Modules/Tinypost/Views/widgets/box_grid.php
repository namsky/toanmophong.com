	<?php
		if(isset($items) && is_array($items) && count($items) > 1) {
	?>
		<div class="related-posts">
			<?php
				if(!empty($title)) {
					if(!empty($title_heading)) {
						$title_html = '<'.$title_heading.'>'.$title.'</'.$title_heading.'>';
					} else $title_html = '<h4>'.$title.'</h4>';
					echo $title_html;
				}
			?>
			<div class="row c-gutter-30 mb-25">
				<?php foreach($items as $item) { ?>
				<div class="col-12 col-md-6 mb-3">
					<div class="vertical-item">
						<div class="item-media">
							<img src="<?=$item->thumb?>" alt="<?=clear_utf8($item->title)?>">
						</div>
						<div class="item-content">
							<p>
								<a href="<?=post_url($item)?>"><?=$item->title?></a>
							</p>
						</div>
						<p class="item-meta link-a">
							<a href="<?=post_url($item)?>"><i class="fa fa-calendar color-main fs-14"></i> <?=date('d.m.y', $item->published)?></a>
							<span class="color-main3 mx-1">-</span>
							<a href="javascript:;"><i class="fa fa-eye color-main fs-14"></i> <?=$item->views?></a>
						</p>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>