<?php
	if(!empty($items)) {
?>
<div class="container-fluid scroll-sec" id="cafe-menu">
    <div class="row">
        <div class="col-lg-8 col-sm-12">
            <div class="section-title mb-5">
                <h2>Thực đơn</h2>
                <img src="<?=CDN?>/themes/tinypost/images/seperate.png" alt="seperate" class="seperate">
            </div>
            <div class="menu-area">
                <?php if(!empty($items['hot'])) { ?>
                <div class="menu-hot d-flex align-items-center justify-content-between">
                    <?php foreach($items['hot'] as $item) { ?>
                    <div class="menu-hot-item">
                        <img src="<?=CDN?>/themes/tinypost/images/dark_frame.png" alt="dark-frame" width="100%">
                        <div class="menu-hot-image">
                            <img src="<?=$item->image?>" alt="coffee-cup">
                        </div>
                        <div class="menu-hot-name text-uppercase">
                            <?=$item->name?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>
                <?php if(!empty($items['main'])) { ?>
                <div class="menu-list">
                    <h3>CÀ PHÊ (COFFEE)</h3>
                    <ul>
                        <?php foreach($items['main'] as $item) { ?>
                        <li>
                            <span><?=$item->name?></span>
                            <?php if(!empty($item->explain_name)) {?>
                            <span class="explain-name">(<?=$item->explain_name?>)</span>
                            <?php } ?>
                            <span><?=$item->price?></span>
                        </li>
                        <?php } ?>
                    </ul>
                    <img src="<?=CDN?>/themes/tinypost/images/arrow_down.png" alt="arrow-down" class="arrow-down">
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>