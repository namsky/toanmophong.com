<?php
	if(!empty($items)) {
?>
<div class="container pt-5 scroll-sec" id="cafes">
	<div class="section-title mb-5">
		<h2>Địa điểm</h2>
		<img src="<?=CDN?>/themes/tinypost/images/seperate.png" alt="seperate" class="seperate">
	</div>
	<div class="tinypost-slider">
		<?php foreach($items as $item) { ?>
		<div class="tinypost slider-item">
			<img src="<?=CDN?>/themes/tinypost/images/dark_frame.png" alt="dark-frame" width="100%">
			<div class="tinypost-image">
				<img src="<?=$item->thumb?>" alt="tiny-post-nguyenkhang">
			</div>
			<div class="tinypost-content text-uppercase">
				<p><?=$item->name?></p>
				<p>add: <?=$item->address?></p>
				<p><?=$item->time?></p>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
<?php } ?>