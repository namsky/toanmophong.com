<?=$this->section('content');?>
	<!-- CONTENT -->
    <main id="container" class="inner clearfix">
        <div class="sub_main clearfix">
            <div class="title_main"><span><?=$item->title?></span></div>
            <div class="content_main">
                <div class="text">
                    <?=$item->content?>
                    <hr />
                    <p style="text-align:center">
                        <span style="color:#ff0000">
                            <span style="line-height:2.0">
                                <span style="font-size:18px">
                                    <span style="font-family:Arial,Helvetica,sans-serif">
                                        <strong style="text-transform: uppercase;"><?=cms_config('company_name');?></strong>
                                    </span>
                                </span>
                            </span>
                        </span>
                    </p>
                    <p style="text-align:center">
                        <span style="color:#2e30d1">
                            <span style="line-height:2.0">
                                <span style="font-size:18px">
                                    <span style="font-family:Arial,Helvetica,sans-serif">
                                        <strong><?=cms_config('address');?></strong>
                                    </span>
                                </span>
                            </span>
                        </span>
                    </p>
                    <p style="text-align:center">
                        <span style="color:#2e30d1">
                            <span style="line-height:2.0">
                                <span style="font-size:18px">
                                    <span style="font-family:Arial,Helvetica,sans-serif">
                                        <strong>Hotline: </strong>
                                    </span>
                                </span>
                            </span>
                        </span>
                        <span style="font-size:20px">
                            <span style="color:#ff0000">
                                <span style="line-height:2.0">
                                    <span style="font-family:Arial,Helvetica,sans-serif">
                                        <strong><?=cms_config('phone');?></strong>
                                    </span>
                                </span>
                            </span>
                        </span>
                    </p>
                    <p style="text-align:center">
                        <span style="color:#2e30d1">
                            <span style="line-height:2.0">
                                <span style="font-size:18px">
                                    <span style="font-family:Arial,Helvetica,sans-serif">
                                        <strong>Email: <?=cms_config('email');?></strong>
                                    </span>
                                </span>
                            </span>
                        </span>
                    </p>
                    <p style="text-align:center">
                        <span style="line-height:2.0">
                            <span style="font-size:18px">
                                <span style="font-family:Arial,Helvetica,sans-serif">
                                    <strong>
                                        <span style="color:#2e30d1">Website: </span>
                                        <a href="http://dothisaigon.com.vn/">
                                            <span style="color:#2e30d1">nhadatnhanhien.com</span>
                                        </a>
                                    </strong>
								</span>
                            </span>
                        </span>
                    </p>
                    <hr/>
                    <!-- AddThis Button BEGIN -->
                    <div class="addthis_toolbox addthis_default_style ">
                        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                        <a class="addthis_button_facebook_share" fb:share:layout="button_count"></a>
                        <a class="addthis_button_tweet"></a>
                        <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                        <a class="addthis_counter addthis_pill_style"></a>
                    </div>
                    <script type="text/javascript">
                        var addthis_config = {
                            "data_track_addressbar": false
                        };
                    </script>
                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51c3fae12493146d"></script>
                    <!-- AddThis Button END -->
                    <div class="fb-comments" data-href="http://dothisaigon.com.vn/trac-dac-cong-trinh-xay-dung-uy-tin" data-width="100%" data-numposts="5"></div>
                </div>
            </div>
        </div>
        <div class="sub_main clearfix">
            <div class="title_news_other">Bài viết khác</div>
            <div class="row_product flex-template-detail">
                <?php
                $args = [];
                $args['limit'] = 4;
                $args['category'] = 1;
                $args['type'] = 'related';
                $args['excludes'] = $item->id;
                echo widget('Tinypost/Box_news', $args);
                ?>
            </div>
            <div class="wrap">
                <div class="auto"></div>
            </div>
        </div>
    </main>
<?=$this->endSection();?>