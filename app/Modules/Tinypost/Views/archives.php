<?=$this->section('content');?>
    <!-- CONTENT -->
    <main id="container" class="inner clearfix">
        <div class="sub_main">
            <div class="title_main"><span><?=$category->title?></span></div>
            <div class="content_main">
                <div class="row flex-template-detail">
                    <?php if(!empty($items) && $category->slug != 'bat-dong-san') {
                        foreach ($items as $key=>$item) { ?>
                            <div class="col_news col-md-3 col-sm-4 col-xs-6">
                                <div class="box_news clearfix">
                                    <a class="box_news_img" href="<?=post_url($item)?>" title="<?=$item->title?>" class="my_glass"><img alt="<?=$item->title?>" src="<?=$item->thumb?>" onError="noImg(this, 300, 260)" class="w100 trans03" /></a>
                                    <div class="right_news">
                                        <h2 class="box_news_name">
                                            <a href="<?=post_url($item)?>" title="<?=$item->title?>"><?=$item->title?></a>
                                        </h2>
                                        <div class="box_news_mota"><?=$item->summary?></div>
                                    </div>
                                </div>
                            </div>
                    <?php }
                    } elseif (!empty($items) && $category->slug == 'bat-dong-san') {
                        foreach ($items as $key=>$item) { ?>
                            <div class="col_news col-md-3 col-sm-4 col-xs-6">
                                <div class="box_news clearfix">
                                    <a class="box_news_img" href="<?=post_url($item)?>" title="<?=$item->title?>" class="my_glass"><img alt="<?=$item->title?>" src="<?=$item->thumb?>" onError="noImg(this, 300, 260)" class="w100 trans03" /></a>
                                    <div class="right_news">
                                        <h2 class="box_news_name">
                                            <a href="<?=post_url($item)?>" title="<?=$item->title?>"><?=$item->title?></a>
                                        </h2>
                                        <div style="padding-top: 7px">
                                            <p><i class="fas fa-dollar-sign"></i></i> Giá: <strong><?=$item->price?> Triệu/m2</strong></p>
                                            <p><i class="fas fa-object-group"></i></i> Diện tích: <strong><?=$item->acreage?> m2</strong></p>
                                            <p><i class="fas fa-map-marker-alt"></i> Địa chỉ: <strong><?=$item->address?></strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    }?>
                    <div class="wrap">
                        <div class="auto"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--end sub main-->
    </main>
<?=$this->endSection();?>