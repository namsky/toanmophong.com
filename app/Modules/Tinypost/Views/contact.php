<?=$this->section('content');?>
    <!-- CONTENT -->
    <main id="container" class="inner clearfix">
        <div class="sub_main">
            <div class="title_main"><span>Liên Hệ</span></div>
            <div class="content_main">
                <div class="col-md-6 col-sm-6 col-xs-12 contact-info">
                    <div class="text">
                        <p><span style="font-size:24px"><?=cms_config('company_name');?></span></p>
                        <p><strong>Địa Chỉ: </strong><?=cms_config('address');?></p>
                        <p><strong>Hotline: </strong><?=cms_config('phone');?></p>
                        <p><strong>Email: </strong><?=cms_config('email');?></p>
                        <p><strong>MST: </strong><?=cms_config('mst');?></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 contact-frm">
                    <form name="frm" id="frm" enctype="multipart/form-data">
                        <div class=" tablelienhe" style="width:100%">
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" id="name" size="50" required="required" placeholder="Họ và tên" /> </div>
                            <!--box input contact-->
                            <div class="form-group">
                                <input name="address" type="text" class="form-control" size="50" id="diachi" required="required" placeholder="Địa chỉ" /> </div>
                            <!--box input contact-->
                            <div class="form-group">
                                <input name="phone" type="text" class="form-control" pattern="^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$" id="dienthoai" size="50" required="required" placeholder="Điện thoại" /> </div>
                            <!--box input contact-->
                            <div class="form-group">
                                <input name="email" type="email" class="form-control" size="50" id="email" required="required" placeholder="Email" /> </div>
                            <!--box input contact-->
                            <div class="form-group">
                                <textarea id="content" name="content" cols="50" rows="7" class="form-control" style="height:150px;" placeholder="Nội dung"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Gửi" class="btn btn-success">
                                <input type="reset" value="Làm mới" class="btn btn-default"> </div>
                            <!--box input contact-->
                        </div>
                        <!--end table lien he-->
                    </form>
                </div>
                <div class="clear"></div>
                <div class="contain_map_lienhe">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d244.99729602343078!2d106.61508015634372!3d10.737817915140923!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752dc3f601fe7b%3A0xb2ba51aecd6e9a22!2zQ8O0bmcgVHkgVE5ISCBUaGnhur90IEvhur8gLSBYw6J5IEThu7FuZyAtIMSQbyDEkOG6oWMgxJDDtCBUaOG7iyBTw6BpIEfDsm4!5e0!3m2!1svi!2s!4v1604218782003!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            <!--content main-->
        </div>
        <!--end sub main-->
    </main>
    <script>
        $("#frm").submit(function(e) {
            console.log('nam');
            e.preventDefault();
            var form = $(this);
            $.ajax({
                type: "POST",
                url: '<?=URL?>/api/contact/form',
                data: form.serialize(),
                success: function(data)
                {
                    if(data.status == 'success') {
                        swal("Thành công!", data.message, "success");
                        $("#name").val('');
                        $("#diachi").val('');
                        $("#dienthoai").val('');
                        $("#email").val('');
                        $("#content").val('');
                    } else swal("Thất bại!", data.message, "error");
                }
            });
        });
    </script>
<?=$this->endSection();?>