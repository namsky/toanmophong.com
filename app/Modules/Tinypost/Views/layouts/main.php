<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=isset($title)?$title:cms_config('site_name');?></title>
    <link rel="icon" href="<?=cms_config('site_favicon');?>" sizes="32x32" />
	<meta name="description" content="<?=isset($description)?$description:cms_config('site_description');?>">
	<meta name="keywords" content="<?=isset($keywords)?$keywords:cms_config('site_keywords');?>">
	<link rel="alternate" href="" hreflang="vi-vn" />
	<meta property="og:image" content="<?=cms_config('image_social');?>">
	<meta property="og:site_name" content="<?=cms_config('site_title');?>">
	<meta property="og:type" content="article">
	<meta property="og:locale" content="vi_VN">
	<meta property="fb:app_id" content="">
	<meta property="fb:pages" content="">
	<meta property="og:title" content="<?=isset($title)?$title:cms_config('site_name');?>">
	<meta property="og:url" content="<?=current_url();?>">
	<meta property="og:description" content="<?=isset($description)?$description:cms_config('site_desc');?>">
	<?=load_css('all.min.css', 'themes/nhadatnhanhien'); ?>
	<?=load_css('bootstrap.min.css', 'themes/nhadatnhanhien'); ?>
	<?=load_css('cart.css', 'themes/nhadatnhanhien'); ?>
	<?=load_css('fonts.css', 'themes/nhadatnhanhien'); ?>
	<?=load_css('jquery.mmenu.all.css', 'themes/nhadatnhanhien'); ?>
	<?=load_css('jquery.simplyscroll.css', 'themes/nhadatnhanhien'); ?>
	<?=load_css('owl.carousel.min.css', 'themes/nhadatnhanhien'); ?>
	<?=load_css('owl.theme.default.min.css', 'themes/nhadatnhanhien'); ?>
	<?=load_css('phantrang.css', 'themes/nhadatnhanhien'); ?>
	<?=load_css('style.css', 'themes/nhadatnhanhien'); ?>
	<?=load_css('style_media.css', 'themes/nhadatnhanhien'); ?>
	<?php $cms->show_asset('css'); ?>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@400;500;600;700&display=swap" rel="stylesheet">
	<script>
		var base_url = "<?=URL?>";
	</script>
    <?=load_js('jquery-1.9.1.min.js', 'themes/nhadatnhanhien'); ?>
</head>

<body>
<!-- Messenger Plugin chat Code -->
<div id="fb-root"></div>

<!-- Your Plugin chat code -->
<div id="fb-customer-chat" class="fb-customerchat">
</div>

<script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "104040198824734");
    chatbox.setAttribute("attribution", "biz_inbox");
</script>

<!-- Your SDK code -->
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v12.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script>
    function noImg(that, width, height) {
        width = width || 200;
        height = height || 200;
        that.removeAttribute('onerror');
        that.removeEventListener('error', noImg);
        //return that.src= "thumb/" + width + "x" + height + "/1/<?=CDN?>/themes/nhadatnhanhien/images/noimage.png";
        return that.src = "https://via.placeholder.com/" + width + "x" + height;
    }
</script>
    <div id="fb-root"></div>
    <div id="full">
        <div id="wrapper">
            <header id="header" class="clearfix lazy">
                <div id="banner">
                    <div class="inner">
                        <h1><?=cms_config('company_name');?></h1>
                        <h2><?=cms_config('company_name');?></h2>
                        <h3><?=cms_config('company_name');?></h3>

                        <div class="banner-flex">
                            <div class="logo">
                                <a href="/"><img src="<?=cms_config('site_logo');?>" alt="<?=cms_config('company_name');?>" class="mw100" /></a>
                            </div>
                            <div class="company"><img src="<?=cms_config('banner_top');?>" alt="<?=cms_config('company_name');?>" class="mw100" /></div>
                            <div class="header-hotline"><span class="header-hotline__number">0907.446.447</span></div>
                        </div>
                    </div>
                </div>
            </header>
            <?php echo widget('Cms/Main_menu'); ?>
            <?=$this->renderSection('content')?>
            <footer>
                <div id="footer" class="clearfix lazy">
                    <div class="content_footer_full">
                        <div class="inner">
                            <div class="row">
                                <div class="item_footer col-md-5 col-sm-12 col-xs-12">
                                    <div class="name_company"><?=cms_config('company_name');?></div>
                                    <div class="content_footer"><p><strong>Địa Chỉ:</strong>&nbsp;<?=cms_config('address')?></p>

                                        <p><strong>Hotline:&nbsp;&nbsp;</strong><?=cms_config('phone');?></p>

                                        <p><strong>Email:&nbsp;</strong><?=cms_config('email');?>&nbsp</p>

                                        <p><strong>MST: </strong><?=cms_config('mst');?></p>
                                    </div>
                                </div>
                                <div class="item_footer col-md-4 col-sm-6 col-xs-12">
                                    <div class="title_footer">Chính sách công ty</div>
                                    <ul class="footer-ul">
                                    </ul>
                                    <div class="footer-letter">
                                        <div class="footer-letter__title">
                                            <div class="footer-letter__title-name">Đăng ký nhận tin</div>
                                            <div class="footer-letter__title-slogan">Đăng ký ngay với chúng tôi để nhận thông báo</div>
                                        </div>
                                        <form name="frmPhone" id="frmPhone" class="dangkymail footer-letter__frm">
                                            <input name="phone" id="number_phone" type="text" class="footer-letter__input" placeholder="Nhập sđt của bạn . " required="required">
                                            <button type="submit" class="footer-letter__btn" value="">Gửi</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="item_footer col-md-3 col-sm-6 col-xs-12 fanpage-box">
                                    <div class="title_footer">Fanpage</div>
                                    <div class="fb-page" data-href="https://www.facebook.com/dodacdiachinhnhanhien" data-tabs="timeline" data-width="500" data-height="210" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                        <blockquote cite="https://www.facebook.com/dodacdiachinhnhanhien" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/dodacdiachinhnhanhien">Facebook</a></blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="bottom" class="clearfix">
                        <div class="inner">
                            <div class="copyright">Copyright &copy; 2021 <?=cms_config('company_name');?>. Web design by <a style="font-weight: bold" href="https://net5s.vn">Net5s.vn</a></div>
<!--                            <div class="counter">-->
<!--                                <span>Online: 2</span>-->
<!--                                <span>Tuần: 78</span>-->
<!--                                <span>Tháng 5272</span>-->
<!--                                <span>Tổng truy cập: 30427</span>-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>

                <div class="footer-map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4792.858708023634!2d106.0816228152578!3d10.065862674657449!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a073df3561a53f%3A0xf8fe382a06b115a5!2zQ8O0bmcgdHkgVE5ISCBNVFYgxJBvIMSQ4bqhYyDEkOG7i2EgQ2jDrW5oIE5ow6JuIEhp4buBbg!5e1!3m2!1svi!2s!4v1641369248463!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>

            </footer>
            <?php echo widget('Cms/Footer_menu'); ?>
            <style>
                .toolbar2 {background: #fff; display: none; padding: 5px; bottom: 0; right: 0; left: 0; position: fixed; z-index: 500; height: auto; border-top: 1px solid #cbcbcb; }
                .toolbar2 ul{list-style: none;}
                .toolbar2 ul li {text-align: center; float: left; width: 25%; }
                .toolbar2 ul li a {display: inline-block; width: 100%; }
                .toolbar2 ul li a span {color: #333; font-size: 3.5vw;font-weight: 400; }
                .icon-m {background: url(<?=CDN?>/themes/nhadatnhanhien/images/icon-m.png) no-repeat; display: inline-block; }
                .icon-t1 {background-position: -315px -30px; width: 25px; height: 20px; background-size: 680px; }
                .icon-t2 {background-position: -348px -30px; width: 25px; height: 20px; background-size: 680px; }
                .icon-t3 {background-position: -382px -30px; width: 50px; height: 20px; background-size: 680px; }
                .icon-t4 {background-position: -440px -29px; width: 25px; height: 21px; background-size: 680px; }
                #chatfb .fas{background: none;color: #616161;font-size: 17px;margin-bottom: 5px;padding-top: 2px}
                @media screen and (min-width: 500px){
                    .toolbar2 ul li a span{font-size: 15px;}
                }

                @media screen and (max-width: 960px) {
                    .toolbar2 {
                        display: block;
                    }
                }
            </style>
            <div class="toolbar2">
            <ul>
                <li><a id="goidien" href="tel:0907446477" title="title"><i class="icon-m icon-t1"></i><br><span>Gọi điện</span></a>
                </li>
                <li><a id="nhantin" href="sms:0907446477" title="title"><i class="fas fa-map-o icon-m icon-t2"></i><br><span>Nhắn tin</span></a>
                </li>
                <li><a id="chatzalo" href="https://zalo.me/<?=cms_config('phone');?>" title="title"><i class="icon-m icon-t3"></i><br><span>Chat zalo</span></a>
                </li>
                <li><a id="chatfb" href="https://goo.gl/maps/TAxZtyUpWp537D5y7" title="title"><i class="fas fa-map-marker-alt"></i><br><span>Chỉ đường</span></a>
                </li>
            </ul>
        </div>
        </div><!-- #wrapper -->
    </div><!-- #full -->
	<?=load_js('modernizr-3.7.1.min.js', 'themes/nhadatnhanhien'); ?>
	<?=load_js('bootstrap.min.js', 'themes/nhadatnhanhien'); ?>
	<?=load_js('lazyload.min.js', 'themes/nhadatnhanhien'); ?>
	<?=load_js('owl.carousel.min.js', 'themes/nhadatnhanhien'); ?>
	<?=load_js('script_menu_top.js', 'themes/nhadatnhanhien'); ?>
	<?=load_js('jquery.simplyscroll.js', 'themes/nhadatnhanhien'); ?>
	<?=load_js('jquery.mmenu.js', 'themes/nhadatnhanhien'); ?>
	<?=load_js('script_menu_top.js', 'themes/nhadatnhanhien'); ?>
	<?php $cms->show_asset('js'); ?>
    <script src="https://www.google.com/recaptcha/api.js?render=6LdwTJkUAAAAANBf_AzZ7uowFtjU-Bq2kXRibflQ"></script>

    <script>
        $("#frmPhone").submit(function(e) {
            e.preventDefault();
            var form = $(this);
            $.ajax({
                type: "POST",
                url: '<?=URL?>/api/contact_phone/form',
                data: form.serialize(),
                success: function(data)
                {
                    if(data.status == 'success') {
                        swal("Thành công!", data.message, "success");
                        $("#number_phone").val('');
                    } else swal("Thất bại!", data.message, "error");
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('body').append('<div id="top" ><img src="<?=CDN?>/themes/nhadatnhanhien/images/top.png" alt="top"/></div>');
            $(window).scroll(function() {
                if($(window).scrollTop() > 100) {
                    $('#top').fadeIn();
                } else {
                    $('#top').fadeOut();
                }
            });
            $('#top').click(function() {
                $('html, body').animate({scrollTop:0},500);
            });


        });
    </script>

    <!-- lazyload init -->
    <script>
        if (typeof LazyLoad != 'undefined') {
            var myLazyLoad = new LazyLoad({
                elements_selector: ".lazy"
            });
        }
    </script>

    <!-- menu bar fixed -->
    <script>
        $(window).scroll(function(event) {
            if ($(window).scrollTop() > $('#header').height()) {
                $('#menu').addClass('fixed');
            } else {
                $('#menu').removeClass('fixed');
            }
        });
    </script>

    <!-- mmenu init -->
    <script>
        if ($.fn.mmenu) {
            $("#menu_bootstrap").mmenu({
                "extensions": [
                    "pagedim-black"
                ]
            });
            var api_mmenu = $("#menu_bootstrap").data('mmenu');
            api_mmenu.bind('opened', function() {
                $('#btn_menu_bootstrap').addClass('move_btn_bootstrap');
            });
            api_mmenu.bind('closed', function() {
                $('#btn_menu_bootstrap').removeClass('move_btn_bootstrap');
            });
        }
    </script>

    <!--  fancybox init -->
    <script>
        if ($.fn.fancybox) {
            // popup init
            $('#popup_fancy').exists(function() {
                $.fancybox.open({
                    src: '#popup_fancy',
                    type: 'inline',

                });
            })

        }
    </script>

    <!-- simplyscroll init -->
    <script>
        $(document).ready(function() {
            if ($.fn.simplyScroll) {
                $(".news-scroll").simplyScroll({
                    orientation: 'vertical',
                    customClass: 'vert'
                });
                // $("#owl_video").simplyScroll({
                //     orientation: 'vertical',
                //     customClass: 'vert'
                // });
            }
        })
    </script>

    <!-- slick init -->
    <script>
        if ($.fn.slick) {
            $('.product-slick').slick({
                centerMode: true,
                infinite: true,
                centerPadding: '0',
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                dots: false
            })
        }
    </script>

    <!-- owlcarousel init -->
    <script>
        $(document).ready(function() {
            if ($.fn.owlCarousel) {
                var owl = $("#owl_img_detail");
                owl.owlCarousel({
                    rtl: false,
                    loop: false,
                    margin: 1,
                    dots: false,
                    nav: false,
                    responsive: {
                        0: {
                            items: 4
                        },
                        600: {
                            items: 5
                        },
                        1000: {
                            items: 6
                        }
                    }
                });
                $(".next_sub_detail").click(function() {
                    owl.trigger('next.owl');
                });
                $(".prev_sub_detail").click(function() {
                    owl.trigger('prev.owl');
                });

                $('#slider').owlCarousel({
                    rtl: false,
                    loop: false,
                    margin: 0,
                    animateOut: 'fadeOut',
                    dots: false,
                    nav: true,
                    navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
                    rewind: true,
                    lazyLoad: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    items: 1
                });

                $('#owl_video').owlCarousel({
                    rtl: false,
                    loop: false,
                    margin: 10,
                    nav: false,
                    dots: false,
                    rewind: true,
                    lazyLoad: true,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: true,
                    items: 3
                });

                $('#news_owl').owlCarousel({
                    rtl: false,
                    loop: false,
                    margin: 45,
                    nav: false,
                    dots: false,
                    rewind: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        1000: {
                            items: 3
                        }
                    }
                });
                $('#product_owl').owlCarousel({
                    rtl: false,
                    loop: false,
                    margin: 30,
                    dots:false,
                    nav: false,
                    rewind: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    responsive: {
                        0: {
                            margin: 15,
                            items: 2
                        },
                        500: {
                            margin: 20,
                            items: 2
                        },
                        768: {
                            margin: 20,
                            items: 3
                        },
                        1000: {
                            margin: 20,
                            items: 4
                        },
                        1028: {
                            items: 4
                        }
                    }
                });

                $('.service-owl').owlCarousel({
                    rtl: false,
                    loop: false,
                    margin: 20,
                    dots:false,
                    nav: false,
                    rewind: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    responsive: {
                        0: {
                            margin: 15,
                            items: 2
                        },
                        500: {
                            margin: 20,
                            items: 2
                        },
                        768: {
                            margin: 20,
                            items: 3
                        },
                        1000: {
                            margin: 20,
                            items: 4
                        },
                        1028: {
                            items: 4
                        }
                    }
                });

                $('.adving-owl').owlCarousel({
                    rtl: false,
                    loop: false,
                    margin: 20,
                    dots:false,
                    nav: false,
                    rewind: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    items: 1
                });
            }
        })
    </script>

    <script>
        function flyToElement(flyer, flyingTo) {
            var $func = $(this);
            var flyerClone = $(flyer).clone();
            $(flyerClone).css({
                position: 'absolute',
                top: $(flyer).offset().top + "px",
                left: $(flyer).offset().left + "px",
                opacity: 1,
                'z-index': 1000
            }).appendTo($('body'));
            var gotoX = $(flyingTo).offset().left;
            var gotoY = $(flyingTo).offset().top;
            $(flyerClone).animate({
                    opacity: 0.4,
                    left: gotoX,
                    top: gotoY,
                    width: $(flyingTo).width(),
                    height: $(flyingTo).height()
                }, 700,
                function() {
                    $(flyerClone).remove();
                });
        }

        // function addtocart(pid, sl, action) {
        //     $.ajax({
        //         url: 'ajax/add_giohang.php',
        //         type: 'POST',
        //         dataType: 'json',
        //         data: {
        //             pid: pid,
        //             sl: sl
        //         },
        //     })
        //         .done(function(result) {
        //             if (action == 1) {
        //                 window.location.href = 'gio-hang';
        //             } else {
        //                 flyToElement(result.img, $('#numcart'));
        //                 $('#numcart').text(result.sl);
        //             }
        //         })
        //         .fail(function() {
        //             console.log("error");
        //         });
        // }
    </script>

    <!--end-->

<!--    <script>-->
<!--        var fired = false;-->
<!--        window.addEventListener("scroll", function() {-->
<!--            if ((document.documentElement.scrollTop != 0 && fired === false) || (document.body.scrollTop != 0 && fired === false)) {-->
<!--                (function(d, s, id) {-->
<!--                    var js, fjs = d.getElementsByTagName(s)[0];-->
<!--                    if (d.getElementById(id)) return;-->
<!--                    js = d.createElement(s);-->
<!--                    js.id = id;-->
<!--                    js.async = true;-->
<!--                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10";-->
<!--                    fjs.parentNode.insertBefore(js, fjs);-->
<!--                }(document, 'script', 'facebook-jssdk'));-->
<!---->
<!--                fired = true;-->
<!--            }-->
<!--        }, true);-->
<!--    </script>-->

<!--    <div class="js-facebook-messenger-box onApp rotate bottom-right cfm rubberBand animated" data-anim="rubberBand">-->
<!--        <svg id="fb-msng-icon" data-name="messenger icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.47 30.66"><path d="M29.56,14.34c-8.41,0-15.23,6.35-15.23,14.19A13.83,13.83,0,0,0,20,39.59V45l5.19-2.86a16.27,16.27,0,0,0,4.37.59c8.41,0,15.23-6.35,15.23-14.19S38,14.34,29.56,14.34Zm1.51,19.11-3.88-4.16-7.57,4.16,8.33-8.89,4,4.16,7.48-4.16Z" transform="translate(-14.32 -14.34)" style="fill:#fff"></path></svg>-->
<!--        <svg id="close-icon" data-name="close icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39.98 39.99"><path d="M48.88,11.14a3.87,3.87,0,0,0-5.44,0L30,24.58,16.58,11.14a3.84,3.84,0,1,0-5.44,5.44L24.58,30,11.14,43.45a3.87,3.87,0,0,0,0,5.44,3.84,3.84,0,0,0,5.44,0L30,35.45,43.45,48.88a3.84,3.84,0,0,0,5.44,0,3.87,3.87,0,0,0,0-5.44L35.45,30,48.88,16.58A3.87,3.87,0,0,0,48.88,11.14Z" transform="translate(-10.02 -10.02)" style="fill:#fff"></path></svg>-->
<!--    </div>-->
<!--    <div class="js-facebook-messenger-container">-->
<!--        <div class="js-facebook-messenger-top-header"><span>Hotline tư vấn miễn phí: 0907.446.477</span></div>-->
<!--        <div class="fb-page" data-tabs="messages" data-href="https://www.facebook.com/dodacdiachinhnhanhien" data-width="320" data-height="350" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>-->
<!--    </div>-->
<!--    <script>-->
<!--        jQuery(document).ready(function(){jQuery(".js-facebook-messenger-box").on("click",function(){jQuery(".js-facebook-messenger-box, .js-facebook-messenger-container").toggleClass("open"),jQuery(".js-facebook-messenger-tooltip").length&&jQuery(".js-facebook-messenger-tooltip").toggle()}),jQuery(".js-facebook-messenger-box").hasClass("cfm")&&setTimeout(function(){jQuery(".js-facebook-messenger-box").addClass("rubberBand animated")},3500),jQuery(".js-facebook-messenger-tooltip").length&&(jQuery(".js-facebook-messenger-tooltip").hasClass("fixed")?jQuery(".js-facebook-messenger-tooltip").show():jQuery(".js-facebook-messenger-box").on("hover",function(){jQuery(".js-facebook-messenger-tooltip").show()}),jQuery(".js-facebook-messenger-close-tooltip").on("click",function(){jQuery(".js-facebook-messenger-tooltip").addClass("closed")}))});-->
<!--    </script>-->
    <a id="btn-zalo" href="https://zalo.me/<?=cms_config('phone');?>" target="_blank">
        <div class="animated infinite zoomIn kenit-alo-circle"></div>
        <div class="animated infinite pulse kenit-alo-circle-fill"></div>
        <i><img src="<?=CDN?>/themes/nhadatnhanhien/images/zalo.png" class="w100" alt="Zalo"></i>
    </a>

</body>

</html>