<?=$this->section('content');?>
    <?php echo widget('Tinypost/Sliders'); ?>
    <main id="container" class=" clearfix">
        <div class="index-box about-box">
            <div class="inner clearfix">
                <div class="about-content">
                    <div class="about-title">
                        <div class="about-title__slogan">Về chúng tôi</div>
                        <div class="about-title__name text-uppercase"><?=cms_config('company_name');?></div>
                    </div>
                    <div class="about-des">GIÁ TRỊ TÍCH LŨY NIỀM TIN.<br />
                        <br />
                        Hiện nay, các công ty tư vấn thiết kế xây dựng và đo đạc tại Tỉnh Vĩnh Long đang ngày càng phát triển mạnh mẽ theo xu hướng của xã hội, đáp ứng nhu cầu kiến tạo không gian sống sang trọng, đẹp, lý tưởng và hoàn thiện, giúp mọi người an cư lạc nghiệp.Nhìn thấy nhu cầu đó chúng tôi <?=cms_config('company_name');?> được ra đời.</div>
                    <a href="/gioi-thieu" class="about-seemore">Xem thêm</a>
                </div>
                <div class="about-img">
                    <a href="gioi-thieu.1"><img src="<?=CDN?>/themes/nhadatnhanhien/images/imgabout-2716.png" alt="<?=cms_config('company_name');?>"></a>
                </div>
            </div>
        </div>
        <div class="index-box dodac-box">
            <div class="inner clearfix">
                <div class="title_main">
                    <span>Đo đạc Nhân Hiền</span>
                    <p class="title_main__slogan">Chúng tôi đồng hành cùng công trình của bạn</p>
                </div>
                <?php echo widget('Tinypost/Do_dac_do_thi'); ?>
            </div>
        </div>
        <div class="index-box">
            <div class="inner clearfix">
                <div class="title_main">
                    <span>Dịch vụ của chúng tôi</span>
                    <p class="title_main__slogan">NHÂN HIỀN - GIÁ TRỊ TÍCH LŨY NIỀM TIN.</p>
                </div>
                <?php echo widget('Tinypost/Dich_vu'); ?>
            </div>
        </div>
        <div class="owl-carousel adving-owl">
            <a href="" class="hover-glass"><img src="<?=CDN?>/themes/nhadatnhanhien/images/519418092729858vi_1366x400.jpg" alt=""></a>
        </div>
        <div class="index-box">
            <div class="inner clearfix">
                <div class="news-box">
                    <div class="title_main">
                        <span class="text-left">Tin tức mới</span>
                        <p class="text-left">NHÂN HIỀN - GIÁ TRỊ TÍCH LŨY NIỀM TIN.</p>
                    </div>
                    <div class="news-scroll">
                        <?php echo widget('Tinypost/News'); ?>
                    </div>
                </div>
                <div class="video-box">
                    <div class="title_main">
                        <span class="text-left">Liên hệ vứi chúng tôi</span>
                        <p class="text-left">NHÂN HIỀN - GIÁ TRỊ TÍCH LŨY NIỀM TIN.</p>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 contact-frm">
                        <form name="frm" id="frm" enctype="multipart/form-data">
                            <div class=" tablelienhe" style="width:100%">
                                <div class="form-group">
                                    <input name="name" type="text" class="form-control" id="name" size="50" required="required" placeholder="Họ và tên" /> </div>
                                <!--box input contact-->
                                <div class="form-group">
                                    <input name="address" type="text" class="form-control" size="50" id="diachi" required="required" placeholder="Địa chỉ" /> </div>
                                <!--box input contact-->
                                <div class="form-group">
                                    <input name="phone" type="text" class="form-control" pattern="^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$" id="dienthoai" size="50" required="required" placeholder="Điện thoại" /> </div>
                                <!--box input contact-->
                                <div class="form-group">
                                    <input name="email" type="email" class="form-control" size="50" id="email" required="required" placeholder="Email" /> </div>
                                <!--box input contact-->
                                <div class="form-group">
                                    <textarea id="content" name="content" cols="50" rows="7" class="form-control" style="height:150px;" placeholder="Nội dung"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Gửi" class="btn btn-success">
                                    <input type="reset" value="Làm mới" class="btn btn-default"> </div>
                                <!--box input contact-->
                            </div>
                            <!--end table lien he-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        $("#frm").submit(function(e) {
            console.log('nam');
            e.preventDefault();
            var form = $(this);
            $.ajax({
                type: "POST",
                url: '<?=URL?>/api/contact/form',
                data: form.serialize(),
                success: function(data)
                {
                    if(data.status == 'success') {
                        swal("Thành công!", data.message, "success");
                        $("#name").val('');
                        $("#diachi").val('');
                        $("#dienthoai").val('');
                        $("#email").val('');
                        $("#content").val('');
                    } else swal("Thất bại!", data.message, "error");
                }
            });
        });
    </script>
<?=$this->endSection();?>