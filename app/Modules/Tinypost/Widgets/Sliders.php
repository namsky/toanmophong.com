<?php
namespace App\Modules\Tinypost\Widgets;
use App\Core\Cms\CmsWidget;

class Sliders extends CmsWidget
{
    function index() {
		/* Set box title */
		/* Load from cache */
		$cached = cms_config('cache');
		$sliders = false;
		if(!$sliders) {
			/* Load from db */
			$model = model('App\Modules\Cms\Models\SliderModel');
			$sliders = $model->where('status', 1)->orderBy('id', 'ASC')->findAll();
		}
		$this->view->setVar('sliders', $sliders);
		return $this->view->render('sliders');
    }
}