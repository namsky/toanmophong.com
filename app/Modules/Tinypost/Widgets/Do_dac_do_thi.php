<?php
namespace App\Modules\Tinypost\Widgets;
use App\Core\Cms\CmsWidget;

class Do_dac_do_thi extends CmsWidget
{
    function index() {
        /* Set box title */
        /* Load from cache */
        $cached = cms_config('cache');
        $items = false;
        $model = model('App\Modules\Cms\Models\PostModel');
        if(!$items) {
            /* Load from db */

            $items['main'] = false;
            $items['main'] = $model->where('status', 1)->where('hot', 0)->orderBy('published', 'ASC')->categories([27])->findAll(4, 0);
            $items['hot'] = false;
            $items['hot'] = $model->where('status', 1)->where('hot', 1)->orderBy('published', 'ASC')->categories([27])->findAll(1, 0);
        }


        $this->view->setVar('items', $items);
        return $this->view->render('do_dac_do_thi');
    }
}