<?php
namespace App\Modules\Tinypost\Widgets;
use App\Core\Cms\CmsWidget;

class News extends CmsWidget
{
    function index() {
		/* Set box title */
		/* Load from cache */
		$cached = cms_config('cache');
		$items = false;
		$model = model('App\Modules\Cms\Models\PostModel');
		if(!$items) {
			/* Load from db */
			$items = false;
			$items = $model->where('status', 1)->orderBy('published', 'ASC')->categories([1])->findAll();
		}
			
		$this->view->setVar('items', $items);
		return $this->view->render('news');
    }
}