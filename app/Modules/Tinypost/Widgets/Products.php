<?php
namespace App\Modules\Tinypost\Widgets;
use App\Core\Cms\CmsWidget;

class Products extends CmsWidget
{
    function index() {
		/* Set box title */
		/* Load from cache */
		$cached = cms_config('cache');
		$items = false;
		$model = model('App\Modules\Cms\Models\ProductModel');
		if(!$items) {
			/* Load from db */
			$items['main'] = false;
			$items['main'] = $model->where('status', 1)->orderBy('id', 'ASC')->findAll();
			$items['hot'] = false;
			$items['hot'] = $model->where('status', 1)->where('hot', 1)->orderBy('id', 'ASC')->findAll();
		}
			
		$this->view->setVar('items', $items);
		return $this->view->render('products');
    }
}