<?php
namespace App\Modules\Tinypost\Widgets;
use App\Core\Cms\CmsWidget;

class Cafes extends CmsWidget
{
    function index($args=[]) {
		/* Set box title */
		/* Load from cache */
		$cached = cms_config('cache');
		$items = false;
		if(!$items) {
			/* Load from db */
			$model = model('App\Modules\Cms\Models\CafeModel');
			$items = $model->where('status', 1)->orderBy('order', 'ASC')->findAll();
		}
		$this->view->setVar('items', $items);
		if(isset($args['position'])) {
			return $this->view->render('cafes_'.$args['position']);
		}
    }
}