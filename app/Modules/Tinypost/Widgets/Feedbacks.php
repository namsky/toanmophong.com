<?php
namespace App\Modules\Tinypost\Widgets;
use App\Core\Cms\CmsWidget;

class Feedbacks extends CmsWidget
{
    function index() {
		/* Set box title */
		/* Load from cache */
		$cached = cms_config('cache');
		$items = false;
		if(!$items) {
			/* Load from db */
			$model = model('App\Modules\Cms\Models\FeedbackModel');
			$items = $model->orderBy('id', 'ASC')->findAll();
		}
		$this->view->setVar('items', $items);
		return $this->view->render('feedbacks');
    }
}