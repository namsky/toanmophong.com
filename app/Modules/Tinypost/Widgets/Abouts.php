<?php
namespace App\Modules\Tinypost\Widgets;
use App\Core\Cms\CmsWidget;

class Abouts extends CmsWidget
{
    function index() {
		/* Set box title */
		/* Load from cache */
		$cached = cms_config('cache');
		$items = false;
		if(!$items) {
			/* Load from db */
			$model = model('App\Modules\Cms\Models\AboutModel');
			$items = $model->orderBy('id', 'ASC')->findAll();
		}
		$this->view->setVar('items', $items);
		return $this->view->render('abouts');
    }
}