<?php
namespace App\Modules\NghienCode\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Core\Cms\CmsController;

class Account extends CmsController
{
	public function __construct()
	{
	}
	public function index()
	{
        echo 'Account';
	}
	public function profile()
	{
        echo 'profile';
	}
	public function messages()
	{
        echo 'messages';
	}
	public function posts()
	{
        echo 'posts';
	}
	public function confirm_email()
	{
        $code = $this->request->getGet('code');
        $verify = false;
        if($code) {
            $userModel = model('App\Modules\Cms\Models\UserModel');
            $user = $userModel->where('activate_code', $code)->first();
            if(!empty($user)) {
                $user->activate_code = NULL;
                $user->group_id = 2;
                $userModel->save($user);
                $verify = true;
            }
        }
		$this->view->setVar('verify', $verify);
		$this->view->setVar('title', 'Xác minh email - '.cms_config('site_name'));
		$this->view->setVar('description', cms_config('site_description'));
		$this->view->setVar('keywords', cms_config('site_keywords'));
		
        $this->cms->load_vendors([
            'fontawesome',
            'jquery',
            'bootstrap',
            'sweetalert2',
            'jquery.lazyload',
        ]);
		return $this->view->render('account/confirm_email');
	}
	public function recover_password()
	{
        echo 'recover_password';
	}
}