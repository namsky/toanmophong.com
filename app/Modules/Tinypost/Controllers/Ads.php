<?php
namespace App\Modules\NghienCode\Controllers;
use App\Core\Cms\CmsController;

class Ads extends CmsController
{
	public function index()
	{
	}
	public function click($banner_id)
	{
		$zone_id = intval($this->request->getGet('zone_id'));
		$bannerModel = model('App\Modules\Cms\Models\AdsBannerModel');
		$adsDataModel = model('App\Modules\Cms\Models\AdsDataModel');
		/* Load from cache */
		$cached = cms_config('cache');
		$banner = $cached?$this->cache->get('ads_banner_'.$banner_id):false;
		if(!$banner) {
			/* Load from db */
			$banner = $bannerModel->find($banner_id);
			$this->cache->save('ads_banner_'.$banner_id, $banner, 86400);
		}
		if(!empty($banner)) {
			if($zone_id) {
				$ads_data = $adsDataModel->where('type', 0)->where('zone_id', $zone_id)->where('banner_id', $banner_id)->where('created', strtotime(date("m/d/Y")))->first();
				$data = [
					'zone_id' => $zone_id,
					'banner_id' => $banner_id,
					'type' => 0,
					'value' => 1,
					'created' => strtotime(date("m/d/Y")),
				];
				if(empty($ads_data->id)) {
					$adsDataModel->insert($data);
				} else {
					$data['value'] = $ads_data->value+1;
					$adsDataModel->update($ads_data->id, $data);
				}
			}
			cms_redirect($banner->link, 302);
		}
	}
}