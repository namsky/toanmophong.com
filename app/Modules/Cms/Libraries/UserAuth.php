<?php
namespace App\Modules\Cms\Libraries;
use CodeIgniter\HTTP\Request;
use CodeIgniter\HTTP\UserAgent;
use App\Modules\Admins\Libraries\GoogleAuthenticator;
use App\Modules\Admins\Libraries\Auth;

class UserAuth extends Auth{
	public function __construct()
	{
        parent::__construct();
		$this->model = model('App\Modules\Cms\Models\UserModel');
		$this->handle = 'cookie';
		$this->prefix = 'user';
	}
}