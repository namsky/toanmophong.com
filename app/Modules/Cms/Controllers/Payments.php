<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Payments extends \App\Modules\Admins\Controllers\AdminController
{
    function get_config() {
        $config = [
            'name' => 'Orders',
            'model' => 'App\Modules\Cms\Models\PaymentsModel',
            'datagrid_options' => [
                'limit_perpage' => 20,
                'orders' => ['created' => 'desc'],
                'bulk_actions' => false,
            ],
            'select_options' => [
                'status' => ['success' => 'Đã thanh toán', 'error' => 'Lỗi'],
            ],
            'columns' => [
                'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
                'id' => ['name' => 'ID', 'class' => 'text-center'],
                'amount' => ['name' => 'Số tiền', 'class' => 'text-center', 'method' => 'number_format'],
                'status' => [
                    'name' => 'Trạng thái',
                    'method' => 'function',
                    'function' => 'status_payment',
                    'class' => 'text-center d-sm-table-cell',
                ],
                'response_message' => ['name' => 'Lý do', 'class' => 'text-center'],
                'created' => [
                    'name' => 'Ngày mua',
                    'method' => 'datetime',
                    'class' => 'text-center d-md-table-cell',
                ],
                'actions' => [
                    'type' => 'actions',
                    'class' => 'text-right',
                ],
            ],
        ];
        return $config;
    }
}