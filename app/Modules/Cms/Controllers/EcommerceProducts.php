<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;
use App\Modules\Admins\Controllers\AdminController;

class EcommerceProducts extends AdminController
{
    function __construct() {
		//parent::__construct();
    }
	function get_config() {
		$config = [
			'name' => 'EcommerceProducts',
			'model' => 'App\Modules\Cms\Models\EcommerceProductModel',
			'datagrid_options' => [
				'limit_perpage' => 12,
				'search_by' => ['name','code'],
				'filter_by' => ['status'],
				'filter_date' => true,
				'orders' => ['id' => 'asc'],
				'bulk_actions' => true,
				'addnew_action' => current_url().'/item/',
			],
			'select_options' => [
				'status' => [1 => 'Active', 0 => 'Inactived'],
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center d-md-table-cell d-none max-width-60'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'priority' => ['name' => 'Thứ tự', 'class' => 'text-center text-success'],
				'thumb' => [
					'name' => 'Thumb',
					'method' => 'template',
					'template' => '<div class="product_thumb"><img src="{$thumb}" /></div>',
					'class' => 'text-center d-lg-table-cell d-none max-width-120'
				],
				'menu_image' => [
					'name' => 'Menu Image',
					'method' => 'template',
					'template' => '<div class="product_thumb"><img src="{$menu_image}" /></div>',
					'class' => 'text-center d-lg-table-cell d-none max-width-120'
				],
				'name' => ['name' => 'Name', 'class' => 'post-title'],
				'category_id' => [
					'name' => 'Type',
					'method' => 'template',
					'template' => '<span style="font-weight: bold;">{$category->name}</span>',
					'class' => 'text-center d-lg-table-cell d-none max-width-120'
				],
				'price' => [
					'name' => 'Price',
					'method' => 'function',
					'function' => 'show_currency',
					'class' => 'text-center d-sm-table-cell d-none max-width-100',
				],
				'status' => [
					'name' => 'Status',
					'method' => 'function',
					'function' => 'update_status|id',
					'class' => 'text-center d-sm-table-cell d-none max-width-100',
				],
				'hot' => [
					'name' => 'Hot',
					'method' => 'function',
					'function' => 'update_hot|id',
					'class' => 'text-center d-sm-table-cell d-none max-width-100',
				],
				'stocks' => [
					'name' => 'Stocks',
					'method' => 'function',
					'function' => 'show_number',
					'class' => 'text-center d-sm-table-cell d-none max-width-100',
				],
				'views' => [
					'name' => 'Views',
					'method' => 'function',
					'function' => 'show_number',
					'class' => 'text-center d-sm-table-cell d-none max-width-100',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right max-width-100',
					'edit' => '/item/',
				],
			],
			'with' => ['category|id,name'],
			'rules' => [
				'required' => ['title', 'slug']
			],
			'record' => [
				'views' => 'post',
			],
		];
		return $config;
	}
	function item() {
        $_relation = model('App\Modules\Cms\Models\EcommerceRelationModel');
        $m_product_data = model('App\Modules\Cms\Models\EcommerceDataModel');
        $_relation->re_index();
		$this->view->setVar('title', 'Edit item');
        $item_id = $this->request->uri->getSegment(4);
		$posts = $this->request->getPost();
//        print_r($posts);die;
		// echo "<pre>";
		// print_r($posts);
		// die();
		if(isset($posts['id'])) {
			$data = $posts;
//            echo "<pre>";
//            print_r($data);die;
            unset($data['id']);
            if(!empty($data['fields'])) {
                $fields = $data['fields'];
                unset($data['fields']);
			}
			if(!isset($data['hot'])) $data['hot'] = 0;
			if(!isset($data['is_adsense'])) $data['is_adsense'] = 0;
			if(!isset($data['is_search'])) $data['is_search'] = 0;
			if(!isset($data['is_comments'])) $data['is_comments'] = 0;
			if(!isset($data['sitemap_priority'])) $data['sitemap_priority'] = 0.5;
			if(!isset($data['tags'])) $data['tags'] = '';
			if(!isset($data['categories'])) $data['categories'] = '';
			if(!isset($data['file_download'])) $data['file_download'] = '';
//			if(!isset($data['sku'])) $data['sku'] = '';
//			if(!isset($data['color'])) $data['color'] = '';
//			if(!isset($data['version'])) $data['version'] = '';
//			if(!isset($data['brand'])) $data['brand'] = '';
			if(!empty($data['per_id']) && is_array($data['per_id'])) {
				$pro_pers = [];
				foreach($data['per_id'] as $key=>$value) {
					if(!empty($data['size'][$key]) && !empty($data['image'][$key])) {
						$pro_pers[$key]['id'] = $value;
						$pro_pers[$key]['sku'] = NULL;
						$pro_pers[$key]['color'] = NULL;
						$pro_pers[$key]['size'] = $data['size'][$key];
						$pro_pers[$key]['stock'] = $data['stock'][$key];
						$pro_pers[$key]['image'] = $data['image'][$key];
					}elseif(!empty($data['sku'][$key]) && !empty($data['size'][$key])){
						$pro_pers[$key]['id'] = $value;
						$pro_pers[$key]['sku'] = $data['sku'][$key];
						$pro_pers[$key]['size'] = $data['size'][$key];
						$pro_pers[$key]['stock'] = $data['stock'][$key];
						$pro_pers[$key]['color'] = NULL;
						$pro_pers[$key]['image'] = NULL;
					}elseif(!empty($data['color'][$key]) && !empty($data['image'][$key])){
						$pro_pers[$key]['id'] = $value;
						$pro_pers[$key]['color'] = $data['color'][$key];
						$pro_pers[$key]['image'] = $data['image'][$key];
						$pro_pers[$key]['sku'] = NULL;
						$pro_pers[$key]['size'] = NULL;
						$pro_pers[$key]['stock'] = NULL;
					}
				}
				unset($data['per_id']);
				unset($data['size']);
				unset($data['stock']);
				unset($data['image']);
			}
		// 	echo "<pre>";
		// print_r($pro_pers);
		// die();
//            $file_upload = $data['file_dowload'];
//            $filepath = WRITEPATH . 'writable/uploads/' . $file_upload->store();
//            $test = ['uploaded_flleinfo' => new File($filepath)];
//            print_r($test);die;

			$data['thumb'] = get_thumb($data);
			// $data['thumb_hover'] = get_thumb($data);
            if(!empty($data['hosted_image']) && $data['hosted_image']) {
                $image = new Images();
                if(cms_config('cdn8')) $host = 'cdn8.net';
                else $host = $_SERVER['SERVER_NAME'];
                if(!empty($data['thumb']) && !strpos($data['thumb'], $host)) {
                    $json = $image->tranload($data['thumb']);
                    if(!empty($json['location'])) $data['thumb'] = $json['location'];
                }
                if(!empty($data['content'])) {
                    preg_match_all('/<img(.*)src=\"(.*)\"/U', $data['content'], $matches);
                    if(!empty($matches[2])) {
                        foreach($matches[2] as $_image) {
                            if(!strpos($_image, $host)) {
                                $json = $image->tranload($_image);
                                if(!empty($json['location'])) {
                                    $data['content'] = str_replace($_image, $json['location'], $data['content']);
                                }
                            }
                        }
                    }
                }
			}
			if(isset($data['name']) && $data['name'] && isset($data['slug']) && $data['slug']) {
				// Check duplicate item
				if($posts['id']) $is_duplicate = $this->model->where('id<>', $posts['id'])->where('slug', $data['slug'])->countAllResults();
				else $is_duplicate = $this->model->where('slug', $data['slug'])->countAllResults();
				if($is_duplicate) {
					set_message('Item '.$data['slug'].' is existed in database', 'warning');
				}
				else {
					$post_id = 0;
					$redirect = false;
					if($posts['id']) {
						$result = $this->model->update($posts['id'], $data);
						if($result) $post_id = $posts['id'];
					} else {
						$result = $this->model->insert($data);
						if($result) {
							$post_id = $result;
							$redirect = true;
						}
						$post_url = post_url($data);
						if($post_url && cms_config('google_auto_index')) auto_index($post_url);
					}
					if($post_id) {
						$faq_ids = [];
						$faqModel = model('App\Modules\Cms\Models\EcommercePropertiesModel');
						if(!empty($pro_pers)) {
							foreach($pro_pers as $faq) {
								$faq_id = intval($faq['id']);
								unset($faq['id']);
								$faq['product_id'] = $post_id;
								if($faq_id) {
									$faq_ids[] = $faq_id;
									$faqModel->update($faq_id, $faq);
								} else {
									$faq_id = $faqModel->insert($faq);
									if($faq_id) $faq_ids[] = $faq_id;
								}
							}
							
						}
						if(!empty($faq_ids)) $faqModel->where('product_id', $post_id)->whereNotIn('id', $faq_ids)->delete();
						else $faqModel->where('product_id', $post_id)->delete();
						set_message('Save successfully!', 'success');
					} else {
						set_message('Save error!', 'warning');
					}
					if($redirect) cms_redirect(current_url().'/'.$post_id);
				}
			} else {
				set_message('Input error', 'warning');
			}
		}
		if($item_id) {
            $item_id = intval($item_id);
			if(check_permissions('post.deleted', 'function')) {
                $item = $this->model->with('pro_pers')->with('relations')->with('data')->with('user', ['fields'=>'id,username,name'])->with_trashed()->find($item_id);
            }
            else {
                $item = $this->model->with('pro_pers')->with('relations')->with('data')->with('user', ['fields'=>'id,username,name'])->find($item_id);
            }
            if(empty($item)) {
                return \cms_redirect(rebuild_url(2));
            } else {
                $this->view->setVar('item', $item);
            }
        }
		$m_category = model('App\Modules\Cms\Models\EcommerceCategoryModel');
        $categories = $m_category->get_list();
		$this->view->setVar('categories', $categories);

		$m_vendor = model('App\Modules\Cms\Models\EcommerceVendorModel');
        $vendors = $m_vendor->findAll();
        $this->view->setVar('vendors', $vendors);
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'lavalamp',
            'moment',
            'bootstrap-datepicker',
            'bootstrap-datetimepicker',
            'select2',
            'tinymce',
			'dropzone',
        ]);
		$this->view->setVar('validation', $this->validator);
		$this->view->setVar('cms', $this->cms);
        set_referer('backend');
		echo $this->view->render('product');
	}
    public function update_hosted_images()
    {
        $item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, thumb, content')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
                $image = new Images();
                if(cms_config('cdn8')) $host = 'cdn8.net';
                else $host = $_SERVER['SERVER_NAME'];
                if(!empty($item->thumb) && !strpos($item->thumb, $host)) {
                    $json = $image->tranload($item->thumb);
                    if(!empty($json['location'])) $item->thumb = $json['location'];
                }
                if(!empty($item->content)) {
                    preg_match_all('/<img(.*)src=\"(.*)\"/U', $item->content, $matches);
                    if(!empty($matches[2])) {
                        foreach($matches[2] as $_image) {
                            if(!strpos($_image, $host)) {
                                $json = $image->tranload($_image);
                                if(!empty($json['location'])) {
                                    $item->content = str_replace($_image, $json['location'], $item->content);
                                }
                            }
                        }
                    }
                }
                if($item->hasChanged()) {
                    $updated = $this->model->save($item);
                    if($updated) $json = ['status' => "success"];
                }
			}
			$this->render_json($json);
		}
    }
    public function update_status()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, status')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
				$status = ($item->status)?0:1;
				$updated = $this->model->update($item_id, ['status' => $status]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
    }
    public function update_hot()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, hot')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
				$hot = ($item->hot)?0:1;
				$updated = $this->model->update($item_id, ['hot' => $hot]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
    }
    public function search_posts()
    {
		$keyword = $this->request->getGet('term');
		if($keyword) $items = $this->model->where('status', 1)->where('type', 1)->like('title', $keyword)->select('id,title,slug')->findAll(10, 0);
		else $items = $this->model->where('status', 1)->where('type', 1)->select('id,title,slug')->findAll(10, 0);
		$results = [];
		if(is_array($items)) {
			foreach($items as $item) {
				$temp = [];
				$temp['id'] = $item->id;
				$temp['text'] = $item->title;
				$results[] = $temp;
			}
		}
		$json = ['results' => $results];
		$this->render_json($json);
    }
    public function search_pages()
    {
		$keyword = $this->request->getGet('term');
		if($keyword) $items = $this->model->where('status', 1)->where('type', -1)->like('title', $keyword)->select('id,title,slug')->findAll(10, 0);
		else $items = $this->model->where('status', 1)->where('type', -1)->select('id,title,slug')->findAll(10, 0);
		$results = [];
		if(is_array($items)) {
			foreach($items as $item) {
				$temp = [];
				$temp['id'] = $item->id;
				$temp['text'] = $item->title;
				$results[] = $temp;
			}
		}
		$json = ['results' => $results];
		$this->render_json($json);
    }
    public function get_slug()
    {
		$increase = 0;
		$id = intval($this->request->getPost('id'));
		$data = trim($this->request->getPost('data'));
		$raw_slug = $slug = clear_utf8($data);
		$check = $this->model->where('slug', $slug)->where('id<>', $id)->first();
		while(is_object($check)) {
			if($increase > 5) break;
			$increase++;
			$slug = $raw_slug.'.'.$increase;
			$check = $this->model->where('slug', $slug)->first();
		}
		if(!$check) $json = ['status' => true, 'slug' => $slug];
		else $json = ['status' => false];	
		$this->render_json($json);
    }
}