<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Contacts extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Contacts',
			'model' => 'App\Modules\Cms\Models\ContactModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['name','phone','email'],
				'orders' => ['created' => 'desc'],
				'bulk_actions' => true,
			],
			'select_options' => [
				'status' => [0 => 'Chưa liên hệ', 1 => 'Đã liên hệ'],
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => ['name' => 'Tên', 'class' => 'text-center'],
				'phone' => ['name' => 'Số điện thoại', 'class' => 'text-center d-sm-table-cell d-none'],
				'email' => ['name' => 'Email', 'class' => 'text-center d-sm-table-cell d-none'],
				'status' => [
					'name' => 'Trạng thái',
					'method' => 'function',
					'function' => 'update_status|id',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'created' => [
					'name' => 'Ngày nhận',
					'method' => 'datetime',
					'class' => 'text-center d-md-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'rules' => [
				'required' => ['title']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'name' => ['name' => 'Tên', 'only_show' => true],
					'phone' => ['name' => 'Số điện thoại', 'only_show' => true],
					'email' => ['name' => 'Email', 'only_show' => true],
					'message' => ['name' => 'tin nhắn', 'type' => 'textarea', 'only_show' => true],
					'status' => [
						'name' => 'Trạng thái',
						'type' => 'select',
					]
				],
			],
		];
		return $config;
	}
	public function update_status()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, status')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
				$status = ($item->status)?0:1;
				$updated = $this->model->update($item_id, ['status' => $status]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
    }
}