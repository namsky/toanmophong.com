<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class About extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'About',
			'model' => 'App\Modules\Cms\Models\AboutModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['name'],
				'orders' => ['id' => 'ASC'],
				'bulk_actions' => true,
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'title' => ['name' => 'Đề mục', 'class' => 'text-center'],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'rules' => [
				'required' => ['title']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'title' => ['name' => 'Đề mục'],
					'content' => ['name' => 'Nội dung', 'type' => 'editor']
				],
			],
		];
		return $config;
	}
}