<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Orders extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Orders',
			'model' => 'App\Modules\Cms\Models\CustomersModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['name','phone','email'],
				'orders' => ['created' => 'desc'],
				'bulk_actions' => true,
			],
			'select_options' => [
				'status' => [0 => 'Chưa xử lý', 1 => 'Đã hoàn thành'],
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => ['name' => 'Khách hàng', 'class' => 'text-center'],
				'email' => ['name' => 'Email', 'class' => 'text-center'],
				'city' => ['name' => 'Tỉnh / thành', 'class' => 'text-center'],
				'phone' => ['name' => 'Số điện thoại', 'class' => 'text-center d-sm-table-cell d-none'],
				'created' => [
					'name' => 'Ngày đặt',
					'method' => 'datetime',
					'class' => 'text-center d-md-table-cell',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right',
					'edit' => '/item/',
				],
			],
			'rules' => [
				'required' => ['title']
			],
		];
		return $config;
	}
	public function item(){
		$this->view->setVar('title', 'Edit item');
		$item_id = $this->request->uri->getSegment(4);
		$customerModel = model('App\Modules\Cms\Models\CustomersModel') ;
		$orderModel = model('App\Modules\Cms\Models\OrdersModel') ;
		$customer = $customerModel->where('id',$item_id)->first();
		$order = $orderModel->where('customer_id',$customer->id)->findAll();
		$this->view->setVar('customer', $customer);
		$this->view->setVar('order', $order);
		$this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'lavalamp',
            'moment',
            'bootstrap-datepicker',
            'bootstrap-datetimepicker',
            'select2',
            'tinymce',
        ]);
		$this->view->setVar('validation', $this->validator);
		$this->view->setVar('cms', $this->cms);
		echo $this->view->render('order');
	}
	public function update_status()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, status')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
				$status = ($item->status)?0:1;
				$updated = $this->model->update($item_id, ['status' => $status]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
	}
	public function update(){
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id){
			$item = $this->model->select('id, status')->find($item_id);
			if (isset($item->id)) {
				$status = $this->request->getPost('status');
				$update = $this->model->update($item_id,['status'=>$status]);
			}
		}
		return redirect()->to(site_url('/v-manager/orders'));
	}
}