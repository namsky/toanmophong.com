<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Ads_banners extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'ADS Banner',
			'model' => 'App\Modules\Cms\Models\AdsBannerModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['name','width','height'],
				'orders' => ['id' => 'desc'],
				'bulk_actions' => true,
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => ['name' => 'Name', 'class' => 'text-center'],
				'width' => ['name' => 'Width', 'class' => 'text-center d-sm-table-cell d-none'],
				'height' => ['name' => 'Height', 'class' => 'text-center d-sm-table-cell d-none'],
				'clicks' => ['name' => 'Clicks', 'class' => 'text-center d-sm-table-cell d-none'],
				'views' => ['name' => 'Views', 'class' => 'text-center d-sm-table-cell d-none'],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'rules' => [
				'required' => ['name']
			],
			'record' => [
				'colums' => 6,
				'fields' => [
					'name' => ['name' => 'Name', 'colums' => 8],
					'responsive' => ['name' => 'Responsive', 'type' => 'switchbox', 'colums' => 4],
					'width' => ['name' => 'Width (px)'],
					'height' => ['name' => 'Height (px)'],
					'link' => ['name' => 'Link'],
					'image' => ['name' => 'Image'],
					'code' => [
						'name' => 'Code',
						'type' => 'textarea',
						'height' => '100px',
						'colums' => 12,
						'extent_button' => '<a class="btn-sm float-right" href="javascript:generate_code()">Generate Code</a>'
					],
				],
			],
			'scripts' => '
			function generate_code() {
				var image = $("#image").val();
				var link = $("#link").val();
				if(image && link) {
					$("#code").val(\'<a href="\'+link+\'" target="_blank"><img src="\'+image+\'" /></a>\');
					try {
						var img = new Image();
						img.src = image;
						img.onload = function(){
							$("#width").val(this.width);
							$("#height").val(this.height);
						};
					} catch(err) {
						return false;
					}
				}
			}
			',
		];
		return $config;
	}
}