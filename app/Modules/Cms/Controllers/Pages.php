<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;
use App\Modules\Admins\Controllers\AdminController;
use App\Modules\Cms\Libraries\Images;

class Pages extends AdminController
{
    function __construct() {
		//parent::__construct();
    }
	function get_config() {
		$config = [
			'name' => 'Pages',
			'model' => 'App\Modules\Cms\Models\PostModel',
			'datagrid_options' => [
				'limit_perpage' => 12,
				'search_by' => ['title','content'],
				'filter_date' => true,
				'orders' => ['published' => 'desc'],
				'where' => ['type' => -1],
				'bulk_actions' => true,
				'addnew_action' => current_url().'/item/',
			],
			'select_options' => [
				'status' => [1 => 'Active', 0 => 'Inactived'],
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center d-md-table-cell d-none max-width-60'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'title' => ['name' => 'Title', 'class' => 'post-title'],
				'status' => [
					'name' => 'Status',
					'method' => 'function',
					'function' => 'update_status|id',
					'class' => 'text-center d-sm-table-cell d-none max-width-100',
				],
				'views' => [
					'name' => 'Views',
					'class' => 'text-center d-lg-table-cell d-none max-width-80',
				],
				'images' => [
					'name' => 'Images',
					'method' => 'function',
					'function' => 'check_images',
					'class' => 'text-center d-sm-table-cell d-none max-width-100',
				],
				'published' => [
					'name' => 'Published',
					'method' => 'datetime',
					'class' => 'text-center d-md-table-cell d-none max-width-100',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right max-width-100',
					'edit' => '/item/',
				],
			],
			'with' => ['user|id,username,name'],
			'rules' => [
				'required' => ['title', 'slug']
			],
			'record' => [
				'views' => 'page',
			],
		];
		return $config;
	}
	function item() {
		$this->view->setVar('title', 'Edit item');
        $item_id = $this->request->uri->getSegment(4);

		$posts = $this->request->getPost();
		if(isset($posts['id'])) {
			$data = $posts;
            unset($data['id']);
            $data['type'] = -1;
            $data['categories'] = '';
            $data['tags'] = '';
            $data['is_comments'] = 0;
            $data['is_adsense'] = 0;
            $data['published'] = time();
			if(!isset($data['hot'])) $data['hot'] = 0;
			if(!isset($data['is_search'])) $data['is_search'] = 0;
			if(!isset($data['sitemap_priority'])) $data['sitemap_priority'] = 0.5;
			if(!empty($data['faq_id']) && is_array($data['faq_id'])) {
				$faqs = [];
				foreach($data['faq_id'] as $key=>$value) {
					if(!empty($data['faq_question'][$key]) && !empty($data['faq_answer'][$key])) {
						$faqs[$key]['id'] = $value;
						$faqs[$key]['question'] = $data['faq_question'][$key];
						$faqs[$key]['answer'] = $data['faq_answer'][$key];
					}
				}
				unset($data['faq_id']);
				unset($data['faq_question']);
				unset($data['faq_answer']);
			}
            $data['thumb'] = get_thumb($data);
            if(!empty($data['hosted_image']) && $data['hosted_image']) {
                $image = new Images();
                if(cms_config('cdn8')) $host = 'cdn8.net';
                else $host = $_SERVER['SERVER_NAME'];
                if(!empty($data['thumb']) && !strpos($data['thumb'], $host)) {
                    $json = $image->tranload($data['thumb']);
                    if(!empty($json['location'])) $data['thumb'] = $json['location'];
                }
                if(!empty($data['content'])) {
                    preg_match_all('/<img(.*)src=\"(.*)\"/U', $data['content'], $matches);
                    if(!empty($matches[2])) {
                        foreach($matches[2] as $_image) {
                            if(!strpos($_image, $host)) {
                                $json = $image->tranload($_image);
                                if(!empty($json['location'])) {
                                    $data['content'] = str_replace($_image, $json['location'], $data['content']);
                                }
                            }
                        }
                    }
                }
            }
			if(isset($data['title']) && $data['title'] && isset($data['slug']) && $data['slug']) {
				// Check duplicate item
				if($posts['id']) $is_duplicate = $this->model->where('id<>', $posts['id'])->where('slug', $data['slug'])->countAllResults();
				else $is_duplicate = $this->model->where('slug', $data['slug'])->countAllResults();
				if($is_duplicate) {
					set_message('Item '.$data['slug'].' is existed in database', 'warning');
				}
				else {
					$post_id = 0;
					$redirect = false;
					if($posts['id']) {
						$result = $this->model->update($posts['id'], $data);
						if($result) $post_id = $posts['id'];
					} else {
						$result = $this->model->insert($data);
						if($result) {
							$post_id = $result;
							$redirect = true;
						}
						$post_url = post_url($data);
					}
					if($post_id) {
						$faq_ids = [];
						$faqModel = model('App\Modules\Cms\Models\PostFaqModel');
						if(!empty($faqs)) {
							foreach($faqs as $faq) {
								$faq_id = intval($faq['id']);
								unset($faq['id']);
								$faq['post_id'] = $post_id;
								if($faq_id) {
									$faq_ids[] = $faq_id;
									$faqModel->update($faq_id, $faq);
								} else {
									$faq_id = $faqModel->insert($faq);
									if($faq_id) $faq_ids[] = $faq_id;
								}
							}
							
						}
						if(!empty($faq_ids)) $faqModel->where('post_id', $post_id)->whereNotIn('id', $faq_ids)->delete();
						else $faqModel->where('post_id', $post_id)->delete();
						set_message('Save successfully!', 'success');
					} else {
						set_message('Save error!', 'warning');
					}
					if($redirect) cms_redirect(current_url().'/'.$post_id);
				}
			} else {
				set_message('Input error', 'warning');
			}
		}
		if($item_id) {
            $item_id = intval($item_id);
			if(check_permissions('post.deleted', 'function')) {
                $item = $this->model->with('faqs')->with('relations')->with('user', ['fields'=>'id,username,name'])->with_trashed()->find($item_id);
            }
            else {
                $item = $this->model->with('faqs')->with('relations')->with('user', ['fields'=>'id,username,name'])->find($item_id);
            }
            if(empty($item)) {
                return \cms_redirect(rebuild_url(2));
            } else {
                $this->view->setVar('item', $item);
            }
		}
		$category = model('App\Modules\Cms\Models\CategoryModel');
		$categories = $category->get_list();
        $this->view->setVar('categories', $categories);
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'lavalamp',
            'moment',
            'bootstrap-datepicker',
            'bootstrap-datetimepicker',
            'select2',
            'tinymce',
        ]);
		$this->view->setVar('validation', $this->validator);
		$this->view->setVar('cms', $this->cms);
        set_referer('backend');
		echo $this->view->render('page');
	}
    public function update_hosted_images()
    {
        $item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, thumb, content')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
                $image = new Images();
                if(cms_config('cdn8')) $host = 'cdn8.net';
                else $host = $_SERVER['SERVER_NAME'];
                if(!empty($item->thumb) && !strpos($item->thumb, $host)) {
                    $json = $image->tranload($item->thumb);
                    if(!empty($json['location'])) $item->thumb = $json['location'];
                }
                if(!empty($item->content)) {
                    preg_match_all('/<img(.*)src=\"(.*)\"/U', $item->content, $matches);
                    if(!empty($matches[2])) {
                        foreach($matches[2] as $_image) {
                            if(!strpos($_image, $host)) {
                                $json = $image->tranload($_image);
                                if(!empty($json['location'])) {
                                    $item->content = str_replace($_image, $json['location'], $item->content);
                                }
                            }
                        }
                    }
                }
                if($item->hasChanged()) {
                    $updated = $this->model->save($item);
                    if($updated) $json = ['status' => "success"];
                }
			}
			$this->render_json($json);
		}
    }
    public function update_status()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, status')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
				$status = ($item->status)?0:1;
				$updated = $this->model->update($item_id, ['status' => $status]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
    }
    public function update_hot()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, hot')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
				$hot = ($item->hot)?0:1;
				$updated = $this->model->update($item_id, ['hot' => $hot]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
    }
    public function get_tags()
    {
		$tag = model('App\Modules\Cms\Models\TagModel');
		$keyword = $this->request->getGet('term');

		if($keyword) $items = $tag->like('name', $keyword)->select('id,name,slug')->findAll(10, 0);
		else $items = $tag->select('id,name,slug')->findAll(10, 0);
		$results = [];
		if(is_array($items)) {
			foreach($items as $item) {
				$temp = [];
				$temp['id'] = $item->id;
				$temp['text'] = $item->name;
				$results[] = $temp;
			}
		}
		$json = ['results' => $results];
		$this->render_json($json);
    }
    public function get_users()
    {
		$model = model('App\Modules\Cms\Models\UserModel');
		$keyword = $this->request->getGet('term');

		if($keyword) $items = $model->like('username', $keyword)->orLike('name', $keyword)->select('id,username,name')->findAll(10, 0);
		else $items = $model->select('id,username,name')->findAll(10, 0);
		$results = [];
		if(is_array($items)) {
			foreach($items as $item) {
				$temp = [];
				$temp['id'] = $item->id;
				$temp['text'] = ($item->name)?$item->name:$item->username;
				$results[] = $temp;
			}
		}
		$json = ['results' => $results];
		$this->render_json($json);
    }
    public function get_slug()
    {
		$increase = 0;
		$id = intval($this->request->getPost('id'));
		$data = trim($this->request->getPost('data'));
		$raw_slug = $slug = clear_utf8($data);
		$check = $this->model->where('slug', $slug)->where('id<>', $id)->first();
		while(is_object($check)) {
			if($increase > 5) break;
			$increase++;
			$slug = $raw_slug.'.'.$increase;
			$check = $this->model->where('slug', $slug)->first();
		}
		if(!$check) $json = ['status' => true, 'slug' => $slug];
		else $json = ['status' => false];	
		$this->render_json($json);
    }
}