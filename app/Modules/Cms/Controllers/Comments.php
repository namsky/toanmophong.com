<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Comments extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Comments',
			'model' => 'App\Modules\Cms\Models\CommentModel',
			'datagrid_options' => [
				'orders' => ['id' => 'desc'],
			],
			'select_options' => [
				'status' => [1 => 'Active', 0 => 'Inactived'],
			],
			'columns' => [
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'user_id' => [
					'name' => 'Author',
					'method' => 'template',
					'template' => '<span style="font-weight: bold;">{$user->username}</span>',
					'class' => 'text-center max-width-120'
				],
				'post_id' => [
					'name' => 'Post',
					'method' => 'template',
					'template' => '<span style="font-weight: bold;">{$post->title}</span>',
					'class' => 'text-center d-lg-table-cell d-none post-title'
				],
				'content' => ['name' => 'Content'],
				'created' => [
					'name' => 'Created',
					'method' => 'datetime',
					'class' => 'text-center',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				]
			],
			'with' => ['user|id,name,username', 'post|id,title'],
			'rules' => [
				'required' => ['user_id', 'post_id', 'content']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'content' => ['name' => 'Content'],
					'user_id' => ['name' => 'User'],
					'post_id' => ['name' => 'Post'],
				],
			],
		];
		return $config;
	}
}