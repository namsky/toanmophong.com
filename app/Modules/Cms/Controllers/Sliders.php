<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Sliders extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Sliders',
			'model' => 'App\Modules\Cms\Models\SliderModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['name'],
				'orders' => ['id' => 'ASC'],
				'bulk_actions' => true,
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => ['name' => 'Name', 'class' => 'text-center'],
				'image' => [
					'name' => 'Image',
					'method' => 'image',
					'image_size' => 150,
					'class' => 'text-center'
				],
				'status' => [
					'name' => 'Trạng thái',
					'method' => 'function',
					'function' => 'update_status|id',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'rules' => [
				'required' => ['name']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'name' => ['name' => 'Name'],
					'text_1' => ['name' => 'Text 1'],
					'text_2' => ['name' => 'Text 2'],
					'image' => ['name' => 'Image '],
				],
			],
		];
		return $config;
	}
	public function update_status()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, status')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
				$status = ($item->status)?0:1;
				$updated = $this->model->update($item_id, ['status' => $status]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
    }
}