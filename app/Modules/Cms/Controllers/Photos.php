<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Photos extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Photos',
			'model' => 'App\Modules\Cms\Models\PhotoModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['title','file_name','origin_url'],
				'orders' => ['id' => 'desc'],
				'bulk_actions' => true,
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'title' => ['name' => 'Title', 'class' => 'text-center'],
				'file_name' => ['name' => 'File name', 'class' => 'text-center d-sm-table-cell d-none'],
				'relation_table' => ['name' => 'Relation', 'class' => 'text-center d-sm-table-cell d-none'],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'rules' => [
				'required' => ['title']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'title' => ['name' => 'Title'],
					'origin_url' => ['name' => 'Origin URL'],
					'file_name' => ['name' => 'File name'],
					'relation_id' => ['name' => 'Relation ID'],
					'relation_table' => ['name' => 'Relation table'],
				],
			],
		];
		return $config;
	}
}