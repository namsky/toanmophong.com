<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;
use App\Modules\Admins\Controllers\AdminController;

class Slider extends AdminController
{
    function __construct() {
		//parent::__construct();
		$this->model = model('App\Modules\Cms\Models\SliderModel');
    }
	public function index()
	{
        $this->view->setVar('title', 'Slider');

        $slider = $this->model->findAll();
        $this->view->setVar('items', $slider);
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'bootstrap-datepicker',
            'nestable',
            'tinymce',
        ]);
        $this->view->setVar('cms', $this->cms);
        set_referer('backend');
		echo $this->view->render('slider');
    }
    public function remove()
    {
        $id = intval($this->request->getPost('id'));
        $status = $this->model->delete($id);
        $json = ['status' => $status];
        $this->render_json($json);
    }
    public function save_data(){
        $posts = $this->request->getPost();
		if(isset($posts['id'])) {
			$id = intval($posts['id']);
			$data = array();
			$data['slider_name'] = $posts['slider_name'];
			$data['type'] = $posts['type'];
            $data['slider_link'] = $posts['slider_link'];
			$data['slider_image'] = $posts['slider_image'];
			$data['slider_text'] = $posts['slider_text'];
			/* Check requiced */
			$status = true;
			if(empty($data['slider_name'])) {
				$status = false;
				$message = 'Name is requiced';
			}
			elseif(empty($data['slider_image'])) {
				$status = false;
				$message = 'Image is requiced';
            }
            if($status) {
				if(!empty($data['parent'])) {
					if($id) $check_parent = $this->model->where('id', $data['parent'])->where('id<>', $id)->countAllResults();
					else $check_parent = $this->model->where('id', $data['parent'])->countAllResults();
				} else $check_parent = 1;
				if($check_parent) {
					if($id) {
						$status = $this->model->update($id, $data);
						$method = 'Update';
					} else {
						$status = $this->model->insert($data);
						$method = 'Add';
					}
				} else {
					$status = false;
					$message = 'Parent is invalid';
				}
			}
			$json = array();
			if($status) {
				$json['message'] = $method.' items successfully!';
				$json['status'] = 'success';
			} else {
				$json['message'] = !empty($message)?$message:'Something went wrong';
				$json['status'] = 'error';
			}
			$this->render_json($json);
		}
    }
}