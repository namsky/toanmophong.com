<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Themes extends \App\Modules\Admins\Controllers\AdminController
{
    private $themes_path;
    function __construct() {
		//parent::__construct();
        $this->model = model('App\Modules\Cms\Models\WidgetModel');
        $this->themes_path = FCPATH.'statics\themes';
    }
	public function index()
	{
        $this->view->setVar('title', 'Themes');
        $configs = $this->request->getPost('configs');
        $updated = false;
        if(!empty($configs)) {
            $configModel = model('App\Modules\Admins\Models\ConfigurationModel');
            foreach($configs as $id_config=>$value_config) {
                $configModel->update($id_config, ['value' => $value_config]);
            }
            $message = ['message'=>'Update configs theme success', 'type'=>'success'];
            $this->view->setVar('notice', $message);
            $updated = true;
        }
        // Get current actived theme
        $router = APPPATH.'Modules\Cms\Config\Routes.php';
        $error = '';
        $active_theme = '';
        if(file_exists($router)) {
            $content = \file_get_contents($router);
            if(strpos($content, "'ACTIVE_THEME'")) {
                $active_theme = getString($content, "'ACTIVE_THEME','", "'");
            } else $error = 'Config theme is not valid';
        } else $error = 'File Routes in CMS not found';
        $this->view->setVar('active_theme', $active_theme);
        $this->view->setVar('error', $error);

        // Get list themes
        $themes = [];
        if(is_dir($this->themes_path)) {
            $directories = array_diff(scandir($this->themes_path), array('..', '.'));
            if(is_array($directories)) {
                foreach($directories as $theme_name) {
                    $theme['name'] = $theme_name;
                    $theme['screenshot'] = (bool) \file_exists($this->themes_path.'/'.$theme_name.'/screenshot.jpg');
                    $theme['active'] = (bool) ($active_theme == $theme_name);
                    $themes[] = $theme;
                }
            }
        }
        $this->view->setVar('themes', $themes);

		$configGroupModel = model('App\Modules\Admins\Models\ConfigurationGroupModel');
        $config_groups = $configGroupModel->where('code', 'themes')->with('configurations')->orderBy('order')->first();
        if(!empty($config_groups->configurations)) {
            $this->view->setVar('configurations', $config_groups->configurations);
            if($updated) {
                foreach($config_groups->configurations as $config) {
                    $this->cache->delete('config_'.$config->code);
                }
            }
        }
		
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'popper',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'bootstrap-colorpicker',
        ]);
		$this->view->setVar('cms', $this->cms);
        set_referer('backend');
		echo $this->view->render('themes');
	}
    public function active()
    {
        $json = ['status'=>'error', 'message'=>'Something went wrong'];
        $theme = $this->request->getPost('theme');
        if(!empty($theme)) {
            if(is_dir($this->themes_path) && is_dir($this->themes_path.'/'.$theme) && is_dir(APPPATH.'Modules\\'.$theme)) {
                $router = APPPATH.'Modules\Cms\Config\Routes.php';
                $error = '';
                $active_theme = '';
                if(file_exists($router)) {
                    $router_content = \file_get_contents($router);
                    if(strpos($router_content, "'ACTIVE_THEME'")) {
                        $active_theme = getString($router_content, "'ACTIVE_THEME','", "'");
                        $router_content = str_replace("'ACTIVE_THEME','".$active_theme, "'ACTIVE_THEME','".$theme, $router_content);
                        $write = \file_put_contents($router, $router_content);
                        if(!$write) $error = 'File Router can\'t write';
                    } else $error = 'Config theme is not valid';
                } else $error = 'File Routes in CMS not found';
                if($error)
                    $json = ['status'=>'error', 'message'=>$error];
                else
                    $json = ['status'=>'success', 'message'=>'Theme '.ucfirst($theme).' is actived'];
            } else {
                $json = ['status'=>'error', 'message'=>'Theme not found'];
            }
        }
        $this->render_json($json);
    }
}