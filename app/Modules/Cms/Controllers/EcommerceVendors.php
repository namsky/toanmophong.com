<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;
use App\Modules\Cms\Libraries\Images;

class EcommerceVendors extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Vendors',
			'model' => 'App\Modules\Cms\Models\EcommerceVendorModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['name'],
				'filter_by' => ['category_id'],
				'filter_date' => false,
				'orders' => ['id' => 'desc'],
				'bulk_actions' => false,
			],
			'select_options' => [
                'category_id' => 'category|id,name|App\Modules\Cms\Models\EcommerceCategoryModel'
            ],
			'columns' => [
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => ['name' => 'Name', 'class' => 'text-center'],
				'thumb' => [
					'name' => 'Thumb',
					'method' => 'template',
					'template' => '<div class="vendor_thumb"><img src="{$thumb}" /></div>',
					'class' => 'text-center d-lg-table-cell d-none',
				],
				'category_id' => [
					'name' => 'Type',
					'method' => 'template',
					'template' => '<span style="font-weight: bold;">{$category->name}</span>',
					'class' => 'text-center d-lg-table-cell d-none max-width-120'
				],
				'images' => [
					'name' => 'Images',
					'method' => 'function',
					'function' => 'check_images',
					'class' => 'text-center d-sm-table-cell d-none max-width-100',
				],
				'created' => [
					'name' => 'Created',
					'method' => 'datetime',
					'class' => 'text-center d-lg-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'with' => ['category|id,name'],
			'rules' => [
				'required' => ['name']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'name' => ['name' => 'Name'],
					'thumb' => ['name' => 'Thumb', 'with_upload_image'=>true],
					'category_id' => [
						'name' => 'Category',
						'type' => 'select',
					],
				],
			],
		];
		return $config;
	}
    public function update_hosted_images()
    {
        $item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, thumb')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
                $image = new Images();
                if(cms_config('cdn8')) $host = 'cdn8.net';
                else $host = $_SERVER['SERVER_NAME'];
                if(!empty($item->thumb) && !strpos($item->thumb, $host)) {
                    $json = $image->tranload($item->thumb);
                    if(!empty($json['location'])) $item->thumb = $json['location'];
                }
                if($item->hasChanged()) {
                    $updated = $this->model->save($item);
                    if($updated) $json = ['status' => "success"];
                }
			}
			$this->render_json($json);
		}
    }
}