<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;
use App\Modules\Admins\Controllers\AdminController;

class EcommerceFields extends AdminController
{
	function get_config() {
		$config = [
			'name' => 'Ecommerce Fields',
			'model' => 'App\Modules\Cms\Models\EcommerceFieldModel',
			'datagrid_options' => [
				'orders' => ['id' => 'desc'],
				'search_by' => ['name'],
				'filter_by' => ['category_id','type'],
			],
			'select_options' => [
				'category_id' => 'category|id,name|App\Modules\Cms\Models\EcommerceCategoryModel',
				'type' => ['text' => 'Text', 'number' => 'Number', 'select' => 'Select', 'switch' => 'Switch', 'tags' => 'Tags'],
				'filter_type' => ['text' => 'Text', 'select' => 'Select', 'multi_select' => 'Multi Select', 'ranger_select' => 'Ranger Select'],
			],
			'columns' => [
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => ['name' => 'Name'],
				'category_id' => [
					'name' => 'Category',
					'method' => 'template',
                    'template' => '{$category->name}',
                    'class' => 'text-center'
				],
				'type' => ['name' => 'Type', 'class' => 'text-center d-sm-table-cell d-none'],
				'filtered' => [
					'name' => 'Filtered',
					'method' => 'function',
					'function' => 'status',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'filter_type' => ['name' => 'FilterType', 'class' => 'text-center d-sm-table-cell d-none'],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				]
			],
			'with' => ['category|id,name'],
			'rules' => [
				'required' => ['name', 'category_id']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'name' => ['name' => 'Name', 'colums' => 6],
					'unit' => ['name' => 'Unit', 'colums' => 6],
					'category_id' => [
						'name' => 'Category',
						'type' => 'select',
                        'colums' => 6,
					],
					'type' => [
						'name' => 'Type',
                        'type' => 'select',
                        'colums' => 6,
					],
                    'options' => [
                        'name' => 'Options',
                        'type' => 'textarea',
						'description' => 'Example: Samsung, iPhone, Xiaomi',
                    ],
					'nulled' => ['name' => 'Nulled', 'type' => 'switchbox', 'colums' => 4],
					'filtered' => ['name' => 'Filtered', 'type' => 'switchbox', 'colums' => 4],
					'filter_type' => [
						'name' => 'FilterType',
                        'type' => 'select',
                        'colums' => 4
					],
					'filter_options' => [
						'name' => 'FilterOptions',
						'description' => 'Ranger: 150,150-300,300',
                        'type' => 'textarea',
					],
				],
			],
		];
		return $config;
	}
    public function get_fields()
    {
        $category_id = $this->request->getGet('category_id');
        $results = [];
		if($category_id) {
            $results = $this->model->select('id,name,type,unit,options,nulled')->where('category_id', $category_id)->findAll();
        }
        $json = ['results' => $results];
		return $this->render_json($json);
    }
}