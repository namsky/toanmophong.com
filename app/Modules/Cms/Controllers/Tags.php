<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Tags extends \App\Modules\Admins\Controllers\AdminController
{
    function __construct() {
		//parent::__construct();
		$this->model = model('App\Modules\Cms\Models\TagModel');
    }
	public function index()
	{
		$this->view->setVar('title', 'Tags');
		$keyword = clearString($this->request->getGet('q'));
		if($keyword) $items = $this->model->like('name', $keyword)->paginate(12);
		else $items = $this->model->paginate(12);
		$paging = $this->model->pager->links();
		$total_records = $this->model->pager->getTotal();
		$this->view->setVar('keyword', $keyword);
		$this->view->setVar('items', $items);
		$this->view->setVar('paging', $paging);
		$this->view->setVar('total_records', $total_records);
		
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'bootstrap-datepicker',
            'nestable',
            'tinymce',
        ]);
		$this->view->setVar('cms', $this->cms);
        set_referer('backend');
		echo $this->view->render('tags');
	}
    public function search_tags()
    {
		$keyword = $this->request->getGet('term');
		if($keyword) $items = $this->model->like('name', $keyword)->select('id,name,slug')->findAll(10, 0);
		else $items = $this->model->select('id,name,slug')->findAll(10, 0);
		$results = [];
		if(is_array($items)) {
			foreach($items as $item) {
				$temp = [];
				$temp['id'] = $item->id;
				$temp['text'] = $item->name;
				$results[] = $temp;
			}
		}
		$json = ['results' => $results];
		$this->render_json($json);
    }
    public function get_data()
    {
        $item_id = $this->request->getGet('item_id');
        $item_id = intval($item_id);
        $item = null;
        if($item_id) {
            $item = $this->model->find($item_id);
        }
        if(isset($item->id)) {
            $json = $item->toArray();
        } else $json = ['id'=>0];
		$this->render_json($json);
	}
    public function remove()
    {
		$id = intval($this->request->getPost('id'));
		$status = $this->model->delete($id);
		$post_relation = model('App\Modules\Cms\Models\PostRelationModel');
		$post_relation->where('foreign_table', 'tags')->where('foreign_key', $id)->delete();
        $json = ['status' => $status];
        $this->render_json($json);
    }
    public function get_slug()
    {
		$id = intval($this->request->getPost('id'));
		$data = $this->request->getPost('data');
		$slug = clear_utf8($data);
        $json = ['status' => 'success', 'slug' => $slug];
        $this->render_json($json);
    }
    public function save_data()
    {
		$posts = $this->request->getPost();
		if(isset($posts['id'])) {
			$id = intval($posts['id']);
			$data = array();
			$data['name'] = $posts['name'];
			$data['slug'] = $posts['slug'];
			$data['parent'] = isset($posts['parent'])?intval($posts['parent']):0;
			$data['title'] = $posts['title'];
			$data['description'] = $posts['description'];
			$data['keywords'] = $posts['keywords'];
			/* Check requiced */
			$status = true;
			if(empty($data['name'])) {
				$status = false;
				$message = 'Name is requiced';
			}
			elseif(empty($data['slug'])) {
				$status = false;
				$message = 'Slug is requiced';
			}
			// Check duplicate item
			if($id) $is_duplicate = $this->model->where('id<>', $id)->where('slug', $data['slug'])->countAllResults();
			else $is_duplicate = $this->model->where('slug', $data['slug'])->countAllResults();
			if($is_duplicate) {
				$message['message'] = 'Item "'.$data['slug'].'" is existed in database';
				$message['status'] = 'error';
				return $message;
			}
			/* Process to database */
			if($status) {
				if($id) {
					$status = $this->model->update($id, $data);
					$method = 'Update';
				} else {
					$status = $this->model->insert($data);
					$method = 'Add';
				}
			}
			$json = array();
			if($status) {
				$json['message'] = $method.' items successfully!';
				$json['status'] = 'success';
			} else {
				$json['message'] = !empty($message)?$message:'Something went wrong';
				$json['status'] = 'error';
			}
			$this->render_json($json);
		}
    }
}