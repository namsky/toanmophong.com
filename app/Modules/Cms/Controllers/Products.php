<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Products extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Products',
			'model' => 'App\Modules\Cms\Models\ProductModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['name'],
				'orders' => ['id' => 'ASC'],
				'bulk_actions' => true,
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => ['name' => 'Đồ', 'class' => 'text-center'],
				'price' => ['name' => 'Giá', 'class' => 'text-center'],
				'image' => [
					'name' => 'Hình ảnh',
					'method' => 'image',
					'image_size' => 100,
					'class' => 'text-center'
				],
				'hot' => [
					'name' => 'Hot',
					'method' => 'function',
					'function' => 'update_hot|id',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'status' => [
					'name' => 'Trạng thái',
					'method' => 'function',
					'function' => 'update_status|id',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'rules' => [
				'required' => ['name', 'price']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'name' => ['name' => 'Tên'],
					'explain_name' => ['name' => 'Chú thích'],
					'price' => ['name' => 'Giá'],
					'image' => ['name' => 'Hình ảnh (160x250)']
				],
			],
		];
		return $config;
	}
	public function update_hot()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, status')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
				$hot = ($item->hot)?0:1;
				$updated = $this->model->update($item_id, ['hot' => $hot]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
    }
	public function update_status()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, status')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
				$status = ($item->status)?0:1;
				$updated = $this->model->update($item_id, ['status' => $status]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
    }
}