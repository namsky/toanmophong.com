<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Menus extends \App\Modules\Admins\Controllers\AdminController
{
    function __construct() {
		//parent::__construct();
		$this->model = model('App\Modules\Cms\Models\MenuModel');
    }
	public function index()
	{
		$this->view->setVar('title', 'Menus');
        $category = model('App\Modules\Cms\Models\CategoryModel');
        
        $name = $this->request->getPost('name');
        if(!empty($name)) {
            $check_menu = $menu->where('name', $name)->first();
            if(empty($check_menu)) {
                $menu->insert(['name'=>$name]);
                set_message('Thêm menu thành công', 'success');
            } else {
                set_message('Menu đã tồn tại', 'warning');
            }
        }

		$menus = $this->model->with('items')->findAll();
		$categories = $category->get_list();
		
        $this->view->setVar('menus', $menus);
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'bootstrap-datepicker',
            'nestable',
            'tinymce',
        ]);
		$this->view->setVar('cms', $this->cms);
        set_referer('backend');
		echo $this->view->render('menus');
	}
}