<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Recruitments extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Recruitments',
			'model' => 'App\Modules\Cms\Models\RecruitmentModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['title'],
				'orders' => ['id' => 'ASC'],
				'bulk_actions' => true,
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'title' => ['name' => 'Công việc', 'class' => 'text-center'],
				'age' => ['name' => 'Tuổi', 'class' => 'text-center'],
				'gender' => ['name' => 'Tuổi', 'class' => 'text-center'],
				'income' => ['name' => 'Lương', 'class' => 'text-center'],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'rules' => [
				'required' => ['customer', 'content']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'name' => ['name' => 'Tên'],
					'image' => ['name' => 'Hình ảnh (200x310)'],
					'content' => ['name' => 'Nội dung']
				],
			],
		];
		return $config;
	}
}