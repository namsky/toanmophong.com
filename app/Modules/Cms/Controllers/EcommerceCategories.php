<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;
use App\Modules\Admins\Controllers\AdminController;

class EcommerceCategories extends AdminController
{
    function __construct() {
		//parent::__construct();
		$this->model = model('App\Modules\Cms\Models\EcommerceCategoryModel');
    }
	public function index()
	{
		$this->view->setVar('title', 'Ecommerce Categories');
		
		$items = $this->model->get_list();
		$this->view->setVar('items', $items);
		
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'bootstrap-datepicker',
            'nestable',
            'tinymce',
        ]);
		$this->view->setVar('cms', $this->cms);
        set_referer('backend');
		echo $this->view->render('ecommerce_categories');
	}
    public function get_data()
    {
        $item_id = $this->request->getGet('item_id');
        $item_id = intval($item_id);
        $item = null;
        if($item_id) {
            $item = $this->model->find($item_id);
        }
        if(isset($item->id)) {
            $json = $item->toArray();
        } else $json = ['id'=>0];
        $this->render_json($json);
    }
    public function remove()
    {
		$id = intval($this->request->getPost('id'));
		$status = $this->model->delete($id);
		$_relation = model('App\Modules\Cms\Models\EcommerceRelationModel');
		$_relation->where('foreign_table', 'categories')->where('foreign_key', $id)->delete();
        $json = ['status' => $status];
        $this->render_json($json);
    }
    public function update()
    {
		$data = $this->request->getPost('data');
		$data = json_decode($data, true);
		$status = $this->update_order($data);
        $json = ['status' => $status];
        $this->render_json($json);
    }
	protected function update_order($data, $parent=0) {
		try {
			$i=0;
			if(is_array($data) && count($data))
			foreach ($data as $key => $item) {
				$arg = array('id'=>$item['id'], 'parent'=>$parent,'order'=>$key);
				$this->model->update($arg['id'], $arg);
				if(isset($item['children']) && $item['children']) $this->update_order($item['children'], $item['id']);
				$i++;
			}
			return true;
		} catch(Excaption $e) {
			return false;
		}
	}
    public function get_slug()
    {
		$id = intval($this->request->getPost('id'));
		$data = $this->request->getPost('data');
		$slug = clear_utf8($data);
        $json = ['status' => 'success', 'slug' => $slug];
        $this->render_json($json);
    }
    public function save_data()
    {
		$posts = $this->request->getPost();
		if(isset($posts['id'])) {
			$id = intval($posts['id']);
			$data = array();
			$data['name'] = $posts['name'];
			$data['slug'] = $posts['slug'];
			$data['parent'] = isset($posts['parent'])?intval($posts['parent']):0;
			$data['title'] = $posts['title'];
			$data['description'] = $posts['description'];
			$data['keywords'] = $posts['keywords'];
			$data['summary'] = $posts['summary'];
			/* Check requiced */
			$status = true;
			if(empty($data['name'])) {
				$status = false;
				$message = 'Name is requiced';
			}
			elseif(empty($data['slug'])) {
				$status = false;
				$message = 'Slug is requiced';
			}
			// Check duplicate item
			if($id) $is_duplicate = $this->model->where('id<>', $id)->where('slug', $data['slug'])->countAllResults();
			else $is_duplicate = $this->model->where('slug', $data['slug'])->countAllResults();
			if($is_duplicate) {
				$message['message'] = 'Item "'.$data['slug'].'" is existed in DB';
				$message['status'] = 'error';
				return $message;
			}
			/* Process to database */
			if($status) {
				if(!empty($data['parent'])) {
					if($id) $check_parent = $this->model->where('id', $data['parent'])->where('id<>', $id)->countAllResults();
					else $check_parent = $this->model->where('id', $data['parent'])->countAllResults();
				} else $check_parent = 1;
				if($check_parent) {
					if($id) {
						$status = $this->model->update($id, $data);
						$method = 'Update';
					} else {
						$status = $this->model->insert($data);
						$method = 'Add';
					}
				} else {
					$status = false;
					$message = 'Parent is invalid';
				}
			}
			$json = array();
			if($status) {
				$json['message'] = $method.' items successfully!';
				$json['status'] = 'success';
			} else {
				$json['message'] = !empty($message)?$message:'Something went wrong';
				$json['status'] = 'error';
			}
			$this->render_json($json);
		}
    }
}