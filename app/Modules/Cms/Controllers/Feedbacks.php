<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Feedbacks extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Feedbacks',
			'model' => 'App\Modules\Cms\Models\FeedbackModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['customer'],
				'orders' => ['id' => 'ASC'],
				'bulk_actions' => true,
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'customer' => ['name' => 'Khách hàng', 'class' => 'text-center'],
				'image' => [
					'name' => 'Hình ảnh',
					'method' => 'image',
					'image_size' => 100,
					'class' => 'text-center'
				],
				'content' => ['name' => 'Nội dung', 'class' => 'text-center'],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'rules' => [
				'required' => ['customer', 'content']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'name' => ['name' => 'Tên'],
					'image' => ['name' => 'Hình ảnh (200x310)'],
					'content' => ['name' => 'Nội dung']
				],
			],
		];
		return $config;
	}
}