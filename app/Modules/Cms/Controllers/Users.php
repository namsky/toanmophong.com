<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Users extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Users',
			'model' => 'App\Modules\Cms\Models\UserModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['email','username'],
				'filter_by' => ['group_id','status'],
				'filter_date' => true,
				'orders' => ['id' => 'desc'],
				'bulk_actions' => true,
			],
			'select_options' => [
				'group_id' => 'group|id,name|App\Modules\Cms\Models\UserGroupModel',
				'status' => [1 => 'Active', 0 => 'Inactived'],
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => ['name' => 'Name', 'class' => 'text-center d-sm-table-cell d-none'],
				'username' => ['name' => 'Username'],
				'email' => ['name' => 'Email', 'class' => 'd-sm-table-cell d-none'],
				'group_id' => [
					'name' => 'Group',
					'method' => 'template',
					'template' => '<span style="font-weight: bold; color:{$group->color}">{$group->name}</span>',
					'class' => 'text-center d-lg-table-cell d-none',
				],
				'status' => [
					'name' => 'Status',
					'method' => 'function',
					'function' => 'update_status|id',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'created' => [
					'name' => 'Created',
					'method' => 'datetime',
					'class' => 'text-center d-lg-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'with' => ['group|id,name,color'],
			'rules' => [
				'required' => ['username', 'email', 'group_id']
			],
			'record' => [
				'colums' => 6,
				'fields' => [
					'username' => ['name' => 'Username'],
					'email' => ['name' => 'Email'],
					'group_id' => [
						'name' => 'Group',
						'type' => 'select',
					],
					'status' => [
						'name' => 'Status',
						'type' => 'select',
					],
					'name' => ['name' => 'Name'],
					'avatar' => ['name' => 'Avatar', 'with_upload_image'=>true],
					'ip_address' => ['only_show' => true],
					'break_line',
					'password' => [
						'type' => 'password',
						'colums' => 12,
						'only_edit' => true
					],
				],
			],
		];
		return $config;
	}
    public function update_status()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$json = ['status' => "error"];
			$item = $this->model->select('id, status')->find($item_id);
			if(isset($item->id)) {
				$status = ($item->status)?0:1;
				$updated = $this->model->update($item_id, ['status' => $status]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
    }
    public function search_users()
    {
		$keyword = $this->request->getGet('term');
		if($keyword) $items = $this->model->like('username', $keyword)->orLike('name', $keyword)->select('id,username,name')->findAll(10, 0);
		else $items = $this->model->select('id,username,name')->findAll(10, 0);
		$results = [];
		if(is_array($items)) {
			foreach($items as $item) {
				$temp = [];
				$temp['id'] = $item->id;
				$temp['text'] = ($item->name)?$item->name:$item->username;
				$results[] = $temp;
			}
		}
		$json = ['results' => $results];
		$this->render_json($json);
    }
}