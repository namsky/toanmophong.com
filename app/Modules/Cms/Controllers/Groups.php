<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Groups extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Groups',
			'model' => 'App\Modules\Cms\Models\UserGroupModel',
			'datagrid_options' => [
				'orders' => ['id' => 'desc'],
			],
			'select_options' => [
				'status' => [1 => 'Active', 0 => 'Inactived'],
			],
			'columns' => [
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => [
					'name' => 'Name',
					'method' => 'template',
					'template' => '<span style="font-weight: bold; color:{$color}">{$name}</span>',
				],
				'slug' => ['name' => 'Slug', 'class' => 'd-lg-table-cell d-none'],
				'description' => ['name' => 'Description', 'class' => 'd-sm-table-cell d-none'],
				'level' => ['name' => 'Level', 'class' => 'text-center d-lg-table-cell d-none'],
				'created' => [
					'name' => 'Created',
					'method' => 'datetime',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				]
			],
			'rules' => [
				'required' => ['name', 'slug', 'value']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'name' => ['name' => 'Name'],
					'color' => ['name' => 'Color'],
					'slug' => ['name' => 'Slug'],
					'description' => ['name' => 'Description'],
					'level' => ['name' => 'Level'],
				],
			],
		];
		return $config;
	}
}