<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Cafes extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Cafes',
			'model' => 'App\Modules\Cms\Models\CafeModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['name', 'address'],
				'orders' => ['id' => 'ASC'],
				'bulk_actions' => true,
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'order' => ['name' => 'Sắp xếp', 'class' => 'text-center text-warning'],
				'name' => ['name' => 'Cửa hàng', 'class' => 'text-center'],
				'address' => ['name' => 'Địa chỉ', 'class' => 'text-center'],
				'thumb' => [
					'name' => 'Hình ảnh',
					'method' => 'image',
					'image_size' => 100,
					'class' => 'text-center'
				],
				'time' => ['name' => 'Giờ hoạt động', 'class' => 'text-center'],
				'status' => [
					'name' => 'Trạng thái',
					'method' => 'function',
					'function' => 'update_status|id',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'rules' => [
				'required' => ['name', 'address']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'name' => ['name' => 'Tên'],
					'address' => ['name' => 'Địa chỉ'],
					'thumb' => ['name' => 'Hình ảnh (360x560)'],
					'time' => ['name' => 'Giờ hoạt động'],
					'order' => ['name' => 'Sắp xếp']
				],
			],
		];
		return $config;
	}
	public function update_status()
    {
		$item_id = intval($this->request->getPost('item_id'));
		if($item_id) {
			$item = $this->model->select('id, status')->find($item_id);
			$json = ['status' => "error"];
			if(isset($item->id)) {
				$status = ($item->status)?0:1;
				$updated = $this->model->update($item_id, ['status' => $status]);
				if($updated) $json = ['status' => "success"];
			}
			$this->render_json($json);
		}
    }
}