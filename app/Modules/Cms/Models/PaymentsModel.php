<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class PaymentsModel extends Model
{
    use \Core\VModels\ModelTrait;
    protected $table      = 'payments';
    protected $primaryKey = 'id';
    protected $allowedFields = ['customer_id', 'orderid', 'order_code','status','product', 'amount', 'response_code','response_message', 'bank_type', 'bank_code','url','transaction_id','updated','created'];

    protected $returnType = 'App\Modules\Cms\Models\Entities\Payments';
    protected $useSoftDeletes = false;
    protected $useTimestamps = true;
    protected $dateFormat = 'int';
    protected $createdField  = 'created';
    protected $updatedField  = 'updated';
    public function __construct()
    {
        parent::__construct();
    }
}