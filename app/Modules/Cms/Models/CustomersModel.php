<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class CustomersModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'customer';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'email', 'phone', 'city', 'district', 'ward', 'address', 'note','status', 'created'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\Customers';
    protected $dateFormat = 'int';
    protected $createdField  = 'created';
	public function __construct()
	{
		parent::__construct();
	}
}