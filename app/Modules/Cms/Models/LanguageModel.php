<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class LanguageModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'languages';
	protected $primaryKey = 'id';
	protected $returnType = 'App\Modules\Cms\Models\Entities\Language';
	protected $useSoftDeletes = false;
	protected $allowedFields = ['name', 'locale'];
	protected $useTimestamps = false;
	public function __construct()
	{
		$this->has_many['data'] = ['App\Modules\Cms\Models\LanguageDataModel','lang_id','id'];
		parent::__construct();
	}
}