<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class PhotoModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'photos';
	protected $primaryKey = 'id';
	protected $allowedFields = ['title', 'relation_id', 'relation_table', 'hash', 'file_name', 'file_size', 'origin_url'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\Photo';
	protected $useSoftDeletes = false;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	public function __construct()
	{
		parent::__construct();
	}
}