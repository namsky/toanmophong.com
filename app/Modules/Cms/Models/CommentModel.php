<?php namespace App\Modules\Cms\Models;
use CodeIgniter\Model;
use App\Modules\Cms\Libraries\Auth;

class CommentModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'comments';
	protected $primaryKey = 'id';
	protected $allowedFields = ['user_id','post_id','parent','content'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\Comment';
	protected $useSoftDeletes = true;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	protected $deletedField  = 'deleted';

	public function __construct()
	{
		$this->has_one['user'] = ['App\Modules\Cms\Models\UserModel','id','user_id'];
		$this->has_one['post'] = ['App\Modules\Cms\Models\PostModel','id','post_id'];
		parent::__construct();
	}
}