<?php namespace App\Modules\Cms\Models;
use CodeIgniter\Model;

class UserLink extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'users';
	protected $primaryKey = 'id';
	protected $allowedFields = ['user_id','token','token_type','identity'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\UserLink';
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';

	public function __construct()
	{
		parent::__construct();
	}
}