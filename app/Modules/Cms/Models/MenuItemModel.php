<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class MenuItemModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'menu_item';
	protected $primaryKey = 'id';
	protected $allowedFields = ['menu_id', 'title', 'type', 'value', 'icon', 'parent', 'order', 'function'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\MenuItem';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	public function get_list($menu_id, $parent=0)
	{
		$categoryModel = model('App\Modules\Cms\Models\CategoryModel');
		$postModel = model('App\Modules\Cms\Models\PostModel');
		$ProductCategoryModel = model('App\Modules\Cms\Models\EcommerceCategoryModel');
		$data = $this->where('menu_id', $menu_id)->where('parent', $parent)->orderBy('order')->findAll();
		if(is_array($data)) {
			foreach($data as $key=>$item) {
				if($item->type == 'category') {
					$category_id = intval($item->value);
					if($category_id) {
						$category = $categoryModel->find($category_id);
						if(isset($category->slug)) $data[$key]->value = $category->slug;
					}
				}elseif($item->type == 'product_category') {
					$product_category_id = intval($item->value);
					if($product_category_id) {
						$product_category = $ProductCategoryModel->find($product_category_id);
						if(isset($product_category->slug)) $data[$key]->value = $product_category->slug;
					}
				}
				elseif($item->type == 'post' || $item->type == 'page') {
					$item_id = intval($item->value);
					if($item_id) {
                        $_item = $postModel->find($item_id);
						if(isset($_item->slug)) $data[$key]->value = $_item->slug;
					}
				}
				$parent = intval($item->id);
				if($parent > 0) {
					$data[$key]->child = $this->get_list($menu_id, $parent);
				} else {
					$data[$key]->child = null;
				}
			}
		}
		return $data;
	}
}