<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;
use App\Modules\Cms\Libraries\UserAuth;

class EcommerceProperties extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ecommerce_properties';
	protected $primaryKey = 'id';
	protected $allowedFields = ['product_id', 'sku', 'color','size','stock','image'];
	protected $returnType = 'App\Modules\Cms\Models\Entities\EcommerceProperties';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	public function __construct()
	{
		parent::__construct();
	}
}