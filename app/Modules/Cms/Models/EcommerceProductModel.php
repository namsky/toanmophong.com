<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;
use App\Modules\Cms\Libraries\UserAuth;

class EcommerceProductModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ecommerce_products';
	protected $primaryKey = 'id';
	protected $allowedFields = [
		'category_id', 'vendor_id', 'name', 'code', 'slug', 'price', 'origin_price', 'content', 'summary', 'alcohol', 'capacity', 'weight', 'year_produce', 'menu_image', 'thumb','thumb_hover', 'background', 'screenshots', 'stocks', 'sales', 'using_guide', 'storage_guide', 'views',
		'comments', 'seo_title', 'seo_description', 'seo_keywords', 'sitemap_priority', 'is_comments', 'is_search', 'status', 'hot', 'tags', 'categories', 'sku', 'color', 'version', 'brand','video','file_download'
	];
	protected $returnType = 'App\Modules\Cms\Models\Entities\EcommerceProduct';
	protected $useSoftDeletes = true;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	protected $deletedField  = 'deleted';

	protected $beforeInsert = ['beforeInsert'];
	protected $beforeUpdate = ['beforeUpdate'];
	protected $afterInsert = ['data_proccess'];
	protected $afterUpdate = ['data_proccess'];
	protected $afterFind = ['afterFind'];
	public function __construct()
	{
		$this->has_many['relations'] = ['App\Modules\Cms\Models\EcommerceRelationModel','item_id','id'];
		$this->has_many['data'] = ['App\Modules\Cms\Models\EcommerceDataModel','item_id','id'];
		$this->has_many['fields'] = ['App\Modules\Cms\Models\EcommerceFieldModel','category_id','category_id'];
		$this->has_one['category'] = ['App\Modules\Cms\Models\EcommerceCategoryModel','id','category_id'];
		$this->has_one['vendor'] = ['App\Modules\Cms\Models\EcommerceVendorModel','id','vendor_id'];
		$this->has_many['pro_pers'] = ['App\Modules\Cms\Models\EcommercePropertiesModel','product_id','id'];
		parent::__construct();
	}

    public function increase_views($item_id)
    {
        $item = $this->select('id, views')->find($item_id);
        if(!empty($item)) {
            $views = $item->views+1;
            $this->update($item->id, ['views'=>$views]);
        }
    }
    protected function beforeInsert($data)
    {
		$data = $this->beforeUpdate($data);
        return $data;
	}
    protected function beforeUpdate($data) {
		if(isset($data['data']['content']) && $data['data']['content'])
			$data['data']['content'] = bzcompress($data['data']['content']);
		if(isset($data['data']['name']) && $data['data']['name'])
			$data['data']['name'] = htmlspecialchars($data['data']['name']);
		if(isset($data['data']['summary']) && $data['data']['summary'])
			$data['data']['summary'] = htmlspecialchars($data['data']['summary']);
		if(isset($data['data']['seo_description']) && $data['data']['seo_description'])
			$data['data']['seo_description'] = htmlspecialchars($data['data']['seo_description']);
		if(isset($data['data']['seo_keywords']) && $data['data']['seo_keywords'])
			$data['data']['seo_keywords'] = htmlspecialchars($data['data']['seo_keywords']);
		if(isset($data['data']['price']) && $data['data']['price'])
			$data['data']['price'] = preg_replace('/[^0-9]/', '', $data['data']['price']);
		if(isset($data['data']['origin_price']) && $data['data']['origin_price'])
			$data['data']['origin_price'] = preg_replace('/[^0-9]/', '', $data['data']['origin_price']);
		if(isset($data['data']['stocks']) && $data['data']['stocks'])
			$data['data']['stocks'] = preg_replace('/[^0-9]/', '', $data['data']['stocks']);
		if(isset($data['data']['price']) && $data['data']['price'])
			$data['data']['price'] = preg_replace('/[^0-9]/', '', $data['data']['price']);
		if(isset($data['data']['categories'])) {
			if($data['data']['categories'])
				$this->categories = $data['data']['categories'];
			else $this->categories = false;
			unset($data['data']['categories']);
		}
		if(isset($data['data']['tags'])) {
			if($data['data']['tags'])
				$this->tags = $data['data']['tags'];
			else
				$this->tags = false;
			unset($data['data']['tags']);
		}
        return $data;
    }
    protected function data_proccess($data) {
        $id = 0;
		if(isset($data['id'])) {
			if(is_array($data['id']) && isset($data['id'][0])) $id = intval($data['id'][0]);
            elseif(is_numeric($data['id'])) $id = intval($data['id']);
		}
		$_relation = model('App\Modules\Cms\Models\EcommerceRelationModel');
		if($id) {
            if(isset($this->categories)) {
                $_relation->where('item_id', $id)->where('foreign_table', 'categories')->delete();
            }
            if(isset($this->tags)) {
                $_relation->where('item_id', $id)->where('foreign_table', 'tags')->delete();
            }
        }
		if($id && (!empty($this->categories) || !empty($this->tags))) {
			$_tag = model('App\Modules\Cms\Models\TagModel');
			$_category = model('App\Modules\Cms\Models\EcommerceCategoryModel');
			if(is_array($this->categories)) {
				foreach($this->categories as $category_id) {
					$relation_data = array('item_id'=>$id, 'foreign_table'=>'categories', 'foreign_key'=>$category_id);
					if($category_id > 0) {
						$_relation->insert($relation_data);
						$count = $_relation->where('foreign_table', 'categories')->where('foreign_key', $category_id)->countAllResults();
						$_category->update($category_id, ['count'=>$count]);
					}
				}
				unset($this->categories);
			}
			if(is_array($this->tags)) {
				foreach($this->tags as $tag_id) {
					if(!is_numeric($tag_id)) {
						$slug = clear_utf8($tag_id);
						$check = $_tag->where('slug', $slug)->first();
						if(empty($check->id)) {
							$arg = array('name'=>$tag_id, 'slug'=>$slug);
							$tag = $_tag->insert($arg);
							if(is_numeric($tag)) $tag_id = $tag;
						} else $tag_id = $check->id;
					}
					$tag_id = intval($tag_id);
					if($tag_id > 0) {
						$_relation->insert(array('item_id'=>$id,'foreign_table'=>'tags','foreign_key'=>$tag_id));
						//$count = $_relation->where('foreign_table', 'tags')->where('foreign_key', $tag_id)->countAllResults();
						//$_tag->update($tag_id, ['count'=>$count]);
					}
				}
				unset($this->tags);
			}
		}
		return $data;
    }
    protected function afterFind($data) {
        if(!empty($data['data'])) $_data = $data['data'];
        else $_data = $data;
        if(is_array($_data)) {
            foreach($_data as $key=>$value) {
                $value = $this->process_data($value);
                $_data[$key] = $value;
            }
		} else {
            $_data = $this->process_data($_data);
        }
        if(!empty($data['data'])) $data['data'] = $_data;
        else $data = $_data;
		return $data;
    }
    private function process_data($value) {
        $tags = array();
        $categories = array();
        if(isset($value->relations) && $value->relations) {
            $tag = model('App\Modules\Cms\Models\TagModel');
            $category = model('App\Modules\Cms\Models\EcommerceCategoryModel');
            foreach($value->relations as $item) {
                if($item->foreign_table == 'tags') {
                    $tags[$item->foreign_key] = $tag->select('id,name,slug')->find($item->foreign_key);
                } elseif($item->foreign_table == 'categories') {
                    $categories[$item->foreign_key] = $category->select('id,name,slug,parent')->find($item->foreign_key);
                }
            }
            unset($value->relations);
            $value->tags = $tags;
            $value->categories = $categories;
        }
        if(isset($value->content)) {
            $content = bzdecompress($value->content);
            if(!is_numeric($content)) {
                $value->content = $content;
            }
        }
        return $value;
    }
    public function categories($categories = array())
    {
		$relation = model('App\Modules\Cms\Models\EcommerceRelationModel');
		$post_relations = $relation->whereIn('foreign_key', $categories)->where('foreign_table', 'categories')->findAll();
		$relations = [];
		if(is_array($post_relations)) {
			foreach($post_relations as $item) {
				if(!in_array($item->item_id, $relations))
					$relations[] = $item->item_id;
			}
		}
		if(count($relations)) {
			$this->whereIn('id', $relations);
		}
		return $this;
    }
    public function tags($tags = array())
    {
		$relation = model('App\Modules\Cms\Models\EcommerceRelationModel');
		$post_relations = $relation->whereIn('foreign_key', $tags)->where('foreign_table', 'tags')->findAll();
		$relations = [];
		if(is_array($post_relations)) {
			foreach($post_relations as $item) {
				if(!in_array($item->item_id, $relations))
					$relations[] = $item->item_id;
			}
		}
		if(count($relations)) {
			$this->whereIn('id', $relations);
		}
		return $this;
	}
	// public function get_image($product_image = array()){

	// }
}