<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class ProductModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'products';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'explain_name', 'price', 'image', 'hot', 'status'];
	
	protected $returnType = 'App\Modules\Cms\Models\Entities\Product';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
}