<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class PaymentMobileModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'payment_mobile';
	protected $primaryKey = 'id';
	protected $allowedFields = ['acc_name', 'transaction_id', 'card_type', 'card_seri', 'card_code', 'amount', 'response', 'response_code', 'response_message', 'status'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\PaymentMobile';
	protected $useSoftDeletes = false;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	protected $deletedField  = 'deleted';
}