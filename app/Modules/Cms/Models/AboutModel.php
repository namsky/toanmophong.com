<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class AboutModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'about_us';
	protected $primaryKey = 'id';
	protected $allowedFields = ['title', 'content'];
	
	protected $returnType = 'App\Modules\Cms\Models\Entities\About';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
}