<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class EcommerceDataModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ecommerce_data';
	protected $primaryKey = 'id';
	protected $allowedFields = ['item_id', 'field_id', 'data', 'special_data'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\EcommerceData';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	public function __construct()
	{
		$this->has_one['field'] = ['App\Modules\Cms\Models\EcommerceFieldModel','id','field_id'];
		parent::__construct();
	}
}