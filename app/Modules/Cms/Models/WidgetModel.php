<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class WidgetModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'widgets';
	protected $primaryKey = 'id';
	protected $allowedFields = ['page_id', 'title', 'title_heading', 'category_heading', 'post_heading', 'type', 'hot', 'excludes', 'category', 'tags', 'post_type', 'limit', 'column', 'order'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\Widget';
	protected $useSoftDeletes = false;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	protected $deletedField  = 'deleted';
}