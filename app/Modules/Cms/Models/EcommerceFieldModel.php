<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class EcommerceFieldModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ecommerce_fields';
	protected $primaryKey = 'id';
	protected $allowedFields = ['category_id', 'name', 'type', 'unit', 'options', 'filtered', 'filter_type', 'filter_options', 'nulled'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\EcommerceField';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	public function __construct()
	{
		$this->has_one['category'] = ['App\Modules\Cms\Models\EcommerceCategoryModel','id','category_id'];
		parent::__construct();
	}
}