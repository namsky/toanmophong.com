<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class EcommerceCategoryModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ecommerce_categories';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'count', 'slug', 'order', 'parent', 'summary', 'title', 'description', 'keywords'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\EcommerceCategory';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	public function re_sort($categories)
	{
		if(is_array($categories) && count($categories)) {
			$_categories = [];
			/* Parent */
			foreach($categories as $key=>$category) {
				if($category->parent == 0) {
					$_categories[$category->id] = $category;
					unset($categories[$category->id]);
					foreach($categories as $sub_category) {
						if($sub_category->parent == $category->id) {
							$_categories[$category->id]->child[$sub_category->id] = $sub_category;
							unset($categories[$sub_category->id]);
						}
					}
				}
			}
			$categories = array_merge($_categories, $categories);
		}
		return $categories;
	}
	public function get_list($parent=0)
	{
		$data = $this->where('parent', $parent)->orderBy('order')->findAll();
		if(is_array($data)) {
			foreach($data as $key=>$item) {
				$parent = intval($item->id);
				if($parent > 0) {
					$data[$key]->child = $this->get_list($parent);
				} else {
					$data[$key]->child = null;
				}
			}
		}
		return $data;
	}
	public function get_parents($parent)
	{
		$parents = [];
		if($parent) {
			while(true) {
				$item = $this->find($parent);
				if(!empty($item->id)) {
					$parents[] = $item;
					if(!empty($item->parent)) {
						$parent = $item->parent;
					} else break;
				} else break;
			}
		}
		$parents = array_reverse($parents);
		return $parents;
	}
}