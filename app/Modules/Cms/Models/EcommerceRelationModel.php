<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class EcommerceRelationModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ecommerce_relations';
	protected $primaryKey = 'id';
	protected $allowedFields = ['item_id', 'foreign_table', 'foreign_key'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\EcommerceRelation';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
}