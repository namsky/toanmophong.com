<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class CafeModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'cafes';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'address', 'thumb', 'time', 'order', 'status'];
	
	protected $returnType = 'App\Modules\Cms\Models\Entities\Cafe';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
}