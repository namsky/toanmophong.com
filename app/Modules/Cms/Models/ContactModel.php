<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class ContactModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'contacts';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'phone', 'email', 'message', 'status'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\Contact';
	protected $useSoftDeletes = false;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'updated';
	protected $deletedField  = 'deleted';
	public function __construct()
	{
		parent::__construct();
	}
}