<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class AdsZoneModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ads_zones';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'description', 'width', 'height', 'page'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\AdsZone';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	public function __construct()
	{
		$this->has_many['links'] = ['App\Modules\Cms\Models\AdsLinkModel','zone_id','id'];
		parent::__construct();
	}
}