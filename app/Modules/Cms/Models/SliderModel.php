<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class SliderModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'slider';
	protected $primaryKey = 'id';
	protected $allowedFields = ['slider_name', 'slider_image', 'slider_link','slider_text','type'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\Slider';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;

}