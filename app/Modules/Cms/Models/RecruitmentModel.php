<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class RecruitmentModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'recruitments';
	protected $primaryKey = 'id';
	protected $allowedFields = ['title', 'introduction', 'time', 'age', 'gender', 'address', 'requirement', 'job_description', 'income', 'treatment', 'deadline', 'email', 'phone'];
	
	protected $returnType = 'App\Modules\Cms\Models\Entities\Recruitment';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
}