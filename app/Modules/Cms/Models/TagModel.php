<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class TagModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'tags';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'count', 'slug', 'parent', 'thumb', 'title', 'description', 'keywords'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\Tag';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	public function get_list($parent=0)
	{
		$data = $this->where('parent', $parent)->orderBy('id')->findAll();
		if(is_array($data)) {
			foreach($data as $key=>$item) {
				$parent = intval($item->id);
				if($parent > 0) {
					$data[$key]->child = $this->get_list($parent);
				} else {
					$data[$key]->child = null;
				}
			}
		}
		return $data;
	}
}