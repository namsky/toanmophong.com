<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class PostRelationModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'post_relations';
	protected $primaryKey = 'id';
	protected $allowedFields = ['post_id', 'foreign_table', 'foreign_key'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\PostRelation';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
}