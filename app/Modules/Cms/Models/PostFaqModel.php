<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;
use App\Modules\Cms\Libraries\UserAuth;

class PostFaqModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'post_faqs';
	protected $primaryKey = 'id';
	protected $allowedFields = ['post_id', 'question', 'answer'];
	protected $returnType = 'App\Modules\Cms\Models\Entities\PostFaq';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	public function __construct()
	{
		parent::__construct();
	}
}