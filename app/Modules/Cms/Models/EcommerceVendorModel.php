<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class EcommerceVendorModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ecommerce_vendors';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'thumb', 'category_id'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\EcommerceVendor';
	protected $useSoftDeletes = false;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	protected $deletedField  = 'deleted';
	public function __construct()
	{
		$this->has_one['category'] = ['App\Modules\Cms\Models\EcommerceCategoryModel','id','category_id'];
		parent::__construct();
	}
}