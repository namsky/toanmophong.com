<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class OrderModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'orders';
	protected $primaryKey = 'id';
	protected $allowedFields = ['customer', 'product_name', 'phone', 'address', 'quantity', 'total_money', 'message', 'status'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\Order';
	protected $useSoftDeletes = false;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'updated';
	protected $deletedField  = 'deleted';
	public function __construct()
	{
		parent::__construct();
	}
}