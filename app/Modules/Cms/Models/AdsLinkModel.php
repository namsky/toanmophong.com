<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class AdsLinkModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ads_links';
	protected $primaryKey = 'id';
	protected $allowedFields = ['zone_id', 'banner_id', 'weight', 'expired'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\AdsLink';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	public function __construct()
	{
		$this->has_one['zone'] = ['App\Modules\Cms\Models\AdsZoneModel','id','zone_id'];
		$this->has_one['banner'] = ['App\Modules\Cms\Models\AdsBannerModel','id','banner_id'];
		parent::__construct();
	}
}