<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class OrdersModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'orderss';
	protected $primaryKey = 'id';
	protected $allowedFields = ['customer_id', 'product_id', 'quantity', 'price', 'size', 'color','product_name','product_image'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\Orders';
	public function __construct()
	{
		parent::__construct();
	}
}