<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class UserGroupModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'user_groups';
	protected $primaryKey = 'id';

	protected $returnType = 'App\Modules\Cms\Models\Entities\UserGroup';
	protected $useSoftDeletes = false;

	protected $allowedFields = ['name', 'slug', 'description', 'color', 'level'];

	protected $useTimestamps = false;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	protected $deletedField  = 'deleted';
    public function get_list()
    {
        $query = $this->findAll();
        $items = array();
        foreach($query as $item) {
            $items[$item->id] = $item;
        }
        return $items;
    }
}