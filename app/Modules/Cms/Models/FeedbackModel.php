<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class FeedbackModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'feedbacks';
	protected $primaryKey = 'id';
	protected $allowedFields = ['customer', 'images', 'content'];
	
	protected $returnType = 'App\Modules\Cms\Models\Entities\Feedback';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
}