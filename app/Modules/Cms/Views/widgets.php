<?=$this->section('content');?>
<?php
	$base_url = rebuild_url(1);
?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
							<div class="float-left"></div>
							<form method="GET" id="form-filter">
								<div class="float-right mb-0">
									<div class="float-left ml-2">
										<button type="button" class="btn btn-primary btn-sm" onclick="render()" data-toggle="modal" data-target="#modal-item"><?=lang('Add new');?></button>
									</div>
								</div>
							</form>
						</div>
						<div class="card-content row">
							<div class="col-md-4">
                                <div class="card-title mx-3 mb-0"><h2>HOME</h2></div>
                                <hr class="m-3" />
								<div class="dd nestable m-3" data-page_id="1">
									<?php
									$code = '<ol class="dd-list">';
										foreach ($items_1 as $item) {
											$code .= '<li id="item-'.$item->id.'" class="dd-item" data-id="'.$item->id.'">';
												$code .= '<div class="dd-handle"><i class="fas fa-columns"></i> '.strtoupper($item->type).'&nbsp;&nbsp;&nbsp;'.clearString($item->title, false).'</div>';
												$code .= '<div style="display: inline-block;position: absolute;right: 5px;top: 5px;">';
												$code .= '<span style="margin-right: 5px;font-size: 11px;"></span>';
												$code .= '<span style="margin-right: 5px;font-size: 11px;"><i class="fas fa-list-alt"></i> '.number_format($item->limit,0,',','.').'</span>';
												$code .= '<a class="edit p-1" href="javascript:;" onclick="render('.$item->id.');" data-toggle="modal" data-target="#modal-item"><i class="fa fa-edit"></i></a>';
												$code .= '<a class="delete p-1 text-red" href="javascript:;" onclick="remove('.$item->id.');"><i class="fa fa-times-circle"></i></a></div>';
											$code .= '</li>';
										}
									$code .= '</ol>';
									echo $code;
									?>
								</div>
							</div>
							<div class="col-md-4">
                                <div class="card-title mx-3 mb-0"><h2>WIDGET ARCHIVES</h2></div>
                                <hr class="m-3" />
								<div class="dd nestable m-3" data-page_id="2">
									<?php
									$code = '<ol class="dd-list">';
										foreach ($items_2 as $item) {
											$code .= '<li id="item-'.$item->id.'" class="dd-item" data-id="'.$item->id.'">';
												$code .= '<div class="dd-handle"><i class="fas fa-columns"></i> '.strtoupper($item->type).'&nbsp;&nbsp;&nbsp;'.clearString($item->title, false).'</div>';
												$code .= '<div style="display: inline-block;position: absolute;right: 5px;top: 5px;">';
												$code .= '<span style="margin-right: 5px;font-size: 11px;"></span>';
												$code .= '<span style="margin-right: 5px;font-size: 11px;"><i class="fas fa-list-alt"></i> '.number_format($item->limit,0,',','.').'</span>';
												$code .= '<a class="edit p-1" href="javascript:;" onclick="render('.$item->id.');" data-toggle="modal" data-target="#modal-item"><i class="fa fa-edit"></i></a>';
												$code .= '<a class="delete p-1 text-red" href="javascript:;" onclick="remove('.$item->id.');"><i class="fa fa-times-circle"></i></a></div>';
											$code .= '</li>';
										}
									$code .= '</ol>';
									echo $code;
									?>
								</div>
							</div>
							<div class="col-md-4">
                                <div class="card-title mx-3 mb-0"><h2>WIDGET SINGLE</h2></div>
                                <hr class="m-3" />
								<div class="dd nestable m-3" data-page_id="3">
									<?php
									$code = '<ol class="dd-list">';
										foreach ($items_3 as $item) {
											$code .= '<li id="item-'.$item->id.'" class="dd-item" data-id="'.$item->id.'">';
												$code .= '<div class="dd-handle"><i class="fas fa-columns"></i> '.strtoupper($item->type).'&nbsp;&nbsp;&nbsp;'.clearString($item->title, false).'</div>';
												$code .= '<div style="display: inline-block;position: absolute;right: 5px;top: 5px;">';
												$code .= '<span style="margin-right: 5px;font-size: 11px;"></span>';
												$code .= '<span style="margin-right: 5px;font-size: 11px;"><i class="fas fa-list-alt"></i> '.number_format($item->limit,0,',','.').'</span>';
												$code .= '<a class="edit p-1" href="javascript:;" onclick="render('.$item->id.');" data-toggle="modal" data-target="#modal-item"><i class="fa fa-edit"></i></a>';
												$code .= '<a class="delete p-1 text-red" href="javascript:;" onclick="remove('.$item->id.');"><i class="fa fa-times-circle"></i></a></div>';
											$code .= '</li>';
										}
									$code .= '</ol>';
									echo $code;
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade cms_box" id="modal-item" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form id="form-item">
						<div class="modal-header">
							<h5 class="modal-title">Widget</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
                            <form id="form-item">
                                <div class="row m-0">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="title" class="form-control-label"><?=lang('Title');?></label>
                                            <input class="form-control form-control-sm" type="hidden" value="" id="id" name="id">
                                            <input class="form-control form-control-sm" type="text" value="" id="title" name="title">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="title_heading" class="form-control-label"><?=lang('Title heading');?></label>
                                            <select class="form-control form-control-sm" id="title_heading" name="title_heading">
                                                <option value="">-</option>
                                                <option value="h1">h1</option>
                                                <option value="h2">h2</option>
                                                <option value="h3">h3</option>
                                                <option value="h4">h4</option>
                                                <option value="h5">h5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="type" class="form-control-label"><?=lang('Type');?></label>
                                            <select class="form-control form-control-sm" id="type" name="type">
                                                <option value="blog">Blog</option>
                                                <option value="video">Video</option>
                                                <option value="line">Line</option>
                                                <option value="grid">Grid</option>
                                                <option value="related">Related</option>
                                                <option value="sidebar">Sidebar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="post_type" class="form-control-label"><?=lang('Post Type');?></label>
                                            <select class="form-control form-control-sm" id="post_type" name="post_type">
                                                <option value="1">News</option>
                                                <option value="2">Video</option>
                                                <option value="-1">Page</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="category_heading" class="form-control-label"><?=lang('Category heading');?></label>
                                            <select class="form-control form-control-sm" id="category_heading" name="category_heading">
                                                <option value="">-</option>
                                                <option value="h1">h1</option>
                                                <option value="h2">h2</option>
                                                <option value="h3">h3</option>
                                                <option value="h4">h4</option>
                                                <option value="h5">h5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="post_heading" class="form-control-label"><?=lang('Post heading');?></label>
                                            <select class="form-control form-control-sm" id="post_heading" name="post_heading">
                                                <option value="">-</option>
                                                <option value="h1">h1</option>
                                                <option value="h2">h2</option>
                                                <option value="h3">h3</option>
                                                <option value="h4">h4</option>
                                                <option value="h5">h5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="page_id" class="form-control-label"><?=lang('Page');?></label>
                                            <select class="form-control form-control-sm" id="page_id" name="page_id">
                                                <option value="1">Home</option>
                                                <option value="2">Widget archives</option>
                                                <option value="3">Widget single</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="column" class="form-control-label"><?=lang('Column');?></label>
                                            <input class="form-control form-control-sm" type="number" step="1" min="1" max="12" value="12" id="column" name="column">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="hot" class="form-control-label"><?=lang('HOT');?></label>
                                            <label class="form-toggle custom-toggle custom-toggle-primary">
                                                <input type="checkbox" id="hot" name="hot" value="1">
                                                <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="limit" class="form-control-label"><?=lang('Limit');?></label>
                                            <input class="form-control form-control-sm" type="number" step="1" min="1" max="100" value="6" id="limit" name="limit">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="excludes" class="form-control-label"><?=lang('Excludes');?></label>
                                            <input class="form-control form-control-sm" type="text" value="" id="excludes" name="excludes">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="category" class="form-control-label"><?=lang('Category');?></label>
                                            <select class="form-control form-control-sm" id="category" name="category">
                                                <option value="">NONE</option>
                                                <option value="-1">CURRENT CATEGORY</option>
                                                <?php
                                                if(isset($categories) && is_array($categories))
                                                    foreach($categories as $category) {
                                                        echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="tags" class="form-control-label"><?=lang('Tags');?></label><br />
                                            <select class="form-control tags select2" name="tags[]" id="tags" multiple><?=$options?></select>
                                            <script>
                                            if(typeof($('.tags').select2) !== 'undefined') {
                                                $('.tags').select2({
                                                    tags: true,
                                                    tokenSeparators: [","],
                                                    ajax: {
                                                        url: '<?=$base_url?>/posts/get_tags',
                                                        results: function (data, page) {
                                                            return { results: data.items };
                                                        },
                                                        cache: true,
                                                        dataType: 'json'
                                                    }
                                                });
                                            }
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </form>
						</div>
						<div class="modal-footer">
							<button type="button" id="btn_save" class="btn btn-sm btn-primary" onClick="save();"><?=lang('SAVE');?></button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
		$args = array(
			'field_slug' => 'name',
			'editor' => true,
		);
		$cms->gen_js($args);
		$cms->load_editor('.editor', 300, 'mini');
		?>
		<script>
			$(document).ready(function(e) {
				if ($('.nestable').length && $.fn.nestable) {
					$('.nestable').nestable({maxDepth: 1});
				}
				$('.dd').on('change', function() {
					var data = $(this).nestable('serialize');
                    var page_id = $(this).data('page_id');
					data = window.JSON.stringify(data);
					$.post("<?=current_url();?>/update?page_id="+page_id, {data:data}, function(data,status){});
				});
			});
		</script>
<?=$this->endSection();?>