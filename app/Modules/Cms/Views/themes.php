<?=$this->section('content');?>
<?php
	$base_url = rebuild_url(1);
?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
                <div class="col-sm-6 row">
                    <?php
                    foreach($themes as $theme) {
                        if(!empty($theme['name'])) {
                        $screenshot = !empty($theme['screenshot'])?CDN.'/themes/'.$theme['name'].'/screenshot.jpg':CDN.'/images/no-thumb.png';
                    ?>
                    <div class="col-12 col-lg-6">
                        <div class="card">
                            <img class="card-img-top" height="200px" width="auto" src="<?=$screenshot?>" />
                            <div class="card-body">
                                <h3 class="card-title text-center"><?=strtoupper($theme['name'])?></h3>
                                <?php
                                if(empty($theme['active'])) echo '<a href="javascript:;" class="btn btn-primary d-block mx-auto" onClick="active(\''.$theme['name'].'\')">Active</a>';
                                else echo '<a href="javascript:;" class="btn btn-success d-block mx-auto" disabled>Actived</a>';
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-content">
                            <form method="POST" id="config_themes">
                                <div class="float-left card-title mx-3 mb-0 mt-3"><h2>CONFIG THEMES</h2></div>
                                <div class="float-right text-right my-3 mr-3">
                                    <button type="button" id="btn_save" class="btn btn-primary btn-sm" onClick="$('#config_themes').submit();"><?=lang('Save');?></button>
                                </div>
                                <div class="clearfix"></div>
                                <hr class="m-3" />
                                <div class="col">
                                    <?php
                                    if(!empty($configurations) && is_array($configurations)) {
                                        usort($configurations, "order_sort");
                                        foreach($configurations as $config) {
                                    ?>
                                    <div id="config-<?=$config->id?>" class="form-group row mb-1 config-item">
                                        <label for="<?=$config->code;?>" class="col-md-4 col-form-label form-control-label form-control-sm">
                                            <?=$config->name;?>
                                        </label>
                                        <div class="col-md-8" style="position:relative">
                                            <input class="form-control form-control-sm config_input" type="<?=$config->type?>" name="configs[<?=intval($config->id)?>]" value="<?=$config->value?>" id="<?=$config->code?>" data-id="<?=intval($config->id)?>">
                                            <div class="color-config" id="color-config-<?=intval($config->id)?>" style="background:<?=$config->value?>"></div>
                                        </div>
                                    </div>
                                    <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
			</div>
		</div>
        <script>
            $(function () {
                $('.config_input').colorpicker();
                $('.config_input').on('colorpickerChange', function(event) {
                    var id = $(this).data('id');
                    $('#color-config-'+id).css('background-color', event.color.toString());
                });
            });
            function active(theme) {
                $.post("<?=current_url();?>/active", {theme:theme}, function(data){
                    if(data.status=="success") {
                        swal({
                            title: data.message,
                            type: "success",
                            buttonsStyling: !1,
                            confirmButtonClass: "btn btn-success"
                        });
                    } else {
                        swal({
                            title: data.message,
                            type: "warning",
                            buttonsStyling: !1,
                            confirmButtonClass: "btn btn-primary"
                        });
                    }
                    setTimeout(function(){
                        location.reload();
                    }, 3000);
                });
            }
        </script>
<?=$this->endSection();?>