<div class="footer-col-title-wrap">
	<div class="footer-menu-wrap">
		<ul class="footer-links-container">
		<?
		function show_footer($items, $current) {
			$html = '';
			foreach($items as $item) {
				$class = '';
				if($item->type=="category") {
					$link = category_url($item->value);
					if($item->value == $current) $class = "active";
				}
				elseif($item->type=="post" || $item->type=="page") {
					$link = post_url($item->value);
					if($item->value == $current) $class = "active";
				}
				else {
					$link = $item->value;
					$temp = str_replace('/', '', $link);
					if($temp == $current) $class = "active";
					if($link == '/') $link = URL.'/';
				}
				$label = strip_tags($item->title);
				if(!$label) $label = cms_config('site_name');
				$html .= '<li class="footer-menu-link-wrap '.$class.'"><a href="'.$link.'" class="link-btn footer-menu-link" title="'.$label.'">'.$label.'</a></li>';
			}
			return $html;
		}
		echo show_footer($items, $current);
		?>
		</ul>
	</div>
</div>