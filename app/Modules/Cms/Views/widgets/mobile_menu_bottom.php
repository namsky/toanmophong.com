<div class="footer-col-title-wrap">
		<?
		function show_menu_bottom($items, $current) {
			$html = '';
			foreach($items as $item) {
				$class = '';
				if($item->type=="category") {
					$link = category_url($item->value);
					if($item->value == $current) $class = "active";
				}
				elseif($item->type=="post" || $item->type=="page") {
					$link = post_url($item->value);
					if($item->value == $current) $class = "active";
				}
				else {
					$link = $item->value;
					$temp = str_replace('/', '', $link);
					if($temp == $current) $class = "active";
					if($link == '/') $link = URL.'/';
				}
				$label = strip_tags($item->title);
				if(!$label) $label = cms_config('site_name');
				$html .= '<li><a href="'.$link.'"><span>'.$label.'</span></a></li>';
			}
			return $html;
		}
		echo show_menu_bottom($items, $current);
		?>
        <!-- <li><a  href="/pages/cac-buoc-mua-hang-tren-marc"><span>Hướng dẫn mua hàng</span></a></li>		
        <li><a  href="/pages/quy-dinh-doi-tra"><span>Quy định đổi hàng</span></a></li>		
        <li><a  href="/blogs/tuyen-dung"><span>Tuyển dụng</span></a></li>		
        <li><a  href="/pages/hop-tac-kinh-doanh"><span>Hợp tác kinh doanh</span></a></li> -->
</div>