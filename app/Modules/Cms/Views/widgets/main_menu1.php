
<div class="menu" id="menu">
	<div class="menu-container">
		<div class="menu-wrap">
			<div class="menu-grid">
				<div class="header-close-wrap" id="header-close-wrap">
					<div class="header-menu-text-wrap">
						<span class="header-close-text">Close</span>
					</div>
					<div class="close-btn-wrap">
						<div class="dots-wrap">
							<div class="close-line first-line">
								<span class="close-dot"></span>
								<span class="close-dot hidden"></span>
								<span class="close-dot"></span>
							</div>
							<div class="close-line middle-line">
								<span class="close-dot hidden"></span>
								<span class="close-dot d-none"></span>
								<span class="close-dot hidden"></span>
							</div>
							<div class="close-line last-line">
								<span class="close-dot hover-hidden"></span>
								<span class="close-dot hidden"></span>
								<span class="close-dot hover-hidden"></span>
							</div>
						</div>
					</div>
					<div class="item-overlay"></div>
				</div>
				<?php if(isset($items) && is_array($items) && count($items)) { ?>
				<div class="menu-left-col">
					<div class="section-wrap">
						<div class="inner-left-col">
							<?php foreach($items as $key=>$item) { 
								if($item->type=="category") {
									$link = category_url($item->value);
								} elseif($item->type=="post" || $item->type=="page") {
									$link = post_url($item->value);
								}
								else {
									$link = $item->value;
									if($link == '/') $link = URL.'/';
								}
								if($key < 3) {
							?>
							<div class="menu-item <?=$item->function;?>">
								<div class="bg-image" style="background-image: url('<?=$item->icon;?>');"></div>
								<div class="title-wrap">
									<h2 class="item-title"><span><?=$item->title;?></span></h2>
								</div>
								<a href="<?=$link?>" class="menu-item-link"></a>
								<div class="item-overlay"></div>
							</div>
							<?php } } ?>
						</div>
						<div class="inner-right-col">
							<div class="menu-item products">
								<div class="item-overlay"></div>
							</div>
						</div>
					</div>
					<?php foreach($items as $key=>$item) {
						if($item->type=="category") {
							$link = category_url($item->value);
						} elseif($item->type=="post" || $item->type=="page") {
							$link = post_url($item->value);
						}
						else {
							$link = $item->value;
							if($link == '/') $link = URL.'/';
						}
						if($key == 3) {
					?>
					<div class="menu-item <?=$item->function;?>">
						<div class="bg-image" style="background-image: url('<?=$item->icon;?>');"></div>
						<div class="title-wrap">
							<h2 class="item-title"><span><?=$item->title;?></span></h2>
						</div>
						<a href="<?=$link;?>" class="menu-item-link"></a>
						<div class="item-overlay"></div>
					</div>
					<?php } } ?>
				</div>
				<?php } ?>
				<?php echo widget('NghienCode/Product_menu'); ?>
				<?php if(isset($items) && is_array($items) && count($items)) { ?>
				<div class="menu-right-col">
					<?php foreach($items as $key=>$item) { 
						if($item->type=="category") {
							$link = category_url($item->value);
						} elseif($item->type=="post" || $item->type=="page") {
							$link = post_url($item->value);
						}
						else {
							$link = $item->value;
							if($link == '/') $link = URL.'/';
						}
						if(3 < $key && $key < 6) {
					?>
					<div class="menu-item <?=$item->function?>">
						<div class="bg-image" style="background-image: url('<?=$item->icon;?>');"></div>
						<div class="title-wrap">
							<h2 class="item-title"><span><?=$item->title;?></span></h2>
						</div>
						<a href="<?=$link;?>" class="menu-item-link"></a>
						<div class="item-overlay"></div>
					</div>
					<?php } } ?>
					<div class="menu-item contacts">
						<div class="contacts-wrap">
							<a href="tel:+35924899450" class="menu-contacts-link">
								<span class="link-image-wrap">
									<img src="<?=CDN?>/themes/9chum/images/homepage/footer-phone.svg" alt="icon" class="contacts-link-image" />
								</span>
								<span class="link-text-wrap"><?=cms_config('hotline')?></span>
							</a>
							<div class="btn-wrap">
								<a href="<?=URL?>/lien-he" class="link-btn">
									Liên hệ
								</a>
							</div>
							<a href="/en/contacts" class="menu-item-link"></a>
							<div class="social-wrap">
								<a href="https://www.facebook.com/domainmenada/" class="social-item" target="_blank">
									<img src="<?=CDN?>/themes/9chum/images/menu/facebook.svg" alt="Facebook logo" class="social-image" />
								</a>
							</div>
						</div>
						<div class="item-overlay"></div>
					</div>
					<?php foreach($items as $key=>$item) { 
						if($item->type=="category") {
							$link = category_url($item->value);
						} elseif($item->type=="post" || $item->type=="page") {
							$link = post_url($item->value);
						}
						else {
							$link = $item->value;
							if($link == '/') $link = URL.'/';
						}
						if($key == 6) {
					?>
					<div class="menu-item <?=$item->function?>">
						<div class="title-wrap">
							<h2 class="item-title"><?=$item->title?></h2>
						</div>
						<a href="<?=$link;?>" class="menu-item-link"></a>
						<div class="item-overlay"></div>
					</div>
					<?php } } ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>