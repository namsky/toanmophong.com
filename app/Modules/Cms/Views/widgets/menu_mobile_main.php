<div class="link_list_mobile">
    <ul class="ct-mobile hidden"> </ul>
    <ul class="ct-mobile">
        <?php
            function show_menu_mobile($items, $current, $is_sub=0) {
                $html = '';
                if($is_sub) {
                    $html .= '<ul class="level0 sub-menu" style="display:none;">';
                }
                foreach($items as $key=>$item) {
                    $class = '';
                    $tag = '';
                    $image = '';
                    $class_text = '';
                    if($item->type=="product_category") {
                        $link = product_category_url($item->value);
                        if($item->value == $current) $class = "active";
                        if ($item->icon) {
                            $image = $item->icon;
                        }
                        if($item->parent == 0){
                            $tag = 'span';
                        }else {
                            $tag = 'p';
                            $class_text = 'text-center';
                        }
                    }
                    elseif($item->type=="post" || $item->type=="page") {
                        $link = post_url($item->value);
                        if($item->value == $current) $class = "active";
                        if($item->parent == 0){
                            $tag = 'span';
                        }else {
                            $tag = 'lable';
                        }
                    }
                    else {
                        $link = $item->value;
                        $temp = str_replace('/', '', $link);
                        if($temp == $current) $class = "menu_active";
                        if($link == '/') $link = URL.'/';
                        if($item->parent == 0){
                            $class_a = '';
                            $tag = 'a';
                        }else {
                            $tag = 'a';
                            $class_a = 'class="transitionAll';
                        }
                    }
                    $label = strip_tags($item->title);
                    if(!$label) $label = cms_config('site_name');
                    if(!isset($is_sub)) {
                        if(!$item->child) {
                            $html .= '<li class="a"><a href="'.$link.'" title="'.$label.'">'.$label.'</a></li>';
                        }
                    } else {
                        if(empty($item->child)) {
                            $html .= '<li class="level0 level-top parent level_ico"><a href="'.$link.'" title="'.$label.'">'.$label.'</a></li>';
                        } else {
                            $sub_menu = show_menu_mobile($item->child, $current, 1);
                            $html .= '<li class="level0 level-top parent level_ico">
                                <a href="'.$link.'" title="'.$label.'">'.$label.'</a> <i class="ti-plus hide_close"></i>'.$sub_menu.'
                            </li>';
                        }
                    }
                }
                if(isset($is_sub)){
                    $html .= '</ul>';
                }
                return $html;
            }
            echo show_menu_mobile($items, $current);
        ?>
    </ul>
</div>
