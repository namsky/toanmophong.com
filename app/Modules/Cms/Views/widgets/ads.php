<?php
$multi_ads = [];
foreach($ads as $item) {
	$weight = ($item->weight<=10)?$item->weight:10;
	if($weight > 0) {
		if(!empty($item->banner)) {
			$_ads = [
				'link' => $item->banner->link,
				'image' => $item->banner->image,
				'width' => $item->banner->width,
				'height' => $item->banner->height,
				'code' => $item->banner->code,
				'banner_id' => $item->banner->id,
			];
			for($i=0; $i<$weight; $i++) {
				$multi_ads[] = $_ads;
			}
		}
	}
}
if(count($multi_ads)) {
	$banner = $multi_ads[rand(0, count($multi_ads)-1)];
	$ads_link = URL.'/ads_click/'.$banner['banner_id'].'?zone_id='.$zone_id;
	if(!empty($banner['code'])) {
		if(!empty($banner['link'])) {
			$banner['code'] = str_replace($banner['link'], $ads_link, $banner['code']);
		}
?>
<div class="widget_ads">
	<?=$banner['code']?>
</div>
<?php
	} else {
		$internal_link = false;
		if(strpos($banner['link'], $host_name)) {
			$internal_link = true;
		}
?>
<div class="widget_ads">
	<span class="ad-label">Advertisement</span>
	<a href="<?=$ads_link?>" <?=!$internal_link?'target="_blank" rel="nofollow"':''?>>
		<img alt="ADS" src="<?=$banner['image']?>" <?=$banner['width']?'width="'.$banner['width'].'"':''?> <?=$banner['height']?'height="'.$banner['height'].'"':''?> />
	</a>
</div>
<?php
	}
}
?>