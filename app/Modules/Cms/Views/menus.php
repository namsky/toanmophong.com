<?=$this->section('content');?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
                            <div class="float-right mb-0">
							    <form method="GET" id="form-filter">
									<div class="float-left ml-2">
										<a href="javascript:;" onclick="render();" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-item"><i class="fas fa-plus-circle"></i> NEW</a>
									</div>
							    </form>
                            </div>
						</div>
						<div class="card-content row px-3 pb-3">
                            <div class="col-md-4">
                                <h2 class="text-title-field">Menu</h2>
                                <p class="text-note">Menu hoặc danh sách liên kết website , giúp khách hàng chuyển trang trong cửa hàng của bạn. Bạn có thể tạo các menu lồng nhau để hiện thị drop-down menus</p>
                            </div>
                            <div class="col-md-8">
                                <div class="table-custom table-responsive">
                                    <table class="table align-items-center table-borderless" id="content_table">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Tiêu đề</th>
                                                <th>Danh mục menu</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?
                                            foreach($menus as $menu) {
                                                $menu_item = '';
                                                if(!empty($menu->items)) {
                                                    foreach($menu->items as $item) {
                                                        if($menu_item) $menu_item .= ', ';
                                                        $menu_item .= $item->title;
                                                    }
                                                }
                                            ?>
                                            <tr id="item-<?=$menu->id?>">
                                                <th><a href="<?=rebuild_url(1)?>/menu_item/<?=$menu->id?>"><?=strtoupper($menu->name)?></a></th>
                                                <th class="pre-wrap"><?=$menu_item?$menu_item:'-'?></th>
                                                <th  class="text-center" style="width: 80px">
                                                    <a class="edit p-1" href="javascript:;" onclick="render(<?=$menu->id?>);" data-toggle="modal" data-target="#modal-item"><i class="fa fa-edit"></i></a>
                                                    <a class="delete p-1 text-red" href="javascript:;" onclick="remove(<?=$menu->id?>);"><i class="fa fa-times-circle"></i></a>
                                                </th>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade cms_box" id="modal-item" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form id="form-item" method="POST">
						<div class="modal-header">
							<h5 class="modal-title">Menu</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" value="" id="id" name="id">
                                <label for="name" class="form-control-label"><?=lang('Name');?></label>
                                <input class="form-control" type="text" value="" id="name" name="name" autocomplete="off">
                            </div>
						</div>
						<div class="modal-footer">
							<button type="button" id="btn_save" class="btn btn-sm btn-primary" onClick="save();"><?=lang('Save');?></button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
		$args = array(
			'field_slug' => 'name',
			'editor' => true,
			'field_slug' => false,
		);
		$cms->gen_js($args);
		$cms->load_editor('.editor', 300, 'mini');
		?>
<?=$this->endSection();?>