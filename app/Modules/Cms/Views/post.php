<?=$this->section('content');?>
<?php
	$base_url = isset($item->id)?rebuild_url(2):rebuild_url(1);
	$root_url = isset($item->id)?rebuild_url(3):rebuild_url(2);
?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
							<div class="float-left">
                                <a href="<?=$base_url?>" class="btn btn-primary btn-sm"><i class="fas fa-angle-left"></i> <?=lang('Back');?></a>
                            </div>
							<form method="GET" id="form-filter">
								<div class="float-right mb-0">
									<div class="float-left ml-2">
										<a href="<?=$base_url.'/item'?>" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> NEW</a>
									</div>
								</div>
							</form>
						</div>
						<form id="form-item" method="POST">
							<div class="card-content row px-3">
								<div class="col-lg-9 col-md-8">
									<div class="form-group mb-1">
										<label for="title" class="form-control-label"><?=lang('Title');?></label>
										<input class="form-control form-control-sm" type="hidden" value="<?=isset($item->id)?$item->id:''?>" id="id" name="id">
										<input class="form-control form-control-sm" type="text" value="<?=isset($item->title)?$item->title:''?>" id="title" name="title">
									</div>
									<div class="form-group mb-1">
										<label for="slug" class="form-control-label"><?=lang('Slug');?></label>
										<input class="form-control form-control-sm" type="text" value="<?=isset($item->slug)?$item->slug:''?>" id="slug" name="slug">
										<?php if(!empty($item->slug)) { ?>
										<a href="<?=post_url($item)?>" target="_blank" class="goto_post"><span class="badge badge-success">VIEW</span></a>
										<?php } ?>
									</div>
									<div class="form-group mb-0">
										<textarea class="editor" id="content" name="content"><?=isset($item->content)?$item->content:''?></textarea>
									</div>
									<div class="form-group">
										<label for="summary" class="form-control-label"><?=lang('Summary');?></label>
										<textarea class="form-control form-control-sm" id="summary" name="summary" rows="4"><?=isset($item->summary)?trim($item->summary):''?></textarea>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group mb-0">
												<label for="seo_title" class="form-control-label"><?=lang('Seo Title');?></label>
												<input class="form-control form-control-sm" type="text" id="seo_title" name="seo_title" value="<?=isset($item->seo_title)?$item->seo_title:''?>">
											</div>
										</div>
										<div class="col-md-6 col-sm-12">
											<div class="form-group">
												<label for="seo_description" class="form-control-label"><?=lang('Description');?></label>
												<input class="form-control form-control-sm" type="text" id="seo_description" name="seo_description" value="<?=isset($item->seo_description)?$item->seo_description:''?>">
											</div>
										</div>
										<div class="col-md-6 col-sm-12">
											<div class="form-group">
												<label for="seo_keywords" class="form-control-label"><?=lang('Keywords');?></label>
												<input class="form-control form-control-sm" type="text" id="seo_keywords" name="seo_keywords" value="<?=isset($item->seo_keywords)?$item->seo_keywords:''?>">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<label for="private_download" class="form-control-label"><?=lang('Private Download');?></label>
												<input class="form-control form-control-sm" type="text" id="private_download" name="private_download" value="<?=isset($item->private_download)?$item->private_download:''?>">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group mb-0">
												<label for="faqs" class="form-control-label float-left"><i class="far fa-question-circle"></i> <?=lang('FAQs');?></label>
												<label for="faqs" class="form-control-label float-right">
													<a href="javascript:;" onclick="add_faq();"><i class="fas fa-plus-circle"></i></a>
												</label>
												<div class="clearfix"></div>
											</div>
											<div class="form-group row">
												<table class="table table-sm table-borderless">
													<tr>
														<th>Question</th>
														<th>Answer</th>
														<th></th>
													</tr>
													<tbody id="faq_content">
														<?php
														if(!empty($item->faqs)) {
															foreach($item->faqs as $faq) {
														?>
														<tr id="faq-<?=$faq->id?>">
															<td>
																<input type="hidden" id="faq_id_<?=$faq->id?>" name="faq_id[]" value="<?=$faq->id?>">
																<input class="form-control form-control-sm" type="text" id="question_0" name="faq_question[]" value="<?=$faq->question?>">
															</td>
															<td>
																<input class="form-control form-control-sm" type="text" id="answer_0" name="faq_answer[]" value="<?=$faq->answer?>">
															</td>
															<td class="text-right">
																<a class="btn btn-danger btn-sm" href="javascript:;" onclick="remove_faq(<?=$faq->id?>)">
																	<i class="fas fa-minus-circle"></i>
																</a>
															</td>
														</tr>
														<?php
															}
														} else {
														?>
														<tr id="faq-0">
															<td>
																<input type="hidden" id="faq_id_0" name="faq_id[]" value="">
																<input class="form-control form-control-sm" type="text" id="question_0" name="faq_question[]" value="">
															</td>
															<td>
																<input class="form-control form-control-sm" type="text" id="answer_0" name="faq_answer[]" value="">
															</td>
															<td class="text-right">
																<a class="btn btn-danger btn-sm" href="javascript:;" onclick="remove_faq(0)">
																	<i class="fas fa-minus-circle"></i>
																</a>
															</td>
														</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-4">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group mb-1">
                                                <input class="form-control btn btn-primary" type="submit" value="<?=lang('SAVE');?>">
                                            </div>
                                        </div>
										<div class="col-md-6">
											<div class="form-group mb-1">
												<label for="status" class="form-control-label mb-0"><?=lang('Status');?></label>
												<select class="form-control form-control-sm" type="text" id="status" name="status">
													<option value="1" <?=(isset($item->status) && $item->status==1)?'selected':''?>>Public</option>
													<option value="0" <?=(isset($item->status) && $item->status==0)?'selected':''?>>Pending</option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group mb-1">
												<label for="type" class="form-control-label mb-0"><?=lang('Type');?></label>
												<select class="form-control form-control-sm" type="text" id="type" name="type">
													<option value="1" <?=(isset($item->type) && $item->type==1)?'selected':''?>>News</option>
													<option value="2" <?=(isset($item->type) && $item->type==2)?'selected':''?>>Video</option>
													<option value="-1" <?=(isset($item->type) && $item->type==-1)?'selected':''?>>Page</option>
												</select>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group mb-1">
												<?php
													if(!empty($item->user)) {
														$name = ($item->user->name)?$item->user->name:$item->user->username;
														$options = '<option value="'.$item->user->id.'" selected="selected">'.$name.'</option>';
													}
												?>
												<label for="user_id" class="form-control-label"><?=lang('Author');?></label><br />
												<select class="form-control user_id" name="user_id" id="user_id"><?=$options?></select>
												<script>
													$('.user_id').select2({
														ajax: {
															url: '<?=$root_url?>/users/search_users',
															results: function (data, page) {
																return { results: data.items };
															},
															cache: true,
															dataType: 'json'
														}
													});
												</script>
											</div>
										</div>
									</div>
									<div class="form-group mb-3 row">
										<div class="col-6 pr-0">
											<label for="published" class="form-control-label mb-0"><?=lang('Date public');?></label>
										</div>
										<div class="col-3 pl-2 pr-2">
											<label for="published_hour" class="form-control-label mb-0"><?=lang('Hour');?></label>
										</div>
										<div class="col-3 pl-0">
											<label for="published_min" class="form-control-label mb-0"><?=lang('Min');?></label>
										</div>
										<div class="col-6 pr-0">
											<input autocomplete="off" name="published" id="published" type="text" value="<?=(isset($item->published) && $item->published)?date("d/m/Y", $item->published):date("d/m/Y");?>" class="form-control form-control-sm datepicker" />
										</div>
										<div class="col-3 pl-2 pr-2">
											<input autocomplete="off" name="published_hour" id="published_hour" type="number" max="24" value="<?=(isset($item->published) && $item->published)?date("G", $item->published):date("G");?>" class="form-control form-control-sm text-center" />
										</div>
										<div class="col-3 pl-0">
											<input autocomplete="off" name="published_min" id="published_min" type="number" max="59" value="<?=(isset($item->published) && $item->published)?date("i", $item->published):date("i");?>" class="form-control form-control-sm text-center" />
										</div>
									</div>
									<hr class="m-2" />
									<div class="mb-1 mt-2">
                                        <div class="col-6 pl-0 float-left">
                                            <label for="hot" class="form-control-label float-left mr-3 mb-0"><?=lang('HOT');?></label>
                                            <label class="form-toggle custom-toggle custom-toggle-danger float-left">
                                                <?
                                                    $check = '';
                                                    if(isset($item->hot)) {
                                                        if($item->hot) $check = 'checked';
                                                    } else $check = 'checked';
                                                ?>
                                                <input type="checkbox" id="hot" name="hot" <?=$check;?> value="1">
                                                <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                            </label>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-6 pr-0 float-left">
                                            <label for="is_comments" class="form-control-label float-left mb-0"><?=lang('Comments');?></label>
                                            <label class="form-toggle custom-toggle custom-toggle-primary float-right">
                                                <?
                                                    $check = '';
                                                    if(isset($item->is_comments)) {
                                                        if($item->is_comments) $check = 'checked';
                                                    } else $check = 'checked';
                                                ?>
                                                <input type="checkbox" id="is_comments" name="is_comments" <?=$check;?> value="1">
                                                <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                            </label>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
									<div class="form-group mb-0">
										<label for="is_adsense" class="form-control-label float-left"><?=lang('Ads');?></label>
										<label class="form-toggle custom-toggle custom-toggle-primary float-right">
											<?
												$check = '';
												if(isset($item->is_adsense)) {
													if($item->is_adsense) $check = 'checked';
												} else $check = 'checked';
											?>
											<input type="checkbox" id="is_adsense" name="is_adsense" <?=$check;?> value="1">
											<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
										</label>
										<div class="clearfix"></div>
									</div>
									<div class="form-group mb-0">
										<label for="is_search" class="form-control-label float-left"><?=lang('Search');?></label>
										<label class="form-toggle custom-toggle custom-toggle-primary float-right">
											<?
												$check = '';
												if(isset($item->is_search)) {
													if($item->is_search) $check = 'checked';
												} else $check = 'checked';
											?>
											<input type="checkbox" id="is_search" name="is_search" <?=$check;?> value="1">
											<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
										</label>
										<div class="clearfix"></div>
									</div>
									<div class="form-group mb-1 row">
                                        <div class="col-8">
                                            <label for="sitemap_priority" class="form-control-label"><?=lang('Sitemap priority');?></label>
                                        </div>
                                        <div class="col-4">
                                            <input class="form-control form-control-sm text-center" type="number" step="0.1" max="1" min="0.1" value="<?=isset($item->sitemap_priority)?$item->sitemap_priority:0.5?>" id="sitemap_priority" name="sitemap_priority">
                                        </div>
									</div>
									<hr class="m-2" />
									<div class="form-group mb-0">
										<label for="hosted_image" class="form-control-label float-left"><?=lang('Self-hosted images');?></label>
										<label class="form-toggle custom-toggle custom-toggle-danger float-right">
											<input type="checkbox" id="hosted_image" name="hosted_image" value="0">
											<span class="custom-toggle-slider rounded-circle" data-label-off="OFF" data-label-on="ON"></span>
										</label>
										<div class="clearfix"></div>
									</div>
									<div class="form-group post-item-thumb">
										<label for="thumb" class="form-control-label"><?=lang('Thumb');?></label>
										<? if(cms_config('upload_image')) { ?>
										<label class="btn btn-default btn-sm float-right">
											<i class="fa fa-upload" aria-hidden="true"></i> Upload
											<input type="file" class="thumb_upload" data-target="thumb" accept="image/*" hidden>
										</label>
										<? } ?>
										<input class="form-control form-control-sm" type="text" id="thumb" name="thumb" value="<?=isset($item->thumb)?$item->thumb:''?>">
										<div class="post_thumb">
											<? if(isset($item->thumb) && $item->thumb) { ?>
											<img class="thumb_preview" src="<?=$item->thumb?>" />
											<? } else { ?>
											<img class="thumb_preview" src="<?=CDN?>/images/no-thumb.png" />
											<? } ?>
											<div class="loading"></div>
										</div>
									</div>
									<hr class="m-2" />
									<div class="form-group">
										<?php
											$tags = [];
											$options = '';
											if(isset($item->tags) && $item->tags) {
												foreach($item->tags as $tag) {
													if(isset($tag->name)) {
														$tags[] = $tag->id;
														$options .= '<option value="'.$tag->id.'" selected="selected">'.$tag->name.'</option>';
													}
												}
											}
											$tags = implode(",", $tags);
										?>
										<label for="tags" class="form-control-label"><?=lang('Tags');?></label><br />
										<select class="form-control tags select2" name="tags[]" id="tags" multiple><?=$options?></select>
										<script>
										if(typeof($('.tags').select2) !== 'undefined') {
											$('.tags').select2({
												tags: true,
												tokenSeparators: [","],
												ajax: {
													url: '<?=$root_url?>/tags/search_tags',
													results: function (data, page) {
														return { results: data.items };
													},
													cache: true,
													dataType: 'json'
												}
											});
										}
										</script>
									</div>
									<div class="form-group">
										<label for="categories" class="form-control-label"><?=lang('Categories');?></label>
										<div class="categories">
											<?php
												$current_categories = [];
												if(isset($item->categories) && $item->categories) {
													foreach($item->categories as $category) {
														$current_categories[] = $category->id;
													}
												}
												echo $cms->show_checkbox_categories($categories, $current_categories);
											?>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script data-minify-level="3">
			$(function(){
				/* Get Slug */
				$("#title").keyup(function (e) {
					$("#btn_save").prop('disabled', true);
					var id = $("#id").val();
					var data = $(this).val();
					if(data) {
						$.post("<?=$base_url;?>/get_slug", {id:id, data:data}, function(data) {
							//data = JSON.parse(result);
							if(data.status==true) {
								$("#slug").val(data.slug);
							} else {
								notify("", data.message, 'warning', true);
							}
							$("#btn_save").prop('disabled', false);
						});
					}
				});

				/* Upload thumb */
				$(".thumb_upload").change(function() {
					var target = $(this).data("target");
					uploadImage(this, target);
				});
				$("#thumb").change(function() {
					var thumb = $(this).val();
					$(".thumb_preview").attr("src", thumb);
				});
				function uploadImage(input, target) {
					if(input.files && input.files[0]) {
						var reader = new FileReader();
						reader.onload = function (e) {
							$("#"+target).addClass('loading'); 
						};
						reader.readAsDataURL(input.files[0]);
						var form_data = new FormData();
						form_data.append('image', input.files[0]);
						$.ajax({
							url: '<?=URL?>/v-manager/uploads/photo',
							type: 'POST',
							data: form_data,
							contentType: false,
							processData: false,
							success: function(response){
								if(response != 0) {
									//response = JSON.parse(response);
									if(response.status == 'success') {
										console.log(0);
										if ($("#"+target).is("textarea")) {
											var current = $("#"+target).html();
											if(current) current += "\n";
											$("#"+target).html(current+response.location); 
										} else {
											$("#"+target).val(response.location);
										}
										$("#"+target).change(); 
									} else alert(response.message);
								} else {
									alert(response);
								}
								$("#"+target).removeClass('loading'); 
							},
						});
					}
				}
			});
			function remove_faq(id) {
				$("#faq-"+id).remove();
			}
			function add_faq() {
				var rand_id = Math.floor((Math.random() * 999999) + 100000);
				var html = '\
					<tr id="faq-'+rand_id+'">\
						<td>\
							<input type="hidden" id="faq_id_'+rand_id+'" name="faq_id[]" value="">\
							<input class="form-control form-control-sm" type="text" id="question_'+rand_id+'" name="faq_question[]" value="">\
						</td>\
						<td>\
							<input class="form-control form-control-sm" type="text" id="answer_'+rand_id+'" name="faq_answer[]" value="">\
						</td>\
						<td class="text-right">\
							<a class="btn btn-danger btn-sm" href="javascript:;" onclick="remove_faq('+rand_id+')">\
								<i class="fas fa-minus-circle"></i>\
							</a>\
						</td>\
					</tr>\
				';
				$("#faq_content").append(html);
			}
		</script>
		<? $cms->load_editor('.editor');?>
<?=$this->endSection();?>