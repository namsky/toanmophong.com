<?//=$this->extend('layouts/main');?>
<?=$this->section('content');?>
<div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
							<div class="float-left"></div>
							<form method="GET" id="form-filter">
								<div class="float-right mb-0">
									<div class="float-left ml-2">
										<button type="button" class="btn btn-primary btn-sm" onclick="render()" data-toggle="modal" data-target="#modal-item"><?=lang('Add_new');?></button>
									</div>
								</div>
							</form>
						</div>
						<div class="card-content row">
                            <table class="container table table-hover">
                                <thead class="thead-light">
                                <tr class="text-center">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?=$cms->show_slider($items);?>
                                </tbody>
                            </table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade cms_box" id="modal-item" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form id="form-item" method="POST">
						<div class="modal-header">
							<h5 class="modal-title"></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
                            <div class="row m-0">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="slider_name" class="form-control-label"><?=lang('Name');?></label>
                                        <input class="form-control form-control-sm" type="hidden" value="" id="id" name="id">
                                        <input class="form-control form-control-sm" type="text" value="" id="slider_name" name="slider_name">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="slider_name" class="form-control-label"><?=lang('Type');?></label>
                                        <select class="form-control" id="sel1" id="type" name="type">
                                            <option value="sliderTop">Slider Top</option>
                                            <option value="bannerTop">Banner Top</option>
                                            <option value="bannerBottom">Banner Bottom</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="slider_image" class="form-control-label"><?=lang('Image');?></label>
                                        <input class="form-control form-control-sm" type="text" value="" id="slider_image" name="slider_image">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="slider_link" class="form-control-label"><?=lang('Link');?></label>
                                        <input class="form-control form-control-sm" type="text" value="" id="slider_link" name="slider_link">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="slider_text"><?=lang('Text');?></label>
                                        <textarea class="form-control" rows="5" id="slider_text" name="slider_text" value=""></textarea>
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="modal-footer">
							<button type="button" id="btn_save" class="btn btn-sm btn-primary" onClick="save();"><?=lang('Save');?></button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
		$args = array(
		);
		$cms->gen_js($args);
		?>
		<script>
			
		</script>
<?=$this->endSection();?>