<?=$this->section('content');?>
<?php
	$base_url = isset($item->id)?rebuild_url(2):rebuild_url(1);
	$root_url = isset($item->id)?rebuild_url(3):rebuild_url(2);
?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
							<div class="float-left">
                                <a href="<?=$base_url?>" class="btn btn-primary btn-sm"><i class="fas fa-angle-left"></i> <?=lang('Back');?></a>
                            </div>
							<form method="GET" id="form-filter">
								<div class="float-right mb-0">
									<div class="float-left ml-2">
										<a href="<?=$base_url.'/item'?>" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> NEW</a>
									</div>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>
						<form id="form-item" method="POST">
							<div class="card-content row px-3">
								<div class="col-lg-9 col-md-8">
									<div class="row">
										<div class="col-sm-12 col-md-6 mb-2">
											<div class="form-group mb-0">
												<label for="name" class="form-control-label"><?=lang('Name');?></label>
												<input class="form-control form-control-sm" type="hidden" value="<?=isset($item->id)?$item->id:''?>" id="id" name="id">
												<input class="form-control form-control-sm" type="text" value="<?=isset($item->name)?$item->name:''?>" id="name" name="name">
											</div>
										</div>
										<div class="col-sm-12 col-md-6 mb-2">
											<div class="form-group mb-0">
												<label for="slug" class="form-control-label"><?=lang('Slug');?></label>
												<input class="form-control form-control-sm" type="text" value="<?=isset($item->slug)?$item->slug:''?>" id="slug" name="slug">
												<?php if(!empty($item->slug)) { ?>
												<a href="<?=product_url($item)?>" target="_blank" class="goto_post"><span class="badge badge-success">VIEW</span></a>
												<?php } ?>
											</div>
										</div>
										<div class="col-md-4 mb-2">
											<div class="form-group mb-0">
												<label for="category_id" class="form-control-label"><?=lang('Type');?></label>
												<select class="form-control form-control-sm" type="text" id="category_id" name="category_id">
                                                <?php
                                                $current_category = !empty($item->category_id)?$item->category_id:0;
                                                echo $cms->dropdown_categories($categories, 0, $current_category, false);
                                                ?>
                                                </select>
											</div>
										</div>
										<div class="col-sm-12 mt-2">
											<div class="form-group">
												<label for="content" class="form-control-label"><?=lang('Content');?></label>
												<textarea class="editor" id="content" name="content"><?=isset($item->content)?$item->content:''?></textarea>
											</div>
										</div>
										<div class="col-sm-12 col-md-12 mb-2">
											<div class="form-group mb-0">
												<label for="video" class="form-control-label"><?=lang('Link youtube');?></label>
                                                <textarea class="form-control form-control-sm" type="text" id="video" name="video" required ><?=isset($item->video)?$item->video:''?></textarea>
											</div>
										</div>
										<!-- <div class="col-sm-12">
											<div class="form-group mb-0">
												<label for="faqs" class="form-control-label float-left"><i class="far fa-question-circle"></i> <?=lang('FAQs');?></label>
												<label for="faqs" class="form-control-label float-right">
													<a href="javascript:;" onclick="add_per();"><i class="fas fa-plus-circle"></i></a>
												</label>
												<div class="clearfix"></div>
											</div>
										</div> -->
										<!-- Color & Image -->
										
									</div>
								</div>
								<div class="col-lg-3 col-md-4">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group mb-1">
                                                <input class="form-control btn btn-primary" type="submit" value="<?=lang('SAVE');?>">
                                            </div>
                                        </div>
										<div class="col-md-6">
											<div class="form-group mb-1">
												<label for="status" class="form-control-label mb-0"><?=lang('Status');?></label>
												<select class="form-control form-control-sm" type="text" id="status" name="status">
													<option value="1" <?=(isset($item->status) && $item->status==1)?'selected':''?>>Public</option>
													<option value="0" <?=(isset($item->status) && $item->status==0)?'selected':''?>>Pending</option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group mb-1">
												<label for="priority" class="form-control-label mb-0">Thứ tự sắp xếp</label>
												<input class="form-control form-control-sm number_format" value="<?=isset($item->priority)?$item->priority:''?>" type="text" id="priority" name="priority" />
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group mb-1">
												<label for="price" class="form-control-label mb-0"><?=lang('Price');?></label>
												<input class="form-control form-control-sm number_format" value="<?=isset($item->price)?show_number($item->price):''?>" type="text" id="price" name="price" />
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group mb-1">
												<label for="origin_price" class="form-control-label mb-0"><?=lang('Origin price');?></label>
												<input class="form-control form-control-sm number_format" value="<?=isset($item->origin_price)?show_number($item->origin_price):''?>" type="text" id="origin_price" name="origin_price" />
											</div>
										</div>
									</div>
									<hr class="m-2" />
									<div class="mb-1 mt-2">
                                        <div class="col-4 pl-0 float-left">
                                            <label for="hot" class="form-control-label float-left mr-3 mb-0"><?=lang('HOT');?></label>
                                            <label class="form-toggle custom-toggle custom-toggle-danger float-left">
                                                <?
                                                    $check = '';
                                                    if(isset($item->hot)) {
                                                        if($item->hot == 1) $check = 'checked';
                                                    } else $check = '';
                                                ?>
                                                <input type="checkbox" id="hot" name="hot" <?=$check;?> value="1">
                                                <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                            </label>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-8 pr-0 float-left">
                                            <label for="hosted_image" class="form-control-label float-left"><?=lang('Self-hosted images');?></label>
                                            <label class="form-toggle custom-toggle custom-toggle-danger float-right">
                                                <input type="checkbox" id="hosted_image" name="hosted_image" value="1">
                                                <span class="custom-toggle-slider rounded-circle" data-label-off="OFF" data-label-on="ON"></span>
                                            </label>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
									<div class="form-group post-item-thumb">
										<label for="thumb" class="form-control-label"><?=lang('Thumb');?></label>
										<? if(cms_config('upload_image')) { ?>
										<label class="btn btn-default btn-sm float-right">
											<i class="fa fa-upload" aria-hidden="true"></i> Upload
											<input type="file" class="thumb_upload" data-target="thumb" accept="image/*" hidden>
										</label>
										<? } ?>
										<input class="form-control form-control-sm" type="text" id="thumb" name="thumb" value="<?=isset($item->thumb)?$item->thumb:''?>">
										<div class="post_thumb">
											<? if(isset($item->thumb) && $item->thumb) { ?>
											<img class="thumb_preview" src="<?=$item->thumb?>" />
											<? } else { ?>
											<img class="thumb_preview" src="<?=CDN?>/images/no-thumb.png" />
											<? } ?>
											<div class="loading"></div>
										</div>
									</div>
									<div class="form-group post-item-file">
										<label for="file" class="form-control-label"><?=lang('File tải về');?></label>
<!--                                        <input type="file" name="file_upload" class="file_upload">-->
                                        <label class="btn btn-default btn-sm float-right">
                                            <i class="fa fa-upload" aria-hidden="true"></i> Upload
                                            <input type="file" class="file_upload" data-target="file_download" hidden>
                                        </label>
                                        <input class="form-control form-control-sm" type="text" id="file_download" name="file_download" value="<?=isset($item->file_download)?$item->file_download:''?>">
									</div>
									<hr class="m-2" />
                                    <!-- <div class="form-group post-item-thumb">
                                        <label for="price" class="form-control-label mb-0"><?=lang('SKU');?></label>
                                        <input class="form-control form-control-sm number_format" value="<?=isset($item->sku)?($item->sku):''?>" type="text" id="sku" name="sku" />
                                    </div> -->
									<!-- <div class="form-group post-item-thumb">
                                        <label for="color" class="form-control-label mb-0"><?=lang('Màu sắc');?></label>
                                        <select class="form-control form-control-sm" type="text" id="color" name="color">
                                            <option value="black" <?=(isset($item->color) && $item->color=='black')?'selected':''?>>Đen</option>
                                            <option value="white" <?=(isset($item->color) && $item->color=='white')?'selected':''?>>Trắng</option>
                                            <option value="red" <?=(isset($item->color) && $item->color=='red')?'selected':''?>>Đỏ</option>
                                            <option value="blue" <?=(isset($item->color) && $item->color=='blue')?'selected':''?>>Xanh lục</option>
                                            <option value="green" <?=(isset($item->color) && $item->color=='green')?'selected':''?>>Xanh lá cây</option>
                                            <option value="yellow" <?=(isset($item->color) && $item->color=='yellow')?'selected':''?>>Vàng</option>
                                            <option value="brown" <?=(isset($item->color) && $item->color=='brown')?'selected':''?>>Nâu</option>
                                            <option value="violet" <?=(isset($item->color) && $item->color=='violet')?'selected':''?>>Tím</option>
                                            <option value="grey" <?=(isset($item->color) && $item->color=='grey')?'selected':''?>>Xám</option>
                                        </select>
									</div> -->
                                    <!-- <div class="form-group post-item-thumb">
                                        <label for="version" class="form-control-label mb-0"><?=lang('Phiên bản');?></label>
                                        <select class="form-control form-control-sm" type="text" id="version" name="version">
                                            <option value="Old Skool" <?=(isset($item->version) && $item->version=='Old Skool')?'selected':''?>>Old Skool</option>
                                            <option value="Slip-on" <?=(isset($item->version) && $item->version=='Slip-on')?'selected':''?>>Slip-on</option>
                                            <option value="Authentic" <?=(isset($item->version) && $item->version=='Authentic')?'selected':''?>>Authentic</option>
                                            <option value="Era" <?=(isset($item->version) && $item->version=='Era')?'selected':''?>>Era</option>
                                            <option value="Sk8" <?=(isset($item->version) && $item->version=='Sk8')?'selected':''?>>Sk8</option>
                                        </select>
                                    </div> -->
                                    <!-- <div class="form-group post-item-thumb">
                                        <label for="brand" class="form-control-label mb-0"><?=lang('Thương hiệu');?></label>
                                        <select class="form-control form-control-sm" type="text" id="brand" name="brand">
                                            <option value="VANS" <?=(isset($item->brand) && $item->brand=='VANS')?'selected':''?>>VANS</option>
                                            <option value="VANS VAULT" <?=(isset($item->brand) && $item->brand=='VANS VAULT')?'selected':''?>>VANS VAULT</option>
                                        </select>
                                    </div> -->
									<hr class="m-2" />
									<div class="form-group">
										<label for="categories" class="form-control-label"><?=lang('Categories');?></label>
										<div class="categories">
											<?php
												$current_categories = [];
												if(isset($item->categories) && $item->categories) {
													foreach($item->categories as $category) {
														$current_categories[] = $category->id;
													}
												}
												echo $cms->show_checkbox_categories($categories, $current_categories);
											?>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script data-minify-level="3">
			$(function(){
				/* Get Slug */
				$(".number_format").keyup(function (e) {
					var number = $(this).val();
					$(this).val(number_format(number));
				});
				$("#name").keyup(function (e) {
					$("#btn_save").prop('disabled', true);
					var id = $("#id").val();
					var data = $(this).val();
					if(data) {
						$.post("<?=$base_url;?>/get_slug", {id:id, data:data}, function(data) {
							//data = JSON.parse(result);
							if(data.status==true) {
								$("#slug").val(data.slug);
							} else {
								notify("", data.message, 'warning', true);
							}
							$("#btn_save").prop('disabled', false);
						});
					}
				});
                <?php
                    echo 'var data = {};';
                    if(!empty($item->data) && is_array($item->data)) {
                        foreach($item->data as $item_data) {
                            echo 'data['.$item_data->field_id.'] = "'.$item_data->data.'";';
                        }
                    }
                    if(!empty($item->category_id)) {
                        echo 'get_fields('.$item->category_id.', data);';
                    }
                ?>
                function get_fields(category_id, data) {
                    $.ajax({url: '<?=$root_url?>/ecommerce_fields/get_fields?category_id='+category_id, success: function(results) {
                        $("#field_data").html('');
                        $.each(results['results'], function(key, value) {
                            var input = '';
                            var field_value = '';
                            if(typeof(data[value['id']]) !== 'undefined') {
                                field_value = data[value['id']];
                            }
                            switch(value['type']) {
                                case 'select':
                                    options = value['options'].split(',');
                                    input = '<select class="form-control form-control-sm" type="text" id="field_'+value['id']+'" name="fields['+value['id']+']">';
                                    input += '<option value="">None</option>';
                                    $.each(options, function(_key, option) {
                                        var selected = '';
                                        if(field_value == option) selected = 'selected';
                                        input += '<option value="'+option+'" '+selected+'>'+option+value['unit']+'</option>';
                                    });
                                    input += '</select>';
                                    break;
                                case 'number':
                                    input = '<input class="form-control form-control-sm" id="field_'+value['id']+'" name="fields['+value['id']+']" type="number" value='+field_value+'>';
                                    break;
                                default:
                                    input = '<input class="form-control form-control-sm" id="field_'+value['id']+'" name="fields['+value['id']+']" type="text" value='+field_value+'>';
                                    break;
                            }
                            var unit = (value['unit'])?value['unit']:'';
                            var html = `
                            <div class="col-md-4 mb-2">
                                <div class="form-group mb-0">
                                    <label for="field_`+value['id']+`" class="form-control-label">`+value['name']+`</label>
                                    <label class="float-right">`+unit+`</label>
                                    `+input+`
                                </div>
                            </div>`;
                            $("#field_data").append(html);
                        });
                    }, dataType: "json"});
                }
                $("#category_id").on('focusin', function(){
                    $(this).data('val', $(this).val());
                });
				$("#category_id").change(function() {
                    var category_id = $(this).val();
                    var prev = $(this).data('val');
                    $(this).data('val', category_id);
                    $("#category_"+prev).prop("checked", false);
                    $("#category_"+category_id).prop("checked", true);
                    get_fields(category_id, data);
                });
                
				/* Upload thumb */
				$(".screenshot_upload").change(function() {
					var target = $(this).data("target");
					screenshot_upload(this, target);
				});
				$(".thumb_upload").change(function() {
					var target = $(this).data("target");
					uploadImage(this, target);
				});
                $(".file_upload").change(function() {
                    var id = $(this).data("target");
                    uploadFile(this, id);
                });
				$("#thumb").change(function() {
					var thumb = $(this).val();
					$(".thumb_preview").attr("src", thumb);
				});
				$(".thumb_hover_upload").change(function() {
					var target = $(this).data("target");
					uploadImage(this, target);
				});
				$("#thumb_hover").change(function() {
					var thumb_hover = $(this).val();
					$(".thumb_hover_preview").attr("src", thumb_hover);
				});
				$("#menu_image").change(function() {
					var menu_image = $(this).val();
					$(".menu_image_preview").attr("src", menu_image);
				});
				$("#background").change(function() {
					var background = $(this).val();
					$(".background_preview").attr("src", background);
				});
                /* Upload File */
                function uploadFile(input, id) {
                    if(input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("#"+id).addClass('loading');
                        };
                        reader.readAsDataURL(input.files[0]);
                        var form_data = new FormData();
                        form_data.append('file', input.files[0]);
                        $.ajax({
                            url: '<?=URL?>/v-manager/uploads/file',
                            type: 'POST',
                            data: form_data,
                            contentType: false,
                            processData: false,
                            success: function(response){
                                if(response != 0) {
                                    if(response.status == 'success') {
                                        if ($("#"+id).is("textarea")) {
                                            var current = $("#"+id).html();
                                            if(current) current += "\n";
                                            $("#"+id).html(current+response.location);
                                        } else {
                                            $("#"+id).val(response.location);
                                        }
                                    } else alert(response.message);
                                } else {
                                    alert(response);
                                }
                                $("#"+id).removeClass('loading');
                            },
                        });
                    }
                }

				function screenshot_upload(input, target) {
				if(input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$("."+target).addClass('loading'); 
					};
					reader.readAsDataURL(input.files[0]);
					var form_data = new FormData();
					form_data.append('image', input.files[0]);
					$.ajax({
						url: '<?=URL?>/v-manager/uploads/photo',
						type: 'POST',
						data: form_data,
						contentType: false,
						processData: false,
						success: function(response){
							if(response != 0) {
								//response = JSON.parse(response);
								if(response.status == 'success') {
									console.log(0);
									if ($("#"+target).is("textarea")) {
										var current = $("#"+target).html();
										if(current) current += "\n";
										$("#"+target).html(current+response.location); 
									} else {
										// $(("#"+target).append('<p>nội dung thêm vào</p>');
										$("."+target).val(response.location);
									}
									$("#"+target).change(); 
								} else alert(response.message);
							} else {
								alert(response);
							}
							// $("#"+target).removeClass('loading'); 
						},
					});
				}
			}

				function uploadImage(input, target) {
					if(input.files && input.files[0]) {
						var reader = new FileReader();
						reader.onload = function (e) {
							$("#"+target).addClass('loading'); 
						};
						reader.readAsDataURL(input.files[0]);
						var form_data = new FormData();
						form_data.append('image', input.files[0]);
						$.ajax({
							url: '<?=URL?>/v-manager/uploads/photo',
							type: 'POST',
							data: form_data,
							contentType: false,
							processData: false,
							success: function(response){
								if(response != 0) {
									//response = JSON.parse(response);
									if(response.status == 'success') {
										console.log(0);
										if ($("#"+target).is("textarea")) {
											var current = $("#"+target).html();
											if(current) current += "\n";
											$("#"+target).html(current+response.location); 
										} else {
											$("#"+target).val(response.location);
										}
										$("#"+target).change(); 
									} else alert(response.message);
								} else {
									alert(response);
								}
								$("#"+target).removeClass('loading'); 
							},
						});
					}
				}
			});
			function remove_per(id) {
				$("#per-"+id).remove();
			}
			function add_per() {
				var rand_id = Math.floor((Math.random() * 999999) + 100000);
				var html = '\
					<tr id="per-'+rand_id+'">\
						<td>\
						    <input type="hidden" id="per_id_'+rand_id+'" name="per_id[]" value="">\
							<input class="form-control form-control-sm" type="text" placeholder="30,31,32,33,..." id="size_'+rand_id+'" name="size[]" value="">\
						</td>\
						<td>\
							<input class="form-control form-control-sm" type="text" placeholder="1,2,3,.." id="stock_'+rand_id+'" name="stock[]" value="">\
						</td>\
						<td class="text-right">\
							<a class="btn btn-danger btn-sm" href="javascript:;" onclick="remove_per('+rand_id+')">\
								<i class="fas fa-minus-circle"></i>\
							</a>\
						</td>\
					</tr>\
				';
				$("#per_content").append(html);
			}
			function remove_color(id) {
				$("#color-"+id).remove();
			}
			function add_color() {
				var rand_id = Math.floor((Math.random() * 999999) + 100000);
				var html = '\
					<tr id="color-'+rand_id+'">\
						<td>\
							<input type="hidden" id="per_id_'+rand_id+'" name="per_id[]" value="">\
							<input class="form-control form-control-sm" type="color" id="color_'+rand_id+'" name="color[]" value="">\
						</td>\
						<td>\
						<label class="btn btn-default btn-sm float-right">\
							<i class="fa fa-upload" aria-hidden="true"></i> Upload\
							<input type="file" class="screenshot_upload_add" data-target="image_'+rand_id+'" accept="image/*" hidden>\
						</label>\
						<textarea class="form-control" rows="3" placeholder="Image" id="image_'+rand_id+'" name="image[]" value=""></textarea>\
						</td>\
						<td class="text-right">\
							<a class="btn btn-danger btn-sm" href="javascript:;" onclick="remove_color('+rand_id+')">\
								<i class="fas fa-minus-circle"></i>\
							</a>\
						</td>\
					</tr>\
				';
				$("#color_content").append(html);
				$(".screenshot_upload_add").change(function() {
					var target = $(this).data("target");
					screenshot_upload_add(this, target);
				});
				function screenshot_upload_add(input, target) {
				if(input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$("."+target).addClass('loading'); 
					};
					reader.readAsDataURL(input.files[0]);
					var form_data = new FormData();
					form_data.append('image', input.files[0]);
					$.ajax({
						url: '<?=URL?>/v-manager/uploads/photo',
						type: 'POST',
						data: form_data,
						contentType: false,
						processData: false,
						success: function(response){
							if(response != 0) {
								//response = JSON.parse(response);
								if(response.status == 'success') {
									console.log(0);
									if ($("#"+target).is("textarea")) {
										var current = $("#"+target).html();
										if(current) current += "\n";
										$("#"+target).html(current+response.location); 
									} else {
										// $(("#"+target).append('<p>nội dung thêm vào</p>');
										$("."+target).val(response.location);
									}
									$("#"+target).change(); 
								} else alert(response.message);
							} else {
								alert(response);
							}
							// $("#"+target).removeClass('loading'); 
						},
					});
				}
			}
			}
			
			
		</script>
		<? $cms->load_editor('.editor');?>
<?=$this->endSection();?>