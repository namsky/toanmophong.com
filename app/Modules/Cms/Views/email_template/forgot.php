<div style="max-width: 450px;margin:0 auto">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td style="border-bottom:1px solid #e6e6e6;font-size:18px;padding:28px 0">Forgot your Password</td>
			</tr>
			<tr>
				<td style="font-size:14px;line-height:30px;padding:20px 0;color:#666">
					Welcome to {site_name}!<br>Click the link below to reset <span class="il">password</span>:
				</td>
			</tr>
			<tr>
				<td style="padding:5px 0">
					<a href="{link}" style="padding:10px 28px;background:#34495f;color:#fff;text-decoration:none" target="_blank"><span class="il">Reset</span> Password</a>
				</td>
			</tr>
			<tr>
				<td style="padding:20px 0 0 0;line-height:26px;color:#666">If this activity is not your own operation, please contact us immediately.</td>
			</tr>
			<tr>
				<td>
					<a style="color:#34495f" href="{URL}" target="_blank">{URL}</a>
				</td>
			</tr>
			<tr>
				<td style="padding:30px 0 15px 0;font-size:12px;color:#999;line-height:20px">{site_name} Team<br>Automated message. Please do not reply</td>
			</tr>
		</tbody>
	</table>
</div>