<?=$this->section('content');?>
<h1 class="text-center">Chi tiết đơn hàng</h1>
<div class="table-responsive">
        <h5 class="text-center pt-3 pb-1">Thông tin khách hàng</h5>
        <table class="table table-bordered">
            <thead class="border">
            <tr class="text-center border">
                <th scope="col">Mã đơn</th>
                <th scope="col">Tên khách hàng</th>
                <th scope="col">Số điện thoại</th>
                <th scope="col">Quê quán</th>
                <th scope="col">Ghi chú</th>
                <th scope="col">Ngày lên đơn</th>
            </tr>
            </thead>
            <tbody>
                <tr class="text-center">
                    <td>#<?=$customer->id?></td>
                    <td><?=$customer->name?></td>
                    <td><?=$customer->phone?></td>
                    <td><?=$customer->address?> - <?=$customer->ward?> - <?=$customer->district?> - <?=$customer->city?></td>
                    <td><?=$customer->note?></td>
                    <td><?=$customer->created?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <h5 class="text-center pt-3">Thông tin sản phẩm</h5>
    <table class="table table-bordered">
        <thead>
        <tr class="text-center">
            <th scope="col">STT</th>
            <th scope="col">Tên sản phẩm</th>
            <th scope="col">Ảnh</th>
            <th scope="col">Size</th>
            <th scope="col">Giá</th>
            <th scope="col">Số lượng</th>
            <th scope="col">Thành tiền</th>
        </tr>
        </thead>
        <tbody>
        <?php $stt=1; $totalPrice = 0;?>
        <?php foreach($order as $item){
            $totalPrice = $totalPrice + $item->price*$item->quantity;
            ?>
            <tr class="text-center">
                <th scope="row"><?=$stt++?></th>
                <td><?=$item->product_name?></td>
                <td><img src="<?=$item->product_image?>" width="100"></td>
                <td><?=$item->size?></td>
                <td><?=number_format($item->price)?> đ</td>
                <td><?=$item->quantity?></td>
                <td><?=number_format($item->price*$item->quantity)?>đ</td>
            </tr>
        <?php } ?>
        </tbody>
        <tr>
            <td></td>
            <td></td>
            <td class="text-right font-weight-bold" colspan="2">Tổng:</td>
            <td class="text-center"><?=number_format($totalPrice)?>đ</td>
        </tr>
    </table>
    <div class="container pt-3">
        <form action="/v-manager/orders/update" method="POST">
            <input type="hidden" name="item_id" value="<?=$customer->id?>">
<!--            <div class="form-group col-sm-4 mb-5">-->
<!--                <label for="status">Tình trạng</label>-->
<!--                <select class="form-control" id="status" name="status">-->
<!--                    <option --><?//=$customer->status == 0 ? "selected = 'selected'" : "" ?><!-- value="0" >Chưa xử lý</option>-->
<!--                    <option --><?//=$customer->status == 1 ? "selected = 'selected'" : "" ?><!-- value="1" >Đã xử lý</option>-->
<!--                </select>-->
<!--            </div>-->
            <input type="submit" class="btn btn-success w-100" value="Xong">
        </form>
    </div>
<?=$this->endSection();?>