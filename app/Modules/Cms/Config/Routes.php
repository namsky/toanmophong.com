<?php
/**
 * --------------------------------------------------------------------
 * Auth Routing
 * --------------------------------------------------------------------
**/


$routes->group('v-manager', ['namespace' => 'App\Modules\Cms\Controllers'], function($routes) {
	/* News CMS */
		// Ads Manager
		$routes->add('ads', 'Ads::index');
		$routes->add('ads/(:any)', 'Ads::$1');
		$routes->add('ads_banners', 'Ads_banners::index');
		$routes->add('ads_banners/(:any)', 'Ads_banners::$1');
		$routes->add('ads_zones', 'Ads_zones::index');
		$routes->add('ads_zones/(:any)', 'Ads_zones::$1');
		// Widgets
		$routes->add('widgets', 'Widgets::index');
		$routes->add('widgets/(:any)', 'Widgets::$1');
		// Categories
		$routes->add('categories', 'Categories::index');
		$routes->add('categories/(:any)', 'Categories::$1');
		// Tags
		$routes->add('tags', 'Tags::index');
		$routes->add('tags/(:any)', 'Tags::$1');
		// Menus
		$routes->add('menus', 'Menus::index');
		$routes->add('menus/(:any)', 'Menus::$1');
		// Menu item
		$routes->add('menu_item', 'MenuItem::index');
		$routes->add('menu_item/(:num)', 'MenuItem::index/$1');
		$routes->add('menu_item/(:any)', 'MenuItem::$1');
		// Posts
		$routes->add('posts', 'Posts::index');
		$routes->add('posts/(:any)', 'Posts::$1');
		// Pages
		$routes->add('pages', 'Pages::index');
		$routes->add('pages/(:any)', 'Pages::$1');
		// Users
		$routes->add('users', 'Users::index');
		$routes->add('users/(:any)', 'Users::$1');
		// User groups
		$routes->add('groups', 'Groups::index');
		$routes->add('groups/(:any)', 'Groups::$1');
		// Comments
		$routes->add('comments', 'Comments::index');
		$routes->add('comments/(:any)', 'Comments::$1');
		// Themes
		$routes->add('themes', 'Themes::index');
		$routes->add('themes/(:any)', 'Themes::$1');
		// Languages
		$routes->add('languages', 'Languages::index');
		$routes->add('languages/(:any)', 'Languages::$1');
		// Contacts
		$routes->add('contacts', 'Contacts::index');
		$routes->add('contacts/(:any)', 'Contacts::$1');
		// Orders
		$routes->add('orders', 'Orders::index');
		$routes->add('orders/(:any)', 'Orders::$1');
		// Orders
		$routes->add('payments', 'Payments::index');
		$routes->add('payments/(:any)', 'Payments::$1');
		// Photos
		$routes->add('photos', 'Photos::index');
		$routes->add('photos/(:any)', 'Photos::$1');
    /* eCommerce CMS */
		// Ecommerce Products
		$routes->add('ecommerce_products', 'EcommerceProducts::index');
		$routes->add('ecommerce_products/(:any)', 'EcommerceProducts::$1');
		// Ecommerce Vendors
		$routes->add('ecommerce_vendors', 'EcommerceVendors::index');
		$routes->add('ecommerce_vendors/(:any)', 'EcommerceVendors::$1');
		// Ecommerce Categories
		$routes->add('ecommerce_categories', 'EcommerceCategories::index');
		$routes->add('ecommerce_categories/(:any)', 'EcommerceCategories::$1');
		// Ecommerce Fields
		$routes->add('ecommerce_fields', 'EcommerceFields::index');
		$routes->add('ecommerce_fields/(:any)', 'EcommerceFields::$1');

	/* Slider */
		$routes->add('slider','Slider::index');
		$routes->add('slider/(:any)', 'Slider::$1');
});
// API for Frontend
$routes->add('api/(:segment)/(:segment)', '\App\Modules\Cms\Controllers\Api::$1/$2');

/*
    CUSTOM THEME
    Don't edit this block, Please using Backend -> Themes
*/
define('ACTIVE_THEME','NghienCode');
$routes->setDefaultNamespace('App\Modules\\'.ACTIVE_THEME.'\Controllers');