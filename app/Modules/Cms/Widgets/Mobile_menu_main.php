<?php namespace App\Modules\Cms\Widgets;
use App\Core\Cms\CmsWidget;

class Mobile_menu_main extends CmsWidget
{
    function index($args=[]) {
		$menuModel = model('App\Modules\Cms\Models\MenuItemModel');
		$menu_id = isset($args['menu_id'])?intval($args['menu_id']):1;
		
		/* Load from cache */
		//$cached = cms_config('cache');
		$items = false;
		if(!$items) {
			/* Load from db */
			$items = $menuModel->get_list(1);
			$this->cache->save('menu_'.$menu_id, $items, 86400);
		}
		
		$request = \Config\Services::request();
		$uri = $request->uri;
		$current_segment = $uri->getSegment(1);
		$this->view->setVar('items', $items);
		$this->view->setVar('current', $current_segment);
		return $this->view->render('menu_mobile_main');
    }
}