<?php namespace App\Modules\Cms\Widgets;
use App\Core\Cms\CmsWidget;

class Ads extends CmsWidget
{
    function index($args) {
		$adsModel = model('App\Modules\Cms\Models\AdsLinkModel');
		$bannerModel = model('App\Modules\Cms\Models\AdsBannerModel');
		$zone_id = isset($args['zone_id'])?intval($args['zone_id']):1;
		/* Load from cache */
		$cached = cms_config('cache');
		$ads = $cached?$this->cache->get('ads_'.$zone_id):false;
		if(!$ads) {
			/* Load from db */
			$ads = $adsModel->with('banner')->where('zone_id', $zone_id)->groupStart()->where('expired>', time())->orWhere('expired', 0)->groupEnd()->findAll();
			$this->cache->save('ads_'.$zone_id, $ads, 86400);
		}
		$this->view->setVar('ads', $ads);
		$this->view->setVar('zone_id', $zone_id);
		$request = \Config\Services::request();
		$this->view->setVar('host_name', $request->getServer('HTTP_HOST'));
		return $this->view->render('ads');
    }
}