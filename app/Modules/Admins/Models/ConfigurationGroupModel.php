<?php namespace App\Modules\Admins\Models;

use CodeIgniter\Model;

class ConfigurationGroupModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'configuration_group';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'code', 'icon', 'order'];

	protected $returnType = 'App\Modules\Admins\Models\Entities\ConfigurationGroup';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	
	public function __construct()
	{
		$this->has_many['configurations'] = ['App\Modules\Admins\Models\ConfigurationModel','in_group','id'];
		parent::__construct();
	}
    public function get_config($item = null)
    {
		$config = $this->where('code', $item)->first();
		if(isset($config->value) && $config->value)
			return $config->value;
		return NULL;
    }
}