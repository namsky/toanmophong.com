<?php namespace App\Modules\Admins\Models;

use CodeIgniter\Model;

class IpAttemptModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ip_attempts';
	protected $primaryKey = 'id';

	protected $returnType = 'App\Modules\Admins\Models\Entities\IpBanned';

	protected $allowedFields = ['ip_address', 'last_attempt', 'attempts'];

	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
}