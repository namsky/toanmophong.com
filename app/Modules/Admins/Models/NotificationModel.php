<?php namespace App\Modules\Admins\Models;

use CodeIgniter\Model;

class NotificationModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'notifications';
	protected $primaryKey = 'id';

	protected $returnType = 'App\Modules\Admins\Models\Entities\Notification';
	protected $useSoftDeletes = false;

	protected $allowedFields = ['admin_id', 'title', 'message', 'target_link', 'thumb', 'is_readed'];

	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
}