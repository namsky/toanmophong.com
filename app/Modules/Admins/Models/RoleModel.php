<?php namespace App\Modules\Admins\Models;

use CodeIgniter\Model;

class RoleModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'roles';
	protected $primaryKey = 'id';

	protected $returnType = 'App\Modules\Admins\Models\Entities\Role';
	protected $useSoftDeletes = false;

	protected $allowedFields = ['name', 'slug', 'description', 'color', 'level'];

	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	protected $deletedField  = 'deleted';
    public function get_list()
    {
        $query = $this->findAll();
        $roles = array();
        foreach($query as $role) {
            $roles[$role->id] = $role;
        }
        return $roles;
    }
}