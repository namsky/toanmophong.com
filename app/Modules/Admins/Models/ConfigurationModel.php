<?php namespace App\Modules\Admins\Models;

use CodeIgniter\Model;

class ConfigurationModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'configurations';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'code', 'value', 'description', 'type', 'in_group', 'extra_data', 'order'];

	protected $returnType = 'App\Modules\Admins\Models\Entities\Configuration';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
    public function get_config($item = null)
    {
		$config = $this->where('code', $item)->first();
		if(isset($config->value) && $config->value)
			return $config->value;
		return NULL;
    }
}