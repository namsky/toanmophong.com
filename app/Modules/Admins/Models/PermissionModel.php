<?php namespace App\Modules\Admins\Models;

use CodeIgniter\Model;

class PermissionModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'permissions';
	protected $primaryKey = 'id';
	protected $returnType = 'App\Modules\Admins\Models\Entities\Permission';
	
	protected $allowedFields = ['name', 'slug', 'description', 'function', 'group', 'order'];

	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	protected $dateFormat = 'int';
	
	public function __construct()
	{
		$this->has_many['data'] = ['App\Modules\Admins\Models\PermissionRoleModel','permission_id','id'];
		parent::__construct();
	}
    public function validate_permission($user_id, $slug=NULL, $function=NULL)
    {
		$pRoleModel = model('App\Modules\Admins\Models\PermissionRoleModel');
		$adminModel = model('App\Modules\Admins\Models\AdminModel');
		
        $admin = $adminModel->find($user_id);
        if(isset($admin->role_id)) {
			if($slug) {
				$permission = $this->where('slug', $slug)->first();
				if(!isset($permission->id) && strpos($slug, '.')) {
					$arr_slug = explode('.', $slug);
					if(count($arr_slug)>2) {
						unset($arr_slug[count($arr_slug)-1]);
						$slug = $arr_slug[0].'.'.$arr_slug[1];
						$permission = $this->where('slug', $slug)->first();
					}
				}
			} else {
				$permission = $this->where('function', $function)->first();
			}
			if(isset($permission->id)) {
				$data = $pRoleModel->where('role_id', $admin->role_id)->where('permission_id', $permission->id)->first();
				if(isset($data->value) && $data->value == 1) {
					return true;
				}
			}
		}
        return false;
    }
}