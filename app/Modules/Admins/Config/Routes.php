<?php
/**
 * --------------------------------------------------------------------
 * Auth Routing
 * --------------------------------------------------------------------
**/

$routes->group('v-manager', ['namespace' => 'App\Modules\Admins\Controllers'], function($routes) {
	// Dashboard
	$routes->get('/', 'Dashboard::index');
	$routes->add('account', 'Account::index');
	$routes->post('account/active_auth', 'Account::active_auth');
	$routes->get('access_denied', 'Access_denied::index');
	// Configurations
	$routes->get('configurations', 'Configurations::index');
	$routes->add('configurations/(:any)', 'Configurations::$1');
	// Admins
	$routes->get('admins', 'Admins::index');
	$routes->add('admins/(:any)', 'Admins::$1');
	// Roles
	$routes->get('roles', 'Roles::index');
	$routes->add('roles/(:any)', 'Roles::$1');
	// Permissions
	$routes->get('permissions', 'Permissions::index');
	$routes->add('permissions/(:any)', 'Permissions::$1');
	// IP Alloweds
	$routes->get('ip_alloweds', 'Ip_alloweds::index');
	$routes->add('ip_alloweds/(:any)', 'Ip_alloweds::$1');
	// IP Banneds
	$routes->get('ip_banneds', 'Ip_banneds::index');
	$routes->add('ip_banneds/(:any)', 'Ip_banneds::$1');
	// Access logs
	$routes->get('access_logs', 'Access_logs::index');
	$routes->add('access_logs/(:any)', 'Access_logs::$1');

    // Login/out
    $routes->add('login', 'AuthController::login');
    $routes->get('logout', 'AuthController::logout');
	
	// API Cached
	$routes->get('cached', 'Cached::index');
	$routes->add('cached/(:any)', 'Cached::$1');
	// API Uploads
	$routes->get('uploads', 'Uploads::index');
	$routes->add('uploads/(:any)', 'Uploads::$1');
});