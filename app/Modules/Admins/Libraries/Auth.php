<?php
namespace App\Modules\Admins\Libraries;
use CodeIgniter\HTTP\Request;
use CodeIgniter\HTTP\UserAgent;
use App\Modules\Admins\Libraries\GoogleAuthenticator;

class Auth {
	protected $prefix;
    protected $auth_password_hash_type = 'md5';
    protected $auth_password_hash_salt;
	protected $handle;
	protected $session;
	protected $cookie;
	protected $permission;
	protected $model;
	protected $cache;
	public function __construct()
	{
		// Most services in this controller require
		// handle is "session" or "cookie"
		$this->handle = 'session';
		// the session to be started - so fire it up!
		$this->session = session();
		$this->permission = model('App\Modules\Admins\Models\PermissionModel');
		$this->model = model('App\Modules\Admins\Models\AdminModel');
		$this->cache = \Config\Services::cache();
		$this->prefix = 'admin';
		//$this->config = config('Auth');
	}
	protected function get_data($key) {
		if($this->handle == 'cookie') {
			helper('cookie');
			$encrypter = \Config\Services::encrypter();
			$auth_data = get_cookie($this->prefix.'_auth');
			if($auth_data) {
				try {
					$authed = decrypt($auth_data);
					$authed = json_decode($authed, true);
				} catch(Exception $e) {
					$authed = [];
				}
				return isset($authed[$key])?$authed[$key]:null;
			} else return null;
		} else {
			return $this->session->get($key);
		}
	}
	protected function set_data($data, $ttl = NULL) {
		$request = \Config\Services::request();
		$domain = $request->getServer('SERVER_NAME');
		if($this->handle == 'cookie') {
			helper('cookie');
			$encrypter = \Config\Services::encrypter();
			if(is_array($data)) {
				$auth_data = get_cookie($this->prefix.'_auth');
				$authed = false;
				if($auth_data) {
					$_authed = decrypt($auth_data);
					$authed = json_decode($_authed, true);
				}
				if(!is_array($authed)) $authed = [];
				foreach($data as $key=>$value) {
					$authed[$key] = $value;
				}
				$encrypted = encrypt(json_encode($authed));
				if($ttl) return set_cookie($this->prefix.'_auth', $encrypted, $ttl, '.'.$domain, '/');
				else return set_cookie($this->prefix.'_auth', $encrypted, 30*MINUTE, '.'.$domain, '/');
			} else return false;
		} else {
			return $this->session->set($data);
		}
	}
	protected function remove_data($key) {
		if($this->handle != 'cookie') {
			return $this->session->remove($key);
		}
	}
    public function change_password($new_pass)
    {
		$account_id = $this->get_id();
		if($account_id) {
			return $this->model->update($account_id, ['password' => $new_pass]);
		} else return false;
    }
    public function get_account()
    {
        if($this->get_data($this->prefix.'_id')) {
            return [
				'id' => $this->get_data($this->prefix.'_id'),
				'name' => $this->get_data($this->prefix.'_name'),
				'username' => $this->get_data($this->prefix.'_username'),
				'avatar' => $this->get_data($this->prefix.'_avatar'),
			];
		}
        else
            return false;
    }
    public function is_logged()
    {
        if($this->get_data($this->prefix.'_id'))
            return true;
        else
            return false;
    }
    public function auth_code($auth_code)
    {
		$email = $this->get_data($this->prefix.'_email');
		if($email) {
			$user = $this->model->where('email', $email)->where('status', 1)->first();
			if(isset($user->id))
			{
				$gg_auth = new GoogleAuthenticator();
				$verifyCode = $gg_auth->verifyCode($user->auth_secret, $auth_code, 1);
				if($verifyCode == false) {
					$this->_add_attempt();
					return ['status'=>'error', 'message'=>cms_lang('auth_code_error')];
				} else {
					$this->_clear_attempt();
					$this->_set_session($user);
					return ['status'=>'success', 'message'=>cms_lang('login_success')];
				}
			}
		}
	}
    /**
     * This function sets an regular account login.
     *
     * @param string $username
     * @param string $password
     * @param bool $remember
     * @param string $backlink
     * @return array
     */
    public function login($username, $password, $remember=false)
    {
        if(cms_config('auth_enable_secure')) {
			$_is_banned = $this->_is_banned();
			if($_is_banned === true)
				return ['status'=>'error', 'message'=>cms_lang('ip_blocked_forever')];
			elseif($_is_banned > time())
				return ['status'=>'error', 'message'=>cms_lang('ip_blocked_ultil').'<br><center>'.show_date($_is_banned, false).'</center>'];
		}
        $user = $this->model->where('username', $username)->where('status', 1)->first();
        if(isset($user->id))
        {
            if($this->check_hash_password($password, $user->salt, $user->password)) {
				if($user->auth_status) {
					$this->set_data([$this->prefix.'_email' => $user->email]);
					return ['status'=>'auth_require', 'message'=>''];
				} else {
					$this->_clear_attempt();
					$this->_set_session($user, $remember);
				}
                return ['status'=>'success', 'message'=>cms_lang('login_success')];
            } else {
                $this->_add_attempt();
                return ['status'=>'error', 'message'=>cms_lang('invalid_password')];
            }
        } else {
            if($this->_num_attempts() >= cms_config('auth_max_attempts'))
            {
                if(cms_config('auth_enable_secure') == TRUE) $this->_ban_ip();
            }
            else $this->_add_attempt();
            return ['status'=>'error', 'message'=>cms_lang('account_not_exist', [$username])];
        }
    }
    /**
     * This function register new account.
     *
     * @param string $username
     * @param bool $email
     * @param string $password
     * @return array
     */
    public function register($args)
    {
		if(empty($args['username']) || empty($args['email']) || empty($args['password'])) {
			return ['status'=>'error', 'message'=>cms_lang('input_invalid')];
		}
		//Secure auth
        if(cms_config('auth_enable_secure')) {
			$_is_banned = $this->_is_banned();
			if($_is_banned === true)
				return ['status'=>'error', 'message'=>cms_lang('ip_blocked_forever')];
			elseif($_is_banned > time())
				return ['status'=>'error', 'message'=>cms_lang('ip_blocked_ultil').'<br><center>'.show_date($_is_banned, false).'</center>'];
		}
		//Check Input
		if(strlen($args['username']) < 6) return ['status'=>'error', 'message'=>cms_lang('account_min_chars', 6)];
		elseif(strlen($args['username']) > 16) return ['status'=>'error', 'message'=>cms_lang('account_max_chars', 16)];
        if(!preg_match("/^([-a-z0-9_])+$/i", $args['username'])) return ['status'=>'error', 'message'=>cms_lang('account_is_invalid')];
        $args['username'] = strtolower($args['username']);
        if(!(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $args['email']))) return ['status'=>'error', 'message'=>cms_lang('email_is_invalid')];
        
        $confirm = !empty($args['confirm'])?$args['confirm']:null;
        $is_valid_password = $this->check_valid_password($args['password'], $confirm);
        if($is_valid_password !== true) {
            return ['status'=>'error', 'message'=>$is_valid_password];
        }
		if(!empty($args['phone'])) {
			$args['phone'] = str_replace([' ', '-', '.', ',', '+'], '', $args['phone']);
			if(strlen($args['phone'])<9) return ['status'=>'error', 'message'=>cms_lang('phone_must_greater', 9)];
			if(strlen($args['phone'])>12) return ['status'=>'error', 'message'=>cms_lang('phone_must_less', 12)];
			if(!is_numeric($args['phone'])) return ['status'=>'error', 'message'=>cms_lang('phone_invalid')];
		}
		
        $user = $this->model->where('username', $args['username'])->orWhere('email', $args['email'])->first();
        if(isset($user->id)) {
			if($user->username == $args['username']) {
				return ['status'=>'error', 'message'=>cms_lang('account_is_exist', [$args['username']])];
			}
			else {
				return ['status'=>'error', 'message'=>cms_lang('email_is_exist', [$args['email']])];
			}
        } else {
            if(!empty($args['confirm'])) unset($args['confirm']);
            $verify_email = cms_config('verify_email');
            if($verify_email) {
                $activate_code = md5($args['username'].$this->_generate_hash());
                $args['activate_code'] = $activate_code;
                $args['group_id'] = 1;
            } else {
                $args['group_id'] = 2;
            }
            $args['status'] = 1;
            $args['ip_address'] = get_ip();
            //Insert user to DB
            $user_id = $this->model->insert($args);
            if($user_id) {
                $account = $this->model->find($user_id);
                $this->_set_session($account);
                if($verify_email) {
                    // Send Verify email
                    $site_name = cms_config('site_name');
                    $data = [
                        'link' => URL.'/account/confirm_email?code='.$activate_code,
                        'action' => 'xác nhận tài khoản',
                        'text_button' => 'Xác nhận',
                        'site_name' => $site_name,
                        'url' => URL,
                        'username' => $account->username,
                    ];
                    $message = view('App\Modules\Cms\Views\email_template\verify_code', $data);
                    $subject = mb_strtoupper(cms_config('site_name')).' - XÁC NHẬN EMAIL TÀI KHOẢN';
                    $send_mail = sendmail($account->email, cms_config('smtp_from'), cms_config('smtp_reply'), $site_name, $subject, $message);
                    if($send_mail) return array('status'=>'success', 'message'=>cms_lang('check_email_to_verify', $account->email));
                    else return ['status'=>'error', 'message'=>cms_lang('send_verify_email_error')];
                } else {
                    return ['status'=>'success', 'message'=>cms_lang('register_success')];
                }
            }
			else {
				return ['status'=>'error', 'message'=>cms_lang('some_thing_went_wrong')];
			}
        }
    }
    public function forgot($account) {

    }
    /**
     * Logout user from login.
     *
     * @return bool
     */
    public function logout()
    {
        $data = array(
            $this->prefix.'_id' => 0,
            $this->prefix.'_name' => NULL,
            $this->prefix.'_username' => NULL,
            $this->prefix.'_email' => NULL,
            $this->prefix.'_role' => 0,
            $this->prefix.'_avatar' => NULL,
            $this->prefix.'_created' => 0
        );
        $this->set_data($data);
		if($this->handle == 'cookie') {
			helper('cookie');
			delete_cookie($this->prefix.'_auth');
		} else {
			$this->remove_data($this->prefix.'_id');
			$this->remove_data($this->prefix.'_name');
			$this->remove_data($this->prefix.'_username');
			$this->remove_data($this->prefix.'_email');
			$this->remove_data($this->prefix.'_role');
			$this->remove_data($this->prefix.'_avatar');
			$this->remove_data($this->prefix.'_created');
		}
        return true;
    }
    /**
     * Check permission method.
     *
     * Example of usage:
     * -------------------------------------------------------------------------
     * Inside a construction to check all methods. You can put inside a method
     * for a more especific check.
     *
     * public function __construct()
     * {
     *     $this->auth->check_permission();
     * }
     * -------------------------------------------------------------------------
     *
     * @param string $url
     * @return boolean
     */
    public function check_permission($url = NULL, $type = 'slug')
    {
		if($type == 'slug') {
			if($url == NULL)
			{
				$url = uri_string();
				$segments = explode("/", $url);
				(count($segments) == 3) ? $url . '/' : $url;
			}
			if(strpos($url, '/')) {
				$slug = explode('/', $url);
				$slug[0] = 'admin';
				$slug = implode('.', $slug);
			}
			else {
				$slug = str_replace('v-manager', 'admin', $url);
			}
		} else $slug = $url;
		/* Load from cache */
		$cached = cms_config('cache');
		if($cached) $permission = $this->cache->get('permission_'.md5($slug.$type));
		else $permission = false;
		if($permission) {
			return isset($permission['status'])?boolval($permission['status']):false;
		} else {
			/* Load from db */
			if($this->get_id() == '')
			{
				$this->set_data(['msg_auth', cms_lang('you_not_logged')]);
				return false;
			} else {
				if($type == 'slug') {
					$permission = $this->permission->validate_permission($this->get_id(), $slug);
				} else {
					$permission = $this->permission->validate_permission($this->get_id(), NULL, $slug);
				}
				$json = ['status'=>$permission];
				$this->cache->save('permission_'.md5($slug.$type), $json, 86400);
				return $permission;
			}
        }
    }

    /**
     * Return true if the logged user is 'ROOT'.
     *
     * @return boolean
     */
    public function is_root()
    {
        if($this->get_data($this->prefix.'_id') == 1)
            return true;
        else
            return false;
    }

    /**
     * Returns the logged user ID.
     *
     * @return int
     */
    public function get_id()
    {
        if($this->is_logged())
            return $this->get_data($this->prefix.'_id');
        else
            return 0;
    }
    public function get_role()
    {
        if($this->is_logged())
            return $this->get_data($this->prefix.'_role');
        else
            return 0;
    }

    /**
     * This function generate an unique token.
     *
     * @return string
     */
    public function _generate_token()
    {
        $token = rtrim(strtr(base64_encode($this->getRandomNumber()), '+/', '-_'), '=');
        $account = $this->get_account_by_token($token);
        if(isset($account->id) && $account->id)
            return $this->_generate_token();
        else
            return $token;
    }

    /**
     * Return an random number based on an sha256 hash.
     *
     * @return string
     */
    protected function getRandomNumber()
    {
        return hash('sha256', uniqid(mt_rand(), true), true);
    }

    /**
     * Get an account data by Token.
     *
     * @param string $token
     * @return object
     */
    public function get_account_by_token($token)
    {
        return $this->model->where('token', $token)->first();
    }

    /**
     * Return a random hash.
     *
     * @param $limit
     * @return string
     */
    public function _generate_hash($limit=16)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $hash = '';
        for($i=0; $i<$limit; $i++) {
            $hash .= $characters[rand(0, strlen($characters)-1)];
        }
        return $hash;
    }
    /**
     * Return a hashed password.
     *
     * @param $password
     * @param string $salt
     * @return string
     */
    public function _hash_password($password, $salt)
    {
        switch ($this->auth_password_hash_type)
        {
            case 'md5':
                return md5(md5($password) . md5($salt));
                break;
            case 'sha512':
                $pass = hash('sha512', $password);
                $salt = hash('sha512', $salt);
                return hash('sha512', $pass.$salt);
                break;
        }
    }

    /**
     * Check an password hash.
     *
     * @param string $password
     * @param string $salt
     * @param string $hash
     * @return boolean
     */
    public function check_hash_password($password, $salt, $hash)
    {
        switch ($this->auth_password_hash_type)
        {
            case 'md5':
                if($hash == md5(md5($password) . md5($salt)))
                    return TRUE;
                else
                    return FALSE;
                break;
            case 'sha512':
                $pass = hash('sha512', $password);
                $salt = hash('sha512', $salt);
                if($hash == hash('sha512', $pass.$salt))
                    return TRUE;
                else
                    return FALSE;
                break;
        }
    }
    public function check_valid_password($password, $confirm=null) {
        if($confirm != NULL && $password != $confirm) {
            return cms_lang('confirm_password_not_match');
        }
        if(cms_config('user_secure_password')) {
            if(strlen($password) < 8) {
                return cms_lang('password_too_short');
            }
            if (!preg_match("#[0-9]+#", $password)) {
                return cms_lang('password_must_include_number');
                //"Password must include at least one number!"
            }
            if (!preg_match("#[a-zA-Z]+#", $password)) {
                return cms_lang('password_must_include_letter');
                //"Password must include at least one letter!"
            }
            if (!preg_match("#\W+#", $password)) {
                return cms_lang('password_must_include_symbol');
                //"Password must include at least one symbol!"
            }
        } else {
            if(strlen($password) < 6) {
                return cms_lang('password_too_short');
            }
        }
        return true;
    }
    /**
     * Setup a login session.
     *
     * @param object $account
     * @return bool
     */
    protected function _set_session($account, $remember=false)
    {
        if(!$account->id)
            return FALSE;
        $data = array(
            $this->prefix.'_id' => $account->id,
            $this->prefix.'_name' => $account->name?$account->name:$account->username,
            $this->prefix.'_username' => $account->username,
            $this->prefix.'_email' => $account->email,
            $this->prefix.'_role' => $account->role_id,
            $this->prefix.'_avatar' => $account->avatar,
            $this->prefix.'_created' => $account->created
        );
		if($remember) {
			$this->set_data($data, DAY);
		} else {
			$this->set_data($data);
		}
		$this->_add_access($account->id);
    }
    /**
     * Check if the current IP addres is banned.
     *
     * @return boolean
     */
    protected function _is_banned()
    {
		$ip_allowed = model('App\Modules\Admins\Models\IpAllowedModel');
		$ip_banned = model('App\Modules\Admins\Models\IpBannedModel');
        if($ip_allowed->is_whitelisted(get_ip())) {
            return FALSE;
        } else {
            $banned = $ip_banned->where('ip_address', get_ip())->first();
            if(isset($banned->id)) {
				if($banned->expired > time()) return $banned->expired;
				if($banned->expired == 0) return true;
				else return FALSE;
            } else return FALSE;
        }
    }

    /**
     * Ban an IP address after many failed attempts to login.
     *
     * @return mixed
     */
    protected function _ban_ip()
    {
		$ip_banned = model('App\Modules\Admins\Models\IpBannedModel');
        $data = array();
        $data['ip_address'] = get_ip();
        return $ip_banned->insert($data);
    }

    /**
     * Increase the numbe of attempts of failed login.
     *
     * @return void
     */
    protected function _add_attempt()
    {
		$ip_address = get_ip();
		$ip_attempt = model('App\Modules\Admins\Models\IpAttemptModel');
        $query = $ip_attempt->where('ip_address', $ip_address)->first();
        if($query)
        {
            $data = array();
            $data['last_attempt'] = time();
            $data['attempts'] = $query->attempts + 1;
            $ip_attempt->update($query->id, $data);
        } else
        {
            $data = array();
            $data['ip_address'] = $ip_address;
            $data['last_attempt'] = time();
            $data['attempts'] = 1;
            $ip_attempt->insert($data);
        }
    }

    /**
     * Clear the login attempts from an Ip address.
     *
     * @return int
     */
    protected function _clear_attempt()
    {
		$ip_attempt = model('App\Modules\Admins\Models\IpAttemptModel');
        $ip_attempt->where('ip_address', get_ip())->delete();
    }

    /**
     * Return a number of attempts of login from the Ip Address.
     *
     * @return int
     */
    protected function _num_attempts()
    {
		$ip_attempt = model('App\Modules\Admins\Models\IpAttemptModel');
        $result = $ip_attempt->where('ip_address', get_ip())->first();
        return isset($result->attempts)?$result->attempts:0;
    }
    /**
     * Log the account logins.
     *
     * @param int $account_id
     * @param string $ip_address
     * @return mixed
     */
    protected function _add_access($account_id)
    {
		if($this->prefix == 'admin') {
			$accessLogModel = model('App\Modules\Admins\Models\AccessLogModel');
			$request = \Config\Services::request();
			$data = array();
			$data['ip_address'] = get_ip();
			$data['useragent'] = $request->getUserAgent();
			$data['admin_id'] = $account_id;
			return $accessLogModel->insert($data);
		}
    }
}