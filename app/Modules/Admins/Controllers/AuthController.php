<?php namespace App\Modules\Admins\Controllers;

use CodeIgniter\Controller;
use Config\Email;
use Config\Services;
use App\Modules\Admins\Libraries\Auth;

class AuthController extends Controller
{
	protected $auth;
	/**
	 * @var Auth
	 */
	protected $config;
	protected $view;
	protected $cms;

	/**
	 * @var \CodeIgniter\Session\Session
	 */
	protected $session;

	public function __construct()
	{
		// Most services in this controller require
		// the session to be started - so fire it up!
		$this->session = session();
		/* Auth */
		$this->auth = new Auth();
		// Preload cms library
		$this->cms = new \App\Libraries\Cms();
		helper('cms');
		// Preload view
		$path = dirname(__DIR__);
		$this->view = new \CodeIgniter\View\View(config('View'), $path.DIRECTORY_SEPARATOR.'Views');
		$this->view->extend('layouts/login');
	}

	//--------------------------------------------------------------------
	// Login/out
	//--------------------------------------------------------------------

	/**
	 * Displays the login form, or redirects
	 * the user to their destination/home if
	 * they are already logged in.
	 */
	public function login()
	{
        $referrer = $this->session->get('backend_referer');
		if($this->auth->is_logged()) {
			if(!$referrer) $referrer = site_url('v-manager');
			cms_redirect($referrer);
		}
		$validation =  \Config\Services::validation();
		$validation->setRules([
			'username' => 'required',
			'password' => 'required|min_length[6]'
		]);
		if($this->request->isAJAX()) {
			if($this->request->getPost('auth_code')) {
				$output = $this->auth->auth_code($this->request->getPost('auth_code'));
			} else {
				$output = $this->auth->login($this->request->getPost('username'), $this->request->getPost('password'));
			}
			$this->response->setHeader("Content-Type", "Application/json");
			echo json_encode($output);
		}
		elseif(!$this->validate([])) {
			$this->view->setVar('title', 'Login');
			$this->cms->load_vendors([
				'nucleo',
				'fontawesome',
				'jquery',
				'bootstrap',
				'bootstrap-notify',
				'js-cookie',
				'sweetalert2',
				'nestable',
			]);
			$this->view->setVar('cms', $this->cms);
			$this->view->setVar('request', $this->request);
			echo $this->view->render('auth/login');
        }
	}


	/**
	 * Log the user out.
	 */
	public function logout()
	{
        $this->auth->logout();
		$login_url = site_url('v-manager/login');
		cms_redirect($login_url);
	}
}