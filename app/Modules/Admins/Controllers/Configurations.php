<?php namespace App\Modules\Admins\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Configurations extends AdminController
{
	public function __construct()
	{
	}
	public function index()
	{
		$this->view->setVar('title', 'Configurations');
		
		$configGroupModel = model('App\Modules\Admins\Models\ConfigurationGroupModel');
		$configurations = $configGroupModel->where('is_privated', 0)->with('configurations')->orderBy('order')->findAll();
		
		$this->view->setVar('items', $configurations);
		
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
        ]);
        $this->view->setVar('cms', $this->cms);
        set_referer('backend');
        echo $this->view->render('configurations');
	}
    public function group() {
        $item_id = $this->request->getGet('item_id');
        $item = $this->_get_group($item_id);
        if(isset($item->id)) {
            $json = $item->toArray();
        } else $json = ['id'=>0];
        $this->render_json($json);
    }
    public function config() {
        $item = $this->_get_configuration();
        if(isset($item->id)) {
            $json = $item->toArray();
        } else $json = ['id'=>0];
        $this->render_json($json);
    }
    public function update_config()
    {
		$posts = $this->request->getPost();
        $json = ['status' => false];
		if(isset($posts['id']) && isset($posts['value'])) {
			$id = intval($posts['id']);
			$value = $posts['value'];
			$configModel = model('App\Modules\Admins\Models\ConfigurationModel');
			$config = $configModel->find($id);
			if(isset($config->id)) {
				$config->value = $value;
				if($config->hasChanged('value')) {
					$configModel->save($config);
					$this->cache->delete('config_'.$config->code);
				}
				$json = ['status' => true];
			}
		}
        $this->render_json($json);
    }
    public function remove_config()
    {
		$id = intval($this->request->getPost('id'));
		$configModel = model('App\Modules\Admins\Models\ConfigurationModel');
		$status = $configModel->delete($id);
		$json = ['status' => $status];
        $this->render_json($json);
    }
    public function remove_group()
    {
		$id = intval($this->request->getPost('id'));
		$configGroupModel = model('App\Modules\Admins\Models\ConfigurationGroupModel');
		$status = $configGroupModel->delete($id);
		$json = ['status' => $status];
        $this->render_json($json);
    }
    public function save_data()
    {
		$posts = $this->request->getPost();
		$form = $posts['form'];
		if(isset($posts['id'])) {
			$message = array();
			if($form == 'group') {
				$id = intval($posts['id']);
				$data = array();
				$data['name'] = $this->request->getPost('name');
				$data['code'] = $this->request->getPost('code');
				$data['icon'] = $this->request->getPost('icon');
				$data['order'] = $this->request->getPost('order');
				/* Check requiced */
				$status = true;
				if(empty($data['name'])) {
					$status = false;
					$message = 'Name is requiced';
				}
				elseif(empty($data['code'])) {
					$status = false;
					$message = 'Code is requiced';
				}
				/* Process to database */
				$configGroupModel = model('App\Modules\Admins\Models\ConfigurationGroupModel');
				if($status) {
					if($id) {
						$status = $configGroupModel->update($id, $data);
						$method = 'Update';
					} else {
						$status = $configGroupModel->insert($data);
						$method = 'Add';
					}
				}
			} elseif($form == 'config') {
				$id = intval($posts['id']);
				$data = array();
				$data['name'] = $this->request->getPost('name');
				$data['code'] = $this->request->getPost('code');
				$data['value'] = $this->request->getPost('value');
				$data['description'] = $this->request->getPost('description');
				$data['type'] = $this->request->getPost('type');
				$data['extra_data'] = $this->request->getPost('extra_data');
				$data['in_group'] = intval($this->request->getPost('in_group'));
				$data['order'] = intval($this->request->getPost('order'));
				/* Check requiced */
				$status = true;
				if(empty($data['name'])) {
					$status = false;
					$message = 'Name is requiced';
				}
				elseif(empty($data['code'])) {
					$status = false;
					$message = 'Code is requiced';
				}
				elseif(empty($data['in_group'])) {
					$status = false;
					$message = 'Group is requiced';
				}
				elseif(empty($data['type'])) {
					$status = false;
					$message = 'Type is requiced';
				}
				$configModel = model('App\Modules\Admins\Models\ConfigurationModel');
				if($status) {
					if($id) {
						$status = $configModel->update($id, $data);
						$method = 'Update';
					} else {
						$status = $configModel->insert($data);
						$method = 'Add';
					}
				}
			} else {
				$message = 'Something went wrong!';
				$status = false;
			}
			$json = array();
			if($status) {
				$json['message'] = $method.' items successfully!';
				$json['status'] = 'success';
			} else {
				$json['message'] = !empty($message)?$message:'Something went wrong';
				$json['status'] = 'error';
			}
            $this->render_json($json);
		}
    }
    protected function _get_group($item_id)
    {
		$item_id = intval($item_id);
		if($item_id) {
			$configGroupModel = model('App\Modules\Admins\Models\ConfigurationGroupModel');
			$item = $configGroupModel->find($item_id);
			return $item;
		}
    }
    protected function _get_configuration()
    {
		$item_id = $this->request->getGet('item_id');
		$item_id = intval($item_id);
		if($item_id) {
			$configModel = model('App\Modules\Admins\Models\ConfigurationModel');
			$item = $configModel->find($item_id);
			return $item;
		}
    }
}