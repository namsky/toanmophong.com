<?php namespace App\Modules\Admins\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;
use App\Modules\Admins\Libraries\GoogleAuthenticator;
use CodeIgniter\HTTP\UserAgent;

class Account extends AdminController
{
	public function index()
	{
		$this->view->setVar('title', 'Account');
		helper(['form', 'url']);
		$adminModel = model('App\Modules\Admins\Models\AdminModel');
		$accessLog = model('App\Modules\Admins\Models\AccessLogModel');
		$agent = new UserAgent();
		$admin_id = $this->auth->get_id();
		
		$validation =  \Config\Services::validation();
		$validation->setRules([
			'current_password' => 'required|min_length[6]|max_length[20]',
			'new_password' => 'required|min_length[6]|max_length[20]',
			'confirm_password' => 'required|min_length[6]|max_length[20]|matches[new_password]',
		]);
        if($this->request->getMethod() == 'post')
        {
			$current_password = $this->request->getPost('current_password');
			$new_password = $this->request->getPost('new_password');
			$confirm_password = $this->request->getPost('confirm_password');
			if($new_password == $confirm_password) {
				$account_id = $this->auth->get_id();
				$admin = $adminModel->find($account_id);
				if ($this->auth->check_hash_password($current_password, $admin->salt, $admin->password)) {
					$this->auth->change_password($new_password);
					//$this->set_message('Đổi mật khẩu thành công', 'success');
					$message = ['message'=>'Change password successfully!', 'type'=>'success'];
					$this->view->setVar('notice', $message);
				} else {
					//$this->set_message('Current password is wrong', 'warning');
					$message = ['message'=>'Current password is wrong', 'type'=>'warning'];
					$this->view->setVar('notice', $message);
				}
            } else {
				$message = ['message'=>'Confirm password is not match', 'type'=>'warning'];
				$this->view->setVar('notice', $message);
			}
        }
		
		
		$admin = $adminModel->find($admin_id);
        $logs = $accessLog->where('admin_id', $admin_id)->orderBy('created', 'DESC')->findAll(20, 0);
		
		if($admin->auth_status == 0) {
			$site_name = cms_config('site_name');
			$gg_auth = new GoogleAuthenticator();
			if($admin->auth_secret) $secret = $admin->auth_secret;
			else {
				$secret = $gg_auth->createSecret();
				$adminModel->update($admin->id, ['auth_secret'=>$secret]);
			}
			$auth = array(
				'status' => 0,
				'secret' => $secret,
				'qr_thumb' => $gg_auth->getQRCodeGoogleUrl($admin->username, $secret, $site_name),
			);
		} else {
			$auth = array(
				'status' => 1
			);
		}
        $this->view->setVar('auth', $auth);
		$this->view->setVar('admin', $admin);
		$this->view->setVar('items', $logs);
		$this->view->setVar('cms', $this->cms);
		$this->view->setVar('validation', $validation);
		$this->view->setVar('agent', $agent);
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
        ]);
        set_referer('backend');
		echo $this->view->render('account');
	}
    public function active_auth()
    {
		$adminModel = model('App\Modules\Admins\Models\AdminModel');
		$password = $this->request->getPost('password');
		$auth_code = $this->request->getPost('auth_code');
		$account_id = $this->auth->get_id();
		$admin = $adminModel->find($account_id);
		if(isset($admin->id) && $this->auth->check_hash_password($password, $admin->salt, $admin->password)) {
			$gg_auth = new GoogleAuthenticator();
			$verifyCode = $gg_auth->verifyCode($admin->auth_secret, $auth_code, 1);
			if($verifyCode == false) {
				$output = ['status'=>'error', 'message'=>'Invalid Auth Code'];
			}
			else {
				$auth_status = $admin->auth_status?0:1;
				$admin->auth_status = $auth_status;
				$adminModel->update($admin->id, ['auth_status'=>$auth_status]);
				if($auth_status)
					$output = ['status'=>'success', 'message'=>'Now! Your account is actived 2FA'];
				else
					$output = ['status'=>'success', 'message'=>'Your account is deactived 2FA'];
			}
		} else {
			$output = ['status'=>'error', 'message'=>'Your password is wrong'];
		}
		$this->response->setHeader("Access-Control-Allow-Origin", "*");
		$this->response->setHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin");
		$this->response->setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
		$this->response->setHeader("Access-Control-Allow-Headers", "Content-Type,X-CSRF-Token, XHR, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
		$this->response->setHeader("Access-Control-Allow-Credentials", "true");
		$this->response->setHeader("Expires", "0");
		$this->response->setHeader("Last-Modified", gmdate("D, d M Y H:i:s") . " GMT");
		$this->response->setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		$this->response->setHeader("Pragma", "no-cache");
		return $this->response->setJSON($output);
	}
}