<?php namespace App\Modules\Admins\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Admins extends AdminController
{
	function get_config() {
		$config = [
			'name' => 'Admins',
			'model' => 'App\Modules\Admins\Models\AdminModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['email','username'],
				'filter_by' => ['role_id','status'],
				'filter_date' => true,
				'orders' => ['id' => 'desc'],
				'bulk_actions' => true,
			],
			'select_options' => [
				'role_id' => 'role|id,name|App\Modules\Admins\Models\RoleModel',
				'status' => [1 => 'Active', 0 => 'Inactived'],
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => ['name' => 'Name', 'class' => 'text-center d-sm-table-cell d-none'],
				'username' => ['name' => 'Username'],
				'email' => ['name' => 'Email', 'class' => 'd-sm-table-cell d-none'],
				'role_id' => [
					'name' => 'Role',
					'method' => 'template',
					'template' => '<span style="font-weight: bold; color:{$role->color}">{$role->name}</span>',
					'class' => 'text-center d-lg-table-cell d-none',
				],
				'status' => [
					'name' => 'Status',
					'method' => 'function',
					'function' => 'status',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'created' => [
					'name' => 'Created',
					'method' => 'datetime',
					'class' => 'text-center d-lg-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'with' => ['role|id,name,color'],
			'rules' => [
				'required' => ['username', 'email', 'role_id']
			],
			'record' => [
				'colums' => 6,
				'fields' => [
					'username' => ['name' => 'Username'],
					'email' => ['name' => 'Email'],
					'role_id' => [
						'name' => 'Role',
						'type' => 'select',
					],
					'status' => [
						'name' => 'Status',
						'type' => 'select',
					],
					'name' => ['name' => 'Name'],
					'avatar' => ['name' => 'Avatar'],
					'ip_address' => ['only_show' => true],
					'break_line',
					'password' => [
						'type' => 'password',
						'colums' => 12,
						'only_edit' => true
					],
				],
			],
		];
		return $config;
	}
}