<?php namespace App\Modules\Admins\Controllers;
use CodeIgniter\Controller;

class Permissions extends AdminController
{
	public function index()
	{
		$this->view->setVar('title', 'Permissions');
		
		$permissionModel = model('App\Modules\Admins\Models\PermissionModel');
		$roleModel = model('App\Modules\Admins\Models\RoleModel');
		
        $items = $permissionModel->with('data')->findAll();
        $roles = $roleModel->findAll();
		$permissions = array();
		foreach($items as $key=>$item) {
			if(!isset($permissions[$item->group])) $permissions[$item->group] = array();
			$permissions[$item->group][] = $item;
		}
		foreach($permissions as $key=>$permission) {
			usort($permission, "order_sort");
			$permissions[$key] = $permission;
		}
        $this->view->setVar('items', $permissions);
        $this->view->setVar('roles', $roles);
		
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'nestable',
        ]);
		$this->view->setVar('cms', $this->cms);
        set_referer('backend');
		echo $this->view->render('permissions');
	}
    function get_data()
    {
        $item_id = $this->request->getGet('item_id');
        $item = $this->get_permission($item_id);
        if(isset($item->id)) {
            $json = $item->toArray();
        } else $json = ['id'=>0];
        $this->render_json($json);
    }
    function update_permission()
    {
		$posts = $this->request->getPost();
        $json = ['status' => false];
		if(isset($posts['permission_id']) && isset($posts['role_id']) && isset($posts['value'])) {
			$permission_id = intval($posts['permission_id']);
			$role_id = intval($posts['role_id']);
			$value = intval($posts['value']);
			$PermissionRoleModel = model('App\Modules\Admins\Models\PermissionRoleModel');
			$permission_role = $PermissionRoleModel->where('permission_id', $permission_id)->where('role_id', $role_id)->first();
			if(isset($permission_role->id)) {
				$permission_role->value = $value;
				if($permission_role->hasChanged('value')) {
					$PermissionRoleModel->save($permission_role);
				}
				$json = ['status' => true];
			} else {
				$data = [
					'permission_id' => $permission_id,
					'role_id' => $role_id,
					'value' => $value
				];
				$PermissionRoleModel->insert($data);
				$json = ['status' => true];
			}
		}
        $this->render_json($json);
    }
    function remove()
    {
		$id = intval($this->request->getPost('id'));
		$permissionRoleModel = model('App\Modules\Admins\Models\PermissionRoleModel');
		$permissionRoleModel->where('permission_id', $id)->delete();
		$permissionModel = model('App\Modules\Admins\Models\PermissionModel');
		$status = $permissionModel->delete($id);
		$json = ['status' => $status];
        $this->render_json($json);
    }
    function save_data()
    {
		$posts = $this->request->getPost();
		if(isset($posts['id'])) {
			$message = array();
			$id = intval($posts['id']);
			$data = array();
			$data['name'] = $this->request->getPost('name');
			$data['description'] = $this->request->getPost('description');
			$data['slug'] = $this->request->getPost('slug');
			$data['function'] = $this->request->getPost('function');
			$data['order'] = $this->request->getPost('order');
			$data['group'] = $this->request->getPost('group');
			/* Check requiced */
			$status = true;
			if(empty($data['name'])) {
				$status = false;
				$message = 'Name is requiced';
			}
			elseif(empty($data['slug'])) {
				$status = false;
				$message = 'Slug is requiced';
			}
			/* Process to database */
			$PermissionModel = model('App\Modules\Admins\Models\PermissionModel');
			if($status) {
				if($id) {
					$status = $PermissionModel->update($id, $data);
					$method = 'Update';
				} else {
					$status = $PermissionModel->insert($data);
					$method = 'Add';
				}
			}
			$json = array();
			if($status) {
				$json['message'] = $method.' items successfully!';
				$json['status'] = 'success';
			} else {
				$json['message'] = !empty($message)?$message:'Something went wrong';
				$json['status'] = 'error';
			}
            $this->render_json($json);
		}
    }
    protected function get_permission($item_id)
    {
		$item_id = intval($item_id);
		if($item_id) {
			$permissionModel = model('App\Modules\Admins\Models\PermissionModel');
			$item = $permissionModel->find($item_id);
			return $item;
		}
    }
}