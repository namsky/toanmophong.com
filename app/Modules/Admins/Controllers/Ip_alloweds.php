<?php namespace App\Modules\Admins\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Ip_alloweds extends AdminController
{
	function get_config() {
		$config = [
			'name' => 'Ip_alloweds',
			'model' => 'App\Modules\Admins\Models\IpAllowedModel',
			'datagrid_options' => [
				'orders' => ['id' => 'desc'],
			],
			'select_options' => [],
			'columns' => [
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'ip_address' => [
					'name' => 'IP Address',
					'method' => 'template',
					'template' => '<span style="color:green; font-weight: bold">{$ip_address}</span>',
					'class' => 'text-center',
				],
				'description' => ['name' => 'Description'],
				'created' => [
					'name' => 'Created',
					'method' => 'datetime',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				]
			],
			'rules' => [
				'required' => ['ip_address']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'ip_address' => ['name' => 'IP Address'],
					'description' => ['name' => 'Description'],
				],
			],
		];
		return $config;
	}
}