<?php namespace App\Modules\Admins\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Ip_banneds extends AdminController
{
	function get_config() {
		$config = [
			'name' => 'Ip_banneds',
			'model' => 'App\Modules\Admins\Models\IpBannedModel',
			'datagrid_options' => [
				'orders' => ['id' => 'desc'],
			],
			'select_options' => [],
			'columns' => [
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'ip_address' => [
					'name' => 'IP Address',
					'method' => 'template',
					'template' => '<span style="color:red; font-weight: bold">{$ip_address}</span>',
					'class' => 'text-center',
				],
				'reason' => ['name' => 'Reason'],
				'expired' => [
					'name' => 'Expired',
					'method' => 'datetime',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'created' => [
					'name' => 'Created',
					'method' => 'datetime',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				]
			],
			'rules' => [
				'required' => ['ip_address']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'ip_address' => ['name' => 'IP Address'],
					'expired' => ['name' => 'Expired', 'type' => 'datetime'],
					'reason' => ['name' => 'Reason'],
				],
			],
		];
		return $config;
	}
}