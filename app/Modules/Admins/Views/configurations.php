<?=$this->section('content');?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
							<h3 class="float-left mb-0"><?=lang('Configurations');?></h3>

							<div class="float-right ml-3 mb-0">
								<a href="javascript:;" onclick="render_group()" data-toggle="modal" data-target="#modal-group"><i class="fas fa-plus-circle"></i> <?=lang('Group');?></a>
                                &nbsp;
								<a href="javascript:;" onclick="render_config()" data-toggle="modal" data-target="#modal-config"><i class="fas fa-plus-circle"></i> <?=lang('Configurations');?></a>
							</div>
						</div>
						<div class="card-content content-pills">
							<div class="row m-0">
								<div class="col-md-3 col-xs-4 p-0">
									<ul class="nav nav-pills">
										<?php
										$first = true;
										if(isset($items) && $items) {
											foreach($items as $item) {
												if($first) {
													$first = false;
													$active = 'active';
												} else $active = '';
												echo '<li id="group-'.$item->id.'">
													<a data-toggle="pill" class="'.$active.'" href="#configs-'.$item->id.'">'.$item->icon.'&nbsp;'.$item->name.'</a>
													<div class="pill-action">
														<span onclick="render_group('.$item->id.')" data-toggle="modal" data-target="#modal-group">
															<i class="fa fa-edit"></i>
														</span>
														<span onclick="remove_group('.$item->id.')">
															<i class="fa fa-trash"></i>
														</span>
													</div>
												</li>';
											}
										}
										?>
									</ul>
								</div>
								<div class="col-md-9 col-xs-8 p-0">
									<div class="tab-content">
										<?php
										$first = true;
										if(isset($items) && $items) {
											foreach($items as $item) {
												if($first) {
													$first = false;
													$active = ' in active show';
												} else $active = '';
										?>
										<div id="configs-<?=$item->id?>" class="tab-pane fade<?=$active?>">
											<?php
											if(isset($item->configurations) && $item->configurations) {
												$configurations = (array)$item->configurations;
												usort($configurations, "order_sort");
												foreach($configurations as $config) {
											?>
												<div id="config-<?=$config->id?>" class="form-group row">
													<label for="<?=$config->code;?>" class="col-md-2 col-form-label form-control-label form-control-sm">
														<?=$config->name;?>
													</label>
													<div class="col-md-4">
														<?
														//'text','password','selectbox','switch'
														if($config->type == 'text' || $config->type == 'password') {
															echo '<input class="form-control form-control-sm config_item" type="'.$config->type.'" value="'.$config->value.'" id="'.$config->code.'" data-id="'.intval($config->id).'">';
														} elseif($config->type == 'switch') {
															$on = lang('On');
															$off = lang('Off');
															if($config->extra_data && strpos($config->extra_data, '|')) {
																$extra_data = explode('|', $config->extra_data);
																if(count($extra_data) == 2) {
																	$on = $extra_data[0];
																	$off = $extra_data[1];
																}
															}
															$checked = intval($config->value)?'checked':'';
															echo '
															<label class="form-toggle custom-toggle custom-toggle-danger">
																<input type="checkbox" class="config_item" id="'.$config->code.'" name="'.$config->code.'" '.$checked.' value="1" data-id="'.intval($config->id).'">
																<span class="custom-toggle-slider rounded-circle" data-label-on="'.$on.'" data-label-off="'.$off.'"></span>
															</label>
															';
														} elseif($config->type == 'selectbox') {
															if($config->extra_data && strpos($config->extra_data, '|')) {
																$extra_data = explode('|', $config->extra_data);
																if(count($extra_data)) {
																	$selects = '';
																	foreach($extra_data as $option) {
																		if($config->value == $option)
																			$selects .= '<option value="'.$option.'" selected>'.$option.'</option>';
																		else
																			$selects .= '<option value="'.$option.'">'.$option.'</option>';
																	}
																	echo '
																	<select class="form-control form-control-sm config_item" id="'.$config->code.'" name="'.$config->code.'" data-id="'.intval($config->id).'">
																		'.$selects.'
																	</select>
																	';
																}
															}
														}
														?>
													</div>
													<label class="col-md-4 col-form-label form-control-label form-control-sm text-right">
														<?=$config->description;?>
													</label>
													<div class="col-md-2 text-right">
														<a href="javascript:;" onclick="render_config(<?=$config->id;?>)" data-toggle="modal" data-target="#modal-config" class="btn btn-sm btn-primary">
															<span class="fa fa-edit"></span>
														</a>
														<a href="javascript:;" onclick="remove_config(<?=$config->id;?>)" class="btn btn-sm btn-danger">
															<span class="fa fa-trash"></span>
														</a>
													</div>
												</div>
											<?php
												}
											}
											?>
										</div>
										<?php
											}
										}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Modal -->
					<div class="modal fade custom" id="modal-group" tabindex="-1" role="dialog" data-backdrop="false">
						<div class="modal-dialog">
							<div class="modal-content">
								<form id="form-group">
									<div class="card-header">
										<a href="javascript:;" class="back" data-dismiss="modal" aria-label="Go back">
											<i class="fas fa-arrow-left"></i> <?=lang('Go_back');?>
										</a>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<input type="hidden" value="" id="group_id" name="id">
													<input type="hidden" value="group" id="form" name="form">
													<label for="group_name" class="form-control-label"><?=lang('Name');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="group_name" name="name" required>
												</div>
												<div class="form-group">
													<label for="group_code" class="form-control-label"><?=lang('Code');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="group_code" name="code" required>
												</div>
												<div class="form-group">
													<label for="group_icon" class="form-control-label"><?=lang('Icon');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="group_icon" name="icon">
												</div>
												<div class="form-group">
													<label for="group_order" class="form-control-label"><?=lang('Order');?></label>
													<input class="form-control form-control-sm" type="number" value="" id="group_order" name="order">
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" id="btn_save_group" class="btn btn-primary" onClick="save('group');"><?=lang('Save');?></button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="modal fade custom" id="modal-config" tabindex="-1" role="dialog" data-backdrop="false">
						<div class="modal-dialog">
							<div class="modal-content">
								<form id="form-config">
									<div class="card-header">
										<a href="javascript:;" class="back" data-dismiss="modal" aria-label="Go back">
											<i class="fas fa-arrow-left"></i> <?=lang('Go_back');?>
										</a>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input type="hidden" value="" id="config_id" name="id">
													<input type="hidden" value="config" id="form" name="form">
													<label for="config_name" class="form-control-label"><?=lang('Name');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="config_name" name="name" required>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="config_code" class="form-control-label"><?=lang('Code');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="config_code" name="code" required>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label for="config_value" class="form-control-label"><?=lang('Value');?> (thêm &lt;br&gt; nếu muốn xuống dòng trong đoạn text dài, dùng &quot; thay cho ngoặc kép)</label>
													<textarea class="form-control form-control-sm" id="config_value" name="value" required rows="4"></textarea>
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label for="config_type" class="form-control-label"><?=lang('Type');?></label>
													<select class="form-control form-control-sm" id="config_type" name="type">
														<option value="text">Text</option>
														<option value="password">Password</option>
														<option value="selectbox">Selectbox</option>
														<option value="switch">Switch</option>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="config_in_group" class="form-control-label"><?=lang('Group');?></label>
													<select class="form-control form-control-sm" id="config_in_group" name="in_group">
														<?php
														if(isset($items) && $items) {
															foreach($items as $item) {
														?>
														<option value="<?=$item->id?>"><?=$item->name?></option>
														<?php
															}
														}
														?>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="config_order" class="form-control-label"><?=lang('Order');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="config_order" name="order">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="config_description" class="form-control-label"><?=lang('Description');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="config_description" name="description">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="config_extra_data" class="form-control-label"><?=lang('Extra_data');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="config_extra_data" name="extra_data">
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" id="btn_save_config" class="btn btn-primary" onClick="save('config');"><?=lang('Save');?></button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		$args = array(
			'field_slug' => 'name',
			'render' => false,
			'save' => false,
		);
		//$this->cms->gen_js($args);
		?>
		<script data-minify-level="0">
			$('.modal.custom').on('show.bs.modal', function (e) {
				$('.card').hide();
			});
			$('.modal.custom').on('hidden.bs.modal', function (e) {
				$('.card').show();
			});
			var local_storage = window.localStorage;
			var current_tab = '';
			if(typeof local_storage['configuration_tabs'] != 'undefined') {
				$('.nav-pills a[href="' + local_storage['configuration_tabs'] + '"]').tab('show');
				current_tab = local_storage['configuration_tabs'];
			}
			$('.nav-pills a').on('shown.bs.tab', function(event){
				local_storage['configuration_tabs'] = event.target.hash;
				current_tab = event.target.hash;
			});
			$('.config_item').on('change', function () {
				update_config(this)
			});
			$('.config_item').keyup(function (e) {
				update_config(this)
			});
			function update_config(element) {
				var id = $(element).data('id');
				if($(element).attr('type') == 'checkbox') {
					var value = $(element).prop('checked')?1:0;
				}
				else {
					var value = $(element).val();
				}
				if(id) {
					$.post("<?=current_url();?>/update_config", {id:id, value:value}, function(data) {
						if(data.status!=true) {
							notify("", data.message, 'warning', true);
						}
					});
				}
			}
			function save(type) {
				var args = $("#form-"+type).serialize();
				$.post("<?=current_url();?>/save_data", args, function(data) {
					if(data.status=="success") {
						location.reload();
					} else {
						notify("", data.message, 'warning', true);
					}
				});
			}
			function render_group(id) {
				$('#form-group')[0].reset();
				if(id) {
					$.ajax({url: "<?=current_url();?>/group?item_id="+id, success: function(result){
						$.each(result, function(key, value) {
							if($("#group_"+key).attr('type') == 'checkbox') {
								var value = parseInt(value);
								$("#group_"+key).attr("checked", !!value);
							}
							else {
								if($.isNumeric(value)) value = parseInt(value);
								$("#group_"+key).val(value).change();
							}
						});
					}, dataType: "json"});
					$("#btn_save_group").html("Update");
				} else {
					$("#group_id").val('');
					$("#btn_save_group").html("Save");
				}
			}
			function render_config(id) {
				$('#form-config')[0].reset();
				if(id) {
					$.ajax({url: "<?=current_url();?>/config?item_id="+id, success: function(result){
						$.each(result, function(key, value) {
							if($("#config_"+key).attr('type') == 'checkbox') {
								var value = parseInt(value);
								$("#config_"+key).attr("checked", !!value);
							}
							else {
								if($.isNumeric(value)) value = parseInt(value);
								$("#config_"+key).val(value).change();
							}
						});
					}, dataType: "json"});
					$("#btn_save_config").html("Update");
				} else {
					var group_id = current_tab.match(/\d+/)[0];
					$("#config_in_group").val(group_id).change();
					$("#config_id").val('');
					$("#btn_save_config").html("Save");
				}
			}
			function remove_config(id) {
				swal({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: !0,
					buttonsStyling: !1,
					confirmButtonClass: "btn btn-danger",
					confirmButtonText: "Yes, delete it!",
					cancelButtonClass: "btn btn-secondary"
				}).then(t => {
					if(id && t.value) {
						$.post("<?=current_url();?>/remove_config", {id:id}, function(data,status){
							if(data) {
								$("#config-"+id).remove();
								swal({
									title: "Your item has been deleted.",
									type: "success",
									buttonsStyling: !1,
									confirmButtonClass: "btn btn-success"
								});
							}
							else {
								swal({
									title: "Something went wrong.",
									type: "warning",
									buttonsStyling: !1,
									confirmButtonClass: "btn btn-primary"
								});
							}
						});
					}
				});
				return false;
			}
			function remove_group(id) {
				swal({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: !0,
					buttonsStyling: !1,
					confirmButtonClass: "btn btn-danger",
					confirmButtonText: "Yes, delete it!",
					cancelButtonClass: "btn btn-secondary"
				}).then(t => {
					if(id && t.value) {
						$.post("<?=current_url();?>/remove_group", {id:id}, function(data, status){
							if(data) {
								$("#group-"+id).remove();
								swal({
									title: "Your item has been deleted.",
									type: "success",
									buttonsStyling: !1,
									confirmButtonClass: "btn btn-success"
								});
							}
							else {
								swal({
									title: "Something went wrong.",
									type: "warning",
									buttonsStyling: !1,
									confirmButtonClass: "btn btn-primary"
								});
							}
						});
					}
				});
				return false;
			}
		</script>
<?=$this->endSection();?>