<?=$this->section('content');?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
							<div class="d-md-block d-none p-0 col-12 mb-3">
                                <?php
                                if(isset($crud['datagrid_options']['bulk_actions']) && $crud['datagrid_options']['bulk_actions']) {
								?>
								<div class="float-left">
                                    <select class="form-control form-control-sm" id="bulk_actions" name="action">
                                        <option value="-1">Bulk Actions</option>
                                        <option value="delete">Delete</option>
                                    </select>
								</div>
								<div class="float-left ml-2">
                                    <button type="button" class="btn btn-primary btn-sm" id="do_action">Apply</button>
								</div>
								<?php } else { ?>
								<h3 class="float-left mb-0"><?=isset($crud['name'])?$crud['name']:'Module'?></h3>
								<?php } ?>

								<div class="float-right ml-3 mb-0">
									<a href="javascript:;" class="btn btn-sm btn-primary" onclick="clear_filter()"><i class="fas fa-sync"></i></a>
									<?php
									if(isset($crud['datagrid_options']['addnew_action']) && $crud['datagrid_options']['addnew_action']) {
									?>
									<a href="<?=$crud['datagrid_options']['addnew_action']?>" class="btn btn-sm btn-primary">
										<i class="fas fa-plus-circle"></i> ADD ITEM
									</a>
									<?php } else { ?>
									<a href="javascript:;" class="btn btn-sm btn-primary" onclick="render()" data-toggle="modal" data-target="#modal-item">
										<i class="fas fa-plus-circle"></i> ADD ITEM
									</a>
									<?php } ?>
                                </div>
                                <div class="clearfix"></div>
							</div>
							<form method="GET" id="form-filter">
								<div class="float-right mb-0">
									<?php
									$filter = false;
									if(isset($crud['datagrid_options']['filter_date']) && $crud['datagrid_options']['filter_date']) {
										$filter = true;
										?>
									<div class="input-daterange datepicker float-left d-md-block d-none">
										<label for="filter-from" class="float-left form-control-sm col-form-label form-control-label text-right">
											<?=lang('From');?>
										</label>
										<div class="float-left">
											<input name="from" class="form-control form-control-sm datepicker" autocomplete="off" onchange="$('#form-filter').submit()" style="width:94px" type="text" value="<?=$request->getGet('from')?>">
										</div>
										<label for="filter-to" class="ml-3 float-left form-control-sm col-form-label form-control-label text-right">
											<?=lang('To');?>
										</label>
										<div class="float-left">
											<input name="to" class="form-control form-control-sm datepicker" autocomplete="off" onchange="$('#form-filter').submit()" style="width:94px" type="text" value="<?=$request->getGet('to')?>">
										</div>
									</div>
									<?php
									}
									?>
									
									<?php
									if(isset($crud['datagrid_options']['filter_by']) && $crud['datagrid_options']['filter_by']) {
										$filter = true;
										foreach($crud['datagrid_options']['filter_by'] as $filter) {
											if(isset($crud['select_options'][$filter])) {
												$options = $crud['select_options'][$filter];
									?>
									<div class="float-left ml-2 d-md-block d-none">
										<select class="form-control form-control-sm" name="<?=$filter?>">
										<?php
											if(is_array($options)) {
												echo '<option value="">None</option>';
												foreach($options as $value=>$option_name) {
													echo '<option value="'.$value.'">'.$option_name.'</option>';
												}
											} elseif(strpos($options, '|') && strpos($options, ',')) {
												$options = explode('|', $options);
												$values = explode(',', $options[1]);
												echo '<option value="">None</option>';
												if(isset(${$options[0]})) {
													foreach(${$options[0]} as $option) {
														echo '<option value="'.$option->{$values[0]}.'">'.$option->{$values[1]}.'</option>';
													}
												}
											}
										?>
										</select>
									</div>
									<?php
											}
										}
									}
									?>
									<?php
									if(isset($crud['datagrid_options']['search_by']) && $crud['datagrid_options']['search_by']) {
										$filter = true;
									?>
									<div class="float-left ml-2">
										<input type="text" class="form-control form-control-sm" placeholder="<?=lang('Search');?>" name="q" value="<?=$request->getGet('q')?>" autocomplete="off">
									</div>
									<?php } ?>
									<?php
									if($filter) {
									?>
									<div class="float-left ml-2">
										<button type="button" class="btn btn-primary btn-sm" onclick="$('#form-filter').submit()"><?=lang('Filter');?></button>
									</div>
									<?php } ?>
								</div>
							</form>
						</div>
						<div class="card-content table-responsive">
							<table class="table align-items-center table-flush" id="content_table">
								<thead class="thead-light">
									<tr>
									<?php
										$columns = "";
										foreach($crud['columns'] as $key=>$column) {
											$class = isset($column['class'])?$column['class']:'';
											$width = isset($column['width'])?$column['width']:'';
											$name = isset($column['name'])?$column['name']:'';
											if(isset($column['type']) && $column['type'] == 'checkbox') {
												$html = '
													<th class="'.$class.'">
														<div class="custom-control custom-checkbox">
															<input class="custom-control-input" id="all_item" type="checkbox">
															<label class="custom-control-label" for="all_item"></label>
														</div>
													</th>
												';
											} else {
												$html = '
													<th class="'.$class.'">'.$name.'</th>
												';
											}
											if($class) $columns .= '{className: "'.$class.'"},';
											else $columns .= 'null,';
											echo $html;
										}
									?>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
                        <!-- Card footer -->
                        <div class="card-footer py-4">
							<div class="col-sm-12 col-md-12">
								<span class="total"><?=lang('Total');?> <b id="total_items">-</b> <?=lang('records'); ?>.</span>
								<nav class="float-right" id="paging"></nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<?php
		if(isset($crud['record']) && isset($crud['record']['fields'])) {
			$colums = isset($crud['record']['colums'])?$crud['record']['colums']:6;
		?>
		<div class="modal fade cms_box" id="modal-item" tabindex="-1" role="dialog">
			<div class="modal-dialog" style="width:<?=isset($crud['record']['width'])?$crud['record']['width']:''?>">
				<div class="modal-content">
					<form id="form-item">
						<div class="modal-header">
							<h5 class="modal-title"><?=isset($crud['name'])?$crud['name']:'Module'?></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<input class="form-control form-control-sm" type="hidden" value="" id="id" name="id">
								<?php
								foreach($crud['record']['fields'] as $key=>$field) {
									if($key == 'break_line') echo '<div class="col-md-12"><hr class="break_line" /></div>';
									else {
										$name = isset($field['name'])?$field['name']:ucfirst($key);
										$only_show = isset($field['only_show'])?$field['only_show']:false;
										$with_upload_image = isset($field['with_upload_image'])?$field['with_upload_image']:0;
										$with_upload_file = isset($field['with_upload_file'])?$field['with_upload_file']:0;
										$type = isset($field['type'])?$field['type']:'text';
										$_colums = isset($field['colums'])?$field['colums']:$colums;
										$extent_button = isset($field['extent_button'])?$field['extent_button']:'';
										$description = isset($field['description'])?$field['description']:'';
								?>
								<div class="col-md-<?=$_colums?>">
									<div class="form-group">
										<label for="<?=$key?>" class="form-control-label"><?=$name;?></label>
										<?php
										echo $extent_button;
										if($type == 'select' && isset($crud['select_options'][$key])) {
											$options = $crud['select_options'][$key];
										?>
										<select class="form-control form-control-sm" id="<?=$key?>" name="<?=$key?>">
										<?php
											if(is_array($options)) {
												echo '<option value="">None</option>';
												foreach($options as $value=>$option_name) {
													echo '<option value="'.$value.'">'.trim($option_name).'</option>';
												}
											} elseif(strpos($options, '|') && strpos($options, ',')) {
												$options = explode('|', $options);
												$values = explode(',', $options[1]);
												echo '<option value="">None</option>';
												if(isset(${$options[0]})) {
													foreach(${$options[0]} as $option) {
														echo '<option value="'.$option->{$values[0]}.'">'.trim($option->{$values[1]}).'</option>';
													}
												}
											}
										?>
										</select>
										<?php
										} elseif($type == 'checkbox') {
										?>
										<div class="custom-control custom-checkbox">
											<input class="custom-control-input" id="<?=$key?>" name="<?=$key?>" <?=$only_show?'disabled':''?> type="checkbox" value="1">
											<label class="custom-control-label" for="<?=$key?>"></label>
										</div>
										<?php
										} elseif($type == 'switchbox') {
										?>
										<label class="custom-toggle custom-toggle-default block">
											<input id="<?=$key?>" name="<?=$key?>" <?=$only_show?'disabled':''?> type="checkbox" value="1" />
											<span class="custom-toggle-slider rounded-circle" data-label-off="NO" data-label-on="YES"></span>
										</label>
										<?php
										} elseif($type == 'textarea') {
											if($with_upload_image) {
												?>
												<label class="btn btn-default btn-xs float-right">
													<i class="fa fa-upload" aria-hidden="true"></i>
													Upload
													<input type="file" class="thumb_upload" data-target="<?=$key?>" accept="image/*" hidden>
												</label>
												<?
											}
											elseif($with_upload_file) {
												?>
												<label class="btn btn-default btn-xs float-right">
													<i class="fas fa-file"></i>
													Upload
													<input type="file" class="file_upload" data-target="<?=$key?>" hidden>
												</label>
												<?
											}
											$style = isset($field['height'])?'height:'.$field['height']:'';
										?>
										<textarea class="form-control form-control-sm" style="<?=$style?>" id="<?=$key?>" name="<?=$key?>" <?=$only_show?'disabled':''?>></textarea>
										<?php
										} elseif($type == 'datetime') {
											$style = isset($field['height'])?'height:'.$field['height']:'';
										?>
										<div class="row">
											<div class="col-6">
												<input autocomplete="off" class="form-control form-control-sm datepicker" type="<?=$type?>" value="" id="<?=$key?>" name="<?=$key?>" <?=$only_show?'disabled':''?>>
											</div>
											<div class="col-3 pl-2 pr-2">
												<input autocomplete="off" class="form-control form-control-sm text-center" name="<?=$key?>_hour" id="<?=$key?>_hour" type="number" max="24" value="" />
											</div>
											<div class="col-3 pl-0">
												<input autocomplete="off" class="form-control form-control-sm text-center" name="<?=$key?>_min" id="<?=$key?>_min" type="number" max="59" value="" />
											</div>
										</div>
										<?php
										} else {
											if($with_upload_image) {
												?>
												<label class="btn btn-default btn-xs float-right">
													<i class="fas fa-image"></i>
													Upload
													<input type="file" class="thumb_upload" data-target="<?=$key?>" accept="image/*" hidden>
												</label>
												<?
											}
											if($with_upload_file) {
												?>
												<label class="btn btn-default btn-xs float-right">
													<i class="fas fa-file"></i>
													Upload
													<input type="file" class="file_upload" data-target="<?=$key?>" hidden>
												</label>
											<? } ?>
										<input class="form-control form-control-sm" type="<?=$type?>" value="" id="<?=$key?>" name="<?=$key?>" <?=$only_show?'disabled':''?>>
                                        <?php
                                        }
                                        if(!empty($description)) {
                                        ?>
                                        <scpan class="description"><?=$description?></scpan>
                                        <?php
                                        }
                                        ?>
									</div>
								</div>
								<?php
									}
								}
								?>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" id="btn_save" class="btn btn-sm btn-primary" onClick="save();"><?=lang('Save');?></button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php } ?>
        <?php 
		$args = array(
            'is_datatable' => true,
            'columns' => $columns,
        );
        $cms->gen_js($args);
        ?>
		<script>
			<?php
				echo isset($crud['scripts'])?$crud['scripts']:''
			?>
        </script>
<?=$this->endSection();?>