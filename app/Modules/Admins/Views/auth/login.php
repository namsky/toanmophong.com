<?=$this->section('content');?>
<!-- Main content -->
    <div class="main-content">
        <!-- Header -->
        <div class="header py-8 py-lg-8 pt-lg-8">
            <div class="container">
                <div class="header-body text-center">
                    <div class="row justify-content-center">
                        <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                            <h1 class="text-white"><?=lang('Welcome')?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-10">
                    <div class="card border-0 mb-0">
                        <div class="card-body px-lg-6 py-lg-6">
							<div class="row">
								<div class="col-lg-6 col-md-12">
									<p class="bg_login"></p>
								</div>
								<div class="col-lg-6 col-md-12">
									<form method="POST" role="form" id="login-form" class="validation" novalidate>
										<div class="form-group my-4">
											<div class="input-group input-group-merge input-group-alternative">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="ni ni-email-83"></i></span>
												</div>
												<input class="form-control" id="username" name="username" type="text" autocomplete="off" required value="<?=$request->getPost('username');?>" placeholder="<?= lang('User'); ?>">
												<div class="invalid-feedback"><?=lang('Username_validator');?></div>
											</div>
										</div>
										<div class="form-group">
											<div class="input-group input-group-merge input-group-alternative">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
												</div>
												<input class="form-control" id="password" name="password" type="password" autocomplete="off" required value="" placeholder="<?= lang('Password'); ?>">
												<div class="invalid-feedback"><?=lang('Password_validator');?></div>
											</div>
										</div>
										<div class="custom-control custom-control-alternative custom-checkbox text-right">
											<input class="custom-control-input" id=" customCheckLogin" type="checkbox">
											<label class="custom-control-label" for=" customCheckLogin">
												<span class="text-muted"><?= lang('Remember'); ?></span>
											</label>
										</div>
										<div class="text-center">
											<button type="submit" class="btn btn-primary mt-4"><?= lang('Login'); ?></button>
										</div>
									</form>
								</div>
							</div>
							<form method="POST" role="form" id="auth-form" class="validation" style="display: none">
                                <div class="form-group">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fab fa-google"></i></span>
                                        </div>
                                        <input class="form-control" id="auth_code" name="auth_code" type="text" autocomplete="off" value="" placeholder="<?= lang('Auth_code'); ?>">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary my-4"><?= lang('Login'); ?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-6">
                            <a href="<?=site_url('admin/recovery')?>" class="text-light"><small><?=lang('Recovery')?></small></a>
						</div>
                        <div class="col-6 text-right">
                            <a href="#" class="text-light"><small><?=lang('Register')?></small></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
$("#auth-form").submit(function(event){
    event.preventDefault();
    var auth_code = $("#auth_code").val();
	$.post("<?=current_url();?>", {auth_code: auth_code}, function(data,status) {
		var title = "";
		var type = "";
		if(data.status == "success") {
			window.location.replace("<?=URL?>/admin/");
		}
		else {
			notify("AUTH ERROR", data.message, "warning");
		}
	});
	return false;
});
$("#login-form").submit(function(event){
    event.preventDefault();
    var username = $("#username").val();
    var password = $("#password").val();
	$.post("<?=current_url();?>", {username:username, password:password}, function(data,status){
		var title = "";
		var type = "";
		//data = JSON.parse(data);
		var message = data.message;
		if(data.status == "auth_require") {
			$("#login-form").hide();
			$("#auth-form").show();
		}
		else if(data.status == "success") {
			title = "LOGIN SUCCESS";
			type = "success";
			notify(title, message, type);
			setTimeout(function(){
				location.reload();
			}, 1000);
		}
		else {
			title = "LOGIN ERROR";
			type = "warning";
			notify(title, message, type);
		}
	});
	return false;
});
function notify(title, message, type) {
	$.notify({
		title: title,
		message: message,
	},{
		position: "absolute",
		element: "body",
		type: type,
		allow_dismiss: false,
		placement: {
			from: "top",
			align: "center"
		},
		offset: {x: 15, y: 15},
		spacing: 10,
		delay: 1500,
		timer: 25e3,
		animate: {
			enter: "animated fadeInDown",
			exit: "animated fadeOutUp"
		},
		template: "<div data-notify=\"container\" class=\"alert alert-dismissible alert-{0} alert-notify\" role=\"alert\"><span class=\"alert-icon\" data-notify=\"icon\"></span> <div class=\"alert-text\"</div> <span class=\"alert-title\" data-notify=\"title\">{1}</span> <span data-notify=\"message\">{2}</span></div><button type=\"button\" class=\"close\" data-notify=\"dismiss\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></div>"
	});
}
</script>
<?=$this->endSection();?>