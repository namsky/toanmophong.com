<!DOCTYPE html>
<html  lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<title><?=isset($title)?$title.' - '.cms_config('site_name'):cms_config('site_tile').' - '.cms_config('site_name');?></title>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
	<link rel="shortcut icon" type="image/x-icon" href="<?=URL.'/favicon.ico'; ?>">
	<?php $cms->show_asset('css'); ?>
	<?php $cms->show_asset('js'); ?>
	<?php if(isset($stylesheets)) {
		foreach($stylesheets as $style) echo '<link rel="stylesheet" href="'.$style.'">';
	} ?>
	<!-- THEME STYLES -->
	<?=load_css('admin.min.css'); ?>
	<?php if(isset($external_scripts)) {
		foreach($external_scripts as $script) echo '<script src="'.$script.'" type="text/javascript"></script>';
	} ?>
</head>
<?php
	$avatar = (isset($account['avatar']) && $account['avatar'])?$account['avatar']:CDN.'/images/no-avatar.jpg';
	$account = isset($account['username'])?$account['username']:'Account';
?>
<body>
    <!-- Sidenav -->
    <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-dark" id="sidenav-main">
        <div class="navbar-content">
            <div class="scrollbar-inner">
                <!-- Brand -->
                <div class="sidenav-header d-flex align-items-center">
                    <a class="navbar-brand" href="<?=site_url('v-manager');?>">
                        <img src="<?=CDN?>/images/logo.png" class="navbar-brand-img" alt="8AM">
                    </a>
                    <div class="ml-auto">
                        <!-- Sidenav toggler -->
                        <div class="sidenav-toggler sidenav-toggler-dark d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                            <div class="sidenav-toggler-inner">
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="navbar-inner">
                    <!-- Collapse -->
                    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                        <!-- Nav items -->
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link <?=active_link('');?>" href="<?=site_url('v-manager');?>">
                                    <i class="ni ni-spaceship"></i>
                                    <span class="nav-link-text"><?=lang('Dashboards');?></span>
                                </a>
                            </li>
                            <?php
                            $active_menu = ['ecommerce_products', 'ecommerce_categories', 'ecommerce_fields'];
                            $permissions = check_permissions($active_menu);
                            if($permissions) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?=active_link($active_menu);?>" href="#navbar-ecommerce" data-toggle="collapse" role="button">
                                    <i class="fas fa-store"></i>
                                    <span class="nav-link-text"><?=lang('eCommerce');?></span>
                                </a>
                                <div class="collapse <?=active_link($active_menu, false, 'show');?>" id="navbar-ecommerce">
                                    <ul class="nav nav-sm flex-column">
                                        <?php if(isset($permissions['ecommerce_products'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/ecommerce_products');?>" class="nav-link <?=active_link('ecommerce_products');?>">
                                                <i class="fas fa-gifts"></i>
                                                <?=lang('Products');?>
                                            </a>
                                        </li>
                                        <? } ?>
                                        <?php if(isset($permissions['ecommerce_categories'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/ecommerce_categories');?>" class="nav-link <?=active_link('ecommerce_categories');?>">
                                                <i class="fas fa-list-alt"></i>
                                                <?=lang('Categories');?>
                                            </a>
                                        </li>
                                        <? } ?>
                                        <?php if(isset($permissions['ecommerce_fields'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/ecommerce_fields');?>" class="nav-link <?=active_link('ecommerce_fields');?>">
                                                <i class="fas fa-list-ul"></i>
                                                <?=lang('Fields');?>
                                            </a>
                                        </li>
                                        <? } ?>
                                    </ul>
                                </div>
                            </li>
                            <? } ?>
                            <?php
                            $active_menu = ['posts','pages','categories','tags', 'comments'];
                            $permissions = check_permissions($active_menu);
                            if($permissions) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?=active_link($active_menu);?>" href="#navbar-posts" data-toggle="collapse" role="button">
                                    <i class="fab fa-hacker-news-square"></i>
                                    <span class="nav-link-text"><?=lang('Posts');?></span>
                                </a>
                                <div class="collapse <?=active_link($active_menu, false, 'show');?>" id="navbar-posts">
                                    <ul class="nav nav-sm flex-column">
                                        <?php if(isset($permissions['posts'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/posts');?>" class="nav-link float-left <?=active_link('posts');?>">
                                                <i class="fa fa-newspaper"></i>
                                                <?=lang('Posts');?>
                                            </a>
                                            <a href="<?=site_url('v-manager/posts/item/');?>" class="nav-link float-right">
                                                <i class="fas fa-plus-circle"></i>
                                            </a>
                                            <div class="clearfix"></div>
                                        </li>
                                        <? } ?>
                                        <?php if(isset($permissions['categories'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/categories');?>" class="nav-link <?=active_link('categories');?>">
                                                <i class="fa fa-list-alt"></i>
                                                <?=lang('Categories');?>
                                            </a>
                                        </li>
                                        <? } ?>
                                    </ul>
                                </div>
                            </li>
                            <? } ?>
                            <?php
                            $active_menu = ['themes','menus','widgets','languages','slider'];
                            $permissions = check_permissions($active_menu);
                            if($permissions) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?=active_link($active_menu);?>" href="#navbar-themes" data-toggle="collapse" role="button">
                                    <i class="fas fa-laptop"></i>
                                    <span class="nav-link-text"><?=lang('Themes');?></span>
                                </a>
                                <div class="collapse <?=active_link($active_menu, false, 'show');?>" id="navbar-themes">
                                    <ul class="nav nav-sm flex-column">
                                        <?php
                                        if(check_permissions('menus')) {
                                        ?>
                                        <li class="nav-item">
                                            <a class="nav-link <?=active_link('menus');?>" href="<?=site_url('v-manager/menus');?>">
                                                <i class="fa fa-bars"></i>
                                                <span class="nav-link-text"><?=lang('Menus');?></span>
                                            </a>
                                        </li>
                                        <? } ?>
                                    </ul>
                                </div>
                            </li>
                            <? } ?>
							<?php
                            if(check_permissions('orders')) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?=active_link('orders');?>" href="<?=site_url('v-manager/orders');?>">
                                    <i class="far fa-image"></i>
                                    <span class="nav-link-text">Danh sách khách hàng</span>
                                </a>
                            </li>
                            <? } ?>
							<?php
                            if(check_permissions('payments')) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?=active_link('payments');?>" href="<?=site_url('v-manager/payments');?>">
                                    <i class="far fa-image"></i>
                                    <span class="nav-link-text">Đơn hàng</span>
                                </a>
                            </li>
                            <? } ?>

                            <?php
                            if(check_permissions('analytics')) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?=active_link('analytics');?>" href="<?=site_url('v-manager/analytics');?>">
                                    <i class="fa fa-chart-pie"></i>
                                    <span class="nav-link-text"><?=lang('Analytics');?></span>
                                </a>
                            </li>
                            <? } ?>
                            <?php
                            if(check_permissions('reports')) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?=active_link('reports');?>" href="<?=site_url('v-manager/reports');?>">
                                    <i class="fa fa-chart-line"></i>
                                    <span class="nav-link-text"><?=lang('Reports');?></span>
                                </a>
                            </li>
                            <? } ?>
                        </ul>
                        <!-- Divider -->
                        <hr class="my-3">
                        <!-- Heading -->
                        <h6 class="navbar-heading p-0 text-muted"><i class="fa fa-cogs"></i> <?=lang('More');?></h6>
                        <!-- Navigation -->
                        <ul class="navbar-nav mb-md-3">
<!--                            --><?php
//                            $active_menu = ['users','groups'];
//                            $permissions = check_permissions($active_menu);
//                            if($permissions) {
//                            ?>
<!--                            <li class="nav-item">-->
<!--                                <a class="nav-link --><?//=active_link($active_menu);?><!--" href="#navbar-users" data-toggle="collapse" role="button">-->
<!--                                    <i class="fa fa-users"></i>-->
<!--                                    <span class="nav-link-text">--><?//=lang('Users');?><!--</span>-->
<!--                                </a>-->
<!--                                <div class="collapse --><?//=active_link($active_menu, false, 'show');?><!--" id="navbar-users">-->
<!--                                    <ul class="nav nav-sm flex-column">-->
<!--                                        --><?php //if(isset($permissions['users'])) { ?>
<!--                                        <li class="nav-item">-->
<!--                                            <a href="--><?//=site_url('v-manager/users');?><!--" class="nav-link --><?//=active_link(['users']);?><!--">-->
<!--                                                <i class="fa fa-user"></i>-->
<!--                                                --><?//=lang('Users');?>
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                        --><?// } ?>
<!--                                        --><?php //if(isset($permissions['groups'])) { ?>
<!--                                        <li class="nav-item">-->
<!--                                            <a href="--><?//=site_url('v-manager/groups');?><!--" class="nav-link --><?//=active_link('groups');?><!--">-->
<!--                                                <i class="fa fa-users"></i>-->
<!--                                                --><?//=lang('Groups');?>
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                        --><?// } ?>
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </li>-->
<!--                            --><?// } ?>
                            <?php
                            $active_menu = ['admins','roles','permissions'];
                            $permissions = check_permissions($active_menu);
                            if($permissions) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?=active_link($active_menu);?>" href="#navbar-admins" data-toggle="collapse" role="button">
                                    <i class="fa fa-user-secret"></i>
                                    <span class="nav-link-text"><?=lang('Admins');?></span>
                                </a>
                                <div class="collapse <?=active_link($active_menu, false, 'show');?>" id="navbar-admins">
                                    <ul class="nav nav-sm flex-column">
                                        <?php if(isset($permissions['admins'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/admins');?>" class="nav-link <?=active_link(['admins']);?>">
                                                <i class="fa fa-user-secret"></i>
                                                <?=lang('Admins');?>
                                            </a>
                                        </li>
                                        <? } ?>
                                        <?php if(isset($permissions['roles'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/roles');?>" class="nav-link <?=active_link('roles');?>">
                                                <i class="fa fa-users"></i>
                                                <?=lang('Roles');?>
                                            </a>
                                        </li>
                                        <? } ?>
                                        <?php if(isset($permissions['permissions'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/permissions');?>" class="nav-link <?=active_link('permissions');?>">
                                                <i class="fa fa-user-lock"></i>
                                                <?=lang('Permissions');?>
                                            </a>
                                        </li>
                                        <? } ?>
                                    </ul>
                                </div>
                            </li>
                            <? } ?>
                            <?php
                            $active_menu = ['ip_alloweds','ip_banneds','access_logs'];
                            $permissions = check_permissions($active_menu);
                            if($permissions) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?=active_link($active_menu);?>" href="#navbar-ip_manager" data-toggle="collapse" role="button" aria-expanded="<?=active_link(['accounts','roles'], false, 'true');?>" aria-controls="navbar-accounts">
                                    <i class="fa fa-map-marker-alt"></i>
                                    <span class="nav-link-text"><?=lang('IP_manager');?></span>
                                </a>
                                <div class="collapse <?=active_link($active_menu, false, 'show');?>" id="navbar-ip_manager">
                                    <ul class="nav nav-sm flex-column">
                                        <?php if(isset($permissions['ip_alloweds'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/ip_alloweds');?>" class="nav-link <?=active_link(['ip_alloweds']);?>">
                                                <i class="fa fa-unlock"></i>
                                                <?=lang('IP_alloweds');?>
                                            </a>
                                        </li>
                                        <? } ?>
                                        <?php if(isset($permissions['ip_banneds'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/ip_banneds');?>" class="nav-link <?=active_link('ip_banneds');?>">
                                                <i class="fa fa-ban"></i>
                                                <?=lang('IP_banneds');?>
                                            </a>
                                        </li>
                                        <? } ?>
                                        <?php if(isset($permissions['access_logs'])) { ?>
                                        <li class="nav-item">
                                            <a href="<?=site_url('v-manager/access_logs');?>" class="nav-link <?=active_link(['access_logs']);?>">
                                                <i class="fa fa-fingerprint"></i>
                                                <?=lang('Access_logs');?>
                                            </a>
                                        </li>
                                        <? } ?>
                                    </ul>
                                </div>
                            </li>
                            <? } ?>
                            <li class="nav-item">
                                <a href="<?=site_url('v-manager/configurations');?>" class="nav-link <?=active_link(['configurations']);?>">
                                    <i class="fa fa-cog"></i>
                                    <?=lang('Configurations');?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <a href="https://8am.us" class="font-weight-bold ml-1" target="_blank">VCMS</a> version 2.0.0
            </div>
        </div>
    </nav>

	<!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        <nav class="navbar navbar-top navbar-expand">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <div class="nav-item d-xl-none">
                        <!-- Sidenav toggler -->
                        <div class="pr-3 sidenav-toggler" data-action="sidenav-pin" data-target="#sidenav-main">
                            <div class="sidenav-toggler-inner">
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                            </div>
                        </div>
                    </div>
                    <!-- Navbar links -->
                    <ul class="navbar-nav align-items-center ml-auto d-sm-flex d-none">
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:;" onclick="clear_cache(this);">
                                <i class="fa fa-sync"></i> Cache
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ni ni-bell-55"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right py-0 overflow-hidden">
                                <!-- Dropdown header -->
                                <div class="px-3 py-3">
                                    <h6 class="text-sm text-muted m-0">You have <strong class="text-primary">0</strong> notifications.</h6>
                                </div>
                                <!-- List group -->
                                <div class="list-group list-group-flush">
                                    <div href="javascript:;" class="list-group-item list-group-item-action">
										<div class="align-items-center text-center m-3">
											<span class="mb-0">No have any notifications!</span>
										</div>
										<!--div class="row align-items-center">
											<div class="col-auto">
												<img alt="Image placeholder" src="<?=CDN?>/images/no-avatar.jpg" class="avatar rounded-circle">
											</div>
											<div class="col ml--2">
											<div class="d-flex justify-content-between align-items-center">
											<div>
											<h4 class="mb-0 text-sm">John Snow</h4>
											</div>
											<div class="text-right text-muted">
											<small>2 hrs ago</small>
											</div>
											</div>
											<p class="text-sm mb-0">Let's meet at Starbucks at 11:30. Wdyt?</p>
											</div>
										</div-->
                                    </div>
                                </div>
                                <!-- View all -->
                                <a href="<?=site_url('v-manager/notifications');?>" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a>
                            </div>
                        </li>
                        <!--li class="nav-item dropdown">
                            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ni ni-ungroup"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-dark bg-default dropdown-menu-right">
                                <div class="row shortcuts px-4">
                                    <a href="#!" class="col-4 shortcut-item">
                                        <span class="shortcut-media avatar rounded-circle bg-gradient-red">
                                            <i class="ni ni-calendar-grid-58"></i>
                                        </span>
                                        <small>Calendar</small>
                                    </a>
                                    <a href="#!" class="col-4 shortcut-item">
                                        <span class="shortcut-media avatar rounded-circle bg-gradient-orange">
                                            <i class="ni ni-email-83"></i>
                                        </span>
                                        <small>Email</small>
                                    </a>
                                    <a href="#!" class="col-4 shortcut-item">
                                        <span class="shortcut-media avatar rounded-circle bg-gradient-info">
                                            <i class="ni ni-credit-card"></i>
                                        </span>
                                        <small>Payments</small>
                                    </a>
                                    <a href="#!" class="col-4 shortcut-item">
                                        <span class="shortcut-media avatar rounded-circle bg-gradient-green">
                                            <i class="ni ni-books"></i>
                                        </span>
                                        <small>Reports</small>
                                    </a>
                                    <a href="#!" class="col-4 shortcut-item">
                                        <span class="shortcut-media avatar rounded-circle bg-gradient-purple">
                                            <i class="ni ni-pin-3"></i>
                                        </span>
                                        <small>Maps</small>
                                    </a>
                                    <a href="#!" class="col-4 shortcut-item">
                                        <span class="shortcut-media avatar rounded-circle bg-gradient-yellow">
                                            <i class="ni ni-basket"></i>
                                        </span>
                                        <small>Shop</small>
                                    </a>
                                </div>
                            </div>
                        </li-->
                    </ul>
                    <ul class="navbar-nav align-items-center ml-auto ml-sm-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="media align-items-center">
                                    <span class="avatar avatar-sm rounded-circle">
                                        <img alt="Image placeholder" src="<?=$avatar?>">
                                    </span>
                                    <div class="media-body ml-2 d-none d-lg-block">
                                        <span class="mb-0 text-sm  font-weight-bold"><?=$account?></span>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0"><?=lang('Welcome');?></h6>
                                </div>
<!--                                <a href="--><?//=site_url('v-manager/account');?><!--" class="dropdown-item">-->
<!--                                    <i class="ni ni-settings-gear-65"></i>-->
<!--                                    <span>--><?//=lang('Account');?><!--</span>-->
<!--                                </a>-->
<!--                                <a href="https://vietvd.com/" class="dropdown-item">-->
<!--                                    <i class="ni ni-support-16"></i>-->
<!--                                    <span>--><?//=lang('Support');?><!--</span>-->
<!--                                </a>-->
                                <div class="dropdown-divider"></div>
                                <a href="<?=site_url('v-manager/logout');?>" class="dropdown-item">
                                    <i class="ni ni-user-run"></i>
                                    <span><?=lang('Logout');?></span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
		<?= $this->renderSection('content') ?>
    </div>
	<?=$cms->load_asset('js', 'admin.min.js'); ?>
	<?php
		$notice = get_message();
		if(isset($notice['message']) && $notice['message']) {
			$type = isset($notice['type'])?$notice['type']:'';
			echo notify($notice['message'], null, $type, true, [55,31], 'right');
		}
	?>
	<script>
		function clear_cache(element) {
			var raw_html = $(element).html();
			$(element).html('Processing...');
			$.ajax({url: "<?=URL;?>/v-manager/cached/flush", success: function(result){
				console.log(result);
					if(result.status == true) {
						$(element).css('color', 'green');
						$(element).css('font-weight', 'bold');
						$(element).html('Done');
					} else {
						$(element).css('color', 'red');
						$(element).css('font-weight', 'bold');
						$(element).html('Error');
					}
					setTimeout(function(){
						$(element).css('color', '#525f7f');
						$(element).css('font-weight', 'nomal');
						$(element).html(raw_html);
					}, 3000);
				}
			});
		}
	</script>
</body>
</html>