<?=$this->section('content');?>
		<!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col-md-6">
					<div class="card">
						<div class="card-content">
							<div class="row m-2">
								<div class="col-md-12">
									<h4 class="navbar-heading text-muted pt-3 pb-3"><?=lang('Account');?></h4>
									<div class="form-group row m-0 mb-2">
										<label class="form-control-label col-md-6 col-lg-4 pt-1" for="username"><?=lang('Username');?></label>
										<input type="text" class="col-md-6 col-lg-8 form-control form-control-sm" id="username" value="<?=$admin->username;?>" disabled>
									</div>
									<div class="form-group row m-0">
										<label class="form-control-label col-md-6 col-lg-4 pt-1" for="email"><?=lang('Email');?></label>
										<input type="text" class="col-md-6 col-lg-8 form-control form-control-sm" id="email" value="<?=$admin->email;?>" disabled>
									</div>
									<hr class="my-5">
									<h4 class="navbar-heading text-muted pb-3"><?=lang('Change password');?></h4>
									<?php
										if(isset($notice) && is_array($notice)) {
									?>
									<div class="alert-box">
										<div class="alert alert-<?=$notice['type']?> alert-dismissible fade show" role="alert">
											<span class="alert-text"><?=$notice['message']?></span>
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</div>
									<div class="clearfix"></div>
									<?php
											unset($notice);
										}
									?>
									<?= $validation->listErrors() ?>
									<form method="POST" action="<?=current_url()?>">
										<div class="form-group row m-0 mb-3">
											<label class="form-control-labell col-md-6 col-lg-4 pt-1" for="current_password"><?=lang('Current password');?></label>
											<input type="password" class="col-md-6 col-lg-8 form-control form-control-sm" name="current_password" id="current_password">
											<?////=form_error('current_password');?>
											<?= $validation->showError('current_password') ?>
										</div>
										<div class="form-group row m-0 mb-1">
											<label class="form-control-labell col-md-6 col-lg-4 pt-1" for="new_password"><?=lang('New password');?></label>
											<input type="password" class="col-md-6 col-lg-8 form-control form-control-sm" name="new_password" id="new_password">
											<?////=form_error('new_password');?>
											<?= $validation->showError('new_password') ?>
										</div>
										<div class="form-group row m-0">
											<label class="form-control-labell col-md-6 col-lg-4 pt-1" for="confirm_password"><?=lang('Confirm password');?></label>
											<input type="password" class="col-md-6 col-lg-8 form-control form-control-sm" name="confirm_password" id="confirm_password">
											<?////=form_error('confirm_password');?>
											<?= $validation->showError('confirm_password') ?>
										</div>
										<div class="mt-3 text-right">
											<button type="submit" class="btn btn-primary btn-sm"><?=lang('Change');?></button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card">
						<div class="card-content">
							<div class="row m-2">
								<div class="col-md-12">
									<h4 class="navbar-heading text-muted pt-3 pb-3"><?=lang('Auth code');?></h4>
									<?php if($auth['status']==0) { ?>
									<div class="form-group text-center">
										<img height="150px" width="150px" src="<?=$auth['qr_thumb']?>" alt="QR Code" />
										<p class="m-2"><?=lang('Add manual');?></p>
										<p class="p-0 m-0"><label class="m-0">ACCOUNT:</label> <span><?=$admin->username?></span></p>
										<p class="p-0 m-0"><label class="m-0">SECRET:</label> <span><?=$auth['secret']?></span></p>
										<hr class="my-2">
									</div>
									<div class="form-group row m-0 mb-1">
										<label class="form-control-label col-md-6 col-lg-4 pt-1" for="password"><?=lang('Current password');?></label>
										<input type="password" class="col-md-6 col-lg-8 form-control form-control-sm" name="password" id="password" required>
										<?//=form_error('password');?>
									</div>
									<div class="form-group row m-0">
										<label class="form-control-label col-md-6 col-lg-4 pt-1" for="auth_code"><?=lang('Auth code');?></label>
										<input type="auth_code" class="col-md-6 col-lg-8 form-control form-control-sm" name="auth_code" id="auth_code" required>
										<?//=form_error('auth_code');?>
									</div>
									<div class="mt-3 text-right">
										<button class="btn btn-primary btn-sm" onclick="active_auth()"><?=lang('Active');?></button>
									</div>
									<? } else { ?>
									<div class="form-group auth_status">
										<i class="fas fa-fingerprint"></i>
									</div>
									<div class="form-group row m-0 mb-1">
										<label class="form-control-label col-md-6 col-lg-4 pt-1" for="password"><?=lang('Current password');?></label>
										<input type="password" class="col-md-6 col-lg-8 form-control form-control-sm" id="deactive_password" required>
										<?//=form_error('password');?>
									</div>
									<div class="form-group row m-0">
										<label class="form-control-label col-md-6 col-lg-4 pt-1" for="auth_code"><?=lang('Auth code');?></label>
										<input type="auth_code" class="col-md-6 col-lg-8 form-control form-control-sm" id="deactive_auth_code" required>
										<?//=form_error('auth_code');?>
									</div>
									<div class="mt-3 text-right">
										<button class="btn btn-primary btn-sm" onclick="deactive_auth()"><?=lang('Dective');?></button>
									</div>
									<? } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="card" style="min-height: 565px">
						<!-- Card header -->
						<div class="card-header border-0">
							<h3 class="float-left mb-0"><?=lang('Access history');?></h3>
						</div>
						<div class="card-content table-responsive">
							<table class="table align-items-center table-flush">
								<thead class="thead-light">
									<tr>
										<th class="text-center"><?=lang('OS');?></th>
										<th class="text-center"><?=lang('Browser');?></th>
										<th class="text-center"><?=lang('IP Address');?></th>
										<th class="text-center"><?=lang('Created');?></th>
									</tr>
								</thead>
								<tbody class="list" id="list-item">
									<?php
										foreach($items as $item) {
											$agent->parse($item->useragent);
											$os = $agent->getPlatform();
											if($os == 'Unknown Windows OS') $os = 'Windows OS';
									?>
									<tr id="item-<?=$item->id;?>">
										<td class="text-center"><?=$item->ip_address;?></td>
										<td class="text-center"><?=$os;?></td>
										<td class="text-center">
											<i class="fab fa-<?=str_replace(" ", "-", strtolower($agent->getBrowser()))?>"></i> 
											<?=$agent->getBrowser().' '.$agent->getVersion();?>
										</td>
										<td class="text-center"><?=date("G:i d/m/Y", $item->created);?></td>
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			function active_auth() {
				var auth_code = $("#auth_code").val();
				var password = $("#password").val();
				$.post("<?=current_url();?>/active_auth", {auth_code:auth_code, password:password}, function(data) {
					if(data.status == "success") {
						notify(null, data.message, 'info');
						setTimeout(function(){
							location.reload();
						}, 1000);
					}
					else notify(null, data.message, 'warning');
				});
				return false;
			}
			function deactive_auth() {
				var auth_code = $("#deactive_auth_code").val();
				var password = $("#deactive_password").val();
				$.post("<?=current_url();?>/active_auth", {auth_code:auth_code, password:password}, function(data) {
					if(data.status == "success") {
						notify(null, data.message, 'info');
						setTimeout(function(){
							location.reload();
						}, 1000);
					}
					else notify(null, data.message, 'warning');
				});
				return false;
			}
		</script>
<?=$this->endSection();?>