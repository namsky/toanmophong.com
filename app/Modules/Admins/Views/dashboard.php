<?=$this->section('content');?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="col">
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="card card-stats">
						<!-- Card body -->
							<div class="card-body">
								<div class="row">
									<div class="col">
										<h5 class="card-title text-uppercase text-muted mb-0">Post in This Week</h5>
										<span class="h2 font-weight-bold mb-0"><?=isset($posts['this_week'])?$posts['this_week']:0?></span>
									</div>
									<div class="col-auto">
										<div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
										<i class="fa fa-newspaper"></i>
										</div>
									</div>
								</div>
								<p class="mt-3 mb-0 text-sm">
									<?
									$icon = 'fas fa-minus';
									$text = '';
									$percent = 0;
									if(isset($posts['this_week']) && isset($posts['last_week'])) {
										$is_up = $posts['this_week']>$posts['last_week'];
										$icon = ($is_up)?'fa fa-arrow-up':'fa fa-arrow-down';
										$text = ($is_up)?'success':'danger';
										if($posts['last_week'] == 0) $percent = 100;
										else $percent = abs(100-round($posts['this_week']/$posts['last_week']*100));
									}
									?>
									<span class="text-<?=$text?> mr-2"><i class="<?=$icon?>"></i> <?=$percent?>%</span>
									<span class="text-nowrap">Since last week</span>
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="card card-stats">
						<!-- Card body -->
							<div class="card-body">
								<div class="row">
									<div class="col">
										<h5 class="card-title text-uppercase text-muted mb-0">Last 7 days traffic</h5>
										<span class="h2 font-weight-bold mb-0"><?=isset($posts['view_this_week'])?number_format($posts['view_this_week']):0?></span>
									</div>
									<div class="col-auto">
										<div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
										<i class="fa fa-eye"></i>
										</div>
									</div>
								</div>
								<p class="mt-3 mb-0 text-sm">
									<?
									$icon = 'fas fa-minus';
									$text = '';
									$percent = 0;
									if(isset($posts['view_this_week']) && isset($posts['view_last_week'])) {
										$is_up = $posts['view_this_week']>$posts['view_last_week'];
										$icon = ($is_up)?'fa fa-arrow-up':'fa fa-arrow-down';
										$text = ($is_up)?'success':'danger';
										if($posts['view_last_week'] == 0) $percent = 100;
										else $percent = abs(100-round($posts['view_this_week']/$posts['view_last_week']*100));
									}
									?>
									<span class="text-<?=$text?> mr-2"><i class="<?=$icon?>"></i> <?=$percent?>%</span>
									<span class="text-nowrap">Since last week</span>
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="card card-stats">
						<!-- Card body -->
							<div class="card-body">
								<div class="row">
									<div class="col">
										<h5 class="card-title text-uppercase text-muted mb-0">Last 30 days traffic</h5>
										<span class="h2 font-weight-bold mb-0"><?=isset($posts['view_this_month'])?number_format($posts['view_this_month']):0?></span>
									</div>
									<div class="col-auto">
										<div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
										<i class="fa fa-globe"></i>
										</div>
									</div>
								</div>
								<p class="mt-3 mb-0 text-sm">
									<?
									$icon = 'fas fa-minus';
									$text = '';
									$percent = 0;
									if(isset($posts['view_this_month']) && isset($posts['view_last_month'])) {
										$is_up = $posts['view_this_month']>$posts['view_last_month'];
										$icon = ($is_up)?'fa fa-arrow-up':'fa fa-arrow-down';
										$text = ($is_up)?'success':'danger';
										if($posts['view_last_month'] == 0) $percent = 100;
										else $percent = abs(100-round($posts['view_this_month']/$posts['view_last_month']*100));
									}
									?>
									<span class="text-<?=$text?> mr-2"><i class="<?=$icon?>"></i> <?=$percent?>%</span>
									<span class="text-nowrap">Since last month</span>
								</p>
							</div>
						</div>
					</div>
					<?php
					$services = cms_config('list_services');
					if($services) {
					if(strpos($services, ',')) {
						$_services = explode(",", $services);
					} else $_services = [$service];
					?>
					<div class="col-lg-6 col-md-12">
						<div class="card">
							<!-- Card header -->
							<div class="card-header">
								<!-- Title -->
								<h5 class="h3 mb-0">SERVICE MONITOR</h5>
							</div>
							<!-- Card body -->
							<div class="card-body p-0">
								<!-- List group -->
								<ul class="list-group list-group-flush pb-1" data-toggle="checklist">
									<?php
									foreach($_services as $serice) {
										$description = '';
										switch($serice) {
											case 'nginx':
												$description = 'NGINX is web serving';
												break;
											case 'mysql':
												$description = 'Database';
												break;
											case 'mysqld':
												$description = 'Database';
												break;
											case 'php-fpm':
												$description = 'PHP is scripting language to run CMS';
												break;
											case 'php-cgi':
												$description = 'PHP is scripting language to run CMS';
												break;
											case 'redis':
												$description = 'REDIS is cache in-memory';
												break;
											case 'redis-server':
												$description = 'REDIS is cache in-memory';
												break;
											case 'memcached':
												$description = 'MEMCACHED is cache in-memory';
												break;
										}
										$is_running = check_service($serice);
									?>
									<li class="checklist-entry list-group-item flex-column align-items-start py-1 px-2">
										<div class="checklist-item checklist-item-<?=$is_running?'success':'danger'?>">
											<div class="checklist-info">
												<h5 class="checklist-title mb-0"><?=strtoupper($serice);?></h5>
												<small><?=$description?></small>
											</div>
											<div>
											<?php
												if($is_running) echo '<i style="font-size: 18px" class="fas fa-play-circle text-success"></i>';
												else echo '<i style="font-size: 18px" class="fas fa-stop-circle text-danger"></i>';
											?>
												
											</div>
										</div>
									</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
					<?php } ?>
					<div class="col-lg-12 col-md-12">
						<div class="card">
							<div class="card-header bg-transparent">
								<h6 class="text-muted text-uppercase ls-1 mb-1">Overview</h6>
								<h2 class="h3 mb-0">Traffic</h2>
							</div>
							<div class="card-body">
								<!-- Chart -->
								<div class="chart">
									<div class="chartjs-size-monitor">
										<div class="chartjs-size-monitor-expand">
											<div class=""></div>
										</div>
										<div class="chartjs-size-monitor-shrink">
											<div class=""></div>
										</div>
									</div>
									<!-- Chart wrapper -->
									<canvas id="chart-sales" class="chart-canvas chartjs-render-monitor" style="display: block; width: 346px; height: 350px;" height="350"></canvas>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
<?=$this->endSection();?>