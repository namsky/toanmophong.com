<?=$this->section('content');?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('admins');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
							<h3 class="float-left mb-0"><?=lang('Permissions');?></h3>
							<div class="float-right ml-3 mb-0">
								<a href="javascript:;"><i class="fas fa-sync"></i></a>
								<a href="javascript:;" onclick="render()" data-toggle="modal" data-target="#modal-item"><i class="fas fa-plus-circle"></i></a>
							</div>
						</div>
						<div class="card-content box-nestable">
							<div class="row nestable-header">
								<div class="col-md-2 text-left"><?=lang('Permissions');?></div>
								<div class="col-md-8 row">
									<div class="col-md-2"></div>
									<?php if(isset($roles) && $roles) { ?>
									<?php foreach($roles as $role) { ?>
									<div class="col-md-2 text-center" style="font-weight: bold;color:<?=$role->color;?>;"><?=$role->name;?></div>
									<?php } ?>
									<?php } ?>
								</div>
								<div class="col-md-2 text-center"><?=lang('Actions');?></div>
							</div>
							<div class="dd nestable">
								<ol class="list dd-list" id="list-item">
									<?php
									if(isset($items) && $items) {
										foreach($items as $permission) {
											foreach($permission as $item) {
												echo '<li id="item-'.$item->id.'" class="dd-item" data-id="'.$item->id.'"><div class="dd-colum row m-0">';
												echo '<div class="col-md-2 text-left dd-handle"><i class="fa fa-chevron-circle-right"></i> <span class="dd-content">'.$item->name.'</span></div>';
												echo '<div class="col-md-8 row">';
												echo '<div class="col-md-2"></div>';
												foreach($roles as $role) {
													$checked = '';
													if(isset($item->data) && $item->data) {
														foreach($item->data as $per) {
															if($per->role_id == $role->id && $per->value == 1) {
																$checked = 'checked';
																break;
															}
														}
													}
													?>
														<div class="col-md-2 text-center">
															<label class="custom-toggle custom-toggle-default">
																<input class="permission_role" type="checkbox" <?=$checked?> data-permission_id="<?=$item->id?>" data-role_id="<?=$role->id?>" />
																<span class="custom-toggle-slider rounded-circle" data-label-off="" data-label-on=""></span>
															</label>
														</div>
													<?php
												}
												echo '</div><div class="col-md-2 text-center">
														<a href="javascript:;" onclick="render('.$item->id.')" data-toggle="modal" data-target="#modal-item" class="btn btn-sm btn-primary">
															<span class="fa fa-edit"></span>
														</a>
														<a href="javascript:;" onclick="remove('.$item->id.')" class="btn btn-sm btn-danger">
															<span class="fa fa-trash"></span>
														</a>
													</div>
													</div>
												</li>';
											}
											echo '<li class="space"><span class="text-center">&nbsp;</span></li>';
										}
									} else {
										?>
										<tr class="dd-item" data-id="space">
											<td colspan="100" class="text-center no-data">No database</td>
										</tr>
										<?
									}
									?>
								</ol>
							</div>
						</div>
                        <!-- Card footer -->
                        <div class="card-footer py-4">
							<div class="col-sm-12 col-md-12">
							</div>
                        </div>
					</div>

					<!-- Modal -->
					<div class="modal fade custom" id="modal-item" tabindex="-1" role="dialog" data-backdrop="false">
						<div class="modal-dialog">
							<div class="modal-content">
								<form id="form-item">
									<div class="card-header">
										<a href="javascript:;" class="back" data-dismiss="modal" aria-label="Go back">
											<i class="fas fa-arrow-left"></i> <?=lang('Go_back');?>
										</a>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="name" class="form-control-label"><?=lang('Name');?></label>
													<input class="form-control form-control-sm" type="hidden" value="" id="id" name="id">
													<input class="form-control form-control-sm" type="text" value="" id="name" name="name">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="description" class="form-control-label"><?=lang('Description');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="description" name="description">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="slug" class="form-control-label"><?=lang('Slug');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="slug" name="slug">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="function" class="form-control-label"><?=lang('Function');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="function" name="function">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="order" class="form-control-label"><?=lang('Order');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="order" name="order">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="order" class="form-control-label"><?=lang('Group');?></label>
													<input class="form-control form-control-sm" type="text" value="" id="group" name="group">
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" id="btn_save" class="btn btn-primary" onClick="save();"><?=lang('Save');?></button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		$cms->gen_js();
		?>
		<script>
			function render_list(data) {
				if(data) {
					$("#list-item").html('');
					$.ajax({url: "<?=current_url();?>/get_data?item_id="+id, success: function(result){
						$.each(result, function(key, value) {
							if($("#"+key).attr('type') == 'checkbox') {
								var value = parseInt(value);
								$("#"+key).attr("checked", !!value);
							}
							else {
								if($.isNumeric(value)) value = parseInt(value);
								$("#"+key).val(value).change();
							}
						});
					}, dataType: "json"});
				} else console.log('No ID');
			}
			(function() {
				$(".permission_role").change(function() {
					var value = 0;
					if(this.checked) value = 1;
					var permission_id = $(this).data('permission_id');
					var role_id = $(this).data('role_id');
					$.post("<?=current_url();?>/update_permission", {permission_id:permission_id, role_id:role_id, value:value}, function(data,status){
						if(data.status!=true) alert(data.message);
					});
				});
				if ($('.nestable').length && $.fn.nestable) {
					//$('.nestable').nestable({maxDepth: 1});
				}
				$('.dd').on('change', function() {
					//var data = $('.nestable').nestable('serialize');
					//data = window.JSON.stringify(data);
					//$.post("<?=current_url();?>/update_order", {data:data}, function(data,status){});
				});
			})();
		</script>
<?=$this->endSection();?>