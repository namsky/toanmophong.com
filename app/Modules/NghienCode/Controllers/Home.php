<?php
namespace App\Modules\NghienCode\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Core\Cms\CmsController;

class Home extends CmsController
{
    public function __construct()
    {
        $this->cart = \Config\Services::cart();
    }
    public function index()
    {

        $this->view->setVar('cart', $this->cart);

        $this->view->setVar('title', cms_config('site_title'));
        $this->view->setVar('description', cms_config('site_description'));
        $this->view->setVar('keywords', cms_config('site_keywords'));
        // $this->view->setVar('video_home', cms_config('video_abc'));
        $this->view->setVar('page', 'home');

        $cached = cms_config('cache');
        $widgets = $cached?$this->cache->get('widget_page_1'):false;
        if(!$widgets) {
            $widgetModel = model('App\Modules\Cms\Models\WidgetModel');
            $widgets = $widgetModel->where('page_id', 1)->orderBy('order', 'ASC')->findAll();
            $this->cache->save('widget_page_1', $widgets, 86400);
        }
        $this->view->setVar('widgets', $widgets);

        $sliders = model('App\Modules\Cms\Models\SliderModel');
        $sliderTop = $sliders->where('type','sliderTop')->orderBy('id', 'DESC')->findAll();
        $this->view->setVar('sliderTop', $sliderTop);
        $bannerTop = $sliders->where('type','bannerTop')->orderBy('id', 'DESC')->findAll(4, 0);
        $this->view->setVar('bannerTop', $bannerTop);
        $bannerBottom = $sliders->where('type','bannerBottom')->orderBy('id', 'DESC')->findAll(4, 0);
        $this->view->setVar('bannerBottom', $bannerBottom);

        $product_model = model('App\Modules\Cms\Models\EcommerceProductModel');
        $new_product = $product_model->orderBy('id','DESC')->findAll(8, 0);
        $this->view->setVar('new_product',$new_product);
        $sale_product = $product_model->orderBy('sales','DESC')->findAll(8, 0);
        $this->view->setVar('sale_product',$sale_product);

        return $this->view->render('home');
    }
    public function about()
    {
        $this->view->setVar('cart', $this->cart);
        $this->view->setVar('title', cms_config('site_title'));
        $this->view->setVar('description', cms_config('site_description'));
        $this->view->setVar('keywords', cms_config('site_keywords'));
        $this->view->setVar('page', 'about');
        $cached = cms_config('cache');

        return $this->view->render('about');
    }
    public function shop()
    {
        $this->view->setVar('cart', $this->cart);
        $this->view->setVar('title', cms_config('site_title'));
        $this->view->setVar('description', cms_config('site_description'));
        $this->view->setVar('keywords', cms_config('site_keywords'));
        $this->view->setVar('page', 'shop');
        $cached = cms_config('cache');

        return $this->view->render('shop');
    }
    public function chantroisangtao() {
        $category_product_model = model('App\Modules\Cms\Models\EcommerceCategoryModel');

        $categorys = $category_product_model->where('parent',37)->orderBy('order','ASC')->findAll();
        $parent_category = $category_product_model->find(37);
        foreach ($categorys as $key=>$value) {
            $list_childs = $category_product_model->where('parent',$value->id)->orderBy('order','ASC')->findAll();
            $data['cate'][$key]['name'] = $value->name;
            $data['cate'][$key]['slug'] = $value->slug;
            foreach ($list_childs as $k=>$item) {
                $data['cate'][$key]['child'][$k]['name'] = $item->name;
                $data['cate'][$key]['child'][$k]['slug'] = $item->slug;
            }
            if(!empty($data['cate'][$key]['child']))  foreach ($data['cate'][$key]['child'] as $ky=>$n) {
                $last_child = $category_product_model->where('slug',$n['slug'])->orderBy('order','ASC')->first();
                $child_childs = $category_product_model->where('parent',$last_child->id)->orderBy('order','ASC')->findAll();
                 if(!empty($child_childs)) foreach ($child_childs as $kk=>$v) {
                     $data['cate'][$key]['child'][$k]['last_child'][$kk]['name'] = $v->name;
                     $data['cate'][$key]['child'][$k]['last_child'][$kk]['slug'] = $v->slug;
                 }
            }
        }
        $this->view->setVar('items', $data);
        $this->view->setVar('list_childs', $list_childs);
        $this->view->setVar('partent_cate', $parent_category);
        return $this->view->render('product_list');
    }
    public function canhdieu() {
        $category_product_model = model('App\Modules\Cms\Models\EcommerceCategoryModel');
        $categorys = $category_product_model->where('parent',38)->orderBy('order','ASC')->findAll();
        $parent_category = $category_product_model->find(38);
        foreach ($categorys as $key=>$value) {
            $list_childs = $category_product_model->where('parent',$value->id)->orderBy('order','ASC')->findAll();
            $data['cate'][$key]['name'] = $value->name;
            $data['cate'][$key]['slug'] = $value->slug;
            foreach ($list_childs as $k=>$item) {
                $data['cate'][$key]['child'][$k]['name'] = $item->name;
                $data['cate'][$key]['child'][$k]['slug'] = $item->slug;
            }
            if(!empty($data['cate'][$key]['child']))  foreach ($data['cate'][$key]['child'] as $ky=>$n) {
                $last_child = $category_product_model->where('slug',$n['slug'])->orderBy('order','ASC')->first();
                $child_childs = $category_product_model->where('parent',$last_child->id)->orderBy('order','ASC')->findAll();
                if(!empty($child_childs)) foreach ($child_childs as $kk=>$v) {
                    $data['cate'][$key]['child'][$k]['last_child'][$kk]['name'] = $v->name;
                    $data['cate'][$key]['child'][$k]['last_child'][$kk]['slug'] = $v->slug;
                }
            }
        }
        $this->view->setVar('items', $data);
        $this->view->setVar('list_childs', $list_childs);
        $this->view->setVar('partent_cate', $parent_category);
        return $this->view->render('product_list');
    }
    public function ketnoitrithucvoicuocsong() {
        $category_product_model = model('App\Modules\Cms\Models\EcommerceCategoryModel');
        $categorys = $category_product_model->where('parent',39)->orderBy('order','ASC')->findAll();
        $parent_category = $category_product_model->find(39);
        foreach ($categorys as $key=>$value) {
            $list_childs = $category_product_model->where('parent',$value->id)->orderBy('order','ASC')->findAll();
            $data['cate'][$key]['name'] = $value->name;
            $data['cate'][$key]['slug'] = $value->slug;
            foreach ($list_childs as $k=>$item) {
                $data['cate'][$key]['child'][$k]['name'] = $item->name;
                $data['cate'][$key]['child'][$k]['slug'] = $item->slug;
            }
            if(!empty($data['cate'][$key]['child']))  foreach ($data['cate'][$key]['child'] as $ky=>$n) {
                $last_child = $category_product_model->where('slug',$n['slug'])->orderBy('order','ASC')->first();
                $child_childs = $category_product_model->where('parent',$last_child->id)->orderBy('order','ASC')->findAll();
                if(!empty($child_childs)) foreach ($child_childs as $kk=>$v) {
                    $data['cate'][$key]['child'][$k]['last_child'][$kk]['name'] = $v->name;
                    $data['cate'][$key]['child'][$k]['last_child'][$kk]['slug'] = $v->slug;
                }
            }
        }
        $this->view->setVar('items', $data);
        $this->view->setVar('list_childs', $list_childs);
        $this->view->setVar('partent_cate', $parent_category);
        return $this->view->render('product_list');
    }
    public function contact()
    {
        $this->view->setVar('cart', $this->cart);
        $this->view->setVar('title', cms_config('site_title'));
        $this->view->setVar('description', cms_config('site_description'));
        $this->view->setVar('keywords', cms_config('site_keywords'));
        $cached = cms_config('cache');
        $this->cms->load_vendors([
            'sweetalert2',
        ]);

        return $this->view->render('contact');
    }
    public function search()
    {
        $this->view->setVar('cart', $this->cart);
        $uri = $this->request->uri;
        $key = strip_tags($this->request->getGet('query'));
        $postModel = model('App\Modules\Cms\Models\EcommerceProductModel');

        $cached = cms_config('cache');
        $widgets = $cached?$this->cache->get('widget_page_2'):false;
        if(!$widgets) {
            $widgetModel = model('App\Modules\Cms\Models\WidgetModel');
            $widgets = $widgetModel->where('page_id', 2)->orderBy('order', 'ASC')->findAll();
            $this->cache->save('widget_page_2', $widgets, 86400);
        }
        $this->view->setVar('widgets', $widgets);

        $this->view->setVar('title', 'tìm kiếm '.$key.' | '.cms_config('site_name'));
        $page = intval($this->request->getGet('page'));
        if($page == 0) $page = 1;
        $items = $cached?$this->cache->get('search_'.$key.'_items_p'.$page):false;
        $paging = $cached?$this->cache->get('search_'.$key.'_paging_p'.$page):false;
        if(!$items) {
            $order = 'created DESC';
            $parameter = array();
            if($this->request->getGet('color')!=null) {
                $parameter['color'] = $this->request->getGet('color');
            }
            if($this->request->getGet('version')!=null) {
                $parameter['version'] = $this->request->getGet('version');
            }
            if($this->request->getGet('brand')!=null) {
                $parameter['brand'] = $this->request->getGet('brand');
            }
            if($this->request->getGet('sortby')!=null) {
                $sortby = $this->request->getGet('sortby');
                switch ($sortby) {
                    case 'alpha-asc':
                        $order = 'name ASC';
                        break;
                    case 'alpha-desc':
                        $order = 'name DESC';
                        break;
                    case 'price-asc':
                        $order = 'price ASC';
                        break;
                    case 'price-desc':
                        $order = 'price DESC';
                        break;
                    case 'created-desc':
                        $order = 'created DESC';
                        break;
                    case 'created-asc':
                        $order = 'created ASC';
                        break;
                }
            }
            $items = $postModel->where('status', 1)->with('user', ['fields'=>'id,username,name'])->where($parameter)->orderBy($order)->where('deleted',NULL)->with('relations')->Like('name', $key)->paginate(12);
            $paging = $postModel->pager->links();
            $this->cache->save('search_'.$key.'_items_p'.$page, $items, 86400);
            $this->cache->save('search_'.$key.'_paging_p'.$page, $paging, 86400);
        }
        $this->view->setVar('key', $key);
        $this->view->setVar('items', $items);
        $this->view->setVar('paging', $paging);
        $this->view->setVar('page', 'search');
        $this->cms->load_vendors([
            'fontawesome',
            'jquery',
            'bootstrap',
            'sweetalert2',
            'jquery.lazyload',
        ]);
        return $this->view->render('products');
    }

    public function router()
    {
        $this->view->setVar('cart', $this->cart);
        /* Controller of Category & Single Post */
        $request_uri = $this->request->getServer('REQUEST_URI');
        $last_of_uri = substr($request_uri, -1);
        if($last_of_uri == '/') {
            return cms_redirect($uri);
        }
        $uri = $this->request->uri;
        $slug = $uri->getSegment(1);
        $categoryModel = model('App\Modules\Cms\Models\CategoryModel');
        $postModel = model('App\Modules\Cms\Models\PostModel');

        $categoryProductModel = model('App\Modules\Cms\Models\EcommerceCategoryModel');
        $categoryProduct = $categoryProductModel->where('slug', $slug)->first();
        if (isset($categoryProduct->id)) {
            $order = 'created DESC';
            $parameter = array();
            if($this->request->getGet('color')!=null) {
                $parameter['color'] = $this->request->getGet('color');
            }
            if($this->request->getGet('version')!=null) {
                $parameter['version'] = $this->request->getGet('version');
            }
            if($this->request->getGet('brand')!=null) {
                $parameter['brand'] = $this->request->getGet('brand');
            }
            if($this->request->getGet('sortby')!=null) {
                $sortby = $this->request->getGet('sortby');
                switch ($sortby) {
                    case 'alpha-asc':
                        $order = 'name ASC';
                        break;
                    case 'alpha-desc':
                        $order = 'name DESC';
                        break;
                    case 'price-asc':
                        $order = 'price ASC';
                        break;
                    case 'price-desc':
                        $order = 'price DESC';
                        break;
                    case 'created-desc':
                        $order = 'created DESC';
                        break;
                    case 'created-asc':
                        $order = 'created ASC';
                        break;
                }
            }
//            echo "<pre>";
//            print_r($order);die;
            $productModel = model('App\Modules\Cms\Models\EcommerceProductModel');
            $items = $productModel->where('status', 1)->where($parameter)->orderBy($order)->where('deleted',NULL)->with('relations')->categories([$categoryProduct->id])->paginate(16);
            $paging = $productModel->pager->links();
            $this->view->setVar('category', $categoryProduct->name);
            $this->view->setVar('items', $items);
            $this->view->setVar('paging', $paging);
            return $this->view->render('collections');
        }

        $cached = cms_config('cache');
        $category = $cached?$this->cache->get('category_'.$slug):NULL;
        if(is_null($category)) {
            $category = $categoryModel->where('slug', $slug)->first();
            if(is_null($category)) $category = false;
            $this->cache->save('category_'.$slug, $category, 86400);
        }
        if(!isset($category->id)) {
            $post = $cached?$this->cache->get('post_'.$slug):NULL;
            $parents = $cached?$this->cache->get('post_'.$slug.'_parents'):false;
            if(is_null($post)) {
                $post = $postModel->with('faqs')->with('user', ['fields'=>'id,username,name,avatar'])->with('relations')->where('slug', $slug)->first();
                if(is_null($post)) {
                    $post = false;
                    $parents = false;
                }
                else {
                    if(!empty($post->categories)) {
                        $categories = $post->categories;
                        if(!empty($categories)) $_category = array_pop($categories);
                        if(!empty($_category->parent)) $parents = $categoryModel->get_parents($_category->parent);
                    }
                }
                $this->cache->save('post_'.$slug.'_parents', $parents, 86400);
                $this->cache->save('post_'.$slug, $post, 86400);
            }
        }
        if(isset($category->id)) {
            /* Render archives */
            $this->view->setVar('title', ($category->title)?$category->title:$category->name.' | '.cms_config('site_name'));
            $this->view->setVar('description', ($category->description)?$category->description:cms_config('site_description'));
            $this->view->setVar('keywords', ($category->keywords)?$category->keywords:cms_config('site_keywords'));
            $page = intval($this->request->getGet('page'));
            if($page == 0) $page = 1;
            $items = $cached?$this->cache->get('category_'.$slug.'_items_p'.$page):false;
            $paging = $cached?$this->cache->get('category_'.$slug.'_paging_p'.$page):false;
            $parents = $cached?$this->cache->get('category_'.$slug.'_parents'.$page):false;
            if(!$items) {
                $items = $postModel->where('status', 1)->where('type>', 0)->where('published<', time())->where('deleted',NULL)->with('user', ['fields'=>'id,username,name'])->with('relations')->orderBy('published', 'DESC')->paginate(40);
                $paging = $postModel->pager->links();
                $parents = $categoryModel->get_parents($category->parent);
                $this->cache->save('category_'.$slug.'_items_p'.$page, $items, 86400);
                $this->cache->save('category_'.$slug.'_paging_p'.$page, $paging, 86400);
                $this->cache->save('category_'.$slug.'_parents'.$page, $parents, 86400);
            }

            $widgets = $cached?$this->cache->get('widget_page_2'):false;
            if(!$widgets) {
                $widgetModel = model('App\Modules\Cms\Models\WidgetModel');
                $widgets = $widgetModel->where('page_id', 2)->orderBy('order', 'ASC')->findAll();
                $this->cache->save('widget_page_2', $widgets, 86400);
            }
            $this->view->setVar('widgets', $widgets);

            $this->view->setVar('parents', $parents);
            $this->view->setVar('category', $category);
            $this->view->setVar('items', $items);
            $this->view->setVar('paging', $paging);
            $this->view->setVar('page', 'category');
            $this->cms->load_vendors([
                'sweetalert2',
            ]);
            return $this->view->render('archives');
        } elseif(isset($post->id)) {
            /* Render single */
            $widgets = $cached?$this->cache->get('widget_page_3'):false;
            if(!$widgets) {
                $widgetModel = model('App\Modules\Cms\Models\WidgetModel');
                $widgets = $widgetModel->where('page_id', 3)->orderBy('order', 'ASC')->findAll();
                $this->cache->save('widget_page_3', $widgets, 86400);
            }
            $this->view->setVar('widgets', $widgets);

            $userModel = model('App\Modules\Cms\Models\UserModel');
            $userName = $userModel->where('id',$post->user_id)->first();

            $amp = $uri->getSegment(2);
            $this->view->setVar('title', ($post->seo_title)?$post->seo_title:$post->title.' | '.cms_config('site_name'));
            $this->view->setVar('description', ($post->seo_description)?$post->seo_description:$post->summary);
            $this->view->setVar('keywords', ($post->seo_keywords)?$post->seo_keywords:cms_config('site_keywords'));
            $this->view->setVar('page', 'single_news');
            $this->view->setVar('item', $post);
            $this->view->setVar('user', $userName);

            if(!empty($parents)) $this->view->setVar('parents', $parents);
            $postModel->increase_views($post->id);
            if($post->type == -1) {
                return $this->view->render('page');
            } else {
                if($amp == 'amp') {
                    $data = $this->view->getData();
                    return view('\App\Modules\Nghiencode\Views\amp\single', $data);
                } else  {
                    return $this->view->render('single_news');
                }
            }
        } else throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }
}