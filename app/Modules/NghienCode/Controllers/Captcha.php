<?php
namespace App\Modules\NghienCode\Controllers;
use App\Core\Cms\CmsController;

class Captcha extends CmsController
{
	private $session;
	public function __construct()
	{
		$this->session = session();
	}
	public function index()
	{
		$width = $this->request->getGet('width');
		$width = $width?$width:150;
		$height = $this->request->getGet('height');
		$height = $height?$height:40;
		$characters = 5;
		$code = $this->generateCode($characters);
		$this->session->set(['secure_code' => $code]);
		$font = FCPATH.'/statics/fonts/captcha.ttf';
		/* font size will be 90% of the image height */
		$font_size = $height * 0.70;
		$image = @imagecreate($width, $height) or die('Cannot initialize new GD image stream');
		/* set the colours */
		$bg = $this->request->getGet('bg');
		if(!empty($bg)) {
			$bg = base64_decode($bg);
			$colors = json_decode($bg, true);
			if(is_array($colors) && count($colors) == 3) {
				$background_color = imagecolorallocate($image, $colors[0], $colors[1], $colors[2]);
			}
		}
		if(empty($background_color)) $background_color = imagecolorallocate($image, 255, 255, 255);
		$text_color = imagecolorallocate($image, 237, 28, 36);
		$noise_color = imagecolorallocate($image, 255, 64, 64);
		/* generate random dots in background */
		//for( $i=0; $i<1000; $i++ ) imagefilledellipse($image, mt_rand(0, $width), mt_rand(0, $height), rand(0,3), rand(0,3), $noise_color);
		/* generate random lines in background */
		//for( $i=0; $i<5; $i++ ) imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $noise_color);
		//for( $i=0; $i<50; $i++ ) imagefilledellipse($image, mt_rand(10, $width-10), mt_rand(10, $height-10), rand(0,4), rand(0,4), $noise_color);
		//for( $i=0; $i<100; $i++ ) imagefilledellipse($image, mt_rand(10, $width-10), mt_rand(10, $height-10), rand(0,10), rand(0,10), $background_color);
		//for( $i=0; $i<50; $i++ ) imageline($image, mt_rand(10,$width-10), mt_rand(10,$height-10), mt_rand(10,$width-10), mt_rand(10,$height-10), $noise_color);
		//for( $i=0; $i<50; $i++ ) imageline($image, mt_rand(10,$width-10), mt_rand(10,$height-10), mt_rand(10,$width-10), mt_rand(10,$height-10), $background_color);
		/* create textbox and add text */
		$textbox = imagettfbbox($font_size, 0, $font, $code) or die('Error in imagettfbbox function');
		$x = ($width - $textbox[4])/2;
		$y = ($height - $textbox[5])/2;
		imagettftext($image, $font_size, 0, $x, $y, $text_color, $font , $code) or die('Error in imagettftext function');
		/* output captcha image to browser */
		$this->response->setHeader("Content-Type", "image/png");
		echo imagepng($image);
	}
	function generateCode($characters) {
		/* list all possible characters, similar looking characters and vowels have been removed */
		$possible = '0123456789abcdfghjkmnpqrstvwxyz';
		$code = '';
		$i = 0;
		while ($i < $characters) {
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		return $code;
	}
}