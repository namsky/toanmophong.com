<?php
namespace App\Modules\NghienCode\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Core\Cms\CmsController;

class Payment extends CmsController
{
    public function __construct(){
        $this->cart = \Config\Services::cart();
    }
    public function index(){

    }
    public function deposit_bank() {
        $paymentsModel = model('App\Modules\Cms\Models\PaymentsModel') ;
        $productModel = model('App\Modules\Cms\Models\EcommerceProductModel') ;
        $session = \Config\Services::session();
        $posts = $this->request->getGet();
        if((isset($posts['ordercode']) && $posts['ordercode']) || (isset($posts['order_code']) && $posts['order_code'])) {
            $chat_id = -686297052;
            $token_telegram = '5102745546:AAHwvxzc00hLm3M_T8oAcu4JtFg7Eb2qlzM';
            $ordercode = (isset($posts['ordercode']) && $posts['ordercode'])?$posts['ordercode']:$posts['order_code'];
            $payment_bank = $paymentsModel->where('order_code',$ordercode)->first();
//            echo "<pre>";
//            print_r($payment_bank);die;
            if(isset($payment_bank->id) && $payment_bank->id) {
                $token = explode('/', $payment_bank->url);
                $result = payment_nganluong_result(end($token));
//                echo "<pre>";
//                print_r($result);die;
                if(isset($result['status']) && $result['status']=='success') {
                    $session->set('download',1);
                    $data['status'] = 200;
                    $data['text'] = 'Chúc mừng bạn đã thanh toán thành công với số tiền là: '.number_format($result['data']['total_amount']).' VNĐ.';
                    $status = 'Thành công';
                    if($payment_bank->status == 'pending') {
                        $arg = array(
                            'transaction_id'=>$result['data']['transaction_id'],
                            'status'=>'success',
                            'url'=>'',
                            'response_code'=>'00',
                            'response_message'=>'Giao dịch thành công'
                        );
                        $paymentsModel->update($payment_bank->id, $arg);
                    }
                    if(!empty($this->cart->contents())) {
                        foreach($this->cart->contents() as $key=>$item){
                            $product = $productModel->find($item['id']);
                            $data['link'][$key] = $product->file_download;
                        }
                    }
                    $params=[
                        'chat_id'=>$chat_id,
                        'text'=>'Khách hàng thanh toán thành công với số tiền : '.number_format($result['data']['total_amount']).'đ . Trạng thái giao dịch: Success.',
                    ];
                    $this->cart->destroy();
                } else if($result['status'] == 'error') {
                    $params=[
                        'chat_id'=>$chat_id,
                        'text'=>'Khách hàng thanh toán không thành công. Trạng thái giao dịch: Error. Lý do: '.$result['message'].'',
                    ];
                    $data['status'] = 201;
                    $data['text'] = $result['message'];
                    if($payment_bank->status == 'pending') {
                        $arg = array(
                            'status'=>'error',
                            'url'=>'',
                            'response_message'=>$result['message']
                        );
                        $paymentsModel->update($payment_bank->id, $arg);
                    }
                } else {
                    $params=[
                        'chat_id'=>$chat_id,
                        'text'=>'Khách hàng thanh toán không thành công. Trạng thái giao dịch: Error.',
                    ];
                    $data['status'] = 201;
                    $data['text'] = 'Giao dịch lỗi! Vui lòng thử lại sau!';
                    $status = 'Giao dịch lỗi';
                    if($payment_bank->status == 'pending') {
                        $arg = array(
                            'transaction_id'=>$result['data']['transaction_id'],
                            'status'=>'error',
                            'url'=>'',
                            'response_code'=>$result['data']['error_code'],
                            'response_message'=>'Giao dịch lỗi'
                        );
                        $paymentsModel->update($payment_bank->id, $arg);
                    }
                }
                $ch = curl_init('https://api.telegram.org/bot'.$token_telegram.'/sendMessage');
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);
            }
        }
        $this->view->setVar('data', $data);
        echo $this->view->render('deposit');
    }
    public function deposit()
    {
        $posts = $this->request->getGet();
//        print_r($posts);die;
        $payment_id = $posts['payment_id'];
        $orderId = $posts['orderId'];
        $payment_gateway = $posts['payment_gateway'];
        $customerModel = model('App\Modules\Cms\Models\CustomersModel') ;
        $orderModel = model('App\Modules\Cms\Models\OrdersModel') ;
        $paymentsModel = model('App\Modules\Cms\Models\PaymentsModel') ;
        if(isset($payment_id) && $payment_id) {
            $accessKey = 'xd6TDa55RvJprP2t';
            $partnerCode = 'MOMOQOW720210808';
            $serect_key = 'kp3dBTAEGXrtNGJ8nLVKPTN9wQ2lHTB4';
            $url_api_momo_query = 'https://test-payment.momo.vn/v2/gateway/api/query';
            if(isset($payment_gateway) && $payment_gateway=='momo') {
                $payment_momo = $paymentsModel->find($payment_id);
//                print_r($payment_momo->orderid);die;
                if($payment_momo->orderid == $orderId) {
//                    $user = $this->user_model->find($payment_momo['user_id']);
//                    $data['user'] = $user;
                    if(isset($payment_momo->id) && $payment_momo->id) {
                        if($payment_momo->status=='pending') {
                            $requestId = $payment_momo->created.'';
                            $orderId = $payment_momo->orderid.'';
                            $rawHash = "accessKey=".$accessKey ."&orderId=".$orderId ."&partnerCode=".$partnerCode ."&requestId=".$requestId ;

                            $signature = hash_hmac("sha256", $rawHash, $serect_key);
                            $data_momo = array(
                                'partnerCode' => $partnerCode,
                                'orderId' => $orderId,
                                'lang' => 'vi',
                                'requestId' => $requestId,
                                'signature' => $signature,
                            );

                            $result = execPostRequest($url_api_momo_query, json_encode($data_momo));
                            $jsonResult =json_decode($result,true);
                            if(isset($jsonResult['resultCode'])) {
                                if($jsonResult['resultCode']===0) {
                                    $paymentsModel->update($payment_momo->id, array('status'=>'success', 'note'=>$jsonResult['message']));

                                    $data['status'] = 200;
                                    $data['text'] = 'Chúc mừng bạn đã thực hiện giao dịch thành công với '.number_format($payment_momo->amount).' VNĐ.';
                                    $status = 'Thành công';
                                    $this->cart->destroy();
                                } elseif($jsonResult['resultCode']==49) {
                                    $data['status'] = 201;
                                    $data['text'] = $jsonResult['message'];
                                    $paymentsModel->update($payment_momo->id, array('status'=>'error', 'note'=>$jsonResult['message']));
                                    $status = 'Thất bại';
                                } else {
                                    $data['status'] = 201;
                                    $data['text'] = $jsonResult['message'];
                                    $paymentsModel->update($payment_momo->id, array('status'=>'error', 'note'=>$jsonResult['message']));
                                    $status = 'Thất bại';
                                }
                            }
                            $chat_id = -686297052;
                            $token = '5102745546:AAHwvxzc00hLm3M_T8oAcu4JtFg7Eb2qlzM';
                            $params=[
                                'chat_id'=>$chat_id,
                                'text'=>'Khách hàng mua tài liệu. Số tiền: '.number_format($payment_momo->amount).'đ . Trạng thái giao dịch: '.$status.'. Note: '.$data['text'].'',
                            ];
                            $ch = curl_init('https://api.telegram.org/bot'.$token.'/sendMessage');
                            curl_setopt($ch, CURLOPT_HEADER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            $result = curl_exec($ch);
                            curl_close($ch);
                        } else {
                            $data['status'] = 201;
                            $data['text'] = 'Giao dịch đã kết thúc!';
                            $status = 'Thất bại';
                        }
                    } else {
                        $data['status'] = 201;
                        $data['text'] = 'Giao dịch không tồn tại!';
                        $status = 'Thất bại';
                    }
                } else {
                    $data['status'] = 201;
                    $data['text'] = 'Giao dịch không tồn tại hoặc không hợp lệ!';
                    $status = 'Thất bại';
                }
            }
        }


        $this->view->setVar('data', $data);
//        $session = \Config\Services::session();
//        $session->setFlashdata('checkoutsuccess', 'Thanh toán thành công !');
        echo $this->view->render('deposit');
    }
}