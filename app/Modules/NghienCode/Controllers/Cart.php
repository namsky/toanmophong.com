<?php
namespace App\Modules\NghienCode\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Core\Cms\CmsController;

class Cart extends CmsController
{
	public function __construct(){
        $this->cart = \Config\Services::cart();
	}
    public function index(){
        $carts = $this->cart->contents();
        $items = array();
        $total_qty = 0;
        if(!empty($carts)) {
            $i = 0;
            foreach ($carts as $key=>$cart) {
                $items[$i]['id'] = $cart['id'];
                $items[$i]['name'] = $cart['name'];
                $items[$i]['qty'] = $cart['qty'];
                $items[$i]['price'] = number_format($cart['price']);
                $items[$i]['thumb'] = $cart['thumb'];
                $items[$i]['url'] = $cart['url'];
                $items[$i]['size'] = $cart['size'];
                $items[$i]['rowid'] = $cart['rowid'];
                $items[$i]['subtotal'] = number_format($cart['subtotal']);
                $items[$i]['total_money_detail'] = number_format($cart['qty']*$cart['price']);
                $total_qty += $cart['qty'];
                $i ++;
            }
        }
        $total_money = number_format($this->cart->total());
        $this->view->setVar('items',$items);
        $this->view->setVar('total_money',$total_money);
        $this->cms->load_vendors([
            'sweetalert2',
        ]);
        $this->view->setVar('cms', $this->cms);
        $session = \Config\Services::session();
		$this->view->setVar('session', $session);
		return $this->view->render('cart');
    }
    public function save(){
        $arg = array(
            'id'      => $this->request->getPost('id'),
            'qty'     => $this->request->getPost('quantity'),
            'price'   => $this->request->getPost('price'),
            'name'    => $this->request->getPost('name'),
            'size' => $this->request->getPost('size'),
            'thumb' => $this->request->getPost('thumb'),
            'url' => $this->request->getPost('url'),
        );
        $return = $this->cart->insert($arg);
        if(isset($return)){
            $session = \Config\Services::session();
            $session->setFlashdata('addtocart', 'Thêm sản phẩm vào giỏ hàng thành công !');
            $json = ['status'=>'success','data'=>$this->cart->contents(), 'total'=>number_format($this->cart->total())];
        } else $json = ['status'=>'error'];

        $this->render_json($json);
    }
    public function get_list(){
        $carts = $this->cart->contents();
        $item = array();
        $total_qty = 0;
        if(!empty($carts)) {
            $i = 0;
            foreach ($carts as $key=>$cart) {
                $item[$i]['id'] = $cart['id'];
                $item[$i]['name'] = $cart['name'];
                $item[$i]['qty'] = $cart['qty'];
                $item[$i]['price'] = number_format($cart['price']);
                $item[$i]['thumb'] = $cart['thumb'];
                $item[$i]['url'] = $cart['url'];
                $item[$i]['rowid'] = $cart['rowid'];
                $total_qty += $cart['qty'];
                $i ++;
            }
        }
        $json = ['status'=>'success','data'=>$item, 'total'=>number_format($this->cart->total()), 'total_qty'=>$total_qty];
        $this->render_json($json);
    }
    public function update_qty(){
        $data = $this->request->getPost();
        $this->cart->update(array(
            'rowid'   => $data['rowid'],
            'qty'     => $data['qty'],
        ));
        $cart = $this->cart->getItem($data['rowid']);
        $cart['subtotal'] = number_format($cart['subtotal']);
        $total_money = number_format($this->cart->total());
        $json = ['status'=>'success','data'=>$cart, 'total'=>number_format($this->cart->total())];
        $this->render_json($json);
    }
    public function update(){
        $data = $this->request->getPost();
        $update = [];
        foreach($data['rowid'] as $key=>$item){
            $update[$key]['rowid'] = $item;
            $update[$key]['qty'] = $data['qty'][$key];
        }
        foreach($update as $key=>$value){
            $this->cart->update(array(
                'rowid'   => $value['rowid'],
                'qty'     => $value['qty'],
             ));
        }
        $session = \Config\Services::session();
        $session->setFlashdata('addtocart', 'Cập nhật sản phẩm thành công !');
        return redirect()->to(site_url('/cart'));
    }
    public function destroy(){
        $rowid = $this->request->getGet('rowid');
        $this->cart->remove($rowid);
//        return redirect()->to(site_url('/cart'));
    }
    public function destroyAll(){
        $this->cart->destroy();
    }
    private function render_json($json)
    {
        if(is_resource($json))
        {
            throw new RenderException('Resources can not be converted to JSON data.');
        }
        $callback = $this->request->getGet('callback');
        $this->response->setHeader("Access-Control-Allow-Origin", "*");
        $this->response->setHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin");
        $this->response->setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
        $this->response->setHeader("Access-Control-Allow-Headers", "Content-Type,X-CSRF-Token, XHR, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
        $this->response->setHeader("Access-Control-Allow-Credentials", "true");
        $this->response->setHeader("Expires", "0");
        $this->response->setHeader("Last-Modified", gmdate("D, d M Y H:i:s") . " GMT");
        $this->response->setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        $this->response->setHeader("Pragma", "no-cache");
        $this->response->setHeader("Content-Type", "Application/json");
        if($callback) {
            echo $callback . '(' . json_encode($json) . ')';
        } else {
            echo json_encode($json);
        }
    }
	
}