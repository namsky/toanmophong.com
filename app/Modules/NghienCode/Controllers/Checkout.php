<?php
namespace App\Modules\NghienCode\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Core\Cms\CmsController;

class Checkout extends CmsController
{
	public function __construct(){
        $this->cart = \Config\Services::cart();
	}
    public function index(){
        $carts = $this->cart->contents();
        $items = array();
        $total_qty = 0;
        if(!empty($carts)) {
            $i = 0;
            foreach ($carts as $key=>$cart) {
                $items[$i]['id'] = $cart['id'];
                $items[$i]['name'] = $cart['name'];
                $items[$i]['qty'] = $cart['qty'];
                $items[$i]['price'] = number_format($cart['price']);
                $items[$i]['thumb'] = $cart['thumb'];
                $items[$i]['url'] = $cart['url'];
                $items[$i]['rowid'] = $cart['rowid'];
                $items[$i]['subtotal'] = number_format($cart['subtotal']);
                $items[$i]['total_money_detail'] = number_format($cart['qty']*$cart['price']);
                $total_qty += $cart['qty'];
                $i ++;
            }
        }
        $total_money = number_format($this->cart->total());
        $this->view->setVar('items',$items);
        $this->view->setVar('total_money',$total_money);
		return $this->view->render('checkout');
    }
    public function save(){
        $customerModel = model('App\Modules\Cms\Models\CustomersModel') ;
        $orderModel = model('App\Modules\Cms\Models\OrdersModel') ;
        $paymentsModel = model('App\Modules\Cms\Models\PaymentsModel') ;
        $data = $this->request->getPost();
        // echo "<pre>";
        // print_r($data);
        $customer_id = 0;
        $data['created']= time();
        if (!empty($data)) {
            $customer = $customerModel->insert($data);
            if ($customer) {
                $customer_id = $customer;
            }
        }
        // $order = [];
        $products = array();
        if ($customer_id) {
            $order_data = array();
            foreach($this->cart->contents() as $key=>$item){
                $order_data['quantity'] = $item['qty'];
                $order_data['customer_id'] = $customer_id;
                $order_data['price'] = $item['price'];
                $order_data['product_name'] = $item['name'];
                $order_data['product_image'] = $item['thumb'];
                $order_data['product_id'] = $item['id'];
                array_push($products,$item['id']);
                $order = $orderModel->insert($order_data);
            }
        }
        $money = $this->cart->total();
//        $accessKey = 'xd6TDa55RvJprP2t';
//        $partnerCode = 'MOMOQOW720210808';
//        $serect_key = 'kp3dBTAEGXrtNGJ8nLVKPTN9wQ2lHTB4';
//        $url_api_momo = 'https://test-payment.momo.vn/v2/gateway/api/create';
        // Tiến hành thanh toán
        if(isset($order)) {
            // Ngân Lượng
            $option_payment = 'ATM_ONLINE';
            $returnUrl = URL.'/payment/deposit_bank';
            $payment = payment_nganluong($money, $option_payment, $data['bankcode_atm'], $data['name'], $data['email'], $data['phone'], (isset($returnUrl) && $returnUrl)?$returnUrl:URL.'/deposit_bank');
            if(isset($payment['status']) && $payment['status']=='success') {
//                echo "<pre>";
//                print_r($payment);die;
                $arg = array(
                    'customer_id'=>$customer_id,
                    'orderid'=>$payment['data']['token'],
                    'order_code'=>$payment['data']['order_code'],
                    'amount'=>$money,
                    'status'=>'pending',
                    'response_code' => $payment['data']['error_code'],
                    'response_message' => $payment['data']['error_message'],
                    'bank_type' => $option_payment,
                    'bank_code' => $data['bankcode_atm'],
                    'url' => $payment['data']['checkout_url'],
                    'product' => json_encode($products),
                    'created'=>time());
                $return = $paymentsModel->insert($arg);
                $chat_id = -686297052;
                $token = '5102745546:AAHwvxzc00hLm3M_T8oAcu4JtFg7Eb2qlzM';
                $params=[
                    'chat_id'=>$chat_id,
                    'text'=>'Khách hàng '.$data['name'].' tạo lệnh mua tài liệu. Số tiền: '.number_format($money).'đ . Trạng thái giao dịch: Pending.',
                ];
                $ch = curl_init('https://api.telegram.org/bot'.$token.'/sendMessage');
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);
                return redirect()->to($payment['data']['checkout_url']);
            } else return $payment;
        }
        // End thanh toán


//        $this->cart->destroy();
//        $session = \Config\Services::session();
//        $session->setFlashdata('checkoutsuccess', 'Thanh toán thành công !');
//        return redirect()->to(site_url('/cart'));
    }
    public function download(){
        $session = \Config\Services::session();
        $productModel = model('App\Modules\Cms\Models\EcommerceProductModel') ;
        if($session->get('download') == 1) {
            $i = 1;
           if(!empty($this->cart->contents())) {
               foreach($this->cart->contents() as $key=>$item){
                   $product = $productModel->find($item['id']);
                   if(isset($product->file_download)) {
                       header("Content-disposition: attachment; filename=toanmophong_".$i.".gsp");
                       header("Content-type: application/octet-stream");
                       readfile($product->file_download);
                       $i++;
                   }
               }
           }
//            unset($_SESSION['download']);
//            $this->cart->destroy();
        } else {
            $data['text'] = 'Bạn đã tải rồi hoặc bạn chưa thanh toán!';
            $this->view->setVar('data', $data);
            echo $this->view->render('deposit');
        }
    }
}