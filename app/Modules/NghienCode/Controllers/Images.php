<?php
namespace App\Modules\NghienCode\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Core\Cms\CmsController;

class Images extends CmsController
{
	public function __construct()
	{
	}
	public function index()
	{
		try {
			/* Prepare args */
			$uri = $this->request->uri;
			$option_rules = $uri->getSegment(2);
			$hash = $uri->getSegment(3);
			$file_name = $uri->getSegment(4);
			if(empty($file_name)) {
				$file_name = $hash;
				if(strpos($hash, '.')) {
					$hash = explode(".", $hash);
					$hash = $hash[0];
				}
			}
			$ext = pathinfo($file_name, PATHINFO_EXTENSION);
			$options = [];
			if(empty($file_name)) {
				$file_name = $option_rules;
				$option_rules = 's0';
			} else {
				$option_rules = '-'.$option_rules.'-';
			}
			if(strpos($option_rules, 'w')) {
				$options['width'] = intval(getString($option_rules, 'w', '-'));
			}
			if(strpos($option_rules, 'h')) {
				$options['height'] = intval(getString($option_rules, 'h', '-'));
			}
			if(strpos($option_rules, 'r')) {
				$options['rotate'] = intval(getString($option_rules, 'r', '-'));
			}
			$options['auto_crop'] = (bool)strpos($option_rules, 'c');
			$options['keep_origin'] = (bool)strpos($option_rules, 'o');
			
			/* Prepare load image */
			$dir = FCPATH.'/writable/uploads/';
			$file_path = $dir.$hash.'.'.$ext;
			if(file_exists($file_path)) {
				$ext = pathinfo($file_path, PATHINFO_EXTENSION);
				$image = \Config\Services::image();
				$image->withFile($file_path);
				
				/* Proccess image */
				$ori_width = $image->getWidth();
				$ori_height = $image->getHeight();
				$width_limit = cms_config('upload_width_limit');
				if(!$width_limit) $width_limit = 1920;
				$height_limit = cms_config('upload_height_limit');
				if(!$height_limit) $height_limit = 1080;
				
				if(!empty($options['width'])) {
					$width = $options['width']<$width_limit?$options['width']:$width_limit;
				}
				else $width = $ori_width;
				if(!empty($options['height'])) {
					$height = $options['height']<$height_limit?$options['height']:$height_limit;
				}
				else $height = $ori_height;
				
				if(!empty($options['rotate']) && in_array($options['rotate'], [90, 180, 270])) {
					$image->rotate($options['rotate']);
				}
				if($options['auto_crop']) $image->fit($width, $height, 'center');
				else $image->resize($width, $height, true);

				/* Return image */
				$this->response->setHeader("Access-control-allow-origin", "*");
				$this->response->setHeader("Access-control-expose-headers", "*");
				$this->response->setHeader("Connection", "keep-alive");
				$this->response->setHeader("Cache-Control", "public, max-age=31536000, immutable");
				$this->response->setHeader("Expires", date("D, d M Y 00:00:00 GTM", time()+604800));
                $this->response->setHeader('Pragma', 'cached');
                $image_content = NULL;
				if(empty($options['keep_origin'])) {
                    $this->response->setHeader("Content-Type", "image/webp");
                    $image_resource = $image->getResource();
                    if(function_exists('imagepalettetotruecolor')) {
                        $image_resource = imagepalettetotruecolor($image_resource);
                    }
                    $image_content = @imagewebp($image_resource);
                }
                if(empty($image_content)) {
					switch($ext) {
						case 'jpg':
							$this->response->setHeader("Content-Type", "image/jpeg");
							$image_content = imagejpeg($image->getResource());
							break;
						case 'jpeg':
							$this->response->setHeader("Content-Type", "image/jpeg");
							$image_content = imagejpeg($image->getResource());
							break;
						case 'png':
							$this->response->setHeader("Content-Type", "image/png");
							$image_content = imagepng($image->getResource());
							break;
						case 'gif':
							$this->response->setHeader("Content-Type", "image/gif");
							$image_content = imagegif($image->getResource());
							break;
						default:
							$this->response->setHeader("Content-Type", "image/webp");
							$image_content = imagewebp($image->getResource());
							break;
					}
                }
                echo $image_content;
			} else {
				throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
			}
		} catch(Exceptions $e) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}
	}
}