<?php
namespace App\Modules\NghienCode\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Core\Cms\CmsController;

class Products extends CmsController
{
	public function __construct()
	{
		$this->cart = \Config\Services::cart();
	}
    public function index()
    {
		$session = \Config\Services::session();
		$this->view->setVar('session', $session);

		$this->view->setVar('cart', $this->cart);
		$this->view->setVar('title', cms_config('site_title'));
		$this->view->setVar('description', cms_config('site_description'));
		$this->view->setVar('keywords', cms_config('site_keywords'));
		
		$cached = cms_config('cache');
		$widgets = $cached?$this->cache->get('widget_page_1'):false;
		if(!$widgets) {
			$widgetModel = model('App\Modules\Cms\Models\WidgetModel');
			$widgets = $widgetModel->where('page_id', 1)->orderBy('order', 'DESC')->findAll();
			$this->cache->save('widget_page_1', $widgets, 86400);
        }
		$this->view->setVar('widgets', $widgets);
		$this->view->setVar('page', 'products');
		$items = false;
		if(!$items) {
			/* Load from db */
			$model = model('App\Modules\Cms\Models\EcommerceProductModel');
			$items = $model->orderBy('id', 'DESC')->paginate(20);
			$paging = $model->pager->links();
		}
		$this->view->setVar('items', $items);
		$this->view->setVar('paging', $paging);
		return $this->view->render('products');
	}
	public function item()
	{
		$session = \Config\Services::session();
		$this->view->setVar('session', $session);
		
		$this->view->setVar('cart', $this->cart);
		$request_uri = $this->request->getServer('REQUEST_URI');
		$last_of_uri = substr($request_uri, -1);
		if($last_of_uri == '/') {
			return cms_redirect($uri);
		}
		$uri = $this->request->uri;
		$slug = $uri->getSegment(2);
		$cached = cms_config('cache');
		$model = model('App\Modules\Cms\Models\EcommerceProductModel');
		$product = $cached?$this->cache->get('product_'.$slug):NULL;
		if(is_null($product)) {
			$product = $model->where('slug', $slug)->first();
			$this->cache->save('product_'.$slug, $product, 86400);
		}
		if(isset($product->id)) {
			/* Render single */
			$per_model = model('App\Modules\Cms\Models\EcommercePropertiesModel');
			$pro_pers = $per_model->where('product_id',$product->id)->findAll();
			$this->view->setVar('pro_pers', $pro_pers);

			$amp = $uri->getSegment(2);
			$this->view->setVar('title', ($product->seo_title)?$product->seo_title:$product->name.' | '.cms_config('site_name'));
			$this->view->setVar('description', ($product->seo_description)?$product->seo_description:$product->summary);
			$this->view->setVar('keywords', ($product->seo_keywords)?$product->seo_keywords:cms_config('site_keywords'));
			$this->view->setVar('page', 'single_product');
			$this->view->setVar('item', $product);
			$category = $product->category_id;
			$product_category = $model->where('category_id',$category)->orderBy('id','DESC')->where('deleted',NULL)->findAll(5, 0);
			$this->view->setVar('sanphamlienquan', $product_category);
            $model->increase_views($product->id);
            $this->cms->load_vendors([
				'sweetalert2',
				'fontawesome',
			]);
			return $this->view->render('single_product');
		} else throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
	}
	public function new_product(){
		$this->view->setVar('cart', $this->cart);
		$model = model('App\Modules\Cms\Models\EcommerceProductModel');
		$items = $model->orderBy('id', 'DESC')->paginate(8);
		$paging = $model->pager->links();
		$this->view->setVar('items', $items);
		$this->view->setVar('paging', $paging);
		return $this->view->render('hang_moi');
	}
}