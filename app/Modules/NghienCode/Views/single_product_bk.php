<?=$this->section('content');?>
	<!-- CONTENT -->
	<main class="">
	<?php if($session->getFlashdata('addtocart')){ ?>
		<script>
		swal("Thành công!", "<?=$session->getFlashdata('addtocart')?>", "success");
		</script>		
	<?php } ?>
		<meta itemprop='sku' content='FAVH123120SDE' />
		<div id="product" class="productDetail-page">
			<div class="container">
				<div class="row product-detail-wrapper">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="row product-detail-main pr_style_01">
							<div class="col-md-8 col-sm-12 col-xs-12">
								<?php foreach($pro_pers as $key=>$pro_per) {
									?>
									
									<?php 
									$image_details = explode("\n",$pro_per->image);
									?>
									<div class="product-gallery" id="product-gallery-<?=$key?>" data-size="9" style=" <?php if($key != 0) {echo "display:none";} ?>  ">
										<div class="product-gallery__thumbs-container hidden-sm hidden-xs">
											<div class="product-gallery__thumbs thumb-fix">
											<?php foreach($image_details as $key=>$image_detail){?>
												<div class="product-gallery__thumb" data-variant="den">
													<a class="product-gallery__thumb-placeholder" onclick="scroll_image('<?=preg_replace('/[^a-zA-Z0-9_ -]/s', '', $image_detail)?>')" href="javascript:void(0);" data-image="<?=$image_detail?>"> 
														<img alt="" src="<?=$image_detail?>" data-image="<?=$image_detail?>" /> 
													</a>
												</div>
											<?php } ?>
											</div>
										</div>
										<div class="product-image-detail box__product-gallery scroll">
											<ul id="sliderproduct" class="site-box-content slide_product">
											<?php foreach($image_details as $key=>$image_detail){ ?>
												<li class="product-gallery-item gallery-item" id="gallery_<?=preg_replace('/[^a-zA-Z0-9_ -]/s', '', $image_detail)?>" data-variant="den"> 
													<img class="product-image-feature" src="<?=$image_detail?>" alt=""> 
												</li>
												<?php } ?>
											</ul>
										</div>
									</div>
								<?php }; ?>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12 product-content-desc" id="detail-product">
								<div class="product-title">
									<h1><?=$item->name?></h1> <span id="pro_sku"><strong>SKU:</strong> <span id="pro_sku_item"></span></span> </div>
								<div class="product-price" id="price-preview"> <span class="pro-price"><?=number_format($item->price)?>₫</span> </div>
								<form id="add-item-form" action="/cart/save" method="POST" class="variants clearfix">
									<input type="hidden" name="name" class="name" value="<?=$item->name?>">
									<input type="hidden" name="price" class="price" value="<?=$item->price?>">
									<input type="hidden" name="id" class="id" value="<?=$item->id?>">
									<input type="hidden" name="image" class="image" value="<?=$item->thumb?>">
									<input type="hidden" name="slug" class="slug" value="<?=$item->slug?>">
									<div class="select-swatch clearfix">
										<div id="variant-swatch-0" class="swatch clearfix" data-option="option1" data-option-index="0">
											<div class="header hide">Màu sắc:</div>
											<div class="select-swap">
											<input type="hiden" name="color" value="" class="color">
											<?php foreach($pro_pers as $key=>$pro_per) {
												if($pro_per->color != NULL){
											?>
												<div data-value="Đen" class="n-sd swatch-element color den ">
													<label class="color" id="color-<?=$key?>" onclick="show_image('<?=$key?>','<?=$pro_per->color?>')" > 
														<span class=" nEw-style icon_den" style="background:<?=$pro_per->color?>;"><?=$pro_per->color?></span>
													 </label>
												</div>
											<?php } }?>
											</div>
										</div>
										<div id="variant-swatch-1" class="swatch clearfix" data-option="option2" data-option-index="1">
											<div class="header hide">Kích thước:</div>
											<div class="select-swap" id="den-show">
											<?php foreach($pro_pers as $key=>$pro_per) {
												if($pro_per->size != NULL){
												?>
												<input type="hidden" name="color" class="color" value="<?=$pro_per->color?>">
												<input type="hidden" name="size" value="<?=$pro_per->size?>" class="size">
												<div data-value="A" class="n-sd swatch-element s ">
													<input type="radio" id="sku_<?=$key?>" value="<?=$pro_per->sku?>">
													<input type="hiden" id="stocks_<?=$key?>" value="<?=$pro_per->stock?>">
													<label class="lable_size"   id="lable_<?=$key?>" onclick="show_sku('<?=$key?>','<?=$pro_per->size?>')" for="a"> <span><?=$pro_per->size?></span> </label>
												</div>
												<?php } }?>
											</div> 
											<a class="pull-right" style="margin: 10px 25px;" href="javascript:;" data-toggle="modal" data-target="#size_chart_modal">
											CÁCH CHỌN SIZE
											</a> 
										</div>
									</div>
									<div class="invenAlert alertPage"> Hiện tại còn <span class="invenNumber">...</span> sản phẩm. </div> 
									<a class="sizechart hide" href="javascript:;" data-toggle="modal" data-target="#size_chart_modal">Cách chọn size</a>
									<div class="quantity-area clearfix">
										<input type="button" value="-" onclick="minusQuantity()" class="qty-btn">
										<input type="text" id="quantity" name="quantity" value="1" min="1" class="quantity-selector">
										<input type="button" value="+" onclick="plusQuantity()" id="plus" class="qty-btn"> </div>
									<div class="selector-actions">
										<div class="info_des_wrap">
											<div> </div>
											<div class="wrap-addcart clearfix">
												<button type="submit" id="buy-now" class=" btn-addtocart button1 dark1 " name="add"> Thêm vào giỏ </button>
												<button type="button" style="display:none" class=" btn-addtocart button2 dark1 "> Hết hàng </button>
											</div>
										</div>
									</div>
								</form>
								<div class="product-description">
									<div class="title-bl"> </div>
									<div class="description-variants">
										<ul>
											<li>
											<?=$item->content?>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="list-productRelated clearfix">
							<div class="heading-title text-center">
								<h2>Sản phẩm liên quan</h2> </div>
							<div class="content-product-list row">
							<?php foreach($sanphamlienquan as $item){ ?>
								<div class="col-md-3 col-sm-6 col-xs-6 pro-loop">
									<div data-price="0 " class="product-block  site-animation" data-anmation="1">
										<div class="product-img">
											<a href="/san-pham/<?=$item->slug?>" class="image-resize"> 
												<img class="img-loop" alt=" <?=$item->name?> " src="<?=$item->thumb?>" /> 
												<img class="img-loop img-hover" alt=" <?=$item->name?> " src="<?=$item->thumb_hover?>" /> 
											</a>
											<div class="button-add hidden">
												<button type="submit" title="Buy now" class="action" onclick="buy_now('1066818302')">Mua ngay<i class="fa fa-long-arrow-right"></i></button>
											</div>
										</div>
										<div class="product-detail clearfix">
											<div class="box-pro-detail">
												<h3 class="pro-name">
													<a href="/san-pham/<?=$item->slug?>">
														<?=$item->name?>
													</a>
												</h3>
												<div class="box-pro-prices">
													<p class="pro-price "><?=number_format($item->price)?>₫ <span class="pro-price-del"></span> </p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
				function show_image(key,color){
					$(".color").css("border","none");
					$("#color-"+key).css("border","2px black solid");
					$(".color").val(color);
					$(".product-gallery").hide();
					$("#product-gallery-"+key).css("display","flex");			}
				function show_sku(key,size,stock){
					$(".size").val(size);
					$(".lable_size").css("border","none");
					$("#lable_"+key).css("border","2px black solid");
					var sku = $("#sku_"+key).val();
					var stocks = $("#stocks_"+key).val();
					$("#pro_sku_item").text(sku);
					$(".invenNumber").text(stocks);
					if(stocks<=0){
						$(".button1").hide();
						$(".button2").show();
					}else{
						$(".button2").hide();
						$(".button1").show();
					}
				}
				function scroll_image(key){
					$('html, body').animate({
						scrollTop: $("#gallery_"+key).offset().top
					}, 1000);
				}
				
		</script>
		<?=$this->endSection();?>