<?=$this->section('content');?>
    <!-- CONTENT -->
    <div class="breadcrumb_background margin-bottom-40">
        <div class="title_full">
            <div class="container a-center">
                <p class="title_page"><?=cms_config('site_name')?> | <?=$partent_cate->name?></p>
            </div>
        </div>
        <section class="bread-crumb"> <span class="crumb-border"></span>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 a-center">
                        <ul class="breadcrumb">
                            <li class="home"> <a href="/"><span >Trang chủ</span></a> <span class="mr_lr">&nbsp;<i class="fa">/</i>&nbsp;</span> </li>
                            <li><strong><span> <?=$partent_cate->name?></span></strong></li>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="container">
        <div class="row">
            <div class="wrap_background_title hidden"
            <h1 class="title_collec">  <?=$partent_cate->name?> - <?=cms_config('site_name')?></h1>
        </div>
        <div class="bg_collection section margin-bottom-30">
            <div class="main_container collection col-lg-12 col-lg-12-fix padding-col-left-0">
                <div class="category-products products margin-top-30">
                    <div class="section hidden">
                        <div class="sortPagiBar">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="f-left inline-block">
                                        <div class="view-mode hidden-xs">
                                            <a href="javascript:;" data-view="grid" onclick="switchView('grid')" title="Grid view"> <b class="btn button-view-mode view-mode-grid active">
                                                    <i class="fa fa-th" aria-hidden="true"></i>
                                                </b> <span>Lưới</span> </a>
                                            <a href="javascript:;" data-view="list" onclick="switchView('list')" title="List view"> <b class="btn button-view-mode view-mode-list ">
                                                    <i class="fa fa-th-list" aria-hidden="true"></i>
                                                </b> <span>Danh sách</span> </a>
                                        </div>
                                    </div>
                                    <div class="bg-white sort-cate clearfix">
                                        <div id="sort-by">
                                            <label class="left">Sắp xếp: </label>
                                            <ul class="ul_col">
                                                <li><span>Thứ tự</span>
                                                    <ul class="content_ul">
                                                        <li><a href="javascript:;" onclick="sortby('default')">Mặc định</a></li>
                                                        <li><a href="javascript:;" onclick="sortby('alpha-asc')">A &rarr; Z</a></li>
                                                        <li><a href="javascript:;" onclick="sortby('alpha-desc')">Z &rarr; A</a></li>
                                                        <li><a href="javascript:;" onclick="sortby('price-asc')">Giá tăng dần</a></li>
                                                        <li><a href="javascript:;" onclick="sortby('price-desc')">Giá giảm dần</a></li>
                                                        <li><a href="javascript:;" onclick="sortby('created-desc')">Hàng mới nhất</a></li>
                                                        <li><a href="javascript:;" onclick="sortby('created-asc')">Hàng cũ nhất</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>
                        .btn-class-pro {
                            width: 100%;
                            background: red;
                            margin-top: 4%;
                            text-align: center;
                            padding: 5px;
                        }
                        .btn-class-pro:hover {
                            background: #c73f3f;
                        }
                        .btn-class-pro a {
                            color: white;
                            font-size: 18px;
                        }
                        .btn-class-pro-child {

                            width: 50%;
                            display: block;
                            padding: 5px;
                            background: red;
                            color: white;
                            margin: 0 auto;
                            text-align: center;
                            margin-top: 4%;
                        }
                        .btn-class-pro-child a{
                            color: white;
                            font-size: 18px;
                        }
                        .btn-class-pro-child:hover {
                            background: #c73f3f;
                        }
                        .child_pro {
                            font-size: 18px;
                            font-weight: 400;
                        }
                    </style>
                    <section class="products-view products-view-grid collection_reponsive list_hover_pro">
                        <div class="row">
                            <?php foreach($items['cate'] as $item){ ?>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 product-col clear">
                                    <div class="item_product_main margin-bottom-15">
                                        <div class="product-col">
                                            <div class="product-loop-1 product-loop-2 product-base product-box">
                                                <div class="product-thumbnail">
                                                    <a class="image_link display_flex" href="/<?=($item['slug'])?>" title="<?=$item['name']?>">
                                                        <?php if($partent_cate->slug == 'chan-troi-sang-tao') { ?>
                                                            <img class="lazyload" src="data:image/png;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" data-src="https://toanmophong.com/writable/uploads/8e5476503293356911507e381ed58b40.png" alt="<?=$item['name']?>">
                                                        <?php } else if($partent_cate->slug == 'canh-dieu') { ?>
                                                            <img class="lazyload" src="data:image/png;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" data-src="https://toanmophong.com/writable/uploads/26ba870db02b505adb3673ed8e0055fa.png" alt="<?=$item['name']?>">
                                                        <?php } else if($partent_cate->slug == 'ket-noi-tri-thuc-voi-cuoc-song') {  ?>
                                                            <img class="lazyload" src="data:image/png;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" data-src="https://toanmophong.com/writable/uploads/f3bbd77a2f4fadb004846656e7624d79.png" alt="<?=$item['name']?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="product-info a-center">
                                                    <div class="action_image">
                                                        <div class="owl_image_thumb_item hidden-md hidden-sm hidden-xs">
                                                            <div class="product_image_list owl-carousel not-owl">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h3 class="product-name margin-top-5"><a href="/<?=($item['slug'])?>" title="<?=$item['name']?>"><?=$item['name']?></a></h3>
                                                </div>
                                                <?php if(isset($item['child']) && !empty($item['child'])) foreach ($item['child'] as $val) { ?>
                                                    <div class="btn-class-pro my-2">
                                                        <a  href="/<?=$val['slug']?>"><?=$val['name']?></a>
                                                    </div>
                                                    <div>
                                                        <?php if(isset($val['last_child']) && !empty($val['last_child'])) foreach ($val['last_child'] as $val1) { ?>
                                                            <div style="display: block;">
                                                                <a class="child_pro"  href="/<?=$val1['slug']?>">- <?=$val1['name']?></a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <style>
                            .pagination li a {
                                margin: 0 5px;
                                border-radius: 0 !important;
                            }
                            .pagination li a:hover {
                                background: #e1182c;
                                color: #fff;
                                border-radius: 0 !important;
                            }
                            .pagination li.active>a {
                                border-color: #e1182c;
                                background: #e1182c;
                                color: #fff;
                                display: block;
                                border-radius: 0;
                            }
                            .pagenav span {
                                margin-right: 0;
                                line-height: 20px;
                            }
                        </style>
                    </section>
                </div>
                <div id="open-filters" class="open-filters hidden-lg"> <i class="fa fa-align-right"></i> </div>
            </div>
        </div>
        <div class="module_like_product margin-bottom-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class=" section_base button-middle">
                            <div class="heading">
                                <h2 class="title-heads">
                                    <a title="BÁN CHẠY NHẤT" href="/vans-classic">BÁN CHẠY NHẤT</a>
                                </h2> </div>
                            <div class="products-view-grid-bb owl-carousel owl-theme products-view-grid not-dot2" data-dot="false" data-nav="true" data-lg-items="3" data-md-items="4" data-sm-items="3" data-xs-items="2" data-margin="10">
                                <?php echo widget('NghienCode/Product_best_sale'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <link href="https://productviewedhistory.sapoapps.vn//Content/styles/css/ProductRecentStyle.css" rel="stylesheet" />
<?=$this->endSection();?>