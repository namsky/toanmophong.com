<?=$this->section('content');?>
    <!-- CONTENT -->
    <div class="breadcrumb_background margin-bottom-40">
        <div class="title_full">
            <div class="container a-center">
                <?php if (isset($key)) {?>
                    <p class="title_page">
                        <?=$key?> - <?=cms_config('site_name')?>
                    </p>
                <?php }else{?>
                    <p class="title_page"><?=cms_config('site_name')?> | <?=$category?></p>
                <?php } ?>
            </div>
        </div>
        <section class="bread-crumb"> <span class="crumb-border"></span>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 a-center">
                        <ul class="breadcrumb">
                            <li class="home"> <a href="/"><span >Trang chủ</span></a> <span class="mr_lr">&nbsp;<i class="fa">/</i>&nbsp;</span> </li>
                            <?php if (isset($key)) {?>
                                <li><strong><span> Tìm kiếm</span></strong>
                                </li>
                            <?php }else{?>
                                <li><strong><span> <?=$category?></span></strong></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="container">
        <div class="row">
            <div class="wrap_background_title hidden">
                <?php if (isset($key)) {?>
                    <h1 class="title_collec">  <?=$key?> - <?=cms_config('site_name')?></h1>
                <?php }else{?>
                    <h1 class="title_collec">  <?=cms_config('site_name')?> | Tất Cả Sản Phẩm Tại <?=cms_config('site_name')?></h1>
                <?php } ?>
            </div>
            <div class="bg_collection section margin-bottom-30">
                <div class="main_container collection col-lg-12 col-lg-12-fix padding-col-left-0">
                    <div class="category-products products margin-top-30">
                        <div class="section hidden">
                            <div class="sortPagiBar">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="f-left inline-block">
                                            <div class="view-mode hidden-xs">
                                                <a href="javascript:;" data-view="grid" onclick="switchView('grid')" title="Grid view"> <b class="btn button-view-mode view-mode-grid active">
                                                        <i class="fa fa-th" aria-hidden="true"></i>
                                                    </b> <span>Lưới</span> </a>
                                                <a href="javascript:;" data-view="list" onclick="switchView('list')" title="List view"> <b class="btn button-view-mode view-mode-list ">
                                                        <i class="fa fa-th-list" aria-hidden="true"></i>
                                                    </b> <span>Danh sách</span> </a>
                                            </div>
                                        </div>
                                        <div class="bg-white sort-cate clearfix">
                                            <div id="sort-by">
                                                <label class="left">Sắp xếp: </label>
                                                <ul class="ul_col">
                                                    <li><span>Thứ tự</span>
                                                        <ul class="content_ul">
                                                            <li><a href="javascript:;" onclick="sortby('default')">Mặc định</a></li>
                                                            <li><a href="javascript:;" onclick="sortby('alpha-asc')">A &rarr; Z</a></li>
                                                            <li><a href="javascript:;" onclick="sortby('alpha-desc')">Z &rarr; A</a></li>
                                                            <li><a href="javascript:;" onclick="sortby('price-asc')">Giá tăng dần</a></li>
                                                            <li><a href="javascript:;" onclick="sortby('price-desc')">Giá giảm dần</a></li>
                                                            <li><a href="javascript:;" onclick="sortby('created-desc')">Hàng mới nhất</a></li>
                                                            <li><a href="javascript:;" onclick="sortby('created-asc')">Hàng cũ nhất</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <section class="products-view products-view-grid collection_reponsive list_hover_pro">
                            <div class="row">
                                <?php foreach($items as $item){ ?>
                                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 product-col clear">
                                        <div class="item_product_main margin-bottom-15">
                                            <div class="product-col">
                                                <div class="product-loop-1 product-loop-2 product-base product-box">
                                                    <div class="product-thumbnail">
                                                        <?php if($item->origin_price > 0) {
                                                            $sale = round((($item->origin_price - $item->price)/$item->origin_price)*100)
                                                            ?>
                                                            <div class="saleright"> - <?=$sale?>% </div>
                                                        <?php } ?>
                                                        <a class="image_link display_flex" href="<?=product_url($item)?>" title="<?=$item->name?>"> <img class="lazyload" src="data:image/png;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" data-src="<?=$item->thumb?>" alt="<?=$item->title?>"> </a>
                                                        <div class="product-action clearfix hidden-xs">
                                                            <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8619825" enctype="multipart/form-data">
                                                                <div class="group_action">
                                                                    <input class="hidden" type="hidden" name="variantId" value="43400599" />
                                                                    <button class="btn btn-cart btn btn-circle left-to" title="Tùy chọn" type="button" onclick="window.location.href='/san-pham/<?=$item->slug?>'"> Xem chi tiết </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="product-info a-center">
                                                        <div class="action_image">
                                                            <div class="owl_image_thumb_item hidden-md hidden-sm hidden-xs">
                                                                <div class="product_image_list owl-carousel not-owl">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3 class="product-name margin-top-5"><a href="<?=product_url($item)?>" title="<?=$item->name?>"><?=$item->name?></a></h3>
                                                        <div class="product-hideoff">
                                                            <div class="product-hide">
                                                                <div class="price-box clearfix">
                                                                    <div class="special-price"> <span class="price product-price"><?=number_format($item->price)?>đ</span> </div>
                                                                    <?php if($item->origin_price > 0){?>
                                                                        <div class="old-price"> <span class="price product-price-old"><?=number_format($item->origin_price)?>₫</span> </div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <style>
                                .pagination li a {
                                    margin: 0 5px;
                                    border-radius: 0 !important;
                                }
                                .pagination li a:hover {
                                    background: #e1182c;
                                    color: #fff;
                                    border-radius: 0 !important;
                                }
                                .pagination li.active>a {
                                    border-color: #e1182c;
                                    background: #e1182c;
                                    color: #fff;
                                    display: block;
                                    border-radius: 0;
                                }
                                .pagenav span {
                                    margin-right: 0;
                                    line-height: 20px;
                                }
                            </style>
                            <div class="section pagenav">
                                <?=$paging ?>
                            </div>
                        </section>
                    </div>
                    <div id="open-filters" class="open-filters hidden-lg"> <i class="fa fa-align-right"></i> </div>
                </div>
            </div>
            <div class="module_like_product margin-bottom-50">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class=" section_base button-middle">
                                <div class="heading">
                                    <h2 class="title-heads">
                                        <a title="BÁN CHẠY NHẤT" href="/vans-classic">BÁN CHẠY NHẤT</a>
                                    </h2> </div>
                                <div class="products-view-grid-bb owl-carousel owl-theme products-view-grid not-dot2" data-dot="false" data-nav="true" data-lg-items="3" data-md-items="4" data-sm-items="3" data-xs-items="2" data-margin="10">
                                    <?php echo widget('NghienCode/Product_best_sale'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link href="https://productviewedhistory.sapoapps.vn//Content/styles/css/ProductRecentStyle.css" rel="stylesheet" />
<?=$this->endSection();?>