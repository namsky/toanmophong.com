<?=$this->section('content');?>
<main class="main-index">
			<div class="container" id="page-wrap">
				<h1 class="hidden">MARC</h1>
				<div class="slider-media-container">
					<div class="swiper-container gallery-top">
					<div class="swiper-wrapper">
							<?php foreach($sliderTop as $item) { ?>
							<div class="swiper-slide relative editorial-image-slide-container">
								<div class="editorial-images-slide-image-background" style="background-image: url(<?=$item->slider_image ?>); background-repeat: no-repeat, repeat;background-position: center;">&nbsp;</div> <img class="visible-xs" src="<?=$item->slider_image ?>" alt="slider 2" />
								<div class="editorial-artifacts">
									<div class="editorial-image-artifacts">
										<div class="btn centered cta occasion-cta-container" style="left: 0%; top: 86.81%;">
											<button onclick="window.location.href='<?=$item->slider_link ?>'" style="background-color: rgb(39, 36, 37); color: rgb(255, 255, 255); font-size: 0.9vw;"> mua ngay </button>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>				
						</div>
						<!-- Add Arrows -->
						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
					</div>
				</div>
				<div class="editorial-masonry-container">
				<?php foreach($bannerTop as $item){ ?>
					<div class="editorial-masonry-item-container" onclick="window.location.href='<?=$item->slider_link?>'">
						<div class="editorial-masonry-image-container">
							<picture>
								<source srcset="<?=$item->slider_image?>" media="(max-width: 767px)" />
								<source srcset="<?=$item->slider_image?>" /> 
								<img class="" src="<?=$item->slider_image?>" alt=""> 
							</picture>
						</div> 
						<span class="title font-bold" style="color:#ffffff"></span> 
					</div>
				<?php } ?>
				</div>
				<video id="homevideo" loop muted autoplay playsinline controls>
					<source src="<?=cms_config('video')?>" type="video/mp4"> Your browser does not support HTML video. </video>
				<!-- Nhóm  -->
				<section class="section section-collection">
					<div class="wrapper-heading-home animation-tran text-center">
						<div class="container-fluid-1">
							<div class="site-animation">
								<h2>
									<a href="/collections/san-pham-moi">SẢN PHẨM mới nhất</a>
								</h2> 
							</div>
						</div>
					</div>
					<div class="wrapper-collection-2">
						<div class="container-fluid-1">
							<div class="row">
								<div class="clearfix content-product-list text-center">
								<?php foreach($sale_product as $item){ ?>
									<div class="col-md-3 col-sm-6 col-xs-6 pro-loop animation-tran active">
										<div data-price="0 " class="product-block  site-animation" data-anmation="1">
											<div class="product-img">
												<a href="/san-pham/<?=$item->slug?>" class="image-resize"> 
													<img class="img-loop" alt=" Quần culotte xếp ly trước cơ bản " src="<?=$item->thumb?>"> 
													<img class="img-loop img-hover" alt="<?=$item->name?>" src="<?=$item->thumb_hover?>"> 
												</a>
												<div class="button-add hidden">
													<button type="submit" title="Buy now" class="action" onclick="buy_now('1066573695')">Mua ngay<i class="fa fa-long-arrow-right"></i></button>
												</div>
											</div>
											<div class="product-detail clearfix">
												<div class="box-pro-detail">
													<h3 class="pro-name">
													<a href="/san-pham/<?=$item->slug?>">
														<?=$item->name?>
													</a>
												</h3>
													<div class="box-pro-prices">
														<p class="pro-price "><?=number_format($item->price)?>₫</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section section-collection">
					<div class="wrapper-heading-home animation-tran text-center">
						<div class="container-fluid-1">
							<div class="site-animation">
								<h2>
									<a href="/collections/best-seller">Sản phẩm bán chạy</a>
								</h2> 
							</div>
						</div>
					</div>
					<div class="wrapper-collection-2">
						<div class="container-fluid-1">
							<div class="row">
								<div class="clearfix content-product-list text-center">
								<?php foreach($sale_product as $item){ ?>
									<div class="col-md-3 col-sm-6 col-xs-6 pro-loop animation-tran">
										<div data-price="0 " class="product-block  site-animation" data-anmation="1">
											<div class="product-img">
												<div class="icon_img"> <img src="//theme.hstatic.net/1000197303/1000641267/14/best_seller.png?v=682" /> </div>
												<a href="/san-pham/<?=$item->slug?>" class="image-resize"> 
													<img class="img-loop" alt=" Quần form baggy cơ bản " src="<?=$item->thumb?>" /> 
													<img class="img-loop img-hover" alt=" Quần form baggy cơ bản " src="<?=$item->thumb_hover?>" /> 
												</a>
												<div class="button-add hidden">
													<button type="submit" title="Buy now" class="action" onclick="buy_now('1066348512')">Mua ngay<i class="fa fa-long-arrow-right"></i></button>
												</div>
											</div>
											<div class="product-detail clearfix">
												<div class="box-pro-detail">
													<h3 class="pro-name">
														<a href="/san-pham/<?=$item->slug?>">
														<?=$item->name?>
														</a>
													</h3>
													<div class="box-pro-prices">
														<?php if ($item->price != $item->origin_price) {?>
															<p class="pro-price "><?=number_format($item->price)?>₫ <del><?=number_format($item->origin_price)?>đ</del> </p>
														<?php } else { ?>
															<p class="pro-price "><?=number_format($item->origin_price)?> ₫</p>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="influencer-list influencer-list-section">
					<h3>#MARCONME</h3>
					<div class="influencer-list influencer-list-wrap hidden-xs">
					<?php foreach($bannerBottom as $item){ ?>
						<a class="influencer-item" href="<?=$item->slider_link?>">
							<div class="influencer-image" style="background-image: url(<?=$item->slider_image?>);"></div>
							<legend class="jsx-2195943204"><?=$item->slider_text?></legend>
							<p class=""><?=cms_config('tag_site')?></p>
						</a>
						<? } ?>
					</div>
					<div class="influencer-list influencer-item-slider influencer-list-wrap visible-xs">
						<div class="swiper-container">
							<div class="swiper-wrapper">
							<?php foreach($bannerBottom as $item){ ?>
								<div class="swiper-slide">
									<a class="influencer-item" href="<?=$item->slider_link?>">
										<div class="influencer-image" style="background-image: url(<?=$item->slider_image?>);"></div>
										<legend class="jsx-2195943204"><?=$item->slider_text?></legend>
										<p class=""><?=cms_config('tag_site')?></p>
									</a>
								</div>
								<? } ?>
							</div>
							<!-- Add Pagination -->
							<div class="swiper-pagination"></div>
						</div>
					</div>
				</section>
			</div>
		</main>
	<!-- home end wine section -->
<?=$this->endSection();?>