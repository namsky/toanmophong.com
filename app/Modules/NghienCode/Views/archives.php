<?=$this->section('content');?>
    <div class="breadcrumb_background margin-bottom-40">
        <div class="title_full">
            <div class="container a-center">
                <p class="title_page"><?=$category->name?></p>
            </div>
        </div>
        <section class="bread-crumb"> <span class="crumb-border"></span>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 a-center">
                        <ul class="breadcrumb">
                            <li class="home"> <a href="/"><span >Trang chủ</span></a> <span class="mr_lr">&nbsp;<i class="fa">/</i>&nbsp;</span> </li>
                            <li><strong><span><?=$category->name?></span></strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="container" itemscope itemtype="https://schema.org/Blog">
        <div class="row">
            <div class="content_all f-left w_100">
                <div class="right-content margin-bottom-fix margin-bottom-50-article col-md-12 col-sm-12 col-xs-12">
                    <div class="box-heading relative hidden">
                        <h1 class="title-head page_title">
                            TIN TỨC VANS MỚI NHẤT CẬP NHẬT MỖI NGÀY
                        </h1> </div>
                    <div class="list-blogs blog-main row content_blog_new">
                    <?php
                    foreach($items as $item){
                        ?>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="blog_index">
                                <div class="myblog" onclick="window.location.href='/<?=$item->slug?>';">
                                    <div class="image-blog-left a-center">
                                        <a href="/<?=$item->slug?>" title="<?=$item->title?>"> <img class="lazyload img-responsive" src="data:image/png;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" data-src="<?=$item->thumb?>" alt="<?=$item->slug?>" /> </a>
                                        <div class="date_blog"> <i class="far fa-calendar"></i><b class="color_main"><?=show_date($item->created)?></b> &nbsp; Đăng bởi: <b class="color_main">Toán Mô Phỏng</b> </div>
                                    </div>
                                    <div class="content_blog">
                                        <div class="content_right">
                                            <h3>
                                                <a href="/<?=$item->slug?>" title="<?=$item->title?>"><?=$item->title?></a>
                                            </h3> </div>
                                        <div class="summary_item_blog">
                                            <p> <?=$item->summary?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <style>
                                .pagination li a {
                                    margin: 0 5px;
                                    border-radius: 0 !important;
                                }
                                .pagination li a:hover {
                                    background: #e1182c;
                                    color: #fff;
                                    border-radius: 0 !important;
                                }
                                .pagination li.active>a {
                                    border-color: #e1182c;
                                    background: #e1182c;
                                    color: #fff;
                                    display: block;
                                    border-radius: 0;
                                }
                                .pagenav span {
                                    margin-right: 0;
                                    line-height: 20px;
                                }
                            </style>
                            <div class="section pagenav">
                                <?=$paging ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


	<!-- CONTENT -->
</main>
<?=$this->endSection();?>