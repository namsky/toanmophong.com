<?=$this->section('content');?>
	<!-- CONTENT -->
    <div class="breadcrumb_background margin-bottom-40">
        <div class="title_full">
            <div class="container a-center">
                <?php if (isset($key)) {?>
                    <p class="title_page">
                        <?=$key?> - <?=cms_config('site_name')?>
                    </p>
                <?php }else{?>
                    <p class="title_page"><?=cms_config('site_name')?> | Tất Cả Sản Phẩm Tại <?=cms_config('site_name')?></p>
                <?php } ?>
            </div>
        </div>
        <section class="bread-crumb"> <span class="crumb-border"></span>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 a-center">
                        <ul class="breadcrumb">
                            <li class="home"> <a href="/"><span >Trang chủ</span></a> <span class="mr_lr">&nbsp;<i class="fa">/</i>&nbsp;</span> </li>
                            <?php if (isset($key)) {?>
                                <li><strong><span> Tìm kiếm</span></strong>
                                </li>
                            <?php }else{?>
                                <li><strong><span> Sản phẩm</span></strong></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="container">
        <div class="row">
            <div class="wrap_background_title hidden">
                <?php if (isset($key)) {?>
                    <h1 class="title_collec">  <?=$key?> - <?=cms_config('site_name')?></h1>
                <?php }else{?>
                    <h1 class="title_collec">  <?=cms_config('site_name')?> | Tất Cả Sản Phẩm Tại <?=cms_config('site_name')?></h1>
                <?php } ?>
            </div>

            <div class="bg_collection section margin-bottom-30">
                <div class="main_container collection col-lg-12 col-lg-12-fix padding-col-left-0">
                    <div class="category-products products margin-top-30">
                        <section class="products-view products-view-grid collection_reponsive list_hover_pro">
                            <div class="row">
                                <?php foreach($items as $item){ ?>
                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 product-col clear">
                                    <div class="item_product_main margin-bottom-15">
                                        <div class="product-col">
                                            <div class="product-loop-1 product-loop-2 product-base product-box">
                                                <div class="product-thumbnail">
                                                    <?php if($item->origin_price > 0) {
                                                        $sale = round((($item->origin_price - $item->price)/$item->origin_price)*100)
                                                        ?>
                                                    <div class="saleright"> - <?=$sale?>% </div>
                                                    <?php } ?>
                                                    <a class="image_link display_flex" href="<?=product_url($item)?>" title="<?=$item->name?>"> <img class="lazyload" src="data:image/png;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" data-src="<?=$item->thumb?>" alt="<?=$item->title?>"> </a>
                                                    <div class="product-action clearfix hidden-xs">
                                                        <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8619825" enctype="multipart/form-data">
                                                            <div class="group_action">
                                                                <input class="hidden" type="hidden" name="variantId" value="43400599" />
                                                                <button class="btn btn-cart btn btn-circle left-to" title="Tùy chọn" type="button" onclick="window.location.href='/san-pham/<?=$item->slug?>'"> Xem chi tiết </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="product-info a-center">
                                                    <div class="action_image">
                                                        <div class="owl_image_thumb_item hidden-md hidden-sm hidden-xs">
                                                            <div class="product_image_list owl-carousel not-owl">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h3 class="product-name margin-top-5"><a href="<?=product_url($item)?>" title="<?=$item->name?>"><?=$item->name?></a></h3>
                                                    <div class="product-hideoff">
                                                        <div class="product-hide">
                                                            <div class="price-box clearfix">
                                                                <div class="special-price"> <span class="price product-price"><?=number_format($item->price)?>đ</span> </div>
                                                                <?php if($item->origin_price > 0){?>
                                                                <div class="old-price"> <span class="price product-price-old"><?=number_format($item->origin_price)?>₫</span> </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <style>
                                .pagination li a {
                                    margin: 0 5px;
                                    border-radius: 0 !important;
                                }
                                .pagination li a:hover {
                                    background: #e1182c;
                                    color: #fff;
                                    border-radius: 0 !important;
                                }
                                .pagination li.active>a {
                                    border-color: #e1182c;
                                    background: #e1182c;
                                    color: #fff;
                                    display: block;
                                    border-radius: 0;
                                }
                                .pagenav span {
                                    margin-right: 0;
                                    line-height: 20px;
                                }
                            </style>
                            <div class="section pagenav">
                                <?=$paging ?>
                            </div>
                        </section>
                    </div>
                    <div id="open-filters" class="open-filters hidden-lg"> <i class="fa fa-align-right"></i> </div>
                </div>
            </div>
            <div class="module_like_product margin-bottom-50">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class=" section_base button-middle">
                                <div class="heading">
                                    <h2 class="title-heads">
                                        <a title="BÁN CHẠY NHẤT" href="/vans-classic">BÁN CHẠY NHẤT</a>
                                    </h2> </div>
                                <div class="products-view-grid-bb owl-carousel owl-theme products-view-grid not-dot2" data-dot="false" data-nav="true" data-lg-items="3" data-md-items="4" data-sm-items="3" data-xs-items="2" data-margin="10">
                                    <?php echo widget('NghienCode/Product_best_sale'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link href="https://productviewedhistory.sapoapps.vn//Content/styles/css/ProductRecentStyle.css" rel="stylesheet" />







<!--	<div id="collection" class="collection-page">-->
<!--		<div class="main-content container">-->
<!--			<div class="row">-->
<!--				<div id="collection-body" class="wrap-collection-body clearfix">-->
<!--					<div class="col-md-12 col-sm-12 col-xs-12">-->
<!--						<div class="wrap-collection-title row">-->
<!--							<div class="heading-collection row">-->
<!--								<div class="col-md-12 col-sm-12 col-xs-12 ">-->
<!--									<div class="collection_left_box text-center">-->
<!--									--><?php //if (isset($key)) {?>
<!--										<h1 class="title text-uppercase font-bold">-->
<!--											--><?//=$key?><!-- - VANS Việt Nam-->
<!--										</h1>-->
<!--									--><?php //}else{?>
<!--										<h1 class="title text-uppercase font-bold">-->
<!--											Tất cả sản phẩm-->
<!--										</h1>-->
<!--									--><?php //} ?>
<!--									</div>-->
<!--								</div>-->
<!--							</div>-->
<!--						</div>-->
<!--						<div class="row filter-here">-->
<!--						--><?php //if(isset($key)){ ?>
<!--							<p class="subtext-result">Kết quả tìm kiếm cho: <strong>--><?//=$key?><!--</strong></p>-->
<!--						--><?php //} ?>
<!--							<div class="content-product-list product-list filter clearfix">-->
<!--							--><?php //foreach($items as $item){ ?>
<!--									<div class="col-md-3 col-sm-6 col-xs-6 pro-loop col-4">-->
<!--										<div data-price="69500000 50" class="product-block  site-animation" data-anmation="1">-->
<!--											<div class="product-img">-->
<!--												<a href="/san-pham/--><?//=$item->slug?><!--" class="image-resize"> -->
<!--													<img class="img-loop" alt=" --><?//=$item->name?><!-- " src="--><?//=$item->thumb?><!-- "/> -->
<!--													<img class="img-loop img-hover" alt=" --><?//=$item->name?><!-- " src="--><?//=$item->thumb_hover?><!--" /> -->
<!--												</a>-->
<!--												<div class="button-add hidden">-->
<!--													<button type="submit" title="Buy now" class="action" onclick="buy_now('1066908278')">Mua ngay<i class="fa fa-long-arrow-right"></i></button>-->
<!--												</div>-->
<!--											</div>-->
<!--											<div class="product-detail clearfix">-->
<!--												<div class="box-pro-detail">-->
<!--													<h3 class="pro-name">-->
<!--													<a href="/san-pham/--><?//=$item->slug?><!--">-->
<!--														--><?//=$item->name?>
<!--													</a>-->
<!--												</h3>-->
<!--													<div class="box-pro-prices">-->
<!--														<p class="pro-price highlight">--><?//=number_format($item->price)?><!--₫</p>-->
<!--													</div>-->
<!--												</div>-->
<!--											</div>-->
<!--										</div>-->
<!--									</div>-->
<!--								--><?php //} ?>
<!--							</div>-->
<!--							<div class="sortpagibar pagi clearfix text-center">-->
<!--								<div id="pagination" class="clearfix">-->
<!--									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
<!--										<div class="pagination_wrap"> -->
<!--										<span>--><?//=$paging ?><!--</span> -->
<!--										</div>-->
<!--									</div>-->
<!--								</div>-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<?=$this->endSection();?>