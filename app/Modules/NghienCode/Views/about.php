<?=$this->section('content');?>
	<!-- CONTENT -->
	<main class="main-index">
			<div class="pageAbout-us page-layout">
                <div class="container">
                    
                    <div class="row wrapper-row pd-page">
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="page-wrapper">
                                <div class="heading-page hide">
                                    <h1>Giới thiệu</h1>
                                </div>
                                <div class="wrapbox-content-page">
                                    <div class="content-page ">
                                        <div class="main-body page-about"><div class="text-center"><div class="main-width-small"><h4 style="text-align: center;">Giới thiệu về thương hiệu <br> thời trang marc</h4><p style="font-style: italic; font-size: 14px; color: #b3b3b3; padding-bottom: 20px; text-align: center;"><span style="color: #808080; font-size: 12pt;">Thương hiệu thời trang MARC FASHION – trực thuộc công ty TNHH SX-TM<br>Nét Việt chính thức ra mắt vào năm 2006.</span></p></div><div class="main-width-small"><span style="font-size: 12pt;">Với khơi nguồn từ lòng đam mê thời trang, khát khao mang đến cái đẹp cho tất cả phụ nữ&nbsp;<br>và hơn thế nữa là mong muốn được góp phần tạo dựng hình ảnh mới lạ cho ngành công nghiệp<br>thời trang Việt Nam, MARC đã tập trung đầu tư vào chất lượng và kiểu dáng sản phẩm để thương hiệu MARC Fashion trở thành một cái tên gần gũi hơn với khách hàng.</span></div><div class="main-width-small"><span style="font-size: 12pt;">Với phương châm "BRIGHTEN YOUR DAY", MARC mong muốn&nbsp;<br>mang lại cho khách hàng những sản phẩm tốt nhất để các bạn không chỉ thể hiện cá tính bản thân mà còn&nbsp;<span>lan toả nguồn năng lượng tích cực, mạnh mẽ đến xung quanh.</span></span></div><div class="main-width-small"><span style="font-size: 12pt;"></span><br></div><div class="main-width-small"><p><img src="https://file.hstatic.net/1000197303/file/_cha1507_3f4254c197f1401e802606fb2f56d7ed_1024x1024.jpg"></p></div><div class="main-width-small"><h4 style="text-align: center;"><strong>Các sản phẩm thời trang của Marc</strong></h4><div class="_2cuy _3dgx _2vxa" style="text-align: center;">Các mẫu thiết kế của MARC luôn mang đậm phong cách phóng khoáng và tự do. Sự kết hợp hài hoà giữa hai dòng thời trang basic và trendy khiến các thiết kế vừa mang tính tiện dụng, phù hợp với nhiều bối cảnh hằng ngày mà vẫn bắt kịp xu hướng mốt của năm.</div><div class="_2cuy _3dgx _2vxa" style="text-align: center;">Không chỉ đem đến cho khách hàng những sản phẩm mang phong cách và kiểu dáng độc đáo mà còn giới thiệu đến khách hàng những sản phẩm đạt chất lượng tốt nhất. Các mẫu thiết kế được kiểm duyệt kĩ càng từ khâu chọn chất liệu, dựng mẫu và hoàn thiện.<br>Chính vì thế, những thiết kế đến tay khách hàng luôn là những sản phẩm tinh tế nhất. Với đội ngũ chuyên nghiệp cùng sự kết hợp chặt chẽ và thống nhất của ban lãnh đạo, thương hiệu thời trang MARC hy vọng sẽ đáp ứng được sự yêu mến và tin cậy của quý khách hàng về dòng sản phẩm thời trang chất lượng cao.</div><p style="text-align: center;"><span style="font-size: 12pt;"><span></span></span><br></p><p><img src="https://file.hstatic.net/1000197303/file/_cha1742_2a6dfbe2a6ba4bd3925d9de80bbf05ba_1024x1024.jpg"></p></div></div><div class="row"><h4 style="text-align: center;">Đối tượng khách hàng</h4><p style="text-align: center;"><span>Dù cho bạn là cô nàng mạnh mẽ, độc lập hay nàng thơ nhẹ nhàng, ngọt ngào thì MARC tin mình có thể đáp ứng nhu cầu mặc đẹp mỗi ngày của bạn. Bạn không cần thay đổi phong cách để chạy theo xu hướng, là một quý cô hiện đại, bạn hãy tận dụng thời trang để làm mới bản thân và tự mình tạo ra những nguồn năng lượng trẻ trung, tích cực.</span><br></p><p><img src="https://file.hstatic.net/1000197303/file/_cha1977__063b7c7538e242b08a8a275a7775b36b_1024x1024.jpg" style="display: block; margin-left: auto; margin-right: auto;"></p><p style="text-align: center;"><strong>Còn chần chờ gì nữa, hãy đến với MARC để trở thành một cô nàng tự tin, yêu bản thân và yêu đời hơn.</strong></p><p><br></p></div></div>
                                    </div>
                                            <div class="button-link-marc">
                                                <a href="/contact" class="link-marc-contact">liên hệ với chúng tôi</a>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</main>
<?=$this->endSection();?>