<?=$this->section('content');?>
	<!-- CONTENT -->
	<div class="contacts-section">
		<div class="contacts-wrap">
			<div class="main-wrap">
				<div class="breadcrumb">
					<div class="breadcrumb-wrap">
						<ul class="breadcrumb-nav">
							<li class="breadcrumb-item">
								<a href="<?=URL?>" class="breadcrumb-link">Trang chủ</a>
							</li>
							<li class="breadcrumb-item symbol">&nbsp;/&nbsp;</li>
							<li class="breadcrumb-item active large-text last">
								<a href="javascript:;" class="breadcrumb-link">Liên hệ</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="contacts-grid">
					<div class="contacts-grid-wrap">
						<div class="contacts-inner-grid anim-block">
							<div class="inner-grid-item anim-elem top">
								<div class="contacts-links-wrap">
									<div class="head-office">
										<div class="aside-bg-block"></div>
										<div class="title-wrap">
											<h2 class="office-title split-text-wrap" data-delay=".5" data-stigger-time=".05"><span class="small-line-text split-block">Rượu</span> <span class="split-block">9 Chum</span></h2>
										</div>
										<div class="contacts-links-item anim-elem">
											<a href="tel:+35924899450" class="contacts-link">
												<span class="link-image-wrap">
													<img src="<?=CDN?>/themes/9chum/images/homepage/footer-phone.svg" alt="icon" class="contacts-link-image" />
												</span>
												<span class="link-text-wrap"><?=cms_config('hotline');?></span>
											</a>
											<div class="item-border"></div>
										</div>
										<div class="contacts-links-item anim-elem">
											<a href="mailto:office.bulgaria@mbws.com" class="contacts-link">
												<span class="link-image-wrap">
													<img src="<?=CDN?>/themes/9chum/images/homepage/footer-mail.svg" alt="icon" class="contacts-link-image" />
												</span>
												<span class="link-text-wrap">
													<span class="small-text">
														<?=cms_config('email');?>
													</span>
												</span>
											</a>

											<div class="item-border"></div>
										</div>
										<div class="contacts-links-item anim-elem">
											<a
												href="https://www.google.com/maps/place/%D0%91%D0%B8%D0%B7%D0%BD%D0%B5%D1%81+%D0%BF%D0%B0%D1%80%D0%BA/@42.626886,23.377104,15z/data=!4m5!3m4!1s0x0:0xa99a6079f95e99ad!8m2!3d42.6277378!4d23.3744328?hl=bg"
												target="_blank"
												class="contacts-link"
											>
												<span class="link-image-wrap">
													<img src="<?=CDN?>/themes/9chum/images/homepage/footer-pin.svg" alt="icon" class="contacts-link-image" />
												</span>
												<span class="link-text-wrap">
													<span class="small-text">
														<?=cms_config('address');?>
													</span>
												</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="contacts-form-container anim-block">
							<div class="contacts-form-container-wrap anim-elem bottom">
								<div class="title-wrap">
									<h1 class="contacts-title split-block split-text-wrap" data-delay="0.5" data-stigger-time=".05">
										Liên hệ với chúng tôi
									</h1>
								</div>
								<form class="contacts-form" id="form_contact" method="POST">
									<div class="form-item anim-elem top">
										<label for="name" class="label-form-fld">Tên</label>
										<input type="text" class="form-fld" id="name" name="name" />
									</div>
									<div class="form-item anim-elem top">
										<label for="phone" class="label-form-fld">Điện thoại</label>
										<input type="text" class="form-fld" id="phone" name="phone" />
									</div>
									<div class="form-item anim-elem top">
										<label for="email" class="label-form-fld">E-mail</label>
										<input type="email" class="form-fld" id="email" name="email" />
									</div>

									<div class="form-item anim-elem top">
										<label for="message" class="label-form-fld message-label">Tin nhắn</label>
										<textarea name="message" id="message" class="form-fld text-message"></textarea>
									</div>
									
									<div class="contacts-btn-container anim-elem top">
										<div class="btn-contacts-wrap">
											<button type="submit" class="contacts-form-btn" id="contacts-form-btn">
												<span class="link-btn contacts">GỬI TIN NHẮN</span>
											</button>
										</div>
									</div>
								</form>
								<div class="form-success">
									<div class="form-success-wrap">
										<img src="<?=CDN?>/themes/9chum/images/success.svg" alt="icon" class="success-image" />
										<p class="success-text">Your message was sent!</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$("#form_contact").submit(function(event) {
			var data = $("#form_contact").serialize();
			$.ajax({
				type : "POST",
				url: base_url+"/api/contact/normal",
				traditional: true,
				data: data,
				dataType: "jsonp",
				success: function(result, textStatus, jqXHR){
					if(result.status=="success") {
						swal({
							text: result.message,
							type: "success",
							showConfirmButton: false
						});
						setTimeout(function(){
							location.reload();
						}, 3000);
					} else {
						var message = 'Có lỗi xảy ra, vui lòng thử lại sau';
						if (typeof result.message !== 'undefined') {
							message = result.message;
						}
						swal({
							text: message,
							type: "warning",
						});
					}
				},
				error: function(xhr,status,error){
					console.log("error occurred.");
				}
			});
			return false;
		});
	</script>
<?=$this->endSection();?>