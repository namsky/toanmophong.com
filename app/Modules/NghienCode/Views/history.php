<?=$this->section('content');?>
	<!-- CONTENT -->
	<div class="history-section">
		<div class="history-wrap">
			<div class="main-wrap">
				<div class="breadcrumb">
					<div class="breadcrumb-wrap">
						<ul class="breadcrumb-nav">
							<li class="breadcrumb-item">
								<a href="<?=URL?>" class="breadcrumb-link">Trang chủ</a>
							</li>
							<li class="breadcrumb-item symbol">&nbsp;/&nbsp;</li>
							<li class="breadcrumb-item">
								<a href="javascript:;" class="breadcrumb-link">Lịch sử</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="section-wrap" style="margin-bottom:120px;">
					<div class="time-line"></div>
					<div class="history-intro anim-block">
						<div class="hisory-intro-wrap">
							<div class="inner-line"></div>
							<div class="intro-content anim-elem">
								<p class="history-text anim-elem delay-03">
									<?=cms_config('big_text');?>
								</p>
							</div>
							<div class="image-content anim-block">
								<div class="date-container">
									<div class="date-container-wrap anim-elem">
										<span class="container-dot anim-elem delay-05"></span>
										<div class="title-wrap">
											<h3 class="date-title split-text-wrap" data-delay="1.2" data-stigger-time=".07"><span class="split-block"><?=cms_config('first_year');?></span> </h3>
										</div>
										<p class="date-text anim-elem delay-1">
											<?=cms_config('first_year_description');?>
										</p>
									</div>
								</div>
								<div class="rect-bg"></div>
								<div class="intro-image-wrap anim-block">
									<img src="<?=cms_config('first_year_image');?>" alt="statue" class="intro-image anim-elem top" />
								</div>
							</div>
						</div>
					</div>
					<div class="timeline-section last">
						<div class="timeline-section-wrap">
							<div class="timeline-image-wrap anim-block">
								<img src="<?=cms_config('second_year_image');?>" alt="background" class="timeline-image anim-elem top-50" />
							</div>
							<div class="date-container top-position anim-block">
								<div class="date-container-wrap anim-elem">
									<span class="container-dot anim-elem delay-03"></span>
									<div class="title-wrap">
										<h3 class="date-title split-text-wrap" data-delay="1.1" data-stigger-time=".07">
											<span class="split-block"><?=cms_config('second_year');?></span>
										</h3>
									</div>
									<p class="date-text anim-elem delay-1">
										<?=cms_config('second_year_description');?>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="timeline-section last revert">
						<div class="timeline-section-wrap">
							<div class="timeline-image-wrap anim-block w-40">
								<img src="<?=cms_config('third_year_image');?>" alt="background" class="timeline-image anim-elem top-50" />
							</div>
							<div class="date-container bottom-position anim-block">
								<div class="date-container-wrap">
									<span class="container-dot anim-elem delay-05"></span>
									<div class="title-wrap">
										<h3 class="date-title split-text-wrap" data-delay="1.5" data-stigger-time=".07">
											<span class="split-block"><?=cms_config('third_year');?></span>
										</h3>
									</div>
									<p class="date-text anim-elem delay-15">
										<?=cms_config('third_year_description');?>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="timeline-section last">
						<div class="timeline-section-wrap">
							<div class="timeline-image-wrap anim-block w-40">
								<img src="<?=cms_config('fourth_year_image');?>" alt="background" class="timeline-image anim-elem top-50" />
							</div>
							<div class="date-container top-position anim-block">
								<div class="date-container-wrap anim-elem">
									<span class="container-dot anim-elem delay-03"></span>
									<div class="title-wrap">
										<h3 class="date-title split-text-wrap" data-delay="1.1" data-stigger-time=".07">
											<span class="split-block"><?=cms_config('fourth_year');?></span>
										</h3>
									</div>
									<p class="date-text anim-elem delay-1">
										<?=cms_config('fourth_year_description');?>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="timeline-section last revert fifth-year">
						<div class="timeline-section-wrap">
							<div class="timeline-image-wrap anim-block">
								<img src="<?=cms_config('fifth_year_image');?>" alt="background" class="timeline-image anim-elem top-50" />
							</div>
							<div class="date-container bottom-position anim-block">
								<div class="date-container-wrap">
									<span class="container-dot anim-elem delay-05"></span>
									<div class="title-wrap">
										<h3 class="date-title split-text-wrap" data-delay="1.5" data-stigger-time=".07">
											<span class="split-block"><?=cms_config('fifth_year');?></span>
										</h3>
									</div>
									<p class="date-text anim-elem delay-15">
										<?=cms_config('fifth_year_description');?>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="timeline-section last real-last">
						<div class="timeline-section-wrap">
							<div class="timeline-image-wrap anim-block">
								<img src="<?=cms_config('sixth_year_image');?>" alt="background" class="timeline-image anim-elem top-50" />
							</div>
							<div class="date-container top-position anim-block">
								<div class="date-container-wrap anim-elem">
									<span class="container-dot anim-elem delay-03"></span>
									<div class="title-wrap">
										<h3 class="date-title split-text-wrap" data-delay="1.1" data-stigger-time=".07">
											<span class="split-block"><?=cms_config('sixth_year');?></span>
										</h3>
									</div>
									<p class="date-text anim-elem delay-1">
										<?=cms_config('sixth_year_description');?>
									</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="timeline-section last end">
						<div class="date-container top-position anim-block">
							<div class="date-container-wrap anim-elem">
								<span class="container-dot anim-elem delay-03"></span>
								<div class="title-wrap">
									<h3 class="date-title split-text-wrap" data-delay="1.1" data-stigger-time=".07">
										<span class="split-block"><?=cms_config('seventh_year');?></span>
									</h3>
								</div>
								<p class="date-text anim-elem delay-1">
									<?=cms_config('seventh_year_description');?>
								</p>
							</div>
						</div>
					</div>
					<div class="timeline-section last">
					</div>
					<div class="timeline-section last">
					</div>
				</div>
			</div>
		</div>
	</div>
<?=$this->endSection();?>