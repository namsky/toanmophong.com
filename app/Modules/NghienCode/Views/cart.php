<?=$this->section('content');?>
<?php if($session->getFlashdata('checkoutsuccess')){ ?>
    <script>
        Swal.fire(
            'Thành công!',
            '<?=$session->getFlashdata('checkoutsuccess')?>',
            'success'
        )
    </script>
<?php } ?>
    <div class="breadcrumb_background margin-bottom-40">
        <div class="title_full">
            <div class="container a-center">
                <p class="title_page">Giỏ hàng</p>
            </div>
        </div>
        <section class="bread-crumb">
            <span class="crumb-border"></span>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 a-center">
                        <ul class="breadcrumb">
                            <li class="home">
                                <a href="/"><span>Trang chủ</span></a>
                                <span class="mr_lr">&nbsp;<i class="fa">/</i>&nbsp;</span>
                            </li>

                            <li><strong><span>Giỏ hàng</span></strong></li>

                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="container">
        <div class="row">
            <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-cart-page main-container col1-layout">
                <div class="main">
                    <div class="wrap_background_aside margin-bottom-40">
                        <div class="header-cart">
                            <h1 class="title_cart">
                                <span>Giỏ hàng</span>
                            </h1>
                            <div class="header-cart title_cart_pc">

                            </div>
                        </div>
                        <div class="col-main cart_desktop_page cart-page">
                            <?php if(!empty($items)){ ?>
                                <div class="cart page_cart row">
                                <form action="/cart" method="post" novalidate="" class="formcartpage col-lg-12 col-md-12 margin-bottom-0">
                                    <div class="bg-scroll">
                                        <div class="cart-thead">
                                            <div style="width: 10%;border-left:0;" class="a-left"></div>
                                            <div style="width: 40%;border-left:0;" class="a-left">Sản phẩm</div>
<!--                                            <div style="width: 11%;border-left:0;" class="a-left"></div>-->
<!--                                            <div style="width: 15%" class="a-center">Số lượng</div>-->
                                            <div style="width: 40%" class="a-center">Tổng tiền</div>
                                            <div style="width: 10%" class="a-center">Xóa</div>
                                        </div>
                                        <div class="cart-tbody">
                                            <?php foreach ($items as $key=>$item) { ?>
                                                <div class="item-cart productid-<?=$item['rowid']?>">
                                                <div style="width: 10%;border-left:0;"></div>
                                                <div style="width: 10%;border-left:0;" class="image">
                                                    <a class="product-image a-left" title="<?=$item['name']?>" href="<?=$item['url']?>">
                                                        <img width="100" height="auto" alt="<?=$item['name']?>" src="<?=$item['thumb']?>">
                                                    </a>
                                                </div>
                                                <div style="width: 30%;border-left:0;" class="a-left contentcart">
                                                    <h3 class="product-name"> <a class="text2line" href="<?=$item['url']?>" title="<?=$item['name']?>"><?=$item['name']?></a> </h3>
                                                    <span class="variant-title"><?=$item['size']?></span>
                                                    <span class="cart-prices">
                                                        <span class="prices"><?=$item['price']?>₫</span>
                                                    </span>
                                                </div>
                                                <div style="width: 40%" class="a-center">
                                                    <span class="cart-price">
                                                        <span class="price"><?=$item['total_money_detail']?><span>₫</span></span>
                                                    </span>
                                                </div>
                                                <div style="width: 10%" class="a-center">
                                                    <a class="remove-itemx remove-item-cart" id="remove-item-cart-detail" title="Xóa" href="javascript:delete_cart_detail('<?=$item['rowid']?>');" data-id="<?=$item['rowid']?>">
                                                        <span><i class="fas fa-times"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                           <?php  } ?>
                                        </div>
                                    </div>
                                </form>

                                <div class="col-lg-12 col-md-12">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-3"></div>
                                        <div class="col-lg-6 col-md-7">
                                            <div class="section bg_cart shopping-cart-table-total">
                                                <div class="table-total">
                                                    <table class="table">
                                                        <tbody><tr>
                                                            <td class="total-text f-left">Tổng tiền</td>
                                                            <td class="txt-right totals_price price_end f-right"><?=$total_money?>₫</td>
                                                        </tr>
                                                        </tbody></table>
                                                </div>
                                                <a href="/checkout" class="btn-checkout-cart button_checkfor_buy">Tiến hành thanh toán</a>
                                                <a href="/san-pham" class="form-cart-continue">Tiếp tục mua hàng</a>

                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2"></div>
                                    </div>
                                </div>
                            </div>
                            <?php } else { ?>
                                <p class="hidden-xs-down ">Không có sản phẩm nào. Quay lại <a href="/collections/all" style="color:;">cửa hàng</a> để tiếp tục mua sắm.</p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <script>
                    function delete_cart_detail(rowid) {
                        console.log(rowid);
                        $.ajax({
                            type: 'get',
                            url: '/cart/destroy',
                            data: {
                                rowid:rowid
                            },
                            success: function (res) {
                                $(".productid-"+rowid+"").hide();
                            }
                        });
                    }
                    function update_qty_detail(rowid) {
                        var iNum  = $("#qtyItem"+rowid+"").val();
                        var qty = parseInt(iNum);
                        $.ajax({
                            type: 'post',
                            url: '/cart/update_qty',
                            data: {
                                rowid:rowid,
                                qty:qty+1,
                            },
                            success: function (res) {
                                $(".totals_price").html(res.total);
                                $(".productid-"+res.data.rowid+" .price").html(res.data.subtotal);
                                get_cart();
                            }
                        });
                    }

                </script>
<!--                <div class="wrap_background_aside padding-top-15 margin-bottom-40 padding-left-0 padding-right-0 hidden-md hidden-lg">-->
<!--                    <div class="cart-mobile">-->
<!--                        <form action="/cart" method="post" novalidate="" class="margin-bottom-0">-->
<!--                            <div class="header-cart">-->
<!---->
<!--                                <div class="title-cart title_cart_mobile hidden">-->
<!--                                    <h3>Giỏ hàng</h3>-->
<!--                                </div>-->
<!---->
<!--                            </div>-->
<!---->
<!--                            <div class="header-cart-content" style="background:#fff;"><div class="cart_page_mobile content-product-list">-->
<!--                                    <div class="item-product item productid-48943329 ">-->
<!--                                        <div class="item-product-cart-mobile">-->
<!--                                            <a class="product-images1" href="/mike-gigliotti-for-vans-x-spongebob-skate-old-skool-gigliotti-vn0a5fcbzau" title="MIKE GIGLIOTTI FOR VANS X SPONGEBOB SKATE OLD SKOOL">-->
<!--                                                <img width="80" height="150" src="//bizweb.dktcdn.net/thumb/compact/100/140/774/products/mike-gigliotti-for-vans-x-spongebob-skate-old-skool-gigliotti-vn0a5fcbzau-1.jpg" alt="MIKE GIGLIOTTI FOR VANS X SPONGEBOB SKATE OLD SKOOL">-->
<!--                                            </a>-->
<!--                                        </div>-->
<!--                                        <div class="title-product-cart-mobile">-->
<!--                                            <h3>-->
<!--                                                <a href="/mike-gigliotti-for-vans-x-spongebob-skate-old-skool-gigliotti-vn0a5fcbzau" title="MIKE GIGLIOTTI FOR VANS X SPONGEBOB SKATE OLD SKOOL">MIKE GIGLIOTTI FOR VANS X SPONGEBOB SKATE OLD SKOOL</a>-->
<!--                                            </h3>-->
<!--                                            <p>-->
<!--                                                Giá: <span>2.990.000₫</span>-->
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                        <div class="select-item-qty-mobile">-->
<!--                                            <div class="txt_center">-->
<!--                                                <input class="variantID" type="hidden" name="variantId" value="48943329">-->
<!--                                                <button onclick="var result = document.getElementById('qtyMobile48943329'); var qtyMobile48943329 = result.value; if( !isNaN( qtyMobile48943329 ) &amp;&amp; qtyMobile48943329 > 1 ) result.value--;return false;" class="reduced items-count btn-minus" type="button"><i class="fa fa-minus"></i></button>-->
<!--                                                <input type="text" maxlength="3" min="1" class="input-text number-sidebar qtyMobile48943329" id="qtyMobile48943329" name="Lines" size="4" value="8">-->
<!--                                                <button onclick="var result = document.getElementById('qtyMobile48943329'); var qtyMobile48943329 = result.value; if( !isNaN( qtyMobile48943329 )) result.value++;return false;" class="increase items-count btn-plus" type="button"><i class="fa fa-plus"></i></button>-->
<!--                                            </div>-->
<!--                                            <a class="button remove-item remove-item-cart" href="javascript:;" data-id="48943329">Xoá</a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="header-cart-price">-->
<!--                                    <div class="title-cart">-->
<!--                                        <h3 class="text-xs-left">Tổng tiền</h3>-->
<!--                                        <a class="text-xs-right  totals_price_mobile">23.920.000₫</a>-->
<!--                                    </div>-->
<!--                                    <div class="checkout">-->
<!--                                        <button class="btn-proceed-checkout-mobile" title="Tiến hành thanh toán" type="button" onclick="window.location.href='/checkout'">-->
<!--                                            <span>Tiến hành thanh toán</span></button>-->
<!--                                        <button class="btn btn-white f-left" title="Tiếp tục mua hàng" type="button" onclick="window.location.href='/collections/all'">-->
<!--                                            <span>Tiếp tục mua hàng</span>-->
<!--                                        </button>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!---->
<!--                        </form>-->
<!--                    </div>-->
<!--                </div>-->

            </section>
        </div>
    </div>
<?=$this->endSection();?>