<?=$this->section('content');?>
    <div class="breadcrumb_background margin-bottom-40">
        <div class="title_full">
            <div class="container a-center">
                <p class="title_page">Giỏ hàng</p>
            </div>
        </div>
        <section class="bread-crumb">
            <span class="crumb-border"></span>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 a-center">
                        <ul class="breadcrumb">
                            <li class="home">
                                <a href="/"><span>Trang chủ</span></a>
                                <span class="mr_lr">&nbsp;<i class="fa">/</i>&nbsp;</span>
                            </li>

                            <li><strong><span>Thanh toán</span></strong></li>

                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="container">
        <div class="row">
            <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-cart-page main-container col1-layout">
                <div class="main ">
                    <div class="wrap_background_aside margin-bottom-40">
                        <div class="header-cart">
                            <h1 class="title_cart hidden">
                                <span>Thanh toán</span>
                            </h1>
                            <div class="header-cart title_cart_pc">

                            </div>
                        </div>
                        <div class="col-main cart_desktop_page cart-page">
                            <?php
                            if(!empty($data)) { ?>
                                <p class="hidden-xs-down "><?=$data['text']?></p>
                                <? if(isset($data['status']) && $data['status'] == 200) {
                                    foreach ($data['link'] as $key=>$item) { ?>
                                        <a style="border-radius: 5px;padding: 10px" href="<?=$item?>" id="btn_dowload_file" class="btn-danger btn-large">Bấm vào đây để Tải File về</a>
                                   <?php }
                                    ?>

                                <?php  } ?>
                           <?php } ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?=$this->endSection();?>