<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8" />
    <meta name="theme-color" content="#f02b2b" />
    <link rel="canonical" href="<?=URL?>" />
    <meta name='revisit-after' content='2 days' />
    <meta name="robots" content="noodp,index,follow" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="<?=cms_config('description');?>">
    <title><?=cms_config('title');?></title>

    <?=load_css('font-roboto.css', 'themes/vansvietnam'); ?>
    <?=load_css('plugin.css', 'themes/vansvietnam'); ?>
    <?=load_css('index.css', 'themes/vansvietnam'); ?>
    <?=load_css('main.css', 'themes/vansvietnam'); ?>
    <?=load_css('cartpage.css', 'themes/vansvietnam'); ?>
    <?=load_css('responsive.css', 'themes/vansvietnam'); ?>
    <?=load_css('all.css', 'themes/vansvietnam'); ?>

    <?=load_js('jquery.min.1.11.0.js', 'themes/vansvietnam'); ?>
    <?=load_js('main.js', 'themes/vansvietnam'); ?>
    
    <?php $cms->show_asset('css'); ?>
    <?php $cms->show_asset('js'); ?>

<!--    <script src="./js/main.js"></script>-->
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?=cms_config('title');?>">
    <meta property="og:image" content="<?=cms_config('logo')?>">
    <meta property="og:image:secure_url" content="<?=cms_config('logo')?>">
    <meta property="og:description" content="<?=cms_config('description');?>">
    <meta property="og:url" content="<?=URL?>">
    <meta property="og:site_name" content="<?=cms_config('title');?>">
    <link rel="icon" href="<?=cms_config('favicon')?>" type="image/x-icon" />
</head>
<script>
    var base_url = "<?=URL?>";
</script>
<body>
<div class="opacity_menu"></div>
<!-- Main content -->
<!-- Menu mobile -->
<div id="mySidenav" class="sidenav menu_mobile hidden-md hidden-lg">
    <div class="top_menu_mobile"> <span class="close_menu"></span> </div>
    <div class="content_memu_mb">
        <?php echo widget('Cms/Mobile_menu_main'); ?>
    </div>
</div>
<!-- End -->
<!-- HEADER NEW -->
<header class="header header_s">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <div class="topbar_left">
                        <ul>
                            <li class="hidden-xs"> <a class="hai01" href="tel:<?=cms_config('email');?>"><?=cms_config('email');?></a> </li>
                            <li class="line_ hidden-xs">/</li>
                            <li class="hidden-xs"> <a class="hai01" href="tel:<?=cms_config('phone');?>"><?=cms_config('phone');?></a> </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 d-list col-xs-12 a-right topbar_right">
                </div>
            </div>
        </div>
    </div>
    <div class="mid-header wid_100">
        <div class="container">
            <div class="row">
                <div class="content_header">
                    <div class="header-main">
                        <div class="col-lg-3 col-md-3 col-xs-6">
                            <div class="logo logo_centers">
                                <a href="/" class="logo-wrapper "> <img src="<?=cms_config('logo')?>" alt="logo Toàn Mô Phỏng"> </a>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7 hidden-xs">
                            <div class="wrap_main hidden-xs hidden-sm">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="bg-header-nav hidden-xs hidden-sm">
                                            <div>
                                                <div class="row row-noGutter-2">
                                                    <?php echo widget('Cms/Main_menu'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-6 padding-0">
                            <div class="menu-bar-h nav-mobile-button hidden-md hidden-lg"> <i class="fas fa-bars"></i> </div>
                            <div class="cartgroup ">
                                <div class="searchion inline-b"> <span class="visible_index nn"><i class="fas fa-search"></i></span>
                                    <div class="searchmini">
                                        <form action="/search" method="get" class="input-group search-bar" role="search">
                                            <input type="text" name="query" value="" autocomplete="off" placeholder="Tìm kiếm..." class="input-group-field auto-search visible_index">
                                            <button type="submit" class="visible_index btn icon-fallback-text"> <span class="fa fa-search"></span> </button>
                                        </form>
                                    </div>
                                </div>
                                <div class="header-right inline-block">
                                    <div class="top-cart-contain f-right">
                                        <div class="mini-cart text-xs-center">
                                            <div class="heading-cart cart_header">
                                                <a class="img_hover_cart" href="/cart" title="Giỏ hàng">
                                                    <div class="icon_hotline visible_index"> <img src="//bizweb.dktcdn.net/100/140/774/themes/827866/assets/shopping-bag.svg?1645722631863" alt="Toàn mô phỏng"> <span class="count_item count_item_pr button_gradient"></span> </div>
                                                </a>
                                            </div>
                                            <div class="top-cart-content hidden-xs hidden-sm hidden-md">
<!--                                                <ul id="cart-sidebar" class="mini-products-list count_li">-->
<!--                                                    <li class="list-item">-->
<!--                                                        <ul></ul>-->
<!--                                                    </li>-->
<!--                                                    <li class="action">-->
<!--                                                        <ul>-->
<!--                                                            <li class="li-fix-1">-->
<!--                                                                <div class="top-subtotal"> Tổng tiền thanh toán: <span class="price"></span> </div>-->
<!--                                                            </li>-->
<!--                                                            <li class="li-fix-2" style="">-->
<!--                                                                <div class="actions">-->
<!--                                                                    <a href="/cart" class="btn btn-primary" title="Giỏ hàng"> <span>Giỏ hàng</span> </a>-->
<!--                                                                    <a href="/checkout" class="btn btn-checkout btn-gray" title="Thanh toán"> <span>Thanh toán</span> </a>-->
<!--                                                                </div>-->
<!--                                                            </li>-->
<!--                                                        </ul>-->
<!--                                                    </li>-->
<!--                                                </ul>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="top-cart-contain f-right hidden">
                                        <div class="mini-cart text-xs-center">
                                            <div class="heading-cart">
                                                <a class="bg_cart" href="/cart" title="Giỏ hàng"> <img src="//bizweb.dktcdn.net/100/140/774/themes/827866/assets/shopping-bag.svg?1645722631863" alt="VANS Việt Nam"> <span class="count_item count_item_pr"></span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- END HEADER NEW -->
<?=$this->renderSection('content')?>
<div class="pr-module-box" style="display: none">
    <div class="pr-module-title"> </div>
    <div class="pr-slide-wrap">
        <ul class="pr-list-product-slide"> </ul>
    </div>
    <div class="pr-slide"> </div>
</div>
<script>
    window.productRecentVariantId = [];
    window.productRecentId = "";
</script>
<link href="https://productviewedhistory.sapoapps.vn//Content/styles/css/ProductRecentStyle.css" rel="stylesheet" />
<footer class="footer">
    <div class="footer_top">
        <div class="new-letter a-center lazyload" data-src="<?=URL?>/statics/themes/vansvietnam//images/bg_footer.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 padding_">
                        <div class="letter-left heading">
                            <h3>Nhập email</h3> <span>Để nhận tin tức khuyến mãi từ cửa hàng của chúng tôi</span> </div>
                        <form action="https://sapo.us19.list-manage.com/subscribe/post?u=2887dcda77021868cccd236ea&amp;id=7ace19f37b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank">
                            <div class="input-group">
                                <input type="email" autocomplete="off" class="form-control" value="" placeholder="Nhập email của bạn" name="EMAIL" id="mail"> <span class="input-group-btn">
							<button class="btn btn-primary button_gradient" name="subscribe" id="subscribe" type="submit" title="Gửi ngay">Gửi ngay</button>
						</span> </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-footer">
        <div class="section top-footer mid-footer margin-bottom-30">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="widget-ft">
                            <h4 class="title-menu">
                                <a role="button">
                                    <?=cms_config('title')?>
                                </a>
                            </h4>
                            <div>
                                <div class="list-menu">
                                    <div class="widget-ft wg-logo">
                                        <div class="item">
                                            <ul class="contact contact_x">
                                                <li> <span class="txt_content_child">
														<span>Địa chỉ:</span> <?=cms_config('address')?> </span>
                                                </li>
                                                <li class="sdt"> <span> Hotline:</span> <a href="tel:<?=cms_config('phone')?>"><?=cms_config('phone')?></a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="widget-ft first">
                                    <h4 class="title-menu">
                                        <a role="button" class="collapsed" data-toggle="collapse" aria-expanded="false" data-target="#collapseListMenu01" aria-controls="collapseListMenu01">
                                            Về Chúng Tôi <i class="fa fa-plus" aria-hidden="true"></i>
                                        </a>
                                    </h4>
                                    <div class="collapse" id="collapseListMenu01">
                                        <ul class="list-menu">
                                            <li class="li_menu"><a href="#">About Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="widget-ft first">
                                    <h4 class="title-menu">
                                        <a role="button" class="collapsed" data-toggle="collapse" aria-expanded="false" data-target="#collapseListMenu02" aria-controls="collapseListMenu02">
                                            Chính Sách <i class="fa fa-plus" aria-hidden="true"></i>
                                        </a>
                                    </h4>
                                    <div class="collapse" id="collapseListMenu02">
                                        <ul class="list-menu">
                                            <li class="li_menu"><a href="#">Chính sách thanh toán</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="widget-ft">
                            <h4 class="title-menu">
                                <a role="button">

                                </a>
                            </h4>
                            <div class="footer-widget">
                                <a href="/" class="bocongthuong"> <img src="<?=cms_config('logo')?>" alt="VANS Việt Nam"> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section copyright clearfix">
        <div class="container">
            <div class="inner clearfix">
                <div class="row tablet">
                    <div id="copyright" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 a-center fot_copyright"> <span class="wsp">
							<span class="mobile">© Bản quyền thuộc về <b><?=cms_config('site_name')?></b><span class="hidden-xs"> | </span></span> <span class="opacity1">Cung cấp bởi</span> <a href="https://net5s.vn?source=toanmophong.com" rel="nofollow" title="<?=cms_config('title')?>" target="_blank">Net5s.vn</a> </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Bizweb javascript -->
<?=load_js('core.js', 'themes/vansvietnam'); ?>
<?=load_js('option-selectors.js', 'themes/vansvietnam'); ?>
<?=load_js('stats.min.js', 'themes/vansvietnam'); ?>
<!--<script src="./js/option-selectors.js" type="text/javascript"></script>-->
<!--<script src="./js/core.js" type="text/javascript"></script>-->
<div class="addcart-popup product-popup awe-popup">
    <div class="overlay no-background"></div>
    <div class="content">
        <div class="row row-noGutter">
            <div class="col-xl-6 col-xs-12">
                <div class="btn btn-full btn-primary a-left popup-title"><i class="fa fa-check"></i>Thêm vào giỏ hàng thành công </div> <a href="javascript:void(0)" class="close-window close-popup"><i class="fa fa-close"></i></a>
                <div class="info clearfix">
                    <div class="product-image margin-top-5"> <img alt="popup" src="//bizweb.dktcdn.net/100/140/774/themes/827866/assets/logo.png?1645722631863" style="max-width:150px; height:auto" /> </div>
                    <div class="product-info">
                        <p class="product-name"></p>
                        <p class="quantity color-main"><span>Số lượng: </span></p>
                        <p class="total-money color-main"><span>Tổng tiền: </span></p>
                    </div>
                    <div class="actions">
                        <button class="btn  btn-primary  margin-top-5 btn-continue">Tiếp tục mua hàng</button>
                        <button class="btn btn-gray margin-top-5" onclick="window.location='/cart'">Kiểm tra giỏ hàng</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="error-popup awe-popup">
    <div class="overlay no-background"></div>
    <div class="popup-inner content">
        <div class="error-message"></div>
    </div>
</div>
<div id="popup-cart" class="modal fade" role="dialog">
    <div id="popup-cart-desktop" class="clearfix">
        <div class="title-popup-cart"> <span class="your_product"><i class="fas fa-check"></i>Bạn đã thêm <span class="cart-popup-name"></span> vào giỏ hàng </span>
        </div>
        <div class="wrap_popup">
            <div class="title-quantity-popup"> <i class="fa fa-shopping-cart"></i><span class="cart_status" onclick="window.location.href='/cart';">Giỏ hàng của bạn có <span class="cart-popup-count"></span> sản phẩm </span>
            </div>
            <div class="content-popup-cart">
                <div class="thead-popup">
                    <div style="width: 55%;" class="text-left">Sản phẩm</div>
                    <div style="width: 15%;" class="text-center">Đơn giá</div>
                    <div style="width: 15%;" class="text-center">Số lượng</div>
                    <div style="width: 15%;" class="text-center">Thành tiền</div>
                </div>
                <div class="tbody-popup scrollbar-dynamic"> </div>
                <div class="tfoot-popup section">
                    <div class="section footer-popup">
                        <div class="inline-block f-left"> <span class="block">
								Giao hàng trên toàn quốc
							</span> <a class="f-left button buy_ btn-proceed-checkout" title="tiếp tục mua hàng" href="javascript:;" onclick="$('#popup-cart').modal('hide');"><span><i class="fa fa-caret-left"></i>Tiếp tục mua hàng</span></a> </div>
                        <div class="tfoot-popup-1 f-right inline-block clearfix"> <span class="total-p popup-total">Thành tiền: <span class="total-price"></span></span>
                        </div>
                    </div>
                    <div class="tfoot-popup-2 clearfix section"> <a class="button checkout_ btn-proceed-checkout" title="Tiến hành đặt hàng" href="/checkout"><span>Tiến hành đặt hàng<i class="fa fa-arrow-right" aria-hidden="true"></i></span></a> </div>
                </div>
            </div> <a title="Close" class="quickview-close close-window" href="javascript:;" onclick="$('#popup-cart').modal('hide');"><i class="fa  fa-close"></i></a> </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog"> </div>
</script>
<!-- Add to cart -->
<div id="popupCartModal" class="modal fade" role="dialog"> </div>
<div class="fb-livechat">
    <a href="https://m.me/370795973629235" target="_blank" title="Chat với chúng tôi!" class="ctrlq fb-button"></a>
</div>
<script>
    $( document ).ready(function() {
        get_cart();
    });
    function get_cart(){
        $.ajax({
            type: 'get',
            url: '/cart/get_list',
            success: function (res) {
                // console.log(typeof res);
                console.log(res);
                var html = '';
                $(".count_item").html(res.total_qty);
                if(res.data.length == 0) {
                  html += '<ul id="cart-sidebar" class="mini-products-list count_li">\
                                    <div class="no-item">\
                                        <p>Không có sản phẩm nào.</p>\
                                    </div>\
                                </ul>';
                } else {
                    html += '<ul id="cart-sidebar" class="mini-products-list count_li">\
                                    <ul class="list-item-cart">';
                    $(res.data).each(function(index, element) {
                        html += '<li class="item productid-'+element.rowid+'">\
                                    <div class="border_list">\
                                        <div class="image_drop">\
                                            <a class="product-image" href="'+element.url+'" title="'+element.name+'">\
                                                <img alt="'+element.name+'" src="'+element.thumb+'" width="100">\
                                            </a>\
                                        </div>\
                                        <div class="detail-item">\
                                            <div class="product-details"> <a href="javascript:;"  data-id="'+element.rowid+'" title="Xóa" class="remove-item-cart fa fa-times" id="remove-item-cart"></a>\
                                                <p class="product-name"> <a href="'+element.url+'" title="'+element.name+'">'+element.name+'</a></p>\
                                            </div>\
                                            <div class="product-details-bottom"><span class="price">'+element.price+'₫</span>\
                                                <div class="quantity-select qty_drop_cart">\
                                                    <input class="variantID" type="hidden" name="variantId" value="48943329">\
                                                        <span>Số lượng: </span>\
                                                        <input type="text" maxlength="3" min="1" readonly="" class="input-text number-sidebar qty_'+element.rowid+'" name="Lines" size="4" value="'+element.qty+'">\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </li>';
                    });
                    html += '</ul>\
                                <div class="pd">\
                                    <div class="top-subtotal">Tổng cộng: <span class="price price_big">'+res.total+'₫</span></div>\
                                </div>\
                                <div class="pd right_ct"><a href="/cart" class="btn btn-white"><span>Giỏ hàng</span></a><a href="/checkout" class="btn btn-primary"><span>Thanh toán</span></a></div>\
                            </ul>';
                }
                $(".top-cart-content").html(html);
                // if(res.status == 'success') {
                //     var number = 0;
                //     var result = res.data;
                //     Object.keys(result).forEach(function(key) {
                //         number += result[key].qty;
                //     });
                //     $(".cart-popup-count").html(number);
                //     $(".total_price_cart").html(res.total);
                //
                //     $("#popupCartModal").modal('show');
                // }
            }
        });
    }
    window.onload = function()
    {
        $("#remove-item-cart").click(function (){
            var rowid = $("#remove-item-cart").attr('data-id');
            $.ajax({
                type: 'get',
                url: '/cart/destroy',
                data: {
                    rowid:rowid
                },
                success: function (res) {
                    get_cart();
                }
            });
        });
        $(".btn-change-qty").click(function (){
            var rowid = $(".btn-change-qty").attr('pro_rowid');
            var qty = $(".qty_"+rowid+"").val();
            console.log(rowid);
            $.ajax({
                type: 'post',
                url: '/cart/update_qty',
                data: {
                    rowid:rowid,
                    qty:qty
                },
                success: function (res) {
                    get_cart();
                }
            });
        });
    };
</script>
<style>
    .fb-livechat,
    .fb-widget {
        display: block
    }

    .ctrlq.fb-button,
    .ctrlq.fb-close {
        position: fixed;
        right: 24px;
        cursor: pointer
    }

    .ctrlq.fb-button {
        z-index: 999;
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+) center no-repeat #0084ff;
        width: 60px;
        height: 60px;
        text-align: center;
        bottom: 24px;
        border: 0;
        outline: 0;
        border-radius: 60px;
        -webkit-border-radius: 60px;
        -moz-border-radius: 60px;
        -ms-border-radius: 60px;
        -o-border-radius: 60px;
        box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16);
        -webkit-transition: box-shadow .2s ease;
        background-size: 80%;
        transition: all .2s ease-in-out
    }

    .ctrlq.fb-button:focus,
    .ctrlq.fb-button:hover {
        transform: scale(1.1);
        box-shadow: 0 2px 8px rgba(0, 0, 0, .09), 0 4px 40px rgba(0, 0, 0, .24)
    }

    .fb-widget {
        background: #fff;
        z-index: 2;
        position: fixed;
        width: 360px;
        height: 435px;
        overflow: hidden;
        opacity: 0;
        bottom: 0;
        right: 24px;
        border-radius: 6px;
        -o-border-radius: 6px;
        -webkit-border-radius: 6px;
        box-shadow: 0 5px 40px rgba(0, 0, 0, .16);
        -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16);
        -moz-box-shadow: 0 5px 40px rgba(0, 0, 0, .16);
        -o-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)
    }

    .fb-credit {
        text-align: center;
        margin-top: 8px
    }

    .fb-credit a {
        transition: none;
        color: #bec2c9;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 12px;
        text-decoration: none;
        border: 0;
        font-weight: 400
    }

    .ctrlq.fb-overlay {
        z-index: 0;
        position: fixed;
        height: 100vh;
        width: 100vw;
        -webkit-transition: opacity .4s, visibility .4s;
        transition: opacity .4s, visibility .4s;
        top: 0;
        left: 0;
        background: rgba(0, 0, 0, .05);
        display: none
    }

    .ctrlq.fb-close {
        z-index: 4;
        padding: 0 6px;
        background: #365899;
        font-weight: 700;
        font-size: 11px;
        color: #fff;
        margin: 8px;
        border-radius: 3px
    }

    .ctrlq.fb-close::after {
        content: 'x';
        font-family: sans-serif
    }
</style>
</body>

</html>