<!DOCTYPE html>
<html class="overflow-y">
<head>
<html class="no-js" lang="vi">
<!--<![endif]-->

<head>
	<link rel="shortcut icon" href="//theme.hstatic.net/1000197303/1000641267/14/favicon.png?v=695" type="image/png" />
		<meta charset="utf-8" />
		<meta name="description" content="<?=cms_config('site_desc');?>" />
		<meta name="keywords" content="<?=cms_config('site_keywords');?>" />
		<meta name="author" content="ELIPHA">
		<title><?=cms_config('site_title');?></title>
		<link rel="canonical" href="https://marc.com.vn/" />
		<meta name="robots" content="index,follow" />
		<meta name="revisit-after" content="1 day" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="HandheldFriendly" content="true">
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=0' name='viewport' />
<!-- ---------------------Js------------ -->
<?=load_js('jquery.min.1.11.0.js', 'themes/marc'); ?>
<?=load_js('eco_tracking_all_order.js', 'themes/marc'); ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<!-- --------------------------Css--------------- -->
<?=load_css('style.css', 'themes/marc'); ?>
<?=load_css('css.css', 'themes/marc');?>
<?=load_css('swiper.min.css', 'themes/marc');?>
<?=load_css('video-js.css', 'themes/marc');?>
<?=load_css('styless.css', 'themes/marc');?>
<?=load_css('doke.css', 'themes/marc');?>
<?=load_css('v5-css-all.css', 'themes/marc');?>


<!-- ------- -->
<script>
	var base_url = "<?=URL?>";
</script>

<!-- ----------- -->
<meta name="google-site-verification" content="MTZHm3xcUmYOFUAXo4cuF0lYmNRAvndw1glC9wanNhM" /><meta name="google-site-verification" content="MTZHm3xcUmYOFUAXo4cuF0lYmNRAvndw1glC9wanNhM" />

<style>.grecaptcha-badge{visibility:hidden;}</style>
<script>
  gtag('event', 'conversion', {'send_to': 'AW-765857136/VYPgCN_1_ZMBEPCamO0C'});
</script>

<script>
var formatMoney = '{{amount}}₫';
var shop_template = "index";
jQuery.themeAssets = {
	arrowDown: '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 12 8" style="enable-background:new 0 0 12 8; width: 12px; height: 8px;" xml:space="preserve"><polyline points="0,2 2,0 6,4 10,0 12,2 6,8 0,2 "/></svg>',
	arrowRight: '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31 10" style="enable-background:new 0 0 31 10; width: 31px; height: 10px;" xml:space="preserve"><polygon points="31,5 25,0 25,4 0,4 0,6 25,6 25,10 "/></svg>',
};
jQuery.themeCartSettings = 'overlay';
</script>
<meta property="og:type" content="website" />
<meta property="og:title" content="MARC Fashion - BRIGHTEN YOUR DAY" />
<meta property="og:image" content="http://theme.hstatic.net/1000197303/1000641267/14/share_fb_home.png?v=695" />
<meta property="og:image:secure_url" content="https://theme.hstatic.net/1000197303/1000641267/14/share_fb_home.png?v=695" />
<meta property="og:description" content="【 MARC 】là thương hiệu thời trang cao cấp đẹp quyến rũ sang trọng & nổi tiếng ở Việt Nam. hiều ưu đãi lớn. Khuyến mại sale giá sốc. Mua ngay. Sản phẩm đa dạng. Thanh Toán Linh Hoạt. Cam kết chất lượng. Giao hàng toàn quốc." /><meta property="og:url" content="https://marc.com.vn/" /><meta property="og:site_name" content="MARC" />

		<script>if (typeof(_sokShops) === "undefined") { _sokShops = {};}_sokShops.getNumber = function(val) { val = val.replace(/,/g, ""); var reg = /\d+(\.\d{1,2})?/; if (val.length && val.match(reg).length) { return parseFloat(val.match(reg)[0]); } else { return 0; }};  _sokShops.cart = {  product_imageurl: [ "https://product.hstatic.net/1000197303/product/pro_da_3_3f775bbced404fd397ad003f6f04f5ff_master.jpg", "https://product.hstatic.net/1000197303/product/pro_den_1_4635d9de850e4453af68451478783b8b_master.jpg", ], sku_id: [ "1066573587", "1066429963", ],  product_names: [ "", "", ], sku_url: [ "/products/ao-kieu-nhan-co-smocking-tay-con", "/products/dam-2-day-no-nguc-hoa-tiet-dap-noi", ],product_prices: [ _sokShops.getNumber("425,000₫"), _sokShops.getNumber("959,000₫"), ], product_quantities: [ 2, 2, ], cart_amount: _sokShops.getNumber("2,768,000₫"), currency: "VND" };</script>

	

	</head>

<body id="lama-theme" class="index">

<div class="main-body">
	
<div id="topbar">
<div class="container-fluid">
<div class="topbar-content text-center">
	<span class="font-bold font-uppercase text-main cursor-default">BỘ SƯU TẬP MỚI NHẤT</span>
	<span class="hidden font-uppercase topbar-learmore">Learn More <i class="icon icon-arrow-white-right">&nbsp;</i></span>
	<span class="hidden-xs1 font-medium underlined center-text z-index-5 cursor-pointer font-uppercase mb-inline" onclick="window.location.href='/the-party1'">MUA NGAY</span>
</div>
</div>
</div>
<div class="wrap-logo-ipad">
<a href="/">
	<img src="//theme.hstatic.net/1000197303/1000641267/14/logo.png?v=695" alt="MARC" class="img-responsive logoimg"/>
</a>
</div>
	<header id="site-header" class="main-header container">
	<div class="menu-desktop hidden-sm hidden-xs">
				<div class="wrap-logo wrap-logp-mb">
					<a href="/"> <img src="//theme.hstatic.net/1000197303/1000641267/14/logo.png?v=682" alt="MARC" class="img-responsive logoimg logo_img" /> <img src="//theme.hstatic.net/1000197303/1000641267/14/logo_fixed.png?v=682" alt="MARC" class="img-responsive logo_img_fixed" /> </a>
				</div>
				<?php echo widget('Cms/Main_menu'); ?>
				<style>
				#nav .main-nav .sub_menu a {
					font-weight: bold;
					font-size: 12px;
				}
				
				#nav > nav > ul > li:nth-child(5) {
					position: relative;
				}
				
				#nav > nav > ul > li:nth-child(5):hover > .sub_menu {
					width: 238px;
					display: block;
				}
				
				#nav > nav > ul > li:nth-child(5):hover > .sub_menu li {
					width: 100%;
					display: block;
				}
				</style>
			</div>
			<div class="visible-sm visible-xs absolute" style="left: 10px"> <span id="site-menu-handle" class="hamburger-menu" aria-hidden="true"><span class="bar"></span></span>
			</div>
			<div class="header-mid wrap-flex-align">
				<div class="visible-lg visible-md col-header-mid"></div>
				<div class="wrap-logo">
					<a href="/"> <img src="//theme.hstatic.net/1000197303/1000641267/14/logo.png?v=682" alt="MARC" class="img-responsive logoimg" /> </a>
				</div>
			</div>
			<div class="header-wrap-icon"> <span class="header-account header-login hidden-xs" title="">
		</span> <span class="hr hidden-xs"></span> <span class="icon-search">
			<a href="/search">
				<span class="search-menu" aria-hidden="true">
					<svg viewBox="0 0 23.38 23.38" xmlns="http://www.w3.org/2000/svg">
						<g fill="#231f20"><path d="m9.78 19.57a9.78 9.78 0 1 1 9.78-9.78 9.79 9.79 0 0 1 -9.78 9.78zm0-17.57a7.78 7.78 0 1 0 7.78 7.78 7.79 7.79 0 0 0 -7.78-7.78z"/>
							<path d="m19.51 17.44h2v6.13h-2z" transform="matrix(.70710678 -.70710678 .70710678 .70710678 -8.49 20.51)"/>
						</g>
					</svg>
				</span> 
			</a>
				</span> 
				<span id="site-cart-handle" class="icon-cart" aria-label="Open cart" title="Giỏ hàng">
					<a href="/cart">
						<span class="cart-menu" aria-hidden="true">
							<svg viewBox="0 0 21.32 23.04" xmlns="http://www.w3.org/2000/svg">
								<g fill="#231f20"><path d="m10.67 13.92c-3.43 0-5.56-2.63-5.56-6.86h2c0 1.81.46 4.86 3.56 4.86s3.54-3 3.54-4.86h2c0 4.23-2.12 6.86-5.54 6.86z"/>
									<path d="m19.85 3.45h-1.67v-3.45h-15v3.45h-1.71l-1.47 19.55h21.32zm-14.71-1.45h11v1.45h-11zm-3 19 1.19-15.55h14.67l1.17 15.55z"/>
								</g>
							</svg>
						</span>
					</a>
				</span>
			</div>
			<div class="header-search-form">
				<div class="search-form-container">
					<form class="mui-form flex-form" action="/search" method="GET">
						<input type="hidden" name="type" value="product"> <i class="icon icon-magnifier">&nbsp;</i>
						<div class="mui-textfield borderless full-with">
							<input name="q" class="mui--is-untouched mui--is-pristine mui--is-empty" placeholder="Bạn muốn tìm kiếm sản phẩm nào hôm nay?..." id="header-search-form-input" autofocus=""> </div> <i class="icon icon-close">&nbsp;</i> </form>
				</div>
			</div>
	</header>
		<div class="container">
			<div class="usp-container usp-container-desktop swiper-container">
				<ul class="swiper-wrapper">
					<li class="relative mobile-slide-item swiper-slide"> <i class="icon icon-timer">&nbsp;</i> <span class="">
					 Miễn phí đổi hàng 
				</span>
						<div class="usp-tooltip">
							<div class="usp-tooltip-content"> Mua sắm trực tuyến thả ga không ngại rủi ro với 7 ngày đổi hàng hoàn toàn miễn phí ( không áp dụng với các sản phẩm sale) </div>
						</div>
					</li>
					<li class="relative mobile-slide-item swiper-slide"> <i class="icon icon-truck">&nbsp;</i> <span class="">
					Ship COD toàn quốc
				</span>
						<div class="usp-tooltip">
							<div class="usp-tooltip-content">
								<p class="">Miễn phí vận chuyển cho đơn hàng trên 300.000 VND. </p>
							</div>
						</div>
					</li>
					<li class="swiper-slide "> <span class="uppercase corner-usp">BRIGHTEN YOUR DAY - CHO NGÀY THÊM RỰC RỠ</span> </li>
				</ul>
			</div>
			
		</div>
		<!-- Main -->
		<div class="total-wrap" id="total-wrap">
			<?=widget('Cms/Ads', ['zone_id'=>1]);?>
			<?=$this->renderSection('content')?>
		</div>
		<!-- End Main  -->
		<footer class="footer">
	<div class="top-footer">
		<div class="usp-container">
			<ul class="">
				<li class=""><i class="icon icon-truck-inverse">&nbsp;</i>
					<p class="">GIAO HÀNG MIỄN PHÍ</p><span class="">đơn hàng từ 300.000 vnđ</span>
				</li>
				<li class=""><img src="//file.hstatic.net/1000197303/file/phone-icon.png" width=33 height=33/>
					<p class="">chăm sóc khách hàng và gọi mua hàng </p><span class="">1900636940 (9h00 - 21h00, T2 - CN)</span>
				</li>
				<li class=""><img src="//file.hstatic.net/1000197303/file/aa_41d2ad59dfdb40ada5b2ed01671a223d.png" width=33 height=33/>
					<p class="">góp ý và khiếu nại</p><span class="">1900636942 (9h00 - 21h00, T2 - CN)</span>
				</li>
				<li class=""><i class="icon icon-tag-inverse">&nbsp;</i>
					<p class="">THANH TOÁN</p><span class="">Khi nhận hàng</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="main-footer container" id="fter-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg">
					<div class="footer-col footer-content1">
						<h4 class="footer-title">MARC</h4>
						<div class="footer-content">
							<p><?=cms_config('footer_introduce')?></br></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg">
					<div class="footer-col footer-block">
						<h4 class="footer-title">
							THÔNG TIN
						</h4>
						<div class="footer-content toggle-footer">
							<?php echo widget('Cms/Footer_menu'); ?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg">
					<div class="footer-col footer-block">
						<h4 class="footer-title">
							CHÍNH SÁCH
						</h4>
						<div class="footer-content toggle-footer">
							<?php echo widget('Cms/Footer2_menu'); ?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg">
					<div class="footer-col">
						<h4 class="footer-title">
							KẾT NỐI VỚI MARC
						</h4>
						<div class="footer-content footer-app">
							<span class="footer-note"></span>
								<div class="footer-last-row">
									<ul class="list-row list-social">
										<li><a href="<?=cms_config('social_facebook')?>" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
										<li><a href="<?=cms_config('social_instagram')?>" target="_blank" rel="nofollow"><i class="fa fa-instagram"></i></a></li>
										<li><a href="<?=cms_config('social_youtube')?>" target="_blank" rel="nofollow"><i class="fa fa-youtube"></i></a></li>																					
									</ul>
								</div>
						</div>
					</div>	
					<a href="http://online.gov.vn/Home/WebDetails/66846" target="_blank" ><img src="//theme.hstatic.net/1000197303/1000641267/14/logo_bct_img.png?v=695" class="logo-bct" alt="Da thong bao bo Cong Thuong"></a>													
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-footer text-center">
		<div class="container">
			<div class="row invert-order">
			</div>
		</div>
	</div>
</footer>
	<div class="back-to-top hidden">
		<a href="javascript:void(0);">
			<div class="btt-back">
				<span class="btt-label-back">back to top</span>
				<span class="btt-icon-back"><i class="fa fa-long-arrow-up"></i></span>
			</div>
		</a>
	</div>
		</div>   
		<div id="site-nav--mobile" class="site-nav style--sidebar">
	<div id="site-navigation" class="site-nav-container">
		<div class="site-nav-container-last">
	<p class="title">Menu</p>
	<div class="main-navbar">
		<nav class="primary-menu">
			<ul class="menu-collection">
				<?php echo widget('Cms/Mobile_menu_main'); ?>
			</ul>
			<ul class="menu-about">
				<?php echo widget('Cms/Mobile_menu_bottom'); ?>
			</ul>
		</nav>
	</div>
</div>
	</div>
	<div id="site-cart" class="site-nav-container" tabindex="-1">
		<div class="site-nav-container-last">
	<p class="title">Giỏ hàng</p>
	<div class="cart-view clearfix">
		<table id="clone-item-cart" class="table-clone-cart">
			<tr class="item_2 hidden">
				<td class="img"><a href="" title=""><img src="" alt="" /></a></td>
				<td>
					<a class="pro-title-view" href="" title=""></a>
					<span class="variant"></span>	
					<span class="pro-quantity-view"></span>
					<span class="pro-price-view"></span>
					<span class="remove_link remove-cart">					
					</span>				
				</td>
			</tr>   
		</table>
		<table id="cart-view">
		<?php 
		$items = $cart->contents();
		$totalPrice = 0;
		foreach($items as $key=>$item){
			$totalPrice = $totalPrice + $item['subtotal'];
		 ?>
			<tr>
				<td class="img">
					<a href="/products/ao-kieu-nhan-co-smocking-tay-con">
						<img src="<?=$item['options']['Image']?>" alt="Áo kiểu nhấn cổ smocking tay con" />
					</a>
				</td>
				<td>
					<a class="pro-title-view" href="/products/ao-kieu-nhan-co-smocking-tay-con" title="Áo kiểu nhấn cổ smocking tay con"><?=$item['name']?></a>
					<span class="pro-quantity-view"><?=$item['qty']?></span>
					<br>
					<span class="pro-price-view"><?=number_format($item['subtotal'])?>₫</span>
				</td>
			</tr>
			
		<?php } ?>
		</table>
		<span class="line"></span>
		<table class="table-total">
			<tr>
				<td class="text-left">TỔNG TIỀN:</td>
				<td class="text-right" id="total-view-cart"><?=number_format($totalPrice)?>₫</td>
			</tr>
			<tr>
				<td colspan="2"><a href="/cart" class="linktocart button dark">Xem giỏ hàng</a></td>
			</tr>
		</table>
	</div>
</div>
	</div>
	<div id="site-search" class="site-nav-container" tabindex="-1">
		<div class="site-nav-container-last">
			<p class="title">Tìm kiếm</p> 
			<div class="search-box wpo-wrapper-search">
				<form action="/search" class="searchform searchform-categoris ultimate-search navbar-form">
					<div class="wpo-search-inner">
						<input type="hidden" name="type" value="product" />
						<input required id="inputSearchAuto" name="q" maxlength="40" autocomplete="off" class="searchinput input-search search-input" type="text" size="20" placeholder="Tìm kiếm sản phẩm...">
					</div>
					<button type="submit" class="btn-search btn" id="search-header-btn">
						<svg version="1.1" class="svg search" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 27" style="enable-background:new 0 0 24 27;" xml:space="preserve"><path d="M10,2C4.5,2,0,6.5,0,12s4.5,10,10,10s10-4.5,10-10S15.5,2,10,2z M10,19c-3.9,0-7-3.1-7-7s3.1-7,7-7s7,3.1,7,7S13.9,19,10,19z"></path><rect x="17" y="17" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -9.2844 19.5856)" width="4" height="8"></rect></svg>
					</button>
				</form>
				<div id="ajaxSearchResults" class="smart-search-wrapper ajaxSearchResults" style="display: none">
					<div class="resultsContent"></div>
				</div>
			</div>
		</div>
	</div>
	<button id="site-close-handle" class="site-close-handle" aria-label="Đóng" title="Đóng">
		<span class="hamburger-menu active" aria-hidden="true"><span class="bar animate"></span></span>
	</button>
</div>
		<div id="site-overlay" class="site-overlay"></div>
		<?=load_js('plugins.js', 'themes/marc'); ?>
		<?=load_js('swiper.min.js', 'themes/marc'); ?>
		<?=load_js('scripts.js', 'themes/marc'); ?>
		<?=load_js('jsfooter.js', 'themes/marc'); ?>
<div id="login_overlay"></div>
<?=load_css('footer.css', 'themes/marc'); ?>

		<?=load_js('app-combo.js', 'themes/marc'); ?>
		<!-- Modal -->
		<div id="loginModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<a href="/" itemprop="url">
							<img itemprop="logo" src="//theme.hstatic.net/1000197303/1000641267/14/logo.png?v=695" alt="MARC" class="img-responsive logoimg">
						</a>
						<div class="title-login pull-right">
							Đăng ký
						</div>
					</div>
					<div class="modal-body">
						<a href="/account/register#login-phone">Số điện thoại</a>
						<a href="/account/register">Email</a>
					</div>
				</div>
			</div>
		</div>
</body>
</html>