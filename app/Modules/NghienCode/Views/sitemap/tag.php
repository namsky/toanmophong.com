<?php
$url = URL;
$list = '';
if(is_array($tags)) foreach($tags as $tag) {
	$tag_url = tag_url($tag);
	$percent = '0.5';
	$list .= '
		<url>
			<loc>'.$tag_url.'</loc>
			<changefreq>Daily</changefreq>
			<priority>'.$percent.'</priority>
			<lastmod>'.date('c', time()).'</lastmod>
		</url>
	';
}
$sitemap = <<<Sitemap
<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="{$url}/sitemap.xsl"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
{$list}
</urlset>
Sitemap;
echo $sitemap;
?>