<?=$this->section('content');?>
	<!-- CONTENT -->
	<main class="">
            <div class="container">
                <div class="content-wrapper offline-container">
                        <picture class="responsive-image">
                            <source srcset="//theme.hstatic.net/1000197303/1000641267/14/store-banner.png?v=682" media="(min-width: 640px) and (max-width: 1139px)">
                            <source srcset="//theme.hstatic.net/1000197303/1000641267/14/store-banner.png?v=682" media="(min-width: 1140px)"> <img class="" srcset="//theme.hstatic.net/1000197303/1000641267/14/store-banner.png?v=682" alt="Pomelo Offline - Retail"></picture>
                        <div class="offline-banner-copy offline-banner-copy-vertical"><strong>VỚI HỆ THỐNG CÁC CỬA HÀNG</strong>
                            <p>Trải nghiệm tại cửa hàng mang tới sự thoải mái.
                                <br>Thử trước và thanh toán sau. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="content-wrapper offline-container">
                    <div class="store--listing">
                        <div class="offline-banner offline-banner-row"> 
                            <img class="" src="//file.hstatic.net/1000197303/article/dsc07339_e601986cca424401bd7f95d2b7284d21_large.jpg" alt="MARC CẦN THƠ">
                            <div class="offline-banner-copy offline-banner-copy-centered"> <strong>MARC CẦN THƠ</strong>
                                <div class="offline-banner-copy-section">
                                    <p>Địa chỉ: <span>77 Nguyễn Trãi, Phường An Hội, Quận Ninh Kiều, Tp. Cần Thơ&nbsp;</span></p>
                                    <p>Điện thoại:&nbsp;0825.898.786</p>
                                </div>
                                <div class="offline-banner-cta">
                                <a href="/marc-can-tho"> <button class="btn btn-transparent-dark" ><span>Xem chi tiết</span> </button></a>
                                </div>
                            </div>
                        </div>
                        <div class="offline-banner offline-banner-row"> 
                            <img class="" src="//file.hstatic.net/1000197303/article/untitled-1_9d56b93c572a4a9a9903fabe37aec33d_7d5c6d02af0940a4ab6dca81d2a78454_large.jpg" alt="MARC CẦN THƠ">
                            <div class="offline-banner-copy offline-banner-copy-centered"> <strong>MARC SC VIVOCITY</strong>
                                <div class="offline-banner-copy-section">
                                    <p>Địa chỉ: <span>1058 Nguyễn Văn Linh, Tân Phong, Quận 7, Hồ Chí Minh&nbsp;</span></p>
                                    <p>Điện thoại:&nbsp;028.35351365</p>
                                </div>
                                <div class="offline-banner-cta">
                                <a href="/marc-sc-vivocity"> <button class="btn btn-transparent-dark" > <span>Xem chi tiết</span> </button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<?=$this->endSection();?>