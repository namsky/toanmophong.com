<?=$this->section('content');?>
	<!-- CONTENT -->
    <div class="breadcrumb_background margin-bottom-40">
        <div class="title_full">
            <div class="container a-center">
                <p class="title_page"><?=$item->title?></p>
            </div>
        </div>
        <section class="bread-crumb"> <span class="crumb-border"></span>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 a-center">
                        <ul class="breadcrumb">
                            <li class="home"> <a href="/"><span >Trang chủ</span></a> <span class="mr_lr">&nbsp;<i class="fa">/</i>&nbsp;</span> </li>
                            <li><strong><span><?=$item->title?></span></strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="page section_base">
        <div class="container">
            <div class="wrap_background_aside padding-top-15 margin-bottom-40">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="page-title category-title">
                            <h1 class="title-head"><a href="#"><?=$item->title?></a></h1>
                        </div>
                        <div class="content-page rte">
                            <?=$item->content ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?=$this->endSection();?>