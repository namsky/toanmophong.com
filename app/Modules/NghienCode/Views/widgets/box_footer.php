			<?php
				if(isset($items) && is_array($items) && count($items)) {
			?>
				<div class="footer-col-title-wrap">
					<h4 class="footer-col-title">Tin tức</h4>
					<div class="footer-news-wrap">
						<?php
						foreach($items as $item) {
						?>
						<div class="footer-news-item">
							<div class="footer-news-item-wrap">
								<span class="news-date" title="<?=show_date($item->published)?>"><?=date('d.m', $item->published)?></span>
								<div class="news-text-wrap">
									<p class="news-text">
										<?=(isset($item->summary) && $item->summary != '')?$item->summary:cutOf(strip_tags($item->content), 300);?>
									</p>
								</div>
								<div class="btn-line-wrap">
									<a href="<?=post_url($item)?>" class="btn-line">
										<span class="line-wrap">
											<span class="line"></span>
										</span>
										<span class="btn-line-text">Xem thêm</span>
									</a>
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="news-btn-wrap">
							<a href="<?=URL?>/tin-tuc" class="link-btn">Xem tất cả</a>
						</div>
					</div>
				</div>
			<?php } ?>