			<?php
				if(isset($items) && is_array($items) && count($items)) {
			?>
				<div class="wine-grid-item background anim-block">
					
				</div>
				<div class="wine-grid-item title anim-block">
					<div class="recipes-content anim-elem top-50">
						<div class="title-wrap">
							<h5 class="recipes-title split-block split-text-wrap">Tin tức</h5>
						</div>
						<div class="recipes-btn-wrap">
							<a href="<?=URL?>/tin-tuc" class="link-btn anim-elem delay-03">
								Xem thêm
							</a>
						</div>
					</div>
				</div>
				<?php foreach($items as $item) { ?>
				<div class="wine-grid-item recipes anim-block">
					<div class="recipes-inner-wrap anim-elem top-50 delay-03">
						<div class="bg-image" style="background-image: url('<?=$item->thumb;?>');"></div>
						<div class="recipes-container">
							<div class="recipes-container-wrap anim-elem">
								<div class="title-wrap">
									<h5 class="recipes-title split-block split-text-wrap">
										<?=$item->title;?>
									</h5>
								</div>
							</div>
							<a href="<?=post_url($item)?>" class="recipes-detail-link"></a>
						</div>
					</div>
				</div>
				<?php } ?>
			<?php } ?>