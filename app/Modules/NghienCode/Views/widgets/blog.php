<?php
if(isset($items) && is_array($items) && count($items)) {
    ?>

    <?php foreach($items as $item) { ?>
        <div class="blog_index">
            <div class="myblog" onclick="window.location.href='/<?=post_url($item)?>';">
                <div class="image-blog-left a-center">
                    <a href="<?=post_url($item)?>" title="<?=$item->title?>"> <img class="lazyload img-responsive" src="data:image/png;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" data-src="<?=$item->thumb?>" alt="<?=$item->title?>" /> </a>
                    <div class="date_blog"> <i class="far fa-calendar"></i><b class="color_main"><?=show_date($item->published)?></b> &nbsp; Đăng bởi: <b class="color_main">Toàn Mô Phỏng</b> </div>
                </div>
                <div class="content_blog">
                    <div class="content_right">
                        <h3>
                            <a href="<?=post_url($item)?>" title="<?=$item->title?>"><?=$item->title?></a>
                        </h3> </div>
                    <div class="summary_item_blog">
                        <p> <?=$item->summary?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>