<?php
if(isset($items) && is_array($items) && count($items)) {
?>

    <?php foreach($items as $item) { ?>
<div class="item saler_item">
    <div class="product-col">
        <div class="product-loop-1 product-loop-2 product-base product-box">
            <div class="product-thumbnail">
                <a class="image_link display_flex" href="<?=product_url($item)?>" title="<?=$item->name?>"> <img class="lazyload" src="<?=$item->thumb?>" data-src="<?=$item->thumb?>" alt="<?=$item->name?>"> </a>
                <div class="product-action clearfix hidden-xs">
                    <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-8633786" enctype="multipart/form-data">
                        <div class="group_action">
                            <input class="hidden" type="hidden" name="variantId" value="16431458" />
                            <button class="btn btn-cart btn btn-circle left-to" title="Xem chi tiết" type="button" onclick="window.location.href='/san-pham/<?=$item->slug?>'"> Xem chi tiết </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="product-info a-center">
                <div class="action_image">
                    <div class="owl_image_thumb_item hidden-md hidden-sm hidden-xs">
                        <div class="product_image_list owl-carousel not-owl">
                        </div>
                    </div>
                </div>
                <h3 class="product-name margin-top-5"><a href="<?=product_url($item)?>" title="<?=$item->name?>"><?=$item->name?></a></h3>
                <div class="product-hideoff">
                    <div class="product-hide">
                        <div class="price-box clearfix">
                            <div class="special-price"> <span class="price product-price"><?=number_format($item->price)?>₫</span> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php } ?>
<?php } ?>