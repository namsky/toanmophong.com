			<?php
				if(isset($items) && is_array($items) && count($items)) {
			?>
			<div class="related-post">
				<?php
					if(!empty($title)) {
						if(!empty($title_heading)) {
							$title_html = '<'.$title_heading.' class="related-title">'.$title.'</'.$title_heading.'>';
						} else $title_html = '<div class="related-title">'.$title.'</div>';
						echo $title_html;
					}
				?>
				<ul class="art-list">
				<?php foreach($items as $item) { ?>
					<li class="art-text">
						<a href="<?=post_url($item)?>" rel="bookmark">
							<?php
								if(!empty($post_heading)) {
									$post_title = '<'.$post_heading.' class="title-text"><i class="fa fa-angle-right"></i>'.$item->title.'</'.$post_heading.'>';
								} else $post_title = '<div class="title-text"><i class="fa fa-angle-right"></i>'.$item->title.'</div>';
								echo $post_title;
							?>
						</a>
					</li>
				<?php } ?>
				</ul>
			</div>
			<?php } ?>