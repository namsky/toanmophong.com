<?php
	if(isset($items) && is_array($items) && count($items)) {
?>
<div class="menada-products-grid">
	<div class="menada-products-grid-inner">
		<?php foreach($items as $item) { ?>
			<div class="menada-products-item anim-elem">
				<input type="hidden" class="product-item-title" value="<?=$item->name?>" />
				<input type="hidden" class="product-item-content" value="<?=cutOf(strip_tags($item->content), 175)?>" />
				<div class="menada-products-item-inner">
					<div class="menada-products-image">
						<img src="<?=$item->thumb?>" alt="bottle" class="template-image" />
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<?php } ?>