			<?php
				if(isset($items) && is_array($items) && count($items)) {
			?>
				<div class="box box-blog">
				<?php
				foreach($items as $item) {
					if(!empty($post_heading)) {
						$post_title = '<'.$post_heading.' class="title-text">'.$item->title.'</'.$post_heading.'>';
					} else $post_title = '<div class="title-text">'.$item->title.'</div>';
				?>
					<div class="art-grid">
						<div class="thumb">
							<a href="<?=post_url($item)?>" rel="bookmark">
								<?=show_img($item->thumb, 305, 220, $item->title);?>
							</a>
						</div>
						<div class="art-text">
							<?php
								$categories = $item->categories;
								if(is_array($categories) && count($categories)) {
									$category = reset($categories);
									if(!empty($category_heading)) {
										$category_title = '<'.$category_heading.' class="category">'.$category->name.'</'.$category_heading.'>';
									} else $category_title = '<div class="category">'.$category->name.'</div>';
							?>
							<a href="<?=category_url($category)?>"><?=$category_title?></a>
							<?php } ?>
							<div class="title">
								<a href="<?=post_url($item)?>" rel="bookmark">
									<?=$post_title?>
								</a>
							</div>
							<div class="detail">
								<div class="author float-left">
									<a href="javascript:;" title="" rel="author"><?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?></a>
								</div>
								<div class="date float-left ml-4"><i class="far fa-clock"></i> <?=show_date($item->published)?></div>
								<div class="clearfix"></div>
								<div class="description">
									<?=$item->summary?>
								</div>
								<div class="tags">
									<?php
									if(!empty($item->tags)) {
										foreach($item->tags as $tag) {
											echo '<a href="'.tag_url($tag).'">'.$tag->name.'</a>';
										}
									}
									?>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
				</div>
			<?php } ?>