<div class="home-products-section">
	<div class="products-section-wrap">
		<div class="main-wrap">
			<div class="products-title-line">
				<div class="products-title-wrap">
					<h3 class="products-title split-text-wrap" data-stagger-time=".07">
						<a href="<?=URL?>/san-pham"><span class="split-block">Rượu 9 Chum</span></a>
					</h3>
				</div>
				<div class="products-line-btn-wrap anim-block">
					<a href="<?=URL?>/san-pham" class="link-btn anim-elem delay-03">
						Xem tất cả
					</a>
				</div>
			</div>
		</div>
		<?php
			if(isset($items) && is_array($items) && count($items)) {
		?>
		<div class="main-wrap">
			<div class="products-section-grid">
				<div class="products-section-grid-wrap">
					<div class="products-grid-item anim-block">
						<div class="products-large-item anim-elem top-150">
							<?php foreach($items as $key=>$item) { 
								if($key == 0) {
							?>
							<div class="products-grid-item-wrap">
								<div class="bg-image-outer">
									<div class="bg-image-inner">
										<div class="bg-image parallax-elem" data-move="y" data-val="-150" style="background-image: url('<?=$item->background?>');"></div>
									</div>
								</div>
								<div class="large-bottle-wrap">
									<div class="large-bottle-inner-wrap anim-elem top-50 delay-05">
										<img src="<?=$item->thumb?>" alt="bottle" class="large-bottle-image" />
									</div>
								</div>
								<div class="products-grid-logo-container anim-block">
									<div class="products-logo-title-wrap anim-elem">
										<h4 class="products-logo-title">
											<?=$item->name?>
										</h4>
									</div>
								</div>
								<div class="products-grid-text-wrap anim-block">
									<p class="products-grid-text anim-elem delay-05">
										<?=cutOf(strip_tags($item->content), 175);?>
									</p>
									<div class="products-grid-btn-wrap anim-elem delay-05">
										<a href="<?=product_url($item)?>" class="link-btn white-bg">
											Xem thêm
										</a>
									</div>
								</div>
								<a href="<?=product_url($item)?>" class="product-link"></a>
							</div>
							<?php } } ?>
						</div>
						<div class="products-large-item-bottom">
							<div class="large-item-bottom-wrap">
								<div class="bg-image anim-elem" style="background-image: url('<?=CDN?>/themes/9chum/images/homepage/grid-item-bottom-bg.jpg');"></div>
								<div class="large-bottom-title-wrap">
									<h6 class="large-bottom-title split-text-wrap" data-view=".95">
										<a href="<?=URL?>/san-pham"><span class="split-block">Xem tất cả</span> <span class="large-text split-block">sản phẩm</span></a>
									</h6>
								</div>
							</div>
						</div>
					</div>
					<div class="products-grid-item">
						<?php foreach($items as $key=>$item) { 
							if($key == 1) $class = 'tcherga';
							else $class = 'menada';
							if($key > 0) {
						?>
						<div class="products-inner-item anim-block">
							<div class="products-inner-wrap anim-elem right-200">
								<div class="bg-image-outer anim-elem">
									<div class="bg-image-inner parallax-elem">
										<div class="bg-image" style="background-image: url('<?=$item->background?>');"></div>
									</div>
								</div>
								<div class="<?=$class?>-bottle-container">
									<div class="bottle-wrap anim-elem top-150 delay-03">
										<img src="<?=$item->thumb?>" alt="bottle" class="bottle-image" />
									</div>
								</div>
								<div class="inner-item-content">
									<div class="products-grid-logo-container">
										<div class="products-logo-title-wrap anim-elem">
											<h4 class="products-logo-title">
												<?=$item->name?>
											</h4>
										</div>
									</div>
									<div class="products-grid-text-wrap">
										<p class="products-grid-text anim-elem">
											<?=cutOf(strip_tags($item->content), 175);?>
										</p>
										<div class="products-grid-btn-wrap anim-elem">
											<a href="<?=product_url($item)?>" class="link-btn">
												Xem thêm
											</a>
										</div>
									</div>
								</div>
								<a href="<?=product_url($item)?>" class="product-link"></a>
							</div>
						</div>
						<?php } } ?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>