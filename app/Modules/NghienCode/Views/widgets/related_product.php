<?php
	if(isset($items) && is_array($items) && count($items)) {
?>
<div class="other-products">
	<div class="other-products-wrap">
		<div class="other-title-wrap">
			<h3 class="other-title split-block split-text-wrap" data-view=".9">Sản phẩm khác</h3>
		</div>
		<div class="other-products-grid">
			<div class="other-products-grid-wrap">
				<?php foreach($items as $item) { ?>
				<div class="product-item red anim-block" data-view="1">
					<div class="product-item-wrap anim-elem bottom-50 delay-03" style="background:url('<?=$item->background?>') center no-repeat;background-size:cover">
						<div class="product-item-inner-wrap">
							<div class="bottle-container">
								<div class="bottle-inner-container">
									<div class="bottle-image-wrap">
										<img src="<?=$item->thumb?>" alt="<?=$item->name?>" title="<?=$item->name?>" class="bottle-image" />
									</div>
								</div>
							</div>
							<div class="title-wrap anim-elem top delay-1">
								<h2 class="wine-title">
									<?=$item->name?>
								</h2>
							</div>
							<p class="size-label">
								<span class="size-label-wrap anim-elem delay-05"><?=$item->capacity?><span class="size-unit">ml</span></span>
							</p>
							<div class="size-label-container anim-elem delay-05">
								<p class="type-label">Nồng độ: <?=$item->alcohol?></p>
							</div>
						</div>
					</div>
					<a href="<?=product_url($item)?>" class="wine-detail-link"></a>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>