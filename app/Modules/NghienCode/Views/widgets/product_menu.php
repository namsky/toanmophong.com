<div class="menu-middle-col">
	<div class="title-wrap">
		<h2 class="item-title">
			<a href="<?=URL?>/products" class="middle-col-title-link">
				<span class="item-overlay"></span>Sản phẩm <span class="line-wrap"><span class="line"></span></span>Rượu 9 Chum
			</a>
		</h2>
	</div>
	<?php
		if(isset($products) && is_array($products) && count($products)) {
	?>
	<div class="row first">
		<?php
			foreach($products as $key => $product) {
				$class = '';
				if($key%2) $class = 'row-item-right';
				else $class = 'row-item-left';
				if($key > 0 && $key < 3) {
		?>
		<div class="row-item <?=$class?> tcherga">
			<div class="row-item-content">
				<div class="name-label">
					<p><?=$product->name?></p>
				</div>
				<div class="bottle-wrap">
					<img src="<?=$product->menu_image?>" alt=" bottle" class="bottle-image" />
				</div>
			</div>
			<span class="bottom-line"></span>
			<a href="<?=product_url($product)?>" class="menu-item-link"></a>
			<div class="item-overlay"></div>
		</div>
		<?php } } ?>
	</div>
	<div class="row second">
		<?php
			foreach($products as $key => $product) {
				$class = '';
				if($key%2) $class = 'row-item-right';
				else $class = 'row-item-left';
				if($key > 2) {
		?>
		<div class="row-item <?=$class?> tcherga">
			<div class="row-item-content">
				<div class="name-label">
					<p><?=$product->name?></p>
				</div>
				<div class="bottle-wrap">
					<img src="<?=$product->menu_image?>" alt=" bottle" class="bottle-image" />
				</div>
			</div>
			<span class="bottom-line"></span>
			<a href="<?=product_url($product)?>" class="menu-item-link"></a>
			<div class="item-overlay"></div>
		</div>
		<?php } } ?>
	</div>
	<?php } ?>
	<?php
		if(isset($products) && is_array($products) && count($products)) {
	?>
	<div class="products">
		<div class="bg-image" style="background-image: url('<?=$products[0]->menu_image?>');"></div>
		<div class="products-content">
			<div class="products-title-wrap">
				<h2 class="products-title"><span><?=$products[0]->name?></span></h2>
			</div>
			<div class="products-btn-wrap">
				<a href="<?=URL?>/san-pham" class="link-btn">
					Tất cả sản phẩm
				</a>
			</div>
		</div>
		<a href="<?=product_url($products[0])?>" class="menu-item-link"></a>
		<div class="item-overlay"></div>
	</div>
	<?php } ?>
</div>