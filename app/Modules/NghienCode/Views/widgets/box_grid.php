	<?php
		if(isset($items) && is_array($items) && count($items) > 1) {
	?>
	<div class="box box-grid">
		<div class="row">
			<?php if(isset($items[0])) { ?>
			<div class="col-sm-12 col-md-6 col-lg-8">
				<div class="item feature">
					<div class="thumb">
						<a href="<?=post_url($items[0])?>" rel="bookmark">
							<?=show_img($items[0]->thumb, 864, 480, $items[0]->title);?>
						</a>
					</div>
					<div class="art-text">
						<?php
							$categories = $items[0]->categories;
							if(is_array($categories) && count($categories)) {
								$category = reset($categories);
						?>
						<div class="category">
							<a href="<?=category_url($category)?>">
								<h2 class="title-text"><?=$category->name?></h2>
							</a>
						</div>
						<?php } ?>
						<div class="title">
							<a href="<?=post_url($items[0])?>" rel="bookmark">
								<h1 class="title-text"><?=$items[0]->title?></h1>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if(isset($items[1])) { ?>
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div class="item feature">
					<div class="thumb">
						<a href="<?=post_url($items[1])?>" rel="bookmark">
							<?=show_img($items[1]->thumb, 864, 480, $items[1]->title);?>
						</a>
					</div>
					<div class="art-text">
						<?php
							$categories = $items[1]->categories;
							if(is_array($categories) && count($categories)) {
								$category = reset($categories);
								if(!empty($category_heading)) {
									$category_title = '<'.$category_heading.' class="category">'.$category->name.'</'.$category_heading.'>';
								} else $category_title = '<div class="category">'.$category->name.'</div>';
						?>
						<a href="<?=category_url($category)?>"><?=$category_title?></a>
						<?php } ?>
						<div class="title">
							<a href="<?=post_url($items[1])?>" rel="bookmark">
								<?php
									if(!empty($post_heading)) {
										$post_title = '<'.$post_heading.' class="title-text">'.$items[1]->title.'</'.$post_heading.'>';
									} else $post_title = '<div class="title-text">'.$items[1]->title.'</div>';
									echo $post_title;
								?>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="row">
			<?php if(count($items)> 2) { ?>
			<?php for($i=2; $i<count($items); $i++) { ?>
			<div class="col-sm-12 col-md-6 col-lg-3">
				<div class="item">
					<div class="thumb">
						<a href="<?=post_url($items[$i])?>" rel="bookmark">
							<?=show_img($items[$i]->thumb, 305, 220, $items[$i]->title);?>
						</a>
					</div>
					<div class="art-text">
						<div class="title">
							<a href="<?=post_url($items[$i])?>" rel="bookmark">
								<?php
									if(!empty($post_heading)) {
										$post_title = '<'.$post_heading.' class="title-text">'.$items[$i]->title.'</'.$post_heading.'>';
									} else $post_title = '<div class="title-text">'.$items[$i]->title.'</div>';
									echo $post_title;
								?>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php } ?>
		</div>
	</div>
	<?php } ?>