<?=$this->section('content');?>
<h1 class="text-center">Thanh toán</h1>
    <style>
        .form-group .form-control {
            border: 1px solid #ccc;
        }
    </style>
<div class="container">
    <div class="row">
        <div class="col-sm-6" style="border-right: 1px solid black">
        <h3>Thông tin giao hàng</h3>
            <form action="/checkout/save" method="POST">
                <div class="form-group">
                    <label for="name">Họ và tên</label>
                    <input type="text" class="form-control" placeholder="Họ và tên" name="name" id="name" required>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" placeholder="Email" name="email" id="email" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="phone">Số điện thoại:</label>
                            <input type="text" class="form-control" placeholder="Số điện thoại" name="phone" id="phone" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                    <div class="form-group">
                        <label for="city">Tỉnh / thành:</label>
                        <input type="text" class="form-control" placeholder="Tỉnh / Thành phố" name="city" id="city" required>
                    </div>
                    </div>
                    <div class="col-sm-6">
                    <div class="form-group">
                        <label for="district">Quận / huyện:</label>
                        <input type="text" class="form-control" placeholder="Quận / huyện" name="district" id="district" required>
                    </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ward">Phường / Xã:</label>
                    <input type="text" class="form-control" placeholder="Phường / Xã" name="ward" id="ward" required>
                </div>
<!--                <div class="form-group">-->
<!--                    <label for="address">Địa chỉ cụ thể:</label>-->
<!--                    <input type="text" class="form-control" placeholder="Địa chỉ cụ thể" name="address" id="address" required>-->
<!--                </div>-->
                <div class="form-group">
                    <label for="bankcode_atm">Chọn ngân hàng thanh toán:</label>
                    <select class="form-control" id="bankcode_atm" name="bankcode_atm">
                        <option value="BIDV">BIDV</option>
                        <option value="VCB">Vietcombank</option>
                        <option value="DAB">DongABank</option>
                        <option value="TCB">Techcombank</option>
                        <option value="MB">MB Bank</option>
                        <option value="VIB">VIB</option>
                        <option value="ICB">Vietinbank</option>
                        <option value="EXB">Eximbank</option>
                        <option value="ACB">ACB</option>
                        <option value="HDB">HDBank</option>
                        <option value="MSB">Maritime Bank</option>
                        <option value="VAB">VietABank</option>
                        <option value="VPB">VPBank</option>
                        <option value="SCB">SCB</option>
                        <option value="PGB">PGBank</option>
                        <option value="GPB">GPBank</option>
                        <option value="AGB">Agribank</option>
                        <option value="SGB">SaiGonBank</option>
                        <option value="BAB">BacABank</option>
                        <option value="TPB">TPBank</option>
                        <option value="NAB">NAB</option>
                        <option value="SHB">SHB</option>
                        <option value="OJB">OceanBank</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="note">Ghi chú:</label>
                    <textarea class="form-control" rows="5" name="note" id="note"></textarea>
                </div>
                <input type="hidden" name="status" value="0">
                <button type="submit" class="btn btn-primary">Thanh toán</button>
            </form>
        </div>
        <div class="col-sm-6">
            <div class="order-summary-section order-summary-section-product-list" data-order-summary-section="line-items">
                <table class="product-table">
                    <thead>
                        <tr >
                            <th class="text-center">STT</th>
                            <th class="text-center" scope="col"><span class="visually-hidden">Hình ảnh</span></th>
                            <th class="text-center" scope="col"><span class="visually-hidden">Mô tả</span></th>
                            <th class="text-center" scope="col"><span class="visually-hidden">Số lượng</span></th>
                            <th class="text-center" scope="col"><span class="visually-hidden"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $totalPrice = 0;
                    $stt =1;
                    foreach($items as $item){
                    ?>
                        <tr class="product" >
                            <td class="text-center"><?=$stt++?></td>
                            <td class="product-image text-center">
                                <div class="product-thumbnail">
                                    <div class="product-thumbnail-wrapper"> 
                                        <img style="width: 100px" class="product-thumbnail-image" src="<?=$item['thumb']?>" />
                                    </div>
                                </div>
                            </td>
                            <td class="product-description text-center"> 
                                <span class="product-description-name order-summary-emphasis"><?=$item['name']?></span>
                            </td>
                            <td class="product-quantity visually-hidden text-center"><?=$item['qty']?></td>
                            <td style="border:none" class="product-price text-center"> <span class="order-summary-emphasis"><?=($item['subtotal'])?>₫</span> </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Tổng cộng:</h3>
                    </div>
                    <div class="col-sm-6">
                        <h3 class="text-right"><?=($total_money)?>₫</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=$this->endSection();?>