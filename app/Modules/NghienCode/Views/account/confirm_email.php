<?=$this->section('content');?>
	<!-- CONTENT -->
	<div class="container">
		<div class="box_account">
            <div class="content confirm_email">
                <?php
                if($verify) {
                ?>
                <p class="icon success">
                    <i class="fas fa-check"></i>
                </p>
                <p class="message">Xác minh email thành công</p>
                <?php
                } else {
                ?>
                <p class="icon">
                    <i class="fas fa-times"></i>
                </p>
                <p class="message">Mã xác nhận không hợp lệ</p>
                <?php
                }
                ?>
                <p class="note">
                    Bạn đang được chuyển đến <a href="<?=URL?>"><b>trang chủ</b></a>.
                    <script>
                        setTimeout(function(){
                            location.href = '<?=URL?>';
                        }, 5000);
                    </script>
                </p>
            </div>
		</div>
	</div>
<?=$this->endSection();?>