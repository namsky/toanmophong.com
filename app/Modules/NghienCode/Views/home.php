<?=$this->section('content');?>
    <h1 class="hidden"><?=cms_config('description');?></h1>
    <section class="awe-section-1">
        <section class="sectionslider section_slider">
            <div class="section home-slider owl-carousel owl-slider-home not-owl">
                <div class="items">
                    <?php foreach($bannerTop as $item){ ?>
                        <a href="<?=$item->slider_link?>" class="clearfix">
                            <picture>
                                <source media="(max-width: 767px)" srcset="<?=$item->slider_image?>"> <img src="<?=$item->slider_image?>" alt="<?=$item->slider_name?>" />
                            </picture>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </section>
    </section>
    <section class="awe-section-5">
        <div class="sec_col_1 section_base button-middle button-hover">
            <div class="container">
                <div class="row">
                    <div class="content_sec clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            <div class="home_banner banner_img">
                                <div class="image bg_img_module headingcol lazyload" data-src="/writable/uploads/6a2b1ca25306ef7be9969e2faede0a0a.jpg" title="CLASSIC">
                                    <h2 class="title-base" title="CLASSIC">
                                        <a href="/product_list/chantroisangtao" class="headline"><img src="https://toanmophong.com/writable/uploads/8e5476503293356911507e381ed58b40.png" alt="Tổng hợp những hình ảnh mô phỏng của SGK"></a>
                                    </h2>
                                    <span style="font-size: 16px; color: black">Tổng hợp những hình ảnh </br> mô phỏng của SGK</span>
                                    <a href="/product_list/chantroisangtao" class="btn btn-primary btn-shop-now" title="Xem thêm">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                            <div class="prd_sec border_wrap">
                                <div class="products owl-carousel owl-theme products-view-grid" data-nav="true" data-lg-items="3" data-md-items="3" data-sm-items="2" data-xs-items="2" data-margin="10">
                                    <?php echo widget('NghienCode/Product_classic'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="awe-section-5">
        <div class="sec_col_1 section_base button-middle button-hover">
            <div class="container">
                <div class="row">
                    <div class="content_sec clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            <div class="home_banner banner_img">
                                <div class="image bg_img_module headingcol lazyload" data-src="/writable/uploads/6a2b1ca25306ef7be9969e2faede0a0a.jpg" title="CLASSIC">
                                    <h2 class="title-base" title="CLASSIC">
                                        <a href="/product_list/canhdieu" class="headline"><img src="https://toanmophong.com/writable/uploads/26ba870db02b505adb3673ed8e0055fa.png" alt="Tổng hợp những hình ảnh mô phỏng của SGK"></a>
                                    </h2>
                                    <span style="font-size: 16px; color: black">Tổng hợp những hình ảnh </br> mô phỏng của SGK</span>
                                    <a href="/product_list/canhdieu" class="btn btn-primary btn-shop-now" title="Xem thêm">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                            <div class="prd_sec border_wrap">
                                <div class="products owl-carousel owl-theme products-view-grid" data-nav="true" data-lg-items="3" data-md-items="3" data-sm-items="2" data-xs-items="2" data-margin="10">
                                    <?php echo widget('NghienCode/Product_new_arrivals'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="awe-section-5">
        <div class="sec_col_1 section_base button-middle button-hover">
            <div class="container">
                <div class="row">
                    <div class="content_sec clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            <div class="home_banner banner_img">
                                <div class="image bg_img_module headingcol lazyload" data-src="/writable/uploads/6a2b1ca25306ef7be9969e2faede0a0a.jpg" title="CLASSIC">
                                    <h2 class="title-base" title="CLASSIC">
                                        <a href="/product_list/ketnoitrithucvoicuocsong" class="headline"><img src="https://toanmophong.com/writable/uploads/cec739b5566b9c729cda722868ed826e.png" alt="Tổng hợp những hình ảnh mô phỏng của SGK"></a>
                                    </h2>
                                    <span style="font-size: 16px; color: black">Tổng hợp những hình ảnh </br> mô phỏng của SGK</span>
                                    <a href="/product_list/ketnoitrithucvoicuocsong" class="btn btn-primary btn-shop-now" title="Xem thêm">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                            <div class="prd_sec border_wrap">
                                <div class="products owl-carousel owl-theme products-view-grid" data-nav="true" data-lg-items="3" data-md-items="3" data-sm-items="2" data-xs-items="2" data-margin="10">
                                    <?php echo widget('NghienCode/Product_vault'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="awe-section-9">
        <div class="section_new_blog">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="heading">
                            <h2 class="title-head" title="Tin tức">
                                <a href="tin-tuc-vans" class="text_gradient">Tin tức
                                </a>
                            </h2> </div>
                        <div class="content_blog_new">
                            <div class="wrap_owl_blog owl-carousel" data-dot="false" data-nav="false" data-lg-items="3" data-md-items="3" data-sm-items="2" data-xs-items="1" data-loop="false" data-height="false" data-dot="false" data-nav="false">
                                <?php echo widget('NghienCode/Blog'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?=$this->endSection();?>