<?=$this->section('content');?>
	<!-- CONTENT -->
	<main class="">
    <div class="breadcrumb_background margin-bottom-40">
        <div class="title_full">
            <div class="container a-center">
                <p class="title_page"><?=cms_config('site_name')?> | <?=$item->name?></p>
            </div>
        </div>
        <section class="bread-crumb"> <span class="crumb-border"></span>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 a-center">
                        <ul class="breadcrumb">
                            <li class="home"> <a href="/"><span >Trang chủ </span></a> <span class="mr_lr">&nbsp;<i class="fa">/</i>&nbsp;</span> </li>
                            <li><strong><span><?=$item->name?></span></strong>
                            <li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="product details-main  f-left w_100" itemscope itemtype="https://schema.org/Product">
<!--        <meta itemprop="url" content="//vansvietnam.com.vn/vans-old-skool-classic-black-white-vn000d3hy28">-->
<!--        <meta itemprop="name" content="VANS OLD SKOOL CLASSIC BLACK/WHITE">-->
<!--        <meta itemprop="image" content="//bizweb.dktcdn.net/thumb/grande/100/140/774/products/vans-old-skool-black-white-vn000d3hy28-1.jpg?v=1641825551587">-->
        <form enctype="multipart/form-data" id="add-to-cart-form"  class="form_background form-inline margin-bottom-0">
            <input class="hidden" type="hidden" name="id" value="<?=$item->id?>" />
            <input class="hidden" type="hidden" name="name" value="<?=$item->name?>" />
            <input class="hidden" type="hidden" name="price" value="<?=$item->price?>" />
            <input class="hidden bk-product-image" type="hidden" name="thumb" value="<?=$item->thumb?>" />
            <input class="hidden bk-product-qty" type="hidden" name="qty" value="1" />
            <input class="hidden" type="hidden" name="url" value="<?=product_url($item)?>" />
            <div class="container">
                <div class="row ">
                    <div class="section wrap-padding-15 wp_product_main">
                        <div class="details-product section section_base button-middle">
                            <div class="product-detail-left product-images col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="col_large_default large-image">
                                    <?=$item->video?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 details-pro">
                                <h1 class="title-product bk-product-name"><?=$item->name?></h1>
                                <div class="fw w_100" itemprop="offers" itemscope itemtype="https://schema.org/Offer">
                                    <!-- <div class="group-status margin-bottom-15"> <span class="first_status">Thương hiệu: <span class="status_name" style="color: green;"><?=$item->brand?></span></span>
                                        <span class="line_tt" style="margin: 0 5px;">/</span> <span class="first_status sku-product">Mã sản phẩm: <span class="variant-sku"  style="color: green;" itemprop="sku" content="<?=$item->sku?>"><?=$item->sku?></span> </span>
                                    </div> -->
                                    <div class="border_product border_hotline margin-top-20 margin-bottom-20"></div>
                                    <div class="reviews_details_product">
                                        <div class="price-box">
                                            <span class="special-price">
                                                <span class="price product-price bk-product-price" ><?=number_format($item->price)?>₫</span>
                                                <meta itemprop="price" content="<?=$item->price?>">
                                                <meta itemprop="priceCurrency" content="VND">
                                            </span>
                                            <span class="old-price" itemprop="priceSpecification" itemscope itemtype="https://schema.org/priceSpecification">
												<?php if($item->origin_price) { ?>
                                                    <del class="price product-price-old"><?=number_format($item->origin_price)?>₫</del>
                                                <?php } ?>
												<meta itemprop="price" content="<?=$item->price?>">
												<meta itemprop="priceCurrency" content="VND">
											</span>
                                            <!-- Giá gốc -->
                                        </div>
                                    </div>
                                    <div class="form-product col-sm-12 col-lg-12 col-md-12 col-xs-12">
                                        <script>
                                            $(window).load(function() {
                                                $('.selector-wrapper:eq(0)').hide();
                                            });
                                        </script>
                                        <div class=" swatch clearfix" data-option-index="0">
                                            <script>
                                                $(window).load(function() {
                                                    $(".swatch-element.available input").first().prop('checked', true);
                                                    $(".swatch-element.soldout input").click(function (){
                                                        $(".text_1").html('Hết hàng');
                                                        $(".text_1").addClass('bk-check-out-of-stock');
                                                        $(".custom-btn-number").hide();
                                                        $(".btn_add_cart").prop('disabled',true);
                                                    });
                                                    $(".swatch-element.available input").click(function (){
                                                        $(".text_1").html('Mua hàng');
                                                        $(".custom-btn-number").show();
                                                        $(".btn_add_cart").prop('disabled',false);
                                                    });
                                                });
                                            </script>
                                        </div>
                                        <div class="form-group form_button_details  margin-top-30 ">
                                            <div class="form_product_content type1 ">
                                                <div class="soluong soluong_type_1 show">
                                                    <div class="custom input_number_product custom-btn-number form-control" style="display: none">
                                                        <button class="btn_num num_1 button button_qty" onClick="var result = document.getElementById('qtym'); var qtypro = result.value; if( !isNaN( qtypro ) &amp;&amp; qtypro &gt; 1 ) result.value--;return false;" type="button"><i class="fas fa-minus"></i></button>
                                                        <input type="text" id="qtym" name="quantity" value="1" maxlength="3" class="form-control prd_quantity" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" onchange="if(this.value == 0)this.value=1;">
                                                        <button class="btn_num num_2 button button_qty" onClick="var result = document.getElementById('qtym'); var qtypro = result.value; if( !isNaN( qtypro )) result.value++;return false;" type="button"><i class="fas fa-plus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="button_actions clearfix d-flex">
                                                    <button type="submit" class="btn btn_base btn_add_cart btn-cart add_to_cart">
                                                        <span class="text_1">Mua ngay</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="border_product border_hotline margin-top-30 margin-bottom-20"></div>
                                        <div class="hotline_product_wrap clearfix">
                                            <div class="hotline_product">Tư vấn: <a class="hai01" href="tel:<?=cms_config('phone')?>"><?=cms_config('phone')?></a> </div>
                                            <div class="payment_product"> <img src="//bizweb.dktcdn.net/100/140/774/themes/827866/assets/payment.png?1645722631863" alt="<?=cms_config('title')?>"> </div>
                                        </div>
                                        <div class="summary">
                                            <p>
                                                <span style="font-size:16px;">
                                                    <strong>
                                                        <a aria-controls="modal-size-chart-link" aria-haspopup="true" data-open="modal-size-chart-link" data-open-ajax="/en_us/utility/size-charts/vans_size_unisex_shoes.contentonly.html" href="https://vansvietnam.com.vn/size-giay-vans" target="_blank"></a>
												</strong>
												</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <!-- BK MODAL -->
    <div id='bk-modal'></div>
    <!-- END BK MODAL -->
    <script>
        $('#add-to-cart-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '/cart/save',
                data: $('#add-to-cart-form').serialize(),
                success: function (res) {
                    // console.log(typeof res);
                    // console.log(res.data);
                    if(res.status == 'success') {
                        var number = 0;
                        var result = res.data;
                        Object.keys(result).forEach(function(key) {
                            number += result[key].qty;
                        });
                        $(".cart-popup-count").html(number);
                        $(".total_price_cart").html(res.total);

                        $("#popupCartModal").modal('show');
                        get_cart();
                    }
                }
            });
        });
    </script>
        <div class="border_product"></div>
        <!-- TOP THƯƠNG HIỆU -->
        <div class="top_brand_product margin-top-50 section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-xs-12" style="display: flex; justify-content: center">
                        <a href="/san-pham" title="<?=cms_config('title')?>"> <img src="<?=cms_config('logo')?>" alt="Vans từ 1966"> </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- END TOP THƯƠNG HIỆU -->
        <div class="section sec_tab ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="wrap_tab_ed">
                            <div class="tab_h">
                                <div class="section bg_white">
                                    <!-- Nav tabs -->
                                    <div class="product-tab e-tabs not-dqtab">
                                        <ul class="tabs tabs-title clearfix">
                                            <li class="tab-link current" data-tab="tab-1">
                                                <h3><span>Thông tin sản phẩm</span></h3>
                                            </li>
                                        </ul>
                                        <div class="tab-float">
                                            <div id="tab-1" class="tab-content content_extab current">
                                                <div class="rte product_getcontent">
                                                    <?=$item->content?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="module_like_product margin-bottom-50">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class=" section_base button-middle">
                                    <div class="heading">
                                        <h2 class="title-heads">
                                            <a title="VANS BEST SELLER" href="/vans-classic">SẢN PHẨM BÁN CHẠY</a>
                                        </h2> </div>
                                    <div class="products-view-grid-bb owl-carousel owl-theme products-view-grid not-dot2" data-dot="false" data-nav="true" data-lg-items="3" data-md-items="4" data-sm-items="3" data-xs-items="2" data-margin="10">
                                        <?php echo widget('NghienCode/Product_best_sale'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <link href="https://productviewedhistory.sapoapps.vn//Content/styles/css/ProductRecentStyle.css" rel="stylesheet" />
        <div id="popupCartModal" class="modal fade in" role="dialog" style="padding-right: 17px;">
            <div class="popup_overlay"></div>
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close hidden-xs" data-dismiss="modal" data-backdrop="false" aria-label="Close" style="position: relative; z-index: 9;"><span aria-hidden="true"><i class="fas fa-times"></i></span></button>
                    <div class="row row-noGutter">
                        <div class="modal-left col-sm-6">
                            <h3 class="title"><i class="fas fa-check"></i> Bạn vừa thêm sản phẩm này vào giỏ hàng</h3>
                            <div class="modal-body">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="thumb-1x1"><img src="<?=$item->thumb?>" alt="<?=$item->name?>"></div>
                                    </div>
                                    <div class="media-body">
                                        <div class="product-title"><?=$item->name?></div>
                                        <div class="product-new-price"><span><?=number_format($item->price)?>đ</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-right col-sm-6">
                            <h3 class="title"><a href="/cart"><i class="fas fa-shopping-basket"></i> Giỏ hàng của bạn có <span><span class="cart-popup-count">6</span> sản phẩm</span></a></h3>
                            <div class="total_price">
                                <div class="total_price_h">Tổng tiền</div>
                                <div class="price"><span class="total_price_cart"></span>đ</div>
                            </div><a href="/cart" class="btn btn-primary btn-pop"><i class="fas fa-shopping-basket"></i>Tới giỏ hàng</a></div>
                    </div>
                </div>
            </div>
        </div>
		<?=$this->endSection();?>