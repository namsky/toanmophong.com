<?=$this->section('content');?>
	<!-- CONTENT -->
	<div id="collection" class="collection-page">
		<div class="main-content container">
			<div class="row">
				<div id="collection-body" class="wrap-collection-body clearfix">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="wrap-collection-title row">
							<div class="heading-collection row">
								<div class="col-md-12 col-sm-12 col-xs-12 ">
									<div class="collection_left_box text-center">
									<?php if (isset($key)) {?>
										<h1 class="title text-uppercase font-bold">
											Tìm kiếm
										</h1>
									<?php }else{?>
										<h1 class="title text-uppercase font-bold">
											Tất cả sản phẩm
										</h1>
									<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<div class="row filter-here">
						<?php if(isset($key)){ ?>
							<p class="subtext-result">Kết quả tìm kiếm cho: <strong><?=$key?></strong></p>
						<?php } ?>
							<div class="content-product-list product-list filter clearfix">
							<?php foreach($items as $item){ ?>
									<div class="col-md-3 col-sm-6 col-xs-6 pro-loop col-4">
										<div data-price="69500000 50" class="product-block  site-animation" data-anmation="1">
											<div class="product-img">
												<a href="/san-pham/<?=$item->slug?>" class="image-resize"> 
													<img class="img-loop" alt=" <?=$item->name?> " src="<?=$item->thumb?> "/> 
													<img class="img-loop img-hover" alt=" <?=$item->name?> " src="<?=$item->thumb_hover?>" /> 
												</a>
												<div class="button-add hidden">
													<button type="submit" title="Buy now" class="action" onclick="buy_now('1066908278')">Mua ngay<i class="fa fa-long-arrow-right"></i></button>
												</div>
											</div>
											<div class="product-detail clearfix">
												<div class="box-pro-detail">
													<h3 class="pro-name">
													<a href="/san-pham/<?=$item->slug?>">
														<?=$item->name?>
													</a>
												</h3>
													<div class="box-pro-prices">
														<p class="pro-price highlight"><?=number_format($item->price)?>₫</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
							<div class="sortpagibar pagi clearfix text-center">
								<div id="pagination" class="clearfix">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="pagination_wrap"> 
										<span><?=$paging ?></span> 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?=$this->endSection();?>