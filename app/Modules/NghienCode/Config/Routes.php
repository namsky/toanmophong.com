<?php
$routes->get('/', 'Home::index');
$routes->get('captcha.png', 'Captcha::index');
$routes->get('account', 'Account::index');
$routes->get('account/(:segment)', 'Account::$1');
$routes->get('profile/(:segment)', 'Account::profile');
// RSS
$routes->get('rss-channels', 'Rss::index');
$routes->get('rss/(:segment)', 'Rss::item');
$routes->get('rss-tag/(:segment)', 'Home::rss');
$routes->get('rss-category/(:segment)', 'Home::rss');
// Sitemap
$routes->get('sitemap.xml', 'Sitemap::index');
$routes->get('sitemap.xsl', 'Sitemap::style');
$routes->get('(:segment)/sitemap.xml', 'Sitemap::$1');
$routes->get('(:segment)/sitemap-(:num).xml', 'Sitemap::$1/$2');
// News
$routes->get('search', 'Home::search');

$routes->get('san-pham', 'Products::index');
$routes->get('san-pham/(:any)', 'Products::item');
$routes->get('hang-moi', 'Products::new_product');

$routes->get('gioi-thieu', 'Home::about');
$routes->get('cua-hang', 'Home::shop');

$routes->get('cart','Cart::index');
$routes->add('cart/(:any)','Cart::$1');

$routes->get('checkout','Checkout::index');
 $routes->get('checkout/(:any)','Checkout::$1');

$routes->get('lich-su', 'Home::history');
$routes->get('tag/(:segment)', 'Home::tag');
$routes->get('images/(:any)', 'Images::index');
$routes->get('(:any).css', 'Images::index');
$routes->get('(:segment)', 'Home::router');
$routes->get('(:segment)/amp', 'Home::router');
// API for ADS Click
$routes->add('ads_click/(:segment)', 'Ads::click/$1');

$routes->add('product_list/(:any)', 'Home::$1');

// Payment
$routes->add('payment/(:any)', 'Payment::$1');