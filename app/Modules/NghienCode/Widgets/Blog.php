<?php
namespace App\Modules\NghienCode\Widgets;
use App\Core\Cms\CmsWidget;

class Blog extends CmsWidget
{
    function index($args=[]) {
        /* Set box title */
        /* Load from cache */
        $model = model('App\Modules\Cms\Models\PostModel');
        $cached = cms_config('cache');
        $items = $cached?$this->cache->get('blog'):false;
        if(!$items) {
            /* Load from db */
            $items = $model->orderBy('published', 'DESC')->where('type',1)->where('deleted',NULL)->where('published<', time())->findAll(20, 0);
            $this->cache->save('blog', $items, 86400);
        }
        $this->view->setVar('items', $items);
        return $this->view->render('blog');
    }
}