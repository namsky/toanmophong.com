<?php
namespace App\Modules\NghienCode\Widgets;
use App\Core\Cms\CmsWidget;

class Product_menu extends CmsWidget
{
    function index() {
		/* Set box title */
		/* Load from cache */
		$cached = cms_config('cache');
		$products = false;
		$hot = false;
		if(!$products && !$hot) {
			/* Load from db */
			$model = model('App\Modules\Cms\Models\EcommerceProductModel');
			$products = $model->orderBy('id', 'ASC')->findAll(5, 0);
			
		}
		$this->view->setVar('products', $products);
		$this->view->setVar('hot', $hot);
		return $this->view->render('product_menu');
    }
}