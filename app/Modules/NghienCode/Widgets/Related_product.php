<?php
namespace App\Modules\NghienCode\Widgets;
use App\Core\Cms\CmsWidget;

class Related_product extends CmsWidget
{
    function index($args=[]) {
		/* Set box title */
		/* Load from cache */
		$model = model('App\Modules\Cms\Models\EcommerceProductModel');
		$cached = cms_config('cache');
		$items = $cached?$this->cache->get('related_product'):false;
		if(!$items) {
			/* Load from db */
			if(isset($args['excludes'])) {
				if(is_numeric($args['excludes'])) $args['excludes'] = [$args['excludes']];
				if(is_array($args['excludes'])) {
					$model->whereNotIn('id', $args['excludes']);
				}
			}
			$items = $model->findAll(4, 0);
			$this->cache->save('related_product', $items, 86400);
		}
		$this->view->setVar('items', $items);
		return $this->view->render('related_product');
    }
}