<?php
namespace App\Modules\NghienCode\Widgets;
use App\Core\Cms\CmsWidget;

class Product_best_sale extends CmsWidget
{
    function index($args=[]) {
        /* Set box title */
        /* Load from cache */
        $model = model('App\Modules\Cms\Models\EcommerceProductModel');
        $cached = cms_config('cache');
        $items = $cached?$this->cache->get('product_best_sale'):false;
        if(!$items) {
            /* Load from db */
            $items = $model->orderBy('sales', 'DESC')->findAll(10, 0);
            $this->cache->save('product_best_sale', $items, 86400);
        }
        $this->view->setVar('items', $items);
        return $this->view->render('product_best_sale');
    }
}