<?php
namespace App\Modules\NghienCode\Widgets;
use App\Core\Cms\CmsWidget;

class Home_product_section extends CmsWidget
{
    function index($args=[]) {
		/* Set box title */
		/* Load from cache */
		$model = model('App\Modules\Cms\Models\EcommerceProductModel');
		$cached = cms_config('cache');
		$items = $cached?$this->cache->get('home_product_section'):false;
		if(!$items) {
			/* Load from db */
			$items = $model->where('hot', 1)->orderBy('id', 'ASC')->findAll(3, 0);
			$this->cache->save('home_product_section', $items, 86400);
		}
		$this->view->setVar('items', $items);
		return $this->view->render('home_product_section');
    }
}