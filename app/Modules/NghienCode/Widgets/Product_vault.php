<?php
namespace App\Modules\NghienCode\Widgets;
use App\Core\Cms\CmsWidget;

class Product_vault extends CmsWidget
{
    function index($args=[]) {
        /* Set box title */
        /* Load from cache */
        $model = model('App\Modules\Cms\Models\EcommerceProductModel');
        $cached = cms_config('cache');
        $items = $cached?$this->cache->get('product_vault'):false;
        if(!$items) {
            /* Load from db */
            $items = $model->orderBy('created', 'DESC')->categories([35])->findAll(20, 0);
            $this->cache->save('product_vault', $items, 86400);
        }
        $this->view->setVar('items', $items);
        return $this->view->render('product_vault');
    }
}