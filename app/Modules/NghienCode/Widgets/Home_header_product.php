<?php
namespace App\Modules\NghienCode\Widgets;
use App\Core\Cms\CmsWidget;

class Home_header_product extends CmsWidget
{
    function index($args=[]) {
		/* Set box title */
		/* Load from cache */
		$model = model('App\Modules\Cms\Models\EcommerceProductModel');
		$cached = cms_config('cache');
		$items = $cached?$this->cache->get('home_header_product'):false;
		if(!$items) {
			/* Load from db */
			$items = $model->orderBy('id', 'ASC')->findAll(5, 0);
			$this->cache->save('home_header_product', $items, 86400);
		}
		$this->view->setVar('items', $items);
		return $this->view->render('home_header_product');
    }
}