<?php
namespace App\Libraries;

use CodeIgniter\Log\Logger;

class CmsLogger extends Logger
{
	public function log($level, $message, array $context = []): bool
	{
		if (is_numeric($level))
		{
			$level = array_search((int) $level, $this->logLevels);
		}

		// Is the level a valid level?
		if (! array_key_exists($level, $this->logLevels))
		{
			throw LogException::forInvalidLogLevel($level);
		}

		// Does the app want to log this right now?
		if (! in_array($level, $this->loggableLevels))
		{
			return false;
		}

		// Parse our placeholders
		$message = $this->interpolate($message, $context);

		if (! is_string($message))
		{
			$message = print_r($message, true);
		}

		if ($this->cacheLogs)
		{
			$this->logCache[] = [
				'level' => $level,
				'msg'   => $message,
			];
		}

		foreach ($this->handlerConfig as $className => $config)
		{
			if (! array_key_exists($className, $this->handlers))
			{
				$this->handlers[$className] = new $className($config);
			}

			/**
			 * @var \CodeIgniter\Log\Handlers\HandlerInterface
			 */
			$handler = $this->handlers[$className];

			if (! $handler->canHandle($level))
			{
				continue;
			}

			// If the handler returns false, then we
			// don't execute any other handlers.
			if (! $handler->setDateFormat($this->dateFormat)->handle($level, $message))
			{
				break;
			}
		}

		return true;
	}
}
