<?php
namespace App\Core\Cms;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;
use Config\View;
use Helper\Cms;
use App\Core\Cms\CmsView;

class CmsWidget
{
	public $view;
	protected $cms;
	protected $cache;
    function __construct() {
		//parent::__construct();
		// Preload cms library
		$this->cms = new \App\Libraries\Cms();
		// Preload view
		$path = dirname(dirname(__DIR__));
		$class = get_class($this);
		if(strpos($class, '\\')) {
			$_class = explode('\\', $class);
			if(count($_class) > 3) {
				$module_name = $_class[count($_class)-3];
                $paths = [$path, 'Modules', $module_name, 'Views', 'widgets'];
				$this->view = new CmsView(config('View'), implode(DIRECTORY_SEPARATOR, $paths));
			}
		}
		// Load cache
		$this->cache = \Config\Services::cache();
    }
}