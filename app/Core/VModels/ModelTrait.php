<?php
namespace Core\VModels;
use Core\VModels\VPager;
use CodeIgniter\View\View;

trait ModelTrait
{
	/**
	 * Whether to reindex results by the primary key
	 *
	 * @var bool
	 */
	protected $reindex = true;
	protected $has_one = [];
	protected $has_many = [];
	private $_relationships = [];
	private $_temp_relationships = [];
    public function re_index()
    {
        $this->db->query('SET @newid=0');
        $this->db->query('UPDATE '.$this->table.' SET id=(@newid:=@newid+1) ORDER BY '.$this->primaryKey);
        $this->db->query('ALTER TABLE '.$this->table.' AUTO_INCREMENT = 1');
    }
	public function paginate(int $perPage = null, string $group = 'default', int $page = null, int $segment = 0)
	{
		$config = new \Config\Pager();
		$view = new View(null, null, null, false);
		$pager =  new VPager($config, $view);

		if ($segment)
		{
			$pager->setSegment($segment);
		}

		$page = $page >= 1 ? $page : $pager->getCurrentPage($group);

		$total = $this->countAllResults(false);

		// Store it in the Pager library so it can be
		// paginated in the views.
		$this->pager = $pager->store($group, $page, $perPage, $total, $segment);
		$perPage     = $this->pager->getPerPage($group);
		$offset      = ($page - 1) * $perPage;

		return $this->findAll($perPage, $offset);
	}
	/**
	 * Add related tables to load along with the next finder.
	 *
	 * @param mixed   $with       Table name, array of table names, or false (to disable)
	 * @param bool    $arguments  Whether to merge with existing table 'with' list
	 *
	 * @return $this
	 * @example $model->with('configurations', ['fields'=>'id,name,type', 'where'=>['type'=>'switch']])->findAll();
	 */
	public function with($with, $arguments = NULL)
	{
		if(empty($this->_temp_relationships)) $this->_set_relationships();
        if(array_key_exists($with, $this->_temp_relationships))
		{
			$this->_relationships[$with] = $this->_temp_relationships[$with];
			if($arguments) $this->_relationships[$with]['arguments'] = $arguments;
        }
		return $this;
	}
	
	
	//--------------------------------------------------------------------
	// FINDERS EXTENSIONS
	//--------------------------------------------------------------------

	/**
	 * Fetches the row of database from $this->table with a primary key
	 * matching $id.
	 *
	 * @param mixed|array|null $id One primary key or an array of primary keys
	 *
	 * @return array|object|null    The resulting row of data, or null.
	 */
	public function find($id = null)
	{
		// Get data from the framework model as usual
		$data = parent::find($id);
		// For singletons, wrap them as a one-item array and then unwrap on return
		if (is_numeric($id) || is_string($id))
		{
			$data = $this->addRelations([$data]);
			return reset($data);
		}
		return $this->addRelations($data);
	}

	//--------------------------------------------------------------------

	/**
	 * Works with the current Query Builder instance to return
	 * all results, while optionally limiting them.
	 *
	 * @param integer $limit
	 * @param integer $offset
	 *
	 * @return array|null
	 */
	public function findAll(int $limit = 0, int $offset = 0)
	{
		$data = parent::findAll($limit, $offset);
        return $this->addRelations($data);
	}

	//--------------------------------------------------------------------

	/**
	 * Returns the first row of the result set. Will take any previous
	 * Query Builder calls into account when determining the result set.
	 *
	 * @return array|object|null
	 */
	public function first()
	{
		$data = parent::first();
		// For singletons, wrap them as a one-item array and then unwrap on return
        $data = $this->addRelations([$data]);
		return reset($data);
	}

	/**
	 * Intercepts data from a finder and injects related items
	 *
	 * @param array  $rows  Array of rows from the finder
	 *
	 * @return array
	 */
	protected function addRelations($rows): ?array
	{
		if(!$this->_relationships) return $rows;
		// Inject related items back into the rows
		$return = [];
		foreach($rows as $item)
		{
			if(isset($item->{$this->primaryKey})) {
				$id = intval($item->{$this->primaryKey});
				if($id) {
					// Inject related items
					foreach ($this->_relationships as $key => $related)
					{
						$item->{$key} = null;
						$foreign_model = model($related['foreign_model_name']);
						if(isset($related['arguments']) && is_array($related['arguments'])) {
							$arguments = $related['arguments'];
							if(isset($arguments['fields']) && $arguments['fields']) {
								$fields = $arguments['fields'];
								$foreign_model->select($fields);
							}
							if(isset($arguments['where']) && is_array($arguments['where'])) {
								$where = $arguments['where'];
								foreach($where as $where_key=>$where_value) {
									$foreign_model->where($where_key, $where_value);
								}
							}
							if(isset($arguments['with'])) {
								$with = $arguments['with'];
                                $foreign_model->with($with);
							}
						}
						if($related['relation'] == 'has_one') {
							$foreign_data = $foreign_model->where($related['foreign_key'], $item->{$related['local_key']})->first();
						}
						else {
							$foreign_data = $foreign_model->where($related['foreign_key'], $item->{$related['local_key']})->findAll();
						}
						if(is_object($foreign_data) || is_array($foreign_data)) {
							$item->{$key} = $foreign_data;
						}
					}
					$return[] = $item;
				}
			}
		}
		// Clear old data and reset per-query properties
		$this->_relationships = NULL;
        unset($rows);
        $return = $this->trigger('afterFind', $return);
		return $return;
	}



    /**
     * private function _set_relationships()
     *
     * Called by the public method with() it will set the relationships between the current model and other models
     */
    private function _set_relationships()
    {
        if(empty($this->_temp_relationships))
        {
			$this->_temp_relationships = [];
            $options = array('has_one','has_many');
            foreach($options as $option)
            {
                if(isset($this->{$option}) && !empty($this->{$option}))
                {
                    foreach($this->{$option} as $key => $relation)
                    {
                        if(is_array($relation) && count($relation)==3)
                        {
							$foreign_model_name = $relation[0];
							$foreign_model = model($foreign_model_name);
							if(isset($foreign_model->table)) {
								$foreign_table = $foreign_model->table;
								$foreign_key = $relation[1];
								$local_key = $relation[2];
								$this->_temp_relationships[$key] = array('relation' => $option, 'relation_key' => $key, 'foreign_model_name'=>$foreign_model_name, 'foreign_key' => $foreign_key, 'local_key' => $local_key);
							}
                        }
                    }
                }
            }
        }

    }
    /**
     * Verifies if an array is associative or not
     * @param array $array
     * @return bool
     */
    protected function is_assoc(array $array) {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }
}