<?php
namespace Core\VModels;
use CodeIgniter\Pager\Pager;
use CodeIgniter\Pager\PagerRenderer;
class VPager extends Pager
{
	public function __construct($config, $view)
	{
        parent::__construct($config, $view);
	}
	/**
	 * Returns the total number of item.
	 *
	 * @param string|null $group
	 *
	 * @return integer
	 */
	public function getTotal(string $group = 'default'): int
	{
		$this->ensureGroup($group);
		return $this->groups[$group]['total'];
	}
	public function getAjaxLink(string $group = 'default')
	{
		$pager = new PagerRenderer($this->getDetails($group));
		$limit_item = 5;
		$pageCount = $this->groups[$group]['pageCount'];
		$currentPage = $this->groups[$group]['currentPage'];
		
		$html = '<ul class="pagination">';
		if($pageCount > $limit_item && $currentPage > 1) {
			$html .= '<li><a href="javascript:;" onclick="page(1)">«</a></li>';
			$html .= '<li><a href="javascript:;" onclick="page('.($currentPage-1).')">‹</a></li>';
		}
		$links = $pager->links();
		$pre_count = intval($limit_item / 2);
		$start = 0;
		$end = $pageCount-1;
		$page = $currentPage - 1;
		if($pageCount > $limit_item) {
			if($currentPage <= $pre_count) {
				$end = 2 * $pre_count;
			} elseif($currentPage > ($pageCount - $pre_count)) {
				$start = $pageCount - 2 * $pre_count - 1;
			} else {
				$start = $currentPage - $pre_count - 1;
				$end = $currentPage + $pre_count - 1;
			}
		}
		for($i=$start; $i <= $end; $i++) {
			if(isset($links[$i])) {
				$link = $links[$i];
				$active = $link['active']?'active':'';
				$page_link = !$link['active']?' onclick="page('.$link['title'].')"':'';
				$html .= '<li class="'.$active.'"><a href="javascript:;"'.$page_link.'>'.$link['title'].'</a></li>';
			}
		}
		if($pageCount > 5 && $currentPage < $pageCount) {
			$html .= '<li><a href="javascript:;" onclick="page('.($currentPage+1).')">›</a></li>';
			$html .= '<li><a href="javascript:;" onclick="page('.$pageCount.')">»</a></li>';
		}
		$html .= '</ul>';
		return $html;
	}
}
