<?php
namespace App\Core\Log;
use CodeIgniter\Log\Handlers\BaseHandler;
use CodeIgniter\Log\Handlers\HandlerInterface;
require_once APPPATH.'ThirdParty/Sentry/autoload.php';
use Sentry;

/**
 * Log error messages to file system
 */
class SentryHandler extends BaseHandler implements HandlerInterface
{
	protected $client;
	public function __construct(array $config = [])
	{
		parent::__construct($config);
		$dsn = $config['dsn'] ?? null;
		$release = $config['release'] ?? null;
		Sentry\init(['dsn' => $dsn, 'release' => $release]);
	}

	//--------------------------------------------------------------------

	/**
	 * Handles logging the message.
	 * If the handler returns false, then execution of handlers
	 * will stop. Any handlers that have not run, yet, will not
	 * be run.
	 *
	 * @param $level
	 * @param $message
	 *
	 * @return boolean
	 * @throws \Exception
	 */
	public function handle($level, $message): bool
	{
        return Sentry\captureMessage($message);
	}

	//--------------------------------------------------------------------
}
