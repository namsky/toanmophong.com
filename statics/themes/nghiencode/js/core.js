$(function() {
	$(window).scroll(function(event){
		var height = 600;
		var scroll = $(window).scrollTop();
		if (scroll > 100){
			$("header").addClass("mini-mode");
		} else {
			$("header").removeClass("mini-mode");
		}
		if (scroll > height){
			$(".back-to-top").addClass("show");
		} else {
			$(".back-to-top").removeClass("show");
		}
	});

	$(".back-to-top").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});
	$(".toc_toggle").click(function() {
		$(".table_of_content ul").toggle();
		if($(".table_of_content ul").css("display") == "none") {
			$(".toc_toggle").html('[ Hiện ]');
		} else 
			$(".toc_toggle").html('[ Ẩn ]');
	});
	/* User Login */
	$(".btn-login").click(function() {
		$(".form-signin").show();
		$(".form-signup").hide();
	});
	$(".btn-register").click(function() {
		$(".form-signin").hide();
		$(".form-signup").show();
	});
	$("#btn-signup").click(function() {
		$(".form-signin").hide();
		$(".form-signup").show();
	});
	$("#forgot_pswd").click(function() {
		$(".form-signin").hide();
		$(".form-reset").show();
	});
	$("#cancel_signup").click(function() {
		$(".form-signup").hide();
		$(".form-signin").show();
	});
	$("#cancel_reset").click(function() {
		$(".form-reset").hide();
		$(".form-signin").show();
	});
	$(".form-signin").submit(function(event) {
		var data = $(".form-signin").serialize();
		$.ajax({
			type : "POST",
			url: base_url+"/api/user/login",
			traditional: true,
			data: data,
			dataType: "jsonp",
			success: function(result, textStatus, jqXHR){
				if(result.status=="success") {
					swal({
						text: "Đăng nhập thành công",
						type: "success",
						showConfirmButton: false
					});
					setTimeout(function(){
						location.reload();
					}, 1000);
				} else {
					var message = 'Có lỗi xảy ra, vui lòng thử lại sau';
					if (typeof result.message !== 'undefined') {
						message = result.message;
					}
					swal({
						text: message,
						type: "warning",
					});
				}
			},
			error: function(xhr,status,error){
				console.log("error occurred.");
			}
		});
		return false;
	});
	$(".form-signup").submit(function(event) {
		var data = $(".form-signup").serialize();
		$.ajax({
			type : "POST",
			url: base_url+"/api/user/register",
			traditional: true,
			data: data,
			dataType: "jsonp",
			success: function(result, textStatus, jqXHR){
				if(result.status=="success") {
					swal({
						text: result.message,
						type: "success",
						showConfirmButton: false
					});
					setTimeout(function(){
						location.reload();
					}, 1000);
				} else {
					var message = 'Có lỗi xảy ra, vui lòng thử lại sau';
					if (typeof result.message !== 'undefined') {
						message = result.message;
					}
					swal({
						text: message,
						type: "warning",
					});
				}
			},
			error: function(xhr,status,error){
				console.log("error occurred.");
			}
		});
		return false;
	});
	$(".form-reset").submit(function(event) {
		var data = $(".form-reset").serialize();
		$.ajax({
			type : "POST",
			url: base_url+"/api/user/reset",
			traditional: true,
			data: data,
			dataType: "jsonp",
			success: function(result, textStatus, jqXHR){
				if(result.status=="success") {
					swal({
						text: "Đăng ký tài khoản thành công",
						type: "success",
						showConfirmButton: false
					});
					setTimeout(function(){
						location.reload();
					}, 1000);
				} else {
					var message = 'Có lỗi xảy ra, vui lòng thử lại sau';
					if (typeof result.message !== 'undefined') {
						message = result.message;
					}
					swal({
						text: message,
						type: "warning",
					});
				}
			},
			error: function(xhr,status,error){
				console.log("error occurred.");
			}
		});
		return false;
	});
	/* 
		GET account info
	*/
	get_account();
	function get_account() {
		$.ajax({
			type : "GET",
			url: base_url+"/api/user/info",
			traditional: true,
			dataType: "jsonp",
			success: function(result, textStatus, jqXHR) {
				if(result.status=="success" && typeof result.data !== 'undefined') {
					account = result.data;
					var name = account.username;
                    if(account.name) name = account.name;
                    if(account.avatar)
                        var avatar = '<span class="avatar"><img src="'+account.avatar+'" alt="Avatar"/></span>';
                    else var avatar = '<span class="avatar"><img src="'+base_url+'/statics/nghiencode/images/no-avatar.png" alt="Avatar"/></span>';
					html = avatar+name;
					$("#account_name").html(html);
					$("#authenticated").removeClass('d-none');
				} else {
					$("#unauthenticated").removeClass('d-none');
				}
			},
			error: function(xhr,status,error){
				console.log("error occurred.");
			}
		});
	}
	$(".user-logout").click(function() {
		$.ajax({
			type : "GET",
			url: base_url+"/api/user/logout",
			traditional: true,
			dataType: "jsonp",
			success: function(result, textStatus, jqXHR) {
				location.reload();
			},
			error: function(xhr,status,error){
				console.log(error);
			}
		});
	});
	/*
		Lazy load image
	*/
	if ($.isFunction($.Lazy)) {
		$('.lazy').Lazy({
			effect: "fadeIn",
		});
    }
	/*
		View more scripts
	*/
	var height = $(".viewmore-content").height();
	if(height > 224) {
		$(".viewmore-content").css("max-height", "224px");
		$(".viewmore-area").show();
		$(".viewmore-area").addClass("with-gradient");
	}
	$(".viewmore").click(function (){
		var status = $(".viewmore").data("status");
		if(status == false) {
			$(".viewmore").data("status", true);
			$(".viewmore-content").css("max-height", height+"px");
			$(".viewmore").html("THU GỌN");
			$(".viewmore-area").removeClass("with-gradient");
		} else {
			$(".viewmore").data("status", false);
			$(".viewmore-content").css("max-height", "224px");
			$(".viewmore").html("XEM THÊM");
			$(".viewmore-area").addClass("with-gradient");
		}
	});
});