$(document).ready(function(){

  // hàm đếm số lượng text field đang có trên màn hình
  function countItem(){
    var items = $("#answer").children().length;
    return items;
  }
  // thêm text field, giới hạn chỉ có tối đa 10 cái
  $("#add").click(function(){
    var n = countItem();
    if (n == 10) {
      alert("Number of answers isn't greater than 10");
    } else {
      n++;
      $("#answer").append("<div class='item row border p-2'><div class='col-sm-6'><div class='form-group'><label for='sku'>SKU</label><input type='hidden' id='p_id' name='p_id[]' value='p_id'><input type='text' class='form-control' id='sku' name='sku[]'></div><div class='form-group'><label for='color'>Color</label><input type='text' class='form-control' id='color' name='color[]'></div></div><div class='col-sm-6'><div class='form-group'><label for='size'>Size:</label><input type='text' class='form-control' id='size' name='size[]'></div><div class='form-group'><label for='stocks'>Stocks:</label><input type='text' class='form-control' id='stocks' name='stocks[]'></div></div><div class='form-group col-12'><label for='image'>Image:</label><textarea class='form-control' rows='5' id='image' name='image'></textarea></div><div class='col-sm-12 text-center'><a class='btn btn-success' href='javascript:void(0)' class='del'>Xóa</a></div></div>");
    }
  });
  // xoá text filed khi click vào nút del ở dòng tương ứng
  // với text file đang có dữ liệu thì không cho xoá
  $(document).on("click", "a.del", function(){
    var n = countItem();
    if (n == 2) {
      alert("Number of answers isn't less than 2");
    } else {
      var check = $(this).siblings().find("input").val();
      // cách viết khác
      // var check = $(this).parent().find("label input").val();
      if (check != "") {
        alert("Cannot delete answer field has content");
      } else {
        $(this).parent().remove();
        for (i=0; i<n-1; i++) {
          $("#answer .item:eq("+i+") div div div label").html("Ans "+(parseInt(i)+1));
        }
      }
    }
  });
    // gán kết quả vào vùng div có id là "result"
    $("#result").html(result);
}); 