
var checkRunFirst = false ;	
var countRun = 0;
window.product=function(){
    $(document).ready(function(){

        $('.info_des_btn').click(function(){
            $(this).parent().find('.description-content').toggleClass('hide')
        })

        $(document).on("click","#product-zoom-in",function(){
            //	var indexThumb = $(this).index();
            $("body").addClass("open_layer");
            $("#divzoom").css({'opcaity':0,'visibility':'hidden'}).show();
            $('.divzoom_main').flickity({
                resize:true,
                draggable: true,
            });
            if($(window).width() > 768){
                var ncurrent = parseInt($(".gallery-index .current").html()) - 1;
            }
            else{
                var ncurrent = parseInt($(".product-gallery-item.is-selected").index());
            }
            $('.divzoom_main').flickity('select',ncurrent);
            setTimeout(function(){$("#divzoom").css({'opcaity': 1,'visibility':'visible'})},50);
        });
        $(document).on('click','#closedivZoom', function(event) {
            $("#divzoom").hide();
            $("body").removeClass("open_layer");
            $('.divzoom_main').flickity('select',0);
            //$('.divzoom_main').slick('unslick');
        });
    })
    jQuery(document).ready(function($){
        $('#add-to-cart').click(function(e){
            e.preventDefault();
            if(comboApp.checkCombo){
                $('#combo-popup button').removeAttr('data-buynow');
                $('#combo-popup').modal('show');
            }else{
                $(this).addClass('clicked_buy');
                add_item_show_modalCart($('#product-select').val());
                getCartModal();
            }
            dataLayer.push({
                "event":"EEaddToCart",
                "ecommerce": {
                    "currencyCode":"VND",
                    "add": {
                        "products": [{
                            "id":"1030332798",
                            "name":"Áo kiểu sheer 3 tầng tay nhún bèo",
                            "price":"455,000",
                            "brand": "MARC",
                            "category":"NEW ITEM T1 - 2021",
                            "variant":$('#product-select option:selected').attr('data-title'),
                            "quantity": $('input#quantity').val()
                        }]
                    }
                }
            });
            setTimeout(function(){
                $('a.pro-title-view[data-id="'+$('#product-select').val()+'"]').attr('data-category', 'NEW ITEM T1 - 2021');
            }, 1500);;
        });
        $('#buy-now').click(function(e){	
            e.preventDefault();
            if(comboApp.checkCombo){
                $('#combo-popup button').attr('data-buynow','true');
                $('#combo-popup').modal('show');
            }
            else{
                var id = $('select#product-select').val();
                var quantity = $('input#quantity').val();
                var params = {
                    type: 'POST',
                    url: '/cart/add.js',
                    async : false,
                    data: 'quantity=' + quantity + '&id=' + id,
                    dataType: 'json',
                    success: function(line_item) {
                        window.location = '/checkout';
                    },
                    error: function(XMLHttpRequest, textStatus) {
                        Haravan.onError(XMLHttpRequest, textStatus);
                    }
                };
                jQuery.ajax(params);
            }
        });
        // Nút thêm tất cả vào giỏ
        $('body').on('click', '#discount-promotion-combo-add-btn', function(e){
            e.preventDefault();
            var that = $(this);
            var pid = $('#combo-program').data('id');
            var vid = $('#product-select').val();
            comboApp.comboAddCart(pid, vid, function(response){
                if(response){
                    if(that.attr('data-buynow') != undefined){
                        window.location = "/checkout";
                    }
                    else{
                        $('#combo-popup').modal('hide');
                        getCartModal();					
                        jQuery('#myCart').modal('show');		
                        jQuery('.modal-backdrop').css({'height':jQuery(document).height(),'z-index':'99'});		
                        $('.addtocart-modal').removeClass('clicked_buy');
                    }
                }
                else{
                    alert('fail');
                }
            });
        });
        // Nút chỉ thêm sản phẩm hiện tại
        $('body').on('click', '#discount-promotion-dismiss-btn', function(e){
            e.preventDefault();
            var that = $(this);
            $.ajax({
                type: 'POST',
                async: false,
                url:'/cart/add.js',
                async:false,
                data: $('form#add-item-form').serialize(),
                success:function(line){	
                    if(that.attr('data-buynow') != undefined){
                        window.location = "/checkout";
                    }
                    else{
                        $('#combo-popup').modal('hide');
                        getCartModal();					
                        jQuery('#myCart').modal('show');		
                        jQuery('.modal-backdrop').css({'height':jQuery(document).height(),'z-index':'99'});		
                        $('.addtocart-modal').removeClass('clicked_buy');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Sản phẩm bạn vừa mua đã vượt quá tồn kho');
                }
            });
        });
    });
    $(".product-gallery__thumb img").click(function(){
        $(".product-gallery__thumb").removeClass('active');
        $(this).parents('.product-gallery__thumb').addClass('active');
        var img_thumb = $(this).data('image');
        
        $('html, body').animate({
            scrollTop: $("#sliderproduct img[src='"+img_thumb+"']").offset().top
        }, 1000);
         
            });

            function change_alias( alias )
            {
                var str = alias;
                str= str.toLowerCase();  
                str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");  
                str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");  
                str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");  
                str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");  
                str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");  
                str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");  
                str= str.replace(/đ/g,"d");  
                str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-"); 
                str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1- 
                str= str.replace(/^\-+|\-+$/g,"");  
                return str;  
            }
            $(".product-gallery__thumb").first().addClass('active');
            var check_variant = true;
            var fIndex = false;
            $('body').on('click', 'input.qty-btn' , function(e){
                e.preventDefault();
                var $input = $(this).parent().find('input[name="quantity"]').eq(0);
                if($input.attr('data-limit') == '' || $input.attr('data-limit') == null)
                    return false;
                var qty = $input.val(), limit = parseInt($input.attr('data-limit'));
                if(isNaN(limit))
                    return false;
                if(qty > limit){
                    $input.val(limit);
                }
            })
            var countVariants = 0;
            var selectCallback = function(variant, selector) {
                $('.invenNumber').html('...');
                if (variant && variant.available) {
                    setTimeout(function(){
                        $('.invenAlert.alertPage').html('Hiện tại còn <span class="invenNumber">'+variant.inventory_quantity+'</span> sản phẩm.')
                    }, 1000);
                    $('.quantity-area input#quantity').attr('data-limit', variant.inventory_quantity);
                    var dem = parseInt($('.product-gallery').data('size'));
                    if(variant.options.length == 1 || variant.options.length == 2 || variant.options.length == 3 ){
                        if(dem > 1){
                            var total_option = variant.options.length;
                            var title1 = change_alias(variant.option1);
                            if($(window).width() > 992){
                                $('#sliderproduct .product-gallery-item').each(function(){
                                    var check = $(this).data('variant');
                                    if(title1 == check){
                                        $(this).removeClass('hidden');
                                    }else{
                                        $(this).addClass('hidden');
                                    }
                                })
                            }
                            $('.product-gallery__thumbs .product-gallery__thumb').each(function(){
                                var check1 = $(this).data('variant');
                                if(title1 == check1){
                                    $(this).removeClass('hidden');
                                }else{
                                    $(this).addClass('hidden');
                                }
                            })

                        }
                    }

                    if(variant.featured_image != null) {
                        if ($(window).width()> 991){
                            
                            var temp = $("#sliderproduct .product-gallery-item:eq(0) img").attr("src");
                             var imgVariant = variant.featured_image.src.replace('https:','');
                             var indexVariant = $(".product-gallery-item img[src='"+ variant.featured_image.src.replace('https:','')+"']").parent().index();
                             $(".product-gallery-item:eq(0) img").attr("src",imgVariant);//Thế vị trí hình đầu tiên sau khi change
                             $(".product-gallery-item:eq("+indexVariant+") img").attr("src",temp);
                             $(".product-thumb:eq(0) img").attr("src",imgVariant);//Thế vị trí hình đầu tiên sau khi change
                             $(".product-thumb:eq("+indexVariant+") img").attr("src",temp);
                             
                                } 
                                else {
                                    setTimeout(function(){
                                        var indexVariant = $(".product-gallery-item img[src='"+ variant.featured_image.src.replace('https:','')+"']").parent().index();
                                        $("#sliderproduct").flickity('select', indexVariant);

                                    },500);
                                }
                             }
                             if (variant.sku != null ){
                                 jQuery('#pro_sku').html('SKU: ' +variant.sku);
                             }
                             jQuery('#detail-product .add-to-cartProduct').removeAttr('disabled').removeClass('disabled').html("Thêm vào giỏ");
                             jQuery('#detail-product #buy-now').removeAttr('disabled').removeClass('disabled').html("Mua ngay").show();
                             jQuery('#detail-product .pro-soldold').addClass('hidden')
                             if(variant.price < variant.compare_at_price){
                                 //jQuery('#price-preview').html("<span>" + Haravan.formatMoney(variant.price, "{{amount}}₫") + "</span><del>" + Haravan.formatMoney(variant.compare_at_price, "{{amount}}₫") + "</del>");
                                 var pro_sold = variant.price ;
                                 var pro_comp = variant.compare_at_price / 100;
                                 var sale = 100 - (pro_sold / pro_comp) ;
                                 var kq_sale = Math.round(sale);
                                 var html = '<span class="pro-sale">-' + kq_sale + '%</span>';									
                                 html += '<span class="pro-price">' + Haravan.formatMoney(pro_sold, "{{amount}}₫") + '</span>';
                                 html += '<del>' + Haravan.formatMoney(variant.compare_at_price, "{{amount}}₫") + '</del>';
                                 jQuery('#detail-product #price-preview').html(html);
                                 jQuery('#detail-product .price-fixed-mb').html(html);
                             } else {
                                 jQuery('#detail-product #price-preview').html("<span class='pro-price'>" + Haravan.formatMoney(variant.price, "{{amount}}₫" + "</span>"));
                                 jQuery('#detail-product .price-fixed-mb').html("<span class='pro-price'>" + Haravan.formatMoney(variant.price, "{{amount}}₫" + "</span>"));
                             }
                             check_variant = true;
                            } 
                            else {
                                setTimeout(function(){
                                    $('.invenAlert.alertPage').html('Hết hàng');
                                }, 1000);
                                jQuery('#detail-product .add-to-cartProduct').addClass('disabled').attr('disabled', 'disabled').html("Hết Hàng");

                                jQuery('#detail-product #buy-now').addClass('disabled').attr('disabled', 'disabled').html("Hết Hàng").hide();
                                var message = variant ? "Hết Hàng" : "Hết Hàng";
                                jQuery('#detail-product .pro-soldold').removeClass('hidden')
                                jQuery('#detail-product .pro-soldold').text(message);
                                check_variant = false;
                            }

                            return check_variant;
                        };

                        jQuery(document).ready(function($){

                            
                            new Haravan.OptionSelectors("product-select", { product: {"available":true,"compare_at_price_max":0.0,"compare_at_price_min":0.0,"compare_at_price_varies":false,"compare_at_price":0.0,"content":null,"description":"<p>- Chất liệu: Sheer</p><p>- Áo kiểu 3 tầng xinh xắn, nữ tính.</p><p>- Thích hợp mặc đi chơi, đi làm.</p><p>- Có khả năng che bụng cho người có khuyết điểm vòng 2.</p><p><span>Hướng dẫn bảo quản:</span><br><span>- Giặt ở nhiệt độ thường.</span><br><span>- Không dùng chất tẩy.</span><br><span>- Phơi trong bóng râm.</span><br><span>- Không ngâm quần áo để tránh ra màu.</span><br><span>- Ủi ở nhiệt độ không quá 180 độ C.</span></p>","featured_image":"https://product.hstatic.net/1000197303/product/pro_den_3_83d36712a3a14ee6af1224c4fb371398.jpg","handle":"ao-kieu-sheer-3-tang-tay-nhun-beo","id":1030332798,"images":["https://product.hstatic.net/1000197303/product/pro_den_3_83d36712a3a14ee6af1224c4fb371398.jpg","https://product.hstatic.net/1000197303/product/pro_trang_3_a50b07b786cb402597a115aff937ede2.jpg","https://product.hstatic.net/1000197303/product/pro_den_1_0445280cccba4be0a7f3f0234bfa2893.jpg","https://product.hstatic.net/1000197303/product/pro_den_4_3a8ebbea8f114a409775797c1841c362.jpg","https://product.hstatic.net/1000197303/product/pro_den_2_b46a3964a5d34e69a73ce6370c369991.jpg","https://product.hstatic.net/1000197303/product/pro_trang_1_946d80979a8d45d5976f4953a76460de.jpg","https://product.hstatic.net/1000197303/product/pro_trang_2_01643735d1ae443ba745ce168420d7e4.jpg","https://product.hstatic.net/1000197303/product/pro_trang_4_9514a15b4f2a4cf8a2cbd99446fa93c3.jpg","https://product.hstatic.net/1000197303/product/icon_trang_4556c0ff457141e28dd80ddf9b9b41b7.jpg","https://product.hstatic.net/1000197303/product/icon_den_7be7f1733d174b71b1dc22280719a594.jpg"],"options":["Màu sắc","Kích thước","Loại"],"price":45500000.0,"price_max":45500000.0,"price_min":45500000.0,"price_varies":false,"tags":["Chiều cao người mẫu: 1m72","Kích cỡ người mẫu: S"],"template_suffix":null,"title":"Áo kiểu sheer 3 tầng tay nhún bèo","type":"ÁO KIỂU","url":"/products/ao-kieu-sheer-3-tang-tay-nhun-beo","pagetitle":"Áo kiểu sheer 3 tầng tay nhún bèo","metadescription":"- Chất liệu: Sheer- Áo kiểu 3 tầng xinh xắn, nữ tính.- Thích hợp mặc đi chơi, đi làm.- Có khả năng che bụng cho người có khuyết điểm vòng 2.Hướng dẫn bảo quản:- Giặt ở nhiệt độ thường.- Không dùng chất tẩy.- Phơi trong bóng râm.- Không ngâm quần áo để tránh ra màu.- Ủi ở nhiệt độ không quá 180 độ C.","variants":[{"id":1066429946,"barcode":"1000013020919","available":true,"price":45500000.0,"sku":"FAVH123120SDE","option1":"Đen","option2":"S","option3":"0I","options":["Đen","S","0I"],"inventory_quantity":3,"old_inventory_quantity":70,"title":"Đen / S / 0I","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":null},{"id":1066429947,"barcode":"1000013020926","available":true,"price":45500000.0,"sku":"FAVH123120MDE","option1":"Đen","option2":"M","option3":"0I","options":["Đen","M","0I"],"inventory_quantity":3,"old_inventory_quantity":85,"title":"Đen / M / 0I","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":null},{"id":1066429948,"barcode":"1000013020933","available":false,"price":45500000.0,"sku":"FAVH123120LDE","option1":"Đen","option2":"L","option3":"0I","options":["Đen","L","0I"],"inventory_quantity":0,"old_inventory_quantity":41,"title":"Đen / L / 0I","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":null},{"id":1066429949,"barcode":"1000013020940","available":true,"price":45500000.0,"sku":"FAVH123120SDO","option1":"Đỏ","option2":"S","option3":"0I","options":["Đỏ","S","0I"],"inventory_quantity":12,"old_inventory_quantity":89,"title":"Đỏ / S / 0I","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":null},{"id":1066429950,"barcode":"1000013020957","available":true,"price":45500000.0,"sku":"FAVH123120MDO","option1":"Đỏ","option2":"M","option3":"0I","options":["Đỏ","M","0I"],"inventory_quantity":14,"old_inventory_quantity":103,"title":"Đỏ / M / 0I","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":null},{"id":1066429951,"barcode":"1000013020964","available":true,"price":45500000.0,"sku":"FAVH123120LDO","option1":"Đỏ","option2":"L","option3":"0I","options":["Đỏ","L","0I"],"inventory_quantity":3,"old_inventory_quantity":46,"title":"Đỏ / L / 0I","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":null},{"id":1066429952,"barcode":"1000013020971","available":true,"price":45500000.0,"sku":"FAVH123120STR","option1":"Trắng","option2":"S","option3":"0I","options":["Trắng","S","0I"],"inventory_quantity":2,"old_inventory_quantity":63,"title":"Trắng / S / 0I","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":null},{"id":1066429953,"barcode":"1000013020988","available":true,"price":45500000.0,"sku":"FAVH123120MTR","option1":"Trắng","option2":"M","option3":"0I","options":["Trắng","M","0I"],"inventory_quantity":2,"old_inventory_quantity":67,"title":"Trắng / M / 0I","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":null},{"id":1066429954,"barcode":"1000013020995","available":true,"price":45500000.0,"sku":"FAVH123120LTR","option1":"Trắng","option2":"L","option3":"0I","options":["Trắng","L","0I"],"inventory_quantity":1,"old_inventory_quantity":31,"title":"Trắng / L / 0I","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":null}],"vendor":"MARC","published_at":"2021-01-11T09:58:20.489Z","created_at":"2020-12-22T09:52:34.404Z","not_allow_promotion":false}, onVariantSelected: selectCallback });

                             // Add label if only one product option and it isn't 'Title'.
                              

                                // Auto-select first available variant on page load.
                                
                                
                                
                                
                                
                                $('#detail-product .single-option-selector:eq(0)').val("Đen").trigger('change');
                                 
                                $('#detail-product .single-option-selector:eq(1)').val("S").trigger('change');
                                 
                                $('#detail-product .single-option-selector:eq(2)').val("0I").trigger('change');
                                 
                                 
                                 
                                
                                 
                                
                                 
                                
                                 
                                
                                 
                                
                                 
                                
                                 
                                
                                 
                                
                                 
                                 $('#detail-product .selector-wrapper select').each(function(){
                                     $(this).wrap( '<span class="custom-dropdown custom-dropdown--white"></span>');
                                     $(this).addClass("custom-dropdown__select custom-dropdown__select--white");
                                 });
                                    
                                    });


                                    var swatch_size = parseInt($('#add-item-form .select-swatch').children().size());

                                    jQuery(document).on('click','#add-item-form .swatch input', function(e) { 
                                        e.preventDefault();
                                        var $this = $(this);
                                        var _available = '';
                                        $this.parent().siblings().find('label').removeClass('sd');
                                        $this.next().addClass('sd');
                                        var name = $this.attr('name');
                                        var value = $this.val();
                                        $('#add-item-form select[data-option='+name+']').val(value).trigger('change');
                                        if(swatch_size == 2){
                                            if(name.indexOf('1') != -1){
                                                $('#add-item-form #variant-swatch-1 .swatch-element').find('input').prop('disabled', false);
                                                $('#add-item-form #variant-swatch-2 .swatch-element').find('input').prop('disabled', false);
                                                $('#add-item-form #variant-swatch-1 .swatch-element label').removeClass('sd');
                                                //$('#add-item-form #variant-swatch-1 .swatch-element').removeClass('soldout');
                                                $('#add-item-form .selector-wrapper .single-option-selector').eq(1).find('option').each(function(){
                                                    var _tam = $(this).val();
                                                    $(this).parent().val(_tam).trigger('change');
                                                    if(check_variant){
                                                        if(_available == '' ){
                                                            _available = _tam;
                                                        }
                                                    }else{
                                                        //$('#add-item-form #variant-swatch-1 .swatch-element[data-value="'+_tam+'"]').addClass('soldout');
                                                        $('#add-item-form #variant-swatch-1 .swatch-element[data-value="'+_tam+'"]').find('input').prop('disabled', true);
                                                    }
                                                })
                                                $('#add-item-form .selector-wrapper .single-option-selector').eq(1).val(_available).trigger('change');
                                                $('#add-item-form #variant-swatch-1 .swatch-element[data-value="'+_available+'"] label').addClass('sd');
                                            }
                                        }
                                        else if (swatch_size == 3){
                                            var _count_op2 = $('#add-item-form #variant-swatch-1 .swatch-element').size();
                                            var _count_op3 = $('#add-item-form #variant-swatch-2 .swatch-element').size();
                                            if(name.indexOf('1') != -1){
                                                $('#add-item-form #variant-swatch-1 .swatch-element').find('input').prop('disabled', false);
                                                $('#add-item-form #variant-swatch-2 .swatch-element').find('input').prop('disabled', false);
                                                $('#add-item-form #variant-swatch-1 .swatch-element label').removeClass('sd');
                                                //$('#add-item-form #variant-swatch-1 .swatch-element').removeClass('soldout');
                                                $('#add-item-form #variant-swatch-2 .swatch-element label').removeClass('sd');
                                                //$('#add-item-form #variant-swatch-2 .swatch-element').removeClass('soldout');
                                                var _avi_op1 = '';
                                                var _avi_op2 = '';
                                                $('#add-item-form #variant-swatch-1 .swatch-element').each(function(ind,value){
                                                    var _key = $(this).data('value');
                                                    var _unavi = 0;
                                                    $('#add-item-form .single-option-selector').eq(1).val(_key).change();
                                                    $('#add-item-form #variant-swatch-2 .swatch-element label').removeClass('sd');
                                                    //$('#add-item-form #variant-swatch-2 .swatch-element').removeClass('soldout');
                                                    $('#add-item-form #variant-swatch-2 .swatch-element').find('input').prop('disabled', false);
                                                    $('#add-item-form #variant-swatch-2 .swatch-element').each(function(i,v){
                                                        var _val = $(this).data('value');
                                                        $('#add-item-form .single-option-selector').eq(2).val(_val).change();
                                                        if(check_variant == true){
                                                            if(_avi_op1 == ''){
                                                                _avi_op1 = _key;
                                                            }
                                                            if(_avi_op2 == ''){
                                                                _avi_op2 = _val;
                                                            }
                                                            //console.log(_avi_op1 + ' -- ' + _avi_op2)
                                                        }else{
                                                            _unavi += 1;
                                                        }
                                                    })
                                                    if(_unavi == _count_op3){
                                                        //$('#add-item-form #variant-swatch-1 .swatch-element[data-value = "'+_key+'"]').addClass('soldout');
                                                        setTimeout(function(){
                                                            //$('#add-item-form #variant-swatch-1 .swatch-element[data-value = "'+_key+'"] input').attr('disabled','disabled');
                                                        })
                                                    }
                                                })
                                                $('#add-item-form #variant-swatch-1 .swatch-element[data-value="'+_avi_op1+'"] input').click();
                                            }
                                            else if(name.indexOf('2') != -1){
                                                $('#add-item-form #variant-swatch-2 .swatch-element label').removeClass('sd');
                                                //$('#add-item-form #variant-swatch-2 .swatch-element').removeClass('soldout');
                                                $('#add-item-form .selector-wrapper .single-option-selector').eq(2).find('option').each(function(){
                                                    var _tam = $(this).val();
                                                    $(this).parent().val(_tam).trigger('change');
                                                    if(check_variant){
                                                        if(_available == '' ){
                                                            _available = _tam;
                                                        }
                                                    }else{
                                                        $('#add-item-form #variant-swatch-2 .swatch-element label').addClass('sd');
                                                        //$('#add-item-form #variant-swatch-2 .swatch-element[data-value="'+_tam+'"]').addClass('soldout');
                                                        $('#add-item-form #variant-swatch-2 .swatch-element[data-value="'+_tam+'"]').find('input').prop('disabled', true);

                                                    }
                                                })
                                                $('#add-item-form .selector-wrapper .single-option-selector').eq(2).val(_available).trigger('change');
                                                $('#add-item-form #variant-swatch-2 .swatch-element[data-value="'+_available+'"] label').addClass('sd');
                                            }
                                        }
                                        else{
                                        }
                                        var VariantIdSelected = $('#product-select').val();
                                        var storePrdTitle = '';
                                        $('.swatch').each(function(){
                                            var titleOption = $(this).find('.header.hide').html().replace(':','');
                                            titleOption = (titleOption == 'Màu sắc')?'màu':'size';
                                            if(storePrdTitle == '' && $(this).find('.sd span').html() != undefined){
                                                storePrdTitle = titleOption+' '+$(this).find('.sd span').html();
                                            }else{
                                                storePrdTitle += ($(this).find('.sd').length > 0)?', ' + titleOption+' '+ $(this).find('.sd span').html():'';
                                            }
                                        });
                                        if(checkRunFirst){
                                            getStore(VariantIdSelected,storePrdTitle,false);
                                        }
                                    })
                                    $(document).ready(function(){
                                        var _chage = '';
                                        $('#add-item-form .swatch-element[data-value="'+$('#add-item-form .selector-wrapper .single-option-selector').eq(0).val()+'"]').find('input').click();
                                        $('#add-item-form .swatch-element[data-value="'+$('#add-item-form .selector-wrapper .single-option-selector').eq(1).val()+'"]').find('input').click();
                                        if(swatch_size == 2){
                                            var _avi_op1 = '';
                                            var _avi_op2 = '';
                                            var _count = $('#add-item-form #variant-swatch-1 .swatch-element').size();
                                            $('#add-item-form #variant-swatch-0 .swatch-element').each(function(ind,value){
                                                var _key = $(this).data('value');
                                                var _unavi = 0;
                                                $('#add-item-form .single-option-selector').eq(0).val(_key).change();
                                                $('#add-item-form #variant-swatch-1 .swatch-element').each(function(i,v){
                                                    var _val = $(this).data('value');
                                                    $('#add-item-form .single-option-selector').eq(1).val(_val).change();
                                                    if(check_variant == true){
                                                        if(_avi_op1 == ''){
                                                            _avi_op1 = _key;
                                                        }
                                                        if(_avi_op2 == ''){
                                                            _avi_op2 = _val;
                                                        }
                                                    }else{
                                                        _unavi += 1;
                                                    }
                                                })
                                                if(_unavi == _count){
                                                    //	$('#add-item-form #variant-swatch-0 .swatch-element[data-value = "'+_key+'"]').addClass('soldout');
                                                    //$('#add-item-form #variant-swatch-0 .swatch-element[data-value = "'+_key+'"]').find('input').attr('disabled','disabled');
                                                }
                                            })
                                            $('#add-item-form #variant-swatch-1 .swatch-element[data-value = "'+_avi_op2+'"] input').click();
                                            $('#add-item-form #variant-swatch-0 .swatch-element[data-value = "'+_avi_op1+'"] input').click();
                                        }
                                        else if(swatch_size == 3){
                                            var _avi_op1 = '';
                                            var _avi_op2 = '';
                                            var _avi_op3 = '';
                                            var _size_op2 = $('#add-item-form #variant-swatch-1 .swatch-element').size();
                                            var _size_op3 = $('#add-item-form #variant-swatch-2 .swatch-element').size();

                                            $('#add-item-form #variant-swatch-0 .swatch-element').each(function(ind,value){
                                                var _key_va1 = $(this).data('value');
                                                var _count_unavi = 0;
                                                $('#add-item-form .single-option-selector').eq(0).val(_key_va1).change();
                                                $('#add-item-form #variant-swatch-1 .swatch-element').each(function(i,v){
                                                    var _key_va2 = $(this).data('value');
                                                    var _unavi_2 = 0;
                                                    $('#add-item-form .single-option-selector').eq(1).val(_key_va2).change();
                                                    $('#add-item-form #variant-swatch-2 .swatch-element').each(function(j,z){
                                                        var _key_va3 = $(this).data('value');
                                                        $('#add-item-form .single-option-selector').eq(2).val(_key_va3).change();
                                                        if(check_variant == true){
                                                            if(_avi_op1 == ''){
                                                                _avi_op1 = _key_va1;
                                                            }
                                                            if(_avi_op2 == ''){
                                                                _avi_op2 = _key_va2;
                                                            }
                                                            if(_avi_op3 == ''){
                                                                _avi_op3 = _key_va3;
                                                            }
                                                        }else{
                                                            _unavi_2 += 1;
                                                        }
                                                    })
                                                    if(_unavi_2 == _size_op3){
                                                        _count_unavi += 1;
                                                    }
                                                })
                                                if(_size_op2 == _count_unavi){
                                                    $('#add-item-form #variant-swatch-0 .swatch-element[data-value = "'+_key_va1+'"]').addClass('soldout');
                                                    $('#add-item-form #variant-swatch-0 .swatch-element[data-value = "'+_key_va1+'"]').find('input').attr('disabled','disabled');
                                                }
                                            })
                                            $('#add-item-form #variant-swatch-0 .swatch-element[data-value = "'+_avi_op1+'"] input').click();
                                        }
                                    });
                                    $(document).ready(function(){
                                        var vl = $('#add-item-form .swatch .color input').val();
                                        $('#add-item-form .swatch .color input').parents(".swatch").find(".header span").html(vl);
                                        $("#add-item-form .select-swap .color" ).hover(function() { 
                                            var value = $( this ).data("value");
                                            $(this).parents(".swatch").find(".header span").html( value );
                                        },function(){
                                            var value = $("#add-item-form .select-swap .color label.sd span").html();
                                            $(this).parents(".swatch").find(".header span").html( value );
                                        });

                                        if($(window).width() < 992){
                                            /*Hàm change slider	*/
                                            var switchSlide = function(opt){
                                                $('#sliderproduct').flickity('destroy');
                                                if($(window).width() > 767){
                                                    $('.divzoom_main').flickity('destroy');
                                                }
                                                $('#sliderproduct').html('');
                                                $('.divzoom_main').html('');

                                                $('#sliderproduct .item').remove();
                                                $('.product-image-detail .images .product-gallery-item[data-option='+opt+']').clone().appendTo('#sliderproduct');
                                                $('.product-image-detail .images_thumbs .product-thumb[data-option='+opt+']').clone().appendTo('.divzoom_main');

                                                $("#sliderproduct").flickity();
                                            };
                                            /* Change slider khi thay đổi variant */
                                            $('.select-swap .color').click(function(){
                                                switchSlide($(this).find('input').attr('data-vhandle'));
                                            })
                                            /*gọi hàm change slider khi mới vào trang*/
                                            setTimeout(function(){
                                                switchSlide($('.swatch .swatch-element.color:first input').attr('data-vhandle'));
                                            },1000)
                                        }
                                    });
                                    $('body').on('click', 'input.qty-btn' , function(e){
                                        e.preventDefault();
                                        var $input = $(this).parent().find('input[name="quantity"]').eq(0) || $(this).parent().find('input[name="updates[]"]').eq(0);
                                        if($input.attr('data-limit') == '' || $input.attr('data-limit') == null)
                                            return false;
                                        var qty = $input.val(), limit = parseInt($input.attr('data-limit'));
                                        if(isNaN(limit))
                                            return false;
                                        if(qty > limit){
                                            $input.val(limit);
                                        }
                                    })
                                    if($(window).width() < 767){
                                        $('button#add-to-cartbottom').click(function(e){
                                            e.preventDefault();
                                            $('button#add-to-cart').trigger('click');
                                        })
                                    }
                                 }

                                    function getStore(variantid,storPrdTitle,isFirstTime){
                                        var storeProvince = {};
                                        $.ajax({
                                            url: "/inventory_location.js?variant_id=" + variantid+"&quantity=1",
                                            success:function(data){
                                                if( data.inventory_locations.length > 0 ){
                                                    $('#stock-box').html('');
                                                    $('#provinceStore').html('');
                                                    $('#stock-box').show();
                                                    var array_html = '';
                                                    var count_show = 0, hcm_temp = '';
                                                    $.each(data.inventory_locations,function(i,v){
                                                        if(!(storeProvince[v.province_code])){ storeProvince[v.province_code] = v.province_name; }
                                                        if( v.location_name.indexOf('Kho hàng lỗi') == -1 && v.location_name.indexOf('Kho đi đường') == -1){
                                                            if (v.location_name.indexOf('Kho ảo online') == -1) {
                                                                count_show++;
                                                                array_html += "<li data-code='"+v.province_code+"' class='hidden'>";
                                                                array_html += "<span class='bold-light'>" + v.location_name + " (" + v.location_phone + ")</span>";
                                                                array_html += "<span class='desc'>" + v.location_address + "</span></label></li>";
                                                            }
                                                        }
                                                    });
                                                    $('#provinceStore').html('');
                                                    $.each(storeProvince,function(j,k){
                                                        if(j=='HC'){
                                                            hcm_temp = '<option value="'+j+'" data-value="'+k+'">'+k+'</option>';
                                                        }else{
                                                            $('#provinceStore').append('<option value="'+j+'" data-value="'+k+'">'+k+'</option>');
                                                        }
                                                    });
                                                    $('#provinceStore').prepend(hcm_temp);
                                                    $('#provinceStore option:first-child').attr('selected',"selected");
                                                    $('#stock-box').append(array_html);
                                                    $('#stock-box li[data-code="'+$('#provinceStore').val()+'"]').removeClass('hidden');
                                                    //jQuery('#detail-product .add-to-cartProduct').removeAttr('disabled').removeClass('disabled').html((isGiftLixi)?"Nhận quà lì xì":"Mua ngay");
                                                    jQuery('#add-to-cart-combo-september').show();
                                                    $('.link-showroom').show();
                                                    var size = $("#stock-box > li").size();
                                                    if (size == 0){
                                                        $('#stock-box').html('Hiện không còn showroom nào còn sản phẩm '+storPrdTitle);
                                                    }
                                                    else{
                                                        $('.link-showroom span').html('Xem danh sách showrooms còn hàng');
                                                        $('.link-showroom').removeClass("disabled");
                                                    }
                                                } 
                                                else {
                                                    $('#stock-box').html('Hiện không còn showroom nào còn sản phẩm '+storPrdTitle);
                                                    //jQuery('#detail-product .add-to-cartProduct').addClass('disabled').attr('disabled', 'disabled').html("Hết hàng");					
                                                    $('.link-showroom span').html('Hết hàng tại showroom');
                                                    $('.link-showroom').addClass("disabled");
                                                }
                                            }
                                        });
                                    }
                                    var firstVariantId = $('#product-select').val();
                                    var qtyInit = $('#product_quantity').val();
                                    if(countRun == 0){
                                        getStore(firstVariantId,'',true);
                                        countRun = countRun + 1;
                                        checkRunFirst = true;
                                    }
                                    $('.link-showroom').on('click',function(){
                                        $('#multistock').modal('show');
                                    });
                                    $('body').on('change', '#provinceStore' ,function(){
                                        $('#stock-box li').addClass('hidden');
                                        $('#stock-box li[data-code="'+$('#provinceStore').val()+'"]').removeClass('hidden');
                                    });